#!/usr/bin/python
import os, binascii

outname="built_image.bin"
goodimage="../images/mtd6_patched_for_norm.bin"
imagename="SephFW"
sqfsloc=1145453
os.chdir(os.path.dirname(os.path.realpath(__file__)))
os.system("rm ../images/%s.fs" % outname)
os.system("./mksquashfs ../fs ../images/%s.fs -all-root" % outname)
gi=open(goodimage,"r").read()
kernel=gi[64:1145453]
sqfs=open("../images/%s.fs" % outname,"r").read()
bootloader=bytearray(gi[0:64])
kfs=kernel+sqfs
kcrc = binascii.crc32(kernel) & 0xFFFFFFFF
bootloader[24]=(kcrc>>24)&0xFF
bootloader[25]=(kcrc>>16)&0xFF
bootloader[26]=(kcrc>>8)&0xFF
bootloader[27]=(kcrc>>0)&0xFF

#
stringpos=0x20
for i in range(0,16):
	if(i<len(imagename)):
		bootloader[stringpos+i]=imagename[i]
	else:
		bootloader[stringpos+i]=0
#set size
sizepos=0xc
s=len(kernel)
print("kernel size %s" % s)
print("kernel crc %s" % kcrc)
bootloader[sizepos+0]=(s>>24)&0xFF
bootloader[sizepos+1]=(s>>16)&0xFF
bootloader[sizepos+2]=(s>>8)&0xFF
bootloader[sizepos+3]=(s>>0)&0xFF

#zero out header crc
bootloader[4]=0
bootloader[5]=0
bootloader[6]=0
bootloader[7]=0

blcrc=binascii.crc32(bootloader) & 0xFFFFFFFF
bootloader[4]=(blcrc>>24)&0xFF
bootloader[5]=(blcrc>>16)&0xFF
bootloader[6]=(blcrc>>8)&0xFF
bootloader[7]=(blcrc>>0)&0xFF

full_image=bootloader+kfs
flash_for_image=0x400000-0x50000
print("full image size %skb of %skb max (%s%%). %skb remaining" % (int(round(len(full_image)/1024,0)),int(round(flash_for_image/1024,0)),round(float(len(full_image))/flash_for_image*100,0),int(round((flash_for_image-len(full_image))/1024,0))))
dd=open("kfs","w");
dd.write(kfs)
dd.close()
f=open("../images/%s" %outname,"w")
f.write(bootloader);
f.write(kfs);
f.close()
