    <HTML>
<HEAD><TITLE>equipment infomation</TITLE>
<META http-equiv="Content-Type" content="text/html; charset=gb2312">
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
<meta http-equiv="Pragma" content="no-cache">
<meta content="MSHTML 6.00.2800.1106" name="GENERATOR">
<link href="../style/normal_ws.css" type="text/css" rel="stylesheet">
<link href="../style/style.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../lang/b28n.js"></script>
<script type="text/javascript" src="/js/global_js.js"></script>

<script language="JavaScript" type="text/javascript">
parent.menu.setUnderFirmwareUpload(0);
var _singleton = 0;
Butterlate.setTextDomain("internet");
var lan2 = '<% getCfgZero(1, "Lan2Enabled"); %>';
var secs
var timerID = null
var timerRunning = false
function StartTheTimer(){
	if (secs==0){
		TimeoutReload(5);
		//window.location.reload();
		window.location.href=window.location.href;	//reload page
    }else{
        self.status = secs
        secs = secs - 1
        timerRunning = true
        timerID = self.setTimeout("StartTheTimer()", 1000)
    }
}

function TimeoutReload(timeout)
{
	secs = timeout;
	if(timerRunning)
		clearTimeout(timerID)
	timerRunning = false
	StartTheTimer();	
}

function display_on()
{
	if (window.ActiveXObject) { // IE
		return "block";
	}
	else if (window.XMLHttpRequest) { // Mozilla, Firefox, Safari,...
		return "table-row";
	}
}


function dhcpTypeSwitch()
{
	document.getElementById("start").style.visibility = "hidden";
	document.getElementById("start").style.display = "none";
	document.lanCfg.dhcpStart.disabled = true;
	document.getElementById("end").style.visibility = "hidden";
	document.getElementById("end").style.display = "none";
	document.lanCfg.dhcpEnd.disabled = true;
	//document.getElementById("mask").style.visibility = "hidden";
	//document.getElementById("mask").style.display = "none";
	//document.lanCfg.dhcpMask.disabled = true;
	document.getElementById("pridns").style.visibility = "hidden";
	document.getElementById("pridns").style.display = "none";
	document.lanCfg.dhcpPriDns.disabled = true;
	document.getElementById("secdns").style.visibility = "hidden";
	document.getElementById("secdns").style.display = "none";
	document.lanCfg.dhcpSecDns.disabled = true;
	document.getElementById("gateway").style.visibility = "hidden";
	document.getElementById("gateway").style.display = "none";
	document.lanCfg.dhcpGateway.disabled = true;
	document.getElementById("lease").style.visibility = "hidden";
	document.getElementById("lease").style.display = "none";
	document.lanCfg.dhcpLease.disabled = true;
/*	document.getElementById("staticlease1").style.visibility = "hidden";
	document.getElementById("staticlease1").style.display = "none";
	document.lanCfg.dhcpStatic1Mac.disabled = true;
	document.lanCfg.dhcpStatic1Ip.disabled = true;
	document.getElementById("staticlease2").style.visibility = "hidden";
	document.getElementById("staticlease2").style.display = "none";
	document.lanCfg.dhcpStatic2Mac.disabled = true;
	document.lanCfg.dhcpStatic2Ip.disabled = true;
	document.getElementById("staticlease3").style.visibility = "hidden";
	document.getElementById("staticlease3").style.display = "none";
	document.lanCfg.dhcpStatic3Mac.disabled = true;
	document.lanCfg.dhcpStatic3Ip.disabled = true;*/
	if (document.lanCfg.lanDhcpType.options.selectedIndex == 1)
	{
		document.getElementById("start").style.visibility = "visible";
		document.getElementById("start").style.display = display_on();
		document.lanCfg.dhcpStart.disabled = false;
		document.getElementById("end").style.visibility = "visible";
		document.getElementById("end").style.display = display_on();
		document.lanCfg.dhcpEnd.disabled = false;
		//document.getElementById("mask").style.visibility = "visible";
		//document.getElementById("mask").style.display = display_on();
		//document.lanCfg.dhcpMask.disabled = false;
		document.getElementById("pridns").style.visibility = "visible";
		document.getElementById("pridns").style.display = display_on();
		document.lanCfg.dhcpPriDns.disabled = false;
		document.getElementById("secdns").style.visibility = "visible";
		document.getElementById("secdns").style.display = display_on();
		document.lanCfg.dhcpSecDns.disabled = false;
		document.getElementById("gateway").style.visibility = "visible";
		document.getElementById("gateway").style.display = display_on();
		document.lanCfg.dhcpGateway.disabled = false;
		document.getElementById("lease").style.visibility = "visible";
		document.getElementById("lease").style.display = display_on();
		document.lanCfg.dhcpLease.disabled = false;
/*		document.getElementById("staticlease1").style.visibility = "visible";
		document.getElementById("staticlease1").style.display = display_on();
		document.lanCfg.dhcpStatic1Mac.disabled = false;
		document.lanCfg.dhcpStatic1Ip.disabled = false;
		document.getElementById("staticlease2").style.visibility = "visible";
		document.getElementById("staticlease2").style.display = display_on();
		document.lanCfg.dhcpStatic2Mac.disabled = false;
		document.lanCfg.dhcpStatic2Ip.disabled = false;
		document.getElementById("staticlease3").style.visibility = "visible";
		document.getElementById("staticlease3").style.display = display_on();
		document.lanCfg.dhcpStatic3Mac.disabled = false;
		document.lanCfg.dhcpStatic3Ip.disabled = false*/;
	}
}

function initTranslation()
{
	var e = document.getElementById("DHCPTitle");
	e.innerHTML = _("lan title");
	e = document.getElementById("DHCPIntroduction");
	e.innerHTML = _("lan introduction");

	e = document.getElementById("lSetup");
	e.innerHTML = _("lan setup");
	//e = document.getElementById("lHostname");
	//e.innerHTML = _("inet hostname");
	e = document.getElementById("lIp");
	e.innerHTML = _("inet ip");
	e = document.getElementById("lNetmask");
	e.innerHTML = _("inet netmask");
	e = document.getElementById("lDhcpType");
	e.innerHTML = _("lan dhcp type");
	e = document.getElementById("lDhcpTypeD");
	e.innerHTML = _("inet disable");
	e = document.getElementById("lDhcpTypeS");
	e.innerHTML = _("lan dhcp type server");
	e = document.getElementById("lDhcpStart");
	e.innerHTML = _("lan dhcp start");
	e = document.getElementById("lDhcpEnd");
	e.innerHTML = _("lan dhcp end");
	//e = document.getElementById("lDhcpNetmask");
	//e.innerHTML = _("inet netmask");
	e = document.getElementById("lDhcpPriDns");
	e.innerHTML = _("inet pri dns");
	e = document.getElementById("lDhcpSecDns");
	e.innerHTML = _("inet sec dns");
	e = document.getElementById("lDhcpGateway");
	e.innerHTML = _("inet gateway");
	e = document.getElementById("lDhcpLease");
	e.innerHTML = _("lan dhcp lease");
	setInnerHTML("oneHour","one hour");
	setInnerHTML("twoHours","two hours")
	setInnerHTML("threeHours","three hours")
	setInnerHTML("oneDay","one day")
	setInnerHTML("twoDays","two days")
	setInnerHTML("oneWeek","one week")
	     
/*	e = document.getElementById("lDhcpStatic1");
	e.innerHTML = _("lan dhcp static");
	e = document.getElementById("lDhcpStatic2");
	e.innerHTML = _("lan dhcp static");
	e = document.getElementById("lDhcpStatic3");
	e.innerHTML = _("lan dhcp static");*/

	//e = document.getElementById("lStp");
	//e.innerHTML = _("lan stp");
	//e = document.getElementById("lStpD");
	//e.innerHTML = _("inet disable");
	//e = document.getElementById("lStpE");
	//e.innerHTML = _("inet enable");

	//e = document.getElementById("lLltd");
	//e.innerHTML = _("lan lltd");
	//e = document.getElementById("lLltdD");
	//e.innerHTML = _("inet disable");
	//e = document.getElementById("lLltdE");
	//e.innerHTML = _("inet enable");

	//e = document.getElementById("lIgmpp");
	//e.innerHTML = _("lan igmpp");
	//e = document.getElementById("lIgmppD");
	//e.innerHTML = _("inet disable");
	//e = document.getElementById("lIgmppE");
	//e.innerHTML = _("inet enable");

	e = document.getElementById("lUpnp");
	e.innerHTML = _("lan upnp");
	e = document.getElementById("lUpnpD");
	e.innerHTML = _("inet disable");
	e = document.getElementById("lUpnpE");
	e.innerHTML = _("inet enable");

	//e = document.getElementById("lRadvd");
	//e.innerHTML = _("lan radvd");
	//e = document.getElementById("lRadvdD");
	//e.innerHTML = _("inet disable");
	//e = document.getElementById("lRadvdE");
	//e.innerHTML = _("inet enable");

	//e = document.getElementById("lPppoer");
	//e.innerHTML = _("lan pppoer");
	//e = document.getElementById("lPppoerD");
	//e.innerHTML = _("inet disable");
	//e = document.getElementById("lPppoerE");
	//e.innerHTML = _("inet enable");

	e = document.getElementById("lDnsp");
	e.innerHTML = _("lan dnsp");
	e = document.getElementById("lDnspD");
	e.innerHTML = _("inet disable");
	e = document.getElementById("lDnspE");
	e.innerHTML = _("inet enable");

	e = document.getElementById("igmpsnoopingHead");
	e.innerHTML = _("lan igmpp");
	e = document.getElementById("igmpsnoopingEnable");
	e.innerHTML = _("inet enable");
	e = document.getElementById("igmpsnoopingDisable");
	e.innerHTML = _("inet disable");
	
	e = document.getElementById("lApply");
	e.value = _("inet apply");
	e = document.getElementById("lCancel");
	e.value = _("inet cancel");
	  
}

function initValue()
{
//	var opmode = "<% getCfgZero(1, "OperationMode"); %>";
	var dhcp = <% getCfgZero(1, "dhcpEnabled"); %>;
//	var stp = <% getCfgZero(1, "stpEnabled"); %>;
//	var lltd = <% getCfgZero(1, "lltdEnabled"); %>;
	var igmp = <% getCfgZero(1, "igmpEnabled"); %>;
	var upnp = <% getCfgZero(1, "upnpEnabled"); %>;
//	var radvd = <% getCfgZero(1, "radvdEnabled"); %>;
//	var pppoe = <% getCfgZero(1, "pppoeREnabled"); %>;
	var dns = <% getCfgZero(1, "dnsPEnabled"); %>;
//	var wan = "<% getCfgZero(1, "wanConnectionMode"); %>";
//	var lltdb = "<% getLltdBuilt(); %>";
//	var igmpb = "<% getIgmpProxyBuilt(); %>";
//	var upnpb = "<% getUpnpBuilt(); %>";
//	var radvdb = "<% getRadvdBuilt(); %>";

//	var pppoeb = "<% getPppoeRelayBuilt(); %>";
//	var dnsp = "<% getDnsmasqBuilt(); %>";

   document.lanCfg.dhcpLease.value = '<% getCfgGeneral(1, "dhcpLease"); %>';
    document.lanCfg.lanDhcpType.options.selectedIndex = 1*dhcp;
	 dhcpTypeSwitch();
	//document.lanCfg.stpEnbl.options.selectedIndex = 1*stp;
	//document.lanCfg.lltdEnbl.options.selectedIndex = 1*lltd;
	document.lanCfg.igmpEnbl.options.selectedIndex = 1*igmp;
	document.lanCfg.upnpEnbl.options.selectedIndex = 1*upnp;
	//document.lanCfg.radvdEnbl.options.selectedIndex = 1*radvd;
	//document.lanCfg.pppoeREnbl.options.selectedIndex = 1*pppoe;
	document.lanCfg.dnspEnbl.options.selectedIndex = 1*dns;
	 initTranslation();
	
	
	all_page_init();	
	
	
	//if (pppoeb == "0") {
	//	document.getElementById("pppoerelay").style.visibility = "hidden";
	//	document.getElementById("pppoerelay").style.display = "none";
	//	document.lanCfg.pppoeREnbl.options.selectedIndex = 0;
	//}
	//if (dnsp == "0") {
	//	document.getElementById("dnsproxy").style.visibility = "hidden";
	//	document.getElementById("dnsproxy").style.display = "none";
	//	document.lanCfg.dnspEnbl.options.selectedIndex = 0;
	//}
}

function atoi(str, num)
{
	i = 1;
	if (num != 1) {
		while (i != num && str.length != 0) {
			if (str.charAt(0) == '.') {
				i++;
			}
			str = str.substring(1);
		}
		if (i != num)
			return -1;
	}

	for (i=0; i<str.length; i++) {
		if (str.charAt(i) == '.') {
			str = str.substring(0, i);
			break;
		}
	}
	if (str.length == 0)
		return -1;
	return parseInt(str, 10);
}

function checkRange(str, num, min, max)
{
	d = atoi(str, num);
	if (d > max || d < min)
		return false;
	return true;
}

function isAllNum(str)
{
	for (var i=0; i<str.length; i++) {
		if ((str.charAt(i) >= '0' && str.charAt(i) <= '9') || (str.charAt(i) == '.' ))
			continue;
		return 0;
	}
	return 1;
}

function checkIpAddr(field, ismask)
{
	if (field.value == "") {
		alert(_("Error.IP address is empty."));
		field.value = field.defaultValue;
		field.focus();
		return false;
	}

	if (isAllNum(field.value) == 0) {
		alert(_("It should be a [0-9] number."));
		field.value = field.defaultValue;
		field.focus();
		return false;
	}

	if (ismask) {
		if ((!checkRange(field.value, 1, 0, 256)) ||
				(!checkRange(field.value, 2, 0, 256)) ||
				(!checkRange(field.value, 3, 0, 256)) ||
				(!checkRange(field.value, 4, 0, 256)))
		{
			alert(_("IP format error."));
			field.value = field.defaultValue;
			field.focus();
			return false;
		}
	}
	else {
		if ((!checkRange(field.value, 1, 0, 255)) ||
				(!checkRange(field.value, 2, 0, 255)) ||
				(!checkRange(field.value, 3, 0, 255)) ||
				(!checkRange(field.value, 4, 1, 254)))
		{
			alert(_("IP format error."));
			field.value = field.defaultValue;
			field.focus();
			return false;
		}
	}
	return true;
}

function CheckValue()
{  
 if (_singleton ==1) return false;
	//if (document.lanCfg.hostname.value.indexOf(" ") >= 0)
	//{
	//	alert(_("Don\'t enter Blank Space in this feild"));
	//	document.lanCfg.hostname.focus();
	//	document.lanCfg.hostname.select();
	//	return false;
	//}
	if (!checkIpAddr(document.lanCfg.lanIp, false))
		return false;
	if (!checkIpAddr(document.lanCfg.lanNetmask, true))
		return false;
	
	//if (document.lanCfg.lanGateway.value != "")
	//	if (!checkIpAddr(document.lanCfg.lanGateway, false))
	//		return false;
	//if (document.lanCfg.lanPriDns.value != "")
	//	if (!checkIpAddr(document.lanCfg.lanPriDns, false))
	//		return false;
	//if (document.lanCfg.lanSecDns.value != "")
	//	if (!checkIpAddr(document.lanCfg.lanSecDns, false))
	//		return false;
	if (document.lanCfg.lanDhcpType.options.selectedIndex == 1) {
		if (!checkIpAddr(document.lanCfg.dhcpStart, false))
			return false;
		if (!checkIpAddr(document.lanCfg.dhcpEnd, false))
			return false;
		//if (!checkIpAddr(document.lanCfg.dhcpMask, true))
		//	return false;
		if(document.lanCfg.dnspEnbl.options.selectedIndex == 0&&document.lanCfg.dhcpPriDns.value == ""&&document.lanCfg.dhcpSecDns.value == "")
		{ 
			alert(_("Plese input DNS Server or Enable DNS Proxy."));
			document.lanCfg.dhcpPriDns.focus();
			return false;
		
		}
		if (document.lanCfg.dhcpPriDns.value != "")
			if (!checkIpAddr(document.lanCfg.dhcpPriDns, false))
				return false;
		if (document.lanCfg.dhcpSecDns.value != "")
			if (!checkIpAddr(document.lanCfg.dhcpSecDns, false))
				return false;
		if (!checkIpAddr(document.lanCfg.dhcpGateway, false))
			return false;
		/*if (document.lanCfg.dhcpStatic1Mac.value != "") {
			var re = /[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}/;
			if (!re.test(document.lanCfg.dhcpStatic1Mac.value)) {
				alert(_("Please fill the MAC Address in correct format! (XX:XX:XX:XX:XX:XX)"));
				document.lanCfg.dhcpStatic1Mac.focus();
				return false;
			}
			if (!checkIpAddr(document.lanCfg.dhcpStatic1Ip, false))
				return false;
			document.lanCfg.dhcpStatic1.value = document.lanCfg.dhcpStatic1Mac.value + ';' + document.lanCfg.dhcpStatic1Ip.value;
		}*/
	/*	if (document.lanCfg.dhcpStatic2Mac.value != "") {
			var re = /[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}/;
			if (!re.test(document.lanCfg.dhcpStatic2Mac.value)) {
				alert(_("Please fill the MAC Address in correct format! (XX:XX:XX:XX:XX:XX)"));
				document.lanCfg.dhcpStatic2Mac.focus();
				return false;
			}
			if (!checkIpAddr(document.lanCfg.dhcpStatic2Ip, false))
				return false;
			document.lanCfg.dhcpStatic2.value = document.lanCfg.dhcpStatic2Mac.value + ';' + document.lanCfg.dhcpStatic2Ip.value;
		}*/
	/*	if (document.lanCfg.dhcpStatic3Mac.value != "") {
			var re = /[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}/;
			if (!re.test(document.lanCfg.dhcpStatic3Mac.value)) {
				alert(_("Please fill the MAC Address in correct format! (XX:XX:XX:XX:XX:XX)"));
				document.lanCfg.dhcpStatic3Mac.focus();
				return false;
			}
			if (!checkIpAddr(document.lanCfg.dhcpStatic3Ip, false))
				return false;
			document.lanCfg.dhcpStatic3.value = document.lanCfg.dhcpStatic3Mac.value + ';' + document.lanCfg.dhcpStatic3Ip.value;
		}*/
	}
	
    //document.getElementById("lApply").disabled = true;
	parent.menu.setUnderFirmwareUpload(1);
    _singleton =1; 
	 document.getElementById("loading").style.display="block";
	 var selects = document.getElementsByTagName("select");
	for (var i=0;i<selects.length;i++)
	{ 
	  selects[i].style.display="none";
	}	 
	setTimeout("parent.menu.setUnderFirmwareUpload(0);top.view.location='../internet/dhcp_set.asp';",5000);
	return true;
}

function lan2_enable_switch()
{
	if (document.lanCfg.lan2enabled[1].checked == true)
	{
		document.lanCfg.lan2Ip.disabled = true;
		document.lanCfg.lan2Netmask.disabled = true;
	}
	else
	{
		document.lanCfg.lan2Ip.disabled = false;
		document.lanCfg.lan2Netmask.disabled = false;
	}
}

var oldIp;
function recIpCfg()
{
	oldIp = document.lanCfg.lanIp.value;
}

/*
 * Try to modify dhcp server configurations:
 *   dhcp start/end ip address to the same as new lan ip address
 */
function modDhcpCfg()
{
	var i, j;
	var mask = document.lanCfg.lanNetmask.value;
	var newNet = document.lanCfg.lanIp.value;

	//support simple subnet mask only
	if (mask == "255.255.255.0")
		mask = 3;
	else if (mask == "255.255.0.0")
		mask = 2;
	else if (mask == "255.0.0.0")
		mask = 1;
	else
		return;

	//get the old subnet
	for (i=0, j=0; i<oldIp.length; i++) {
		if (oldIp.charAt(i) == '.') {
			j++;
			if (j != mask)
				continue;
			oldIp = oldIp.substring(0, i);
			break;
		}
	}

	//get the new subnet
	for (i=0, j=0; i<newNet.length; i++) {
		if (newNet.charAt(i) == '.') {
			j++;
			if (j != mask)
				continue;
			newNet = newNet.substring(0, i);
			break;
		}
	}

	document.lanCfg.dhcpStart.value = document.lanCfg.dhcpStart.value.replace(oldIp, newNet);
	document.lanCfg.dhcpEnd.value = document.lanCfg.dhcpEnd.value.replace(oldIp, newNet);
	document.lanCfg.dhcpGateway.value = document.lanCfg.lanIp.value; 
}
</script>
</head>
<BODY   onload="initValue();">
<h2 class="btnl" id="DHCPTitle" >&nbsp;</h2>

<table  width="100%" class="tintro">
  <tbody>
    <tr>
      <td class="intro" id="DHCPIntroduction" >  </td>
      <td class ="image_col" align="right">
        <script language="javascript" type="text/javascript" >
	    var lang =_("next");
	   if (lang == "Next") lang="en";
	  else lang="zh_cn";
	   var page_name = "lansetting"
	    help_display(page_name,lang);
	  </script>
      </td>
    </tr>
  </tbody>
</table> 

<hr>
	<table width="571" class="body" bordercolor="#9BABBD">
  <tbody><tr><td width="563"><CENTER>
  <form method=post name="lanCfg" action="/goform/setLan" target="hiddenframe">


 <table border="1" cellpadding="2" cellspacing="1" width="450" bordercolor="#9BABBD">
<tbody>
    <tr>
      <td colspan=2 align="left" class=title id="lSetup">&nbsp;</td>
    </tr>
    <!--tr var hashost = getHostSupp();
      if (hashost != "1") write("style=\"visibility:hidden;display:none\""); >
      <td class=head width=37% align="left" id="lHostname">Hostname</td>
      <td width="63%" align="left" class=value1><input name="hostname" maxlength=16
              value="<% getCfgGeneral(1, "HostName"); %>" /></td>
    </tr-->
    <tr>
      <td class=head  align="left" id="lIp">&nbsp;</td>
      <td width="60%" align="left" class=value1><input name="lanIp" maxlength=15 value='<% getCfgGeneral(1,"lan_ipaddr"); %>' onFocus="recIpCfg()" onBlur="modDhcpCfg()" /></td>
    </tr>
    <tr>
      <td class=head  align="left" id="lNetmask">&nbsp;</td>
      <td align="left" class=value1><input name="lanNetmask" maxlength=15 value="<% getCfgGeneral(1,"lan_netmask"); %>" /></td>
    </tr>
    <!--tr>
      <td class=head width=37% align="left" id="lLan2">LAN2</td>
      <td align="left" class=value1><input type="radio" name="lan2enabled" value="1" onClick="lan2_enable_switch()" />
          <font id="lLan2Enable">Enablei</font>&nbsp;
          <input type="radio" name="lan2enabled" value="0" onClick="lan2_enable_switch()" checked />
        <font id="lLan2Disable">Disable</font> </td>
    </tr>
    <tr>
      <td class=head width=37% align="left" id="lLan2Ip">LAN2 IP Address</td>
      <td align="left" class=value1><input name="lan2Ip" maxlength=15 value="" /></td>
    </tr>
    <tr>
      <td class=head width=37% align="left" id="lLan2Netmask">LAN2 Subnet Mask</td>
      <td align="left" class=value1><input name="lan2Netmask" maxlength=15 value="" /></td>
    </tr>
    <tr id="brGateway">
      <td class=head width=37% align="left" id="lGateway">Default Gateway</td>
      <td align="left" class=value1><input name="dhcpGateway" maxlength=15 value="<% getWanGateway(); %>" /></td>
    </tr>
    <tr id="brPriDns">
      <td class=head width=37% align="left" id="lPriDns">Primary DNS Server</td>
      <td align="left" class=value1><input name="lanPriDns" maxlength=15 value="<% getDns(1); %>" /></td>
    </tr>
    <tr id="brSecDns">
      <td class=head width=37% align="left" id="lSecDns">Secondary DNS Server</td>
      <td align="left" class=value1><input name="lanSecDns" maxlength=15 value="<% getDns(2); %>" /></td>
    </tr>
    <tr>
      <td class=head width=37% align="left" id="lMac">MAC Address</td>
      <td align="left" class=value1><% getLanMac(); %></td>
    </tr-->
    <tr>
      <td class=head  align="left" id="lDhcpType">&nbsp;</td>
      <td align="left" class=value1><select name="lanDhcpType" size="1" onChange="dhcpTypeSwitch();" id="lanDhcpType">
          <option value="DISABLE" id="lDhcpTypeD">Disable</option>
          <option value="SERVER" id="lDhcpTypeS">Server</option>
        </select>
      </td>
    </tr>
    <tr id="start">
      <td class=head  align="left" id="lDhcpStart" >&nbsp;</td>
      <td align="left" class=value1><input name="dhcpStart" maxlength=15
             value="<% getCfgGeneral(1, "dhcpStart"); %>" /></td>
    </tr>
    <tr id="end">
      <td class=head  align="left" id="lDhcpEnd" >&nbsp;</td>
      <td align="left" class=value1><input name="dhcpEnd" maxlength=15
             value="<% getCfgGeneral(1, "dhcpEnd"); %>" /></td>
    </tr>
    <!--tr id="mask">
      <td class=head width=37% align="left" id="lDhcpNetmask" >DHCP Subnet Mask</td>
      <td align="left" class=value1><input name="dhcpMask" maxlength=15
             value="<% getCfgGeneral(1, "dhcpMask"); %>" /></td>
    </tr-->
    <tr id="pridns">
      <td class=head  align="left" id="lDhcpPriDns" >&nbsp;</td>
      <td align="left" class=value1><input name="dhcpPriDns" id="dhcpPriDns" maxlength=15
             value='<% getCfgGeneral(1, "dhcpPriDns"); %>' /></td>
    </tr>
    <tr id="secdns">
      <td class=head  align="left" id="lDhcpSecDns" >&nbsp;</td>
      <td align="left" class=value1><input name="dhcpSecDns" maxlength=15
             value='<% getCfgGeneral(1, "dhcpSecDns"); %>' /></td>
    </tr>
    <tr id="gateway">
      <td class=head  align="left" id="lDhcpGateway" >&nbsp;</td>
      <td align="left" class=value1><input name="dhcpGateway" maxlength=15
             value='<% getCfgGeneral(1, "dhcpGateway"); %>' /></td>
    </tr>
    <tr id="lease">
      <td class=head  align="left" id="lDhcpLease" >&nbsp;</td>
      <td align="left" class=value1>
			 <select NAME=dhcpLease SIZE=1 style="width:80">
		<option VALUE="3600" id="oneHour"> </option>
		<option VALUE="7200" id="twoHours"> </option>
		<option VALUE="10800" id="threeHours"> </option>
		<option VALUE="86400" id="oneDay"> </option>
		<option VALUE="172800" id="twoDays"> </option>
		<option VALUE="604800" id="oneWeek"> </option>
		</select>

</td>
    </tr>
<!--    <tr id="staticlease1">
      <td class=head  align="left" id="lDhcpStatic1" >&nbsp;</td>
      <td align="left" class=value1><input type=hidden name=dhcpStatic1 value="" />
        MAC:
        <input name="dhcpStatic1Mac" maxlength=17
             value="<% getCfgNthGeneral(1, "dhcpStatic1", 0); %>" />
        <br />
        IP:
        <input name="dhcpStatic1Ip" maxlength=15
             value="<% getCfgNthGeneral(1, "dhcpStatic1", 1); %>" /></td>
    </tr>
    <tr id="staticlease2">
      <td class=head  align="left" id="lDhcpStatic2" >&nbsp;</td>
      <td align="left" class=value1><input type=hidden name=dhcpStatic2 value="" />
        MAC:
        <input name="dhcpStatic2Mac" maxlength=17
             value="<% getCfgNthGeneral(1, "dhcpStatic2", 0); %>" />
        <br />
        IP:
        <input name="dhcpStatic2Ip" maxlength=15
             value="<% getCfgNthGeneral(1, "dhcpStatic2", 1); %>" /></td>
    </tr>
    <tr id="staticlease3">
      <td class=head  align="left" id="lDhcpStatic3" >&nbsp;</td>
      <td align="left" class=value1><input type=hidden name=dhcpStatic3 value="" />
        MAC:
        <input name="dhcpStatic3Mac" maxlength=17
             value="<% getCfgNthGeneral(1, "dhcpStatic3", 0); %>" />
        <br />
        IP:
        <input name="dhcpStatic3Ip" maxlength=15
             value="<% getCfgNthGeneral(1, "dhcpStatic3", 1); %>" /></td>
    </tr>
     -->
    <tr id="dnsproxy">  
         <td class="head" id="lDnsp">&nbsp;</td>  
         <td class=value1>    
                 <select name="dnspEnbl" size="1" id="dnspEnbl">     
                         <option value="0" id="lDnspD">Disable</option>     
                         <option value="1" id="lDnspE">Enable</option>    </select>  
         </td>
       </tr>
	  <tr style="display:none; visibility:hidden">
	<td class="head" id="igmpsnoopingHead">&nbsp;</td>
	<td  class="value1" >
	<select name="igmpEnbl" size="1">
	<option value=0 id="igmpsnoopingDisable">Disable</option>
	<option value=1 id="igmpsnoopingEnable">Enable</option>
	</select>
	</td>
</tr>
<tr id="upnp" style="display:none; visibility:hidden">
  <td class="head" id="lUpnp">UPNP</td>
  <td class="value1">
    <select name="upnpEnbl" size="1">
      <option value="0" id="lUpnpD">Disable</option>
      <option value="1" id="lUpnpE">Enable</option>
    </select>
  </td>
</tr>
 </tbody></table>
<table  cellpadding="2" cellspacing="1" width="450" >
<tbody>
<tr align="center">
  <td colspan="2">
    <input type=submit style="{width:110px;}" value="Apply" id="lApply"   onClick="return CheckValue()">&nbsp;&nbsp;
	
    <input type=reset  style="{width:110px;}" value="Cancel" id="lCancel" onClick="reset_clicked()">
</td>
</tr>
</tbody></table>
<iframe name="hiddenframe"  id="hiddenframe" frameborder="0" border="0" style="display:none"></iframe>
</form></center>

</td></tr></tbody></table>
<!--table width="70%" bordercolor="#9BABBD">
<tbody><tr><td >
<p class="value1">

<strong>Note:</strong><br>
<strong>IP address: </strong>The IP address of the router to the LAN ��all the default gateway of the computer must set up as this IP address in the LAN .
<br>
<strong>Subnet Mask:</strong> This router is the subnet mask for LAN ��commonly is 255.255.255.0��the subnet of all the computer in LAN must be the same with this setting.
<br>
<strong>DHCP Lease Time:</strong> When you have start using the dynamic DHCP distribution , please set up DHCP lease time.
<br>
<strong>IP Address Range:</strong> Please input IP pool starting address and ending address. 
<br><br>
<strong>Notice��</strong>  <br>
<strong> 1��</strong>If having changed LAN IP address, you must use new IP address to log on the router for WEB interface management. <br>
<strong> 2��</strong>If the IP address of the new LAN port which you setup is not in the same net sect of the original LAN IP address. The router virtual server and DMZ host function will be invalidated ,if you want enable these functions, please setup again. </p>
 </td></tr></tbody></table-->
 <script language="JavaScript" type="text/javascript">document.write('<div id="loading"  style="display: none;z-index = 9999"><br><br><br><div align="center">'+_("Submitting settings")+
'</div><br><br>&nbsp;&nbsp;' +_("LAN Submitting be patient")+'</div>');
</script>
</BODY>
</HTML>
