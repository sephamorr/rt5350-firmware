<html>
<head>
<title>DHCP Binding</title>
<META http-equiv="Content-Type" content="text/html; charset=gb2312">
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
<meta http-equiv="Pragma" content="no-cache">
<meta content="MSHTML 6.00.2800.1106" name="GENERATOR">
<link href="../style/normal_ws.css" type="text/css" rel="stylesheet">
<link href="../style/style.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../lang/b28n.js"></script>
<script type="text/javascript" src="../js/global_js.js"></script>
<script language="JavaScript" type="text/javascript">
var LANIP = '<% getCfgGeneral(1,"lan_ipaddr"); %>';/**/
var netip = LANIP.replace(/\.\d{1,3}$/,".");
 var dhcpStaticRules = '<% getCfgGeneral(1,"dhcpStaticRules"); %>'
Butterlate.setTextDomain("internet");
parent.menu.setUnderFirmwareUpload(0);
var _singleton = 0;
function initTranslation()
{  
	var e = document.getElementById("ip_bind_btn");
	e.innerHTML = _("ip_bind_title");
	e = document.getElementById("ip_mac_binding_Introduction");
	e.innerHTML = _("ip_mac_binding_Introduction");
	e = document.getElementById("ip_bind");
	e.innerHTML = _("ip_bind");
	e = document.getElementById("Mac_head");
	e.innerHTML = _("Mac_head");
	e = document.getElementById("IP_head");
	e.innerHTML = _("IP_head");
	e = document.getElementById("bind_Apply");
	e.value = _("inet apply");
	e = document.getElementById("bind_Reset");
	e.value = _("inet cancel");
	
	e = document.getElementById("ip_mac_bind_rule");
	e.innerHTML = _("ip_mac_bind_rule");
	e = document.getElementById("rule_num");
	e.innerHTML = _("rule_num");
	e = document.getElementById("cur_Mac");
	e.innerHTML = _("Mac_head");
	e = document.getElementById("cur_IP");
	e.innerHTML = _("IP_head");
	e = document.getElementById("rule_Reset");
	e.value = _("routing del reset");
	e = document.getElementById("rule_Del");
	e.value = _("routing del");
	      
}
function deleteClick()
{
if (_singleton ==1) return false;
    singleton =1; 
	// document.getElementById("loading").style.display="block";
	setTimeout("top.view.location='../internet/ip_mac_binding.asp';",1000);
	return true;	
}

function MAC_IP_FormCheck_old()
{  
 if (_singleton ==1) return false;
 var dhcpStaticIP = trimValue("dhcpStaticIP"); //去掉两端空格，并改为两端无空格的值，参数为ID
  var dhcpStaticMac = trimValue("dhcpStaticMac");
		if(!checkMac_("dhcpStaticMac")) return false; //参数为元素的ID
		if (!checkIpAddr_("dhcpStaticIP", false)) return false;
	//	parent.menu.setUnderFirmwareUpload(1);
    _singleton =1; 
	 document.getElementById("loading").style.display="block";
	setTimeout("top.view.location='../internet/ip_mac_binding.asp';",500);
	return true;		
 }
 
 function MAC_IP_FormCheck()
 { 
  if (_singleton ==1) return false;
  
  var input_rule = "";
  
 /* var mac_Array = new Array();
  mac_Array = getElementByName("mac0");
  var macaddr = combinMAC2(mac_Array);
  getElById("mac").value =  macaddr; 
  var mac = trimValue("mac");*/
  var mac = combinMAC("mac0","mac")// form names to id
  
  getElById("ip").value = netip + getElById("dhcpStaticIP").value ;
  //去掉两端空格，并改为两端无空格的值，参数为ID
   var ip = trimValue("ip"); 
   
 //  debug(getElById("mac").value+" "+getElById("mac").value.length);
   //debug(getElById("ip").value+" "+getElById("ip").value.length);
   
   if (!checkIpAddr_("ip", false,false)) return false;
    if(!checkMac_("mac",false)) return false; //参数为元素的ID
    
	input_rule = ip +","+ mac ; debug(input_rule);
	if(!sameRuleCheck(dhcpStaticRules,input_rule)) return false;
	 _singleton =1; 
	 document.getElementById("loading").style.display="block";
	setTimeout("top.view.location='../internet/ip_mac_binding.asp';",500);
	return true;	
 }
 


function init()
{
 all_page_init();
initTranslation();

}

</script>
</head>

<body onLoad="init()">
<h2 id="ip_bind_btn" class="btnl"> </h2>
<table  width="100%" class="tintro" >
  <tbody>
     <tr>
      <td class="intro" id="ip_mac_binding_Introduction" > 
       </td>
      <td class ="image_col" align="right">
	      <script language="javascript" type="text/javascript" >
	   var lang =_("next");
	   if (lang == "Next") lang="en";
	  else lang="zh_cn";
	   var page_name = "ip_mac_binding"
	     help_display(page_name,lang);
	  </script>
	  </td>
    </tr>
  </tbody>
</table>

<hr>
<table width="571" class="body" bordercolor="#9BABBD">
<tbody>  <tr><td width="563">
<center>
<form method=put name="dhcpStatic" action=/goform/dhcpStatic target="hiddenframe1" >
<table width="450" border="1" cellpadding="2" cellspacing="1" bordercolor="#9BABBD" id="set_table_MAC" >
<tr>
	<td class="title" colspan="2" id="ip_bind"></td>
</tr>
<tr>
	<td class="head"  id="IP_head">
		IP Address
	</td>
	<td  class="value1" >
		<input type="hidden" name="ip" id="ip"><script type="text/javascript">
          document.write(netip)
		</script>
		<input type="text" size="3" maxlength="3" id="dhcpStaticIP" name="part_ip" onKeyUp="check_value_long(3,this,'dhcpStaticMac0')">
	</td>
</tr>
<tr>
	<td class="head"  id="Mac_head">
		Mac address    
	</td>
	<td  class="value1" >
	<input type="hidden" name="mac" id="mac" value="">
		 <input type="text" size="2" maxlength="2" name="mac0"  id="dhcpStaticMac0" onKeyUp="check_value_long(2,this,'dhcpStaticMac1')">
		  :<input type="text" size="2" maxlength="2" name="mac0"  id="dhcpStaticMac1" onKeyUp="check_value_long(2,this,'dhcpStaticMac2')">
		   :<input type="text" size="2" maxlength="2" name="mac0"  id="dhcpStaticMac2"onKeyUp="check_value_long(2,this,'dhcpStaticMac3')">
		   :<input type="text" size="2" maxlength="2" name="mac0"  id="dhcpStaticMac3"onKeyUp="check_value_long(2,this,'dhcpStaticMac4')">
			 :<input type="text" size="2" maxlength="2" name="mac0"  id="dhcpStaticMac4"onKeyUp="check_value_long(2,this,'dhcpStaticMac5')">
			 :<input type="text" size="2" maxlength="2" name="mac0"  id="dhcpStaticMac5"onKeyUp="check_value_long(2,this,'bind_Apply')">
			 
	</td>
</tr> 

</table> 
<table  cellpadding="2" cellspacing="1" width="450" >
<tbody>  
<tr align="center">
  <td ><input type="submit"  value="Apply" id="bind_Apply" name="bind_Apply" onClick="return MAC_IP_FormCheck()">    &nbsp;&nbsp;
	<input type="reset"  value="Reset" id="bind_Reset" name="reset"></td></tr></table>
<iframe name="hiddenframe1"  id="hiddenframe1" frameborder="0" border="0" style="display:none; margin:0px"></iframe>
</form>
<form action=/goform/dhcpStaticDelete method=POST name="dhcpStaticDelete" target="hiddenframe1">
  <table width="450" border="1" cellpadding="2" cellspacing="1" bordercolor="#9BABBD" align="center">
    <tbody >
      <tr>
        <td class="title" colspan="10" id="ip_mac_bind_rule">Current IP/MAC binding rules</td>
      </tr>
      <tr>  
        <td width="10%" class="head11" id="rule_num"> No.</td>
        <td width="45%" align=center class="head11" id="cur_IP"> IP Address</td>
        <td width="45%" align=center class="head11" id="cur_Mac">Mac Address</td>
       
      </tr>
      <%  showDhcpStaticRulesASP (); %>
    </table>
  <table  cellpadding="2" cellspacing="1" width="451" >
<tbody>
<tr align="center">
  <td > 
<input type="submit"  value="Delete Selected" id="rule_Del" name="rule_Del" onClick="return deleteClick()">&nbsp;&nbsp;
<input type="reset"  style="width:110px;" value="Reset" id="rule_Reset" name="reset"></td>
</tr>
</tbody>
</table>
<iframe name="hiddenframe1"  id="hiddenframe1" frameborder="0" border="0" style="display:none"></iframe>
</form>
</center>
</td></tr></tbody></table>
<!-- =========================  delete rules  ========================= -->
</td></tr></tbody></table>
</BODY>
<script language="JavaScript" type="text/javascript">document.write('<div id="loading"  style="display:none;z-index:9999"><br><br><br><div align="center">'+_("Submitting settings")+
'</div><br><br>&nbsp;&nbsp;' +_("Submitting be patient")+'</div>');
</script>
</HTML>