<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=gb2312">
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
<meta http-equiv="Pragma" content="no-cache">
<meta content="MSHTML 6.00.2800.1106" name="GENERATOR">
<link href="../style/normal_ws.css" type="text/css" rel="stylesheet">
<link href="../style/style.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../lang/b28n.js"></script>
<TITLE>equipment infomation</TITLE>
<script type="text/javascript" src="../js/global_js.js"></script>
<!--<script type="text/javascript">
    function iframeAutoFit(){
        var ex;
        try {
            if(window!=parent) {
              //  var a = parent.document.getElementsByTagName("IFRAME");
                for(var i=0; i<a.length; i++) //author:meizz
				{
                    if(a[i].contentWindow==window){
                        var h1=0, h2=0;
                        if(document.documentElement&&document.documentElement.scrollHeight)
                        {h1=document.documentElement.scrollHeight; }
                        if(document.body) h2=document.body.scrollHeight;

                        var h=Math.max(h1, h2);
                        if(document.all) {h += 4;}
                        if(window.opera) {h += 1;}
                        a[i].style.height = h +"px";
                     }
					 } 
					 }
					}catch(e){
					debug(e.message);
				}
    }
    if(document.attachEvent)
    { window.attachEvent("onload",  iframeAutoFit);
        window.attachEvent("onresize",  iframeAutoFit);}
    else {window.addEventListener('load',  iframeAutoFit,  false);
        window.addEventListener('resize',  iframeAutoFit,  false);}
    </script>-->
<script language="JavaScript" type="text/javascript">
parent.menu.setUnderFirmwareUpload(0);
var _singleton = 0;
function init_wifi_trans()
{
	setInnerHTML("wifi_chanel","Chanel")
	setInnerHTML("wifi_mac","MAC");
	setInnerHTML("mac_option","MAC Option");
 	setInnerHTML("WIFISetting","Wireless Client Mode")
	setInnerHTML("wifi_ssid","SSID")
	setInnerHTML("wifi_security_mode","Security Mode")
	setInnerHTML("wep_mode_td","WEP")
	setInnerHTML("secureWEPDefaultKey","Default Key")
	setInnerHTML("secureWEPDefaultKey1","Key 1")
	setInnerHTML("secureWEPDefaultKey2","Key 2")
	setInnerHTML("secureWEPDefaultKey3","Key 3")
	setInnerHTML("secureWEPDefaultKey4","Key 4")
	setInnerHTML("wifi_wep_mode","KEY Form")
	setInnerHTML("secureWEPKey1","WEP Key 1 :")
	setInnerHTML("secureWEPKey2","WEP Key 2 :")
	setInnerHTML("secureWEPKey3","WEP Key 3 :")
	setInnerHTML("secureWEPKey4","WEP Key 4 :")
	setInnerHTML("wifi_cipher_td","WPA Algorithms")
	setInnerHTML("wifi_passphrase_td","Password")
    document.getElementById("wlSurveyBtn").value=_("Open Scan");
}

function wifi_init()
{ 
  init_wifi_trans()
var sta_ssid = "<% getCfgGeneral(1, "sta_ssid"); %>";
var sta_mac =  "<% getCfgGeneral(1, "sta_mac"); %>";
var sta_channel =  "<% getCfgGeneral(1, "sta_channel"); %>";
var sta_security_mode =  "<% getCfgGeneral(1, "sta_security_mode"); %>"; //Disable WEP WPAPSK WPA2PSK 
var sta_wep_mode  =  "<% getCfgGeneral(1, "sta_wep_mode"); %>";
      /*     a)value = "OPEN"  
          b)value ="SHARED" */
var sta_wep_default_key = "<% getCfgGeneral(1, "sta_wep_default_key"); %>"; 
         /* a) value = "1" 
          b) value = "2" 
          c) value = "3" 
          d) value = "4"*/
var sta_wep_select ="<% getCfgZero(1, "sta_wep_select"); %>"
         /* a)value = "1"   --ASCII  
          b)value = "0"   --Hex */
var sta_wep_key_1 =  "<% getCfgGeneral(1, "sta_wep_key_1"); %>";
var sta_wep_key_2 =  "<% getCfgGeneral(1, "sta_wep_key_2"); %>";
var sta_wep_key_3 =  "<% getCfgGeneral(1, "sta_wep_key_3"); %>";
var sta_wep_key_4 =  "<% getCfgGeneral(1, "sta_wep_key_4"); %>";
var sta_cipher = "<% getCfgZero(1, "sta_cipher"); %>";
        /* a)value =0       --TKIP
          b)value =1       --AES*/
var sta_passphrase =  "<% getCfgGeneral(1, "sta_passphrase"); %>";
var wifi_form = document.getElementById("wanCfg2");
   if(sta_channel!="")
	  wifi_form.sta_channel.value = sta_channel;
	else 
	  wifi_form.sta_channel.options.selectedIndex = 0;
	
	if(sta_security_mode!="")
	  wifi_form.sta_security_mode.value = sta_security_mode;  
	else
	  wifi_form.sta_security_mode.options.selectedIndex = 0;
	  
	  if(sta_wep_default_key!="")
	wifi_form.sta_wep_default_key.value = sta_wep_default_key; 
	else
	  wifi_form.sta_wep_default_key.options.selectedIndex = 0;
    
//	if(sta_ssid!="")
	   wifi_form.sta_ssid.value = sta_ssid;
	wifi_form.sta_mac.value = sta_mac;
	
	if(sta_wep_mode !="")
		wifi_form.sta_wep_mode.value  = sta_wep_mode  ;
	else
		wifi_form.sta_wep_mode.options.selectedIndex = 0;
	wifi_form.sta_wep_select.value = sta_wep_select;
	wifi_form.sta_wep_key_1.value = sta_wep_key_1 ;
	wifi_form.sta_wep_key_2.value = sta_wep_key_2;
	wifi_form.sta_wep_key_3.value = sta_wep_key_3;
	wifi_form.sta_wep_key_4.value = sta_wep_key_4;
	
	if(sta_cipher=="TKIP")
	    wifi_form.sta_cipher[0].checked = true;
	 else 
		wifi_form.sta_cipher[1].checked = true;
	
	wifi_form.sta_passphrase.value = sta_passphrase;
	onChangeSec();
}

function CheckWifiValue()
{
var wifi_form = document.getElementById("wanCfg2");
	if(wifi_form.sta_security_mode.value =="WEP") 
	{
		 if(!check_Wep())
		 {
			 return false;
		 }
		 
	}
    if(wifi_form.sta_security_mode.value =="WPAPSK" || wifi_form.sta_security_mode.value=="WPA2PSK" )
     {
	      var keyvalue = wifi_form.sta_passphrase.value;
			if (keyvalue.length == 0)
			{
			   alert(_("Please input wpapsk key!"));
				return false;
			}
			if (keyvalue.length < 8)
			{
				alert(_("Please input at least 8 character of wpapsk key!"));
				return false;
			}
			
			if(checkInjection(wifi_form.sta_passphrase.value) == false)
			{
				alert(_("Invalid characters in Pass Phrase."));
				return false;
			}
		 	if(wifi_form.sta_cipher[0].checked != true && 
		        wifi_form.sta_cipher[1].checked != true)
		   {
			   alert(_("Please choose a WPA Algorithms."));
			   return false;
		   }
	 }
	 return true;
}
function check_Wep()
{   
	var wifi_form = document.getElementById("wanCfg2");
	var defaultid = wifi_form.sta_wep_default_key.value;
	var key_input;
	var keyvalue ;   
	if ( defaultid == 1 )
		  keyvalue = wifi_form.sta_wep_key_1.value;
	else if (defaultid == 2)
		  keyvalue = wifi_form.sta_wep_key_2.value;
	else if (defaultid == 3)
		  keyvalue = wifi_form.sta_wep_key_3.value;
	else if (defaultid == 4)
		  keyvalue = wifi_form.sta_wep_key_4.value;
 	if (keyvalue.length == 0 ){ // shared wep  || md5
		alert(_("Please input wep key")+defaultid+'!');
		return false;
	}

	var keylength = wifi_form.sta_wep_key_1.value.length;
	if (keylength != 0)
	{
		if (wifi_form.sta_wep_select.options.selectedIndex == 0)
		{
			if(keylength != 5 && keylength != 13) 
			{
				alert(_("Please input 5 or 13 characters of wep key1 !"));
				return false;
			}
			if(checkInjection(wifi_form.sta_wep_key_1.value)== false){
				alert(_("Wep key1 contains invalid characters."));
				return false;
			}
		}          
		if (wifi_form.sta_wep_select.options.selectedIndex == 1)
		{
			if(keylength != 10 && keylength != 26) 
			{
				alert(_("Please input 10 or 26 characters of wep key1 !"));
				return false;
			}
			if(checkHex(wifi_form.sta_wep_key_1.value) == false){
				alert(_("Invalid Wep key1 format!"));
				return false;
			}
		}
	}

	keylength = wifi_form.sta_wep_key_2.value.length;
	if (keylength != 0){
		if (wifi_form.sta_wep_select.options.selectedIndex == 0){
			if(keylength != 5 && keylength != 13) {
				alert(_("Please input 5 or 13 characters of wep key2 !"));
				return false;
			}
			if(checkInjection(wifi_form.sta_wep_key_2.value)== false){
				alert(_("Wep key2 contains invalid characters."));
				return false;
			}			
		}
		if (wifi_form.sta_wep_select.options.selectedIndex == 1){
			if(keylength != 10 && keylength != 26) {
				alert(_("Please input 10 or 26 characters of wep key2 !"));
				return false;
			}
			if(checkHex(wifi_form.sta_wep_key_2.value) == false){
				alert(_("Invalid Wep key2 format!"));
				return false;
			}
		}
	}

	keylength = wifi_form.sta_wep_key_3.value.length;
	if (keylength != 0){
		if (wifi_form.sta_wep_select.options.selectedIndex == 0)
		{
			if(keylength != 5 && keylength != 13) {
				alert(_("Please input 5 or 13 characters of wep key3 !"));
				return false;
			}
			if(checkInjection(wifi_form.sta_wep_key_3.value)== false)
			{
				alert(_("Wep key3 contains invalid characters."));
				return false;
			}
		}
		if (wifi_form.sta_wep_select.options.selectedIndex == 1)
		{
			if(keylength != 10 && keylength != 26) 
			{
				alert(_("Please input 10 or 26 characters of wep key3 !"));
				return false;
			}
			if(checkHex(wifi_form.sta_wep_key_3.value) == false)
			{
				alert(_("Invalid Wep key3 format!"));
				return false;
			}			
		}
	}

	keylength = wifi_form.sta_wep_key_4.value.length;
	if (keylength != 0){
		if (wifi_form.sta_wep_select.options.selectedIndex == 0)
		{
			if(keylength != 5 && keylength != 13) {
				alert(_("Please input 5 or 13 characters of wep key4 !"));
				return false;
			}
			if(checkInjection(wifi_form.sta_wep_key_4.value)== false)
			{
				alert(_("Wep key4 contains invalid characters."));
				return false;
			}			
		}
		if (wifi_form.sta_wep_select.options.selectedIndex == 1){
			if(keylength != 10 && keylength != 26) {
				alert(_("Please input 10 or 26 characters of wep key4 !"));
				return false;
			}

			if(checkHex(wifi_form.sta_wep_key_4.value) == false){
				alert(_("Invalid Wep key4 format!"));
				return false;
			}			
		}
	}
	return true;
}
function onChangeSec()
{
	var  wep_table = document.getElementById("div_wep").style;
	var  wpa_table = document.getElementById("div_wpa").style;
	var sec_mode_index = document.getElementById("sta_security_mode").selectedIndex *1 ;
   
	if(sec_mode_index == 1)
	{
		wep_table.display = "";
		wpa_table.display = "none";
	}
	else if(sec_mode_index == 2 || sec_mode_index == 3) 
	{
		wep_table.display = "none";
		wpa_table.display = "";
	}
	else
	{
		wep_table.display = "none";
		wpa_table.display = "none";
	}
}

function SurveyClose()
{   
     if (_singleton ==1) return false;
	var tbl = document.getElementById("wifiScanTable").style;
 //	if (tbl.display == "")
//	{
//		tbl.display = "none";
//		document.getElementById("wlSurveyBtn").value=_("Open Scan");
//	}
//	else
//	{
		tbl.display = "";
	//	document.getElementById("wlSurveyBtn").value=_("Rescan");
	    document.getElementById("wlSurveyBtn").value=_("Scaning...");
		 http_request = creat_http_request();
 	    var url = "/goform/ssid_scan";
 		http_request.open("POST", url, true);
	//	alert("http_request.open ok")
 		http_request.onreadystatechange = RequestRes;
	// 	http_request.setRequestHeader("If-Modified-Since","0");
 	//	http_request.send(null);
    	 http_request.send('n\a');	
	//	alert("http_request.send end")	
//	}	
}

function RequestRes()
{  //  alert("onreadystatechange"+http_request.readyState)
	if (http_request.readyState == 4)
	{  
	   if (http_request.status == 200) 
		{  
	 //    alert(http_request.responseText);
		initScan(http_request.responseText);
		}
		else
		{
		 alert("can not  get scan_info")
		 }
	}
}

//initilize scan table
function initScan(scan_info)
{	
//   alert(scan_info)
 	if(scan_info != '')
 	{
		var str1 = scan_info.split(";");
		var len = str1.length;
		document.getElementById("wifiScanTable").style.display = "";
		document.getElementById("wlSurveyBtn").value=_("Rescan");
	//	 alert(document.getElementById("wlSurveyBtn").value)
		var tbl = document.getElementById("wifiScanTable").getElementsByTagName('tbody')[0];
		//delete
		var maxcell = tbl.rows.length;
		for (var j = maxcell; j > 1; j --)
		{
			tbl.deleteRow(j - 1);
		}	
 		var count = parseInt(len);
		for (i = 0; i < count; i ++)
		{   
		    if(str1[i]=="") continue;
		//	alert(str1[i])
			var str = str1[i].split(",");
			var nrow=document.createElement('tr');
			var ncol=document.createElement('td');
		//	 alert(str[2]+str[3]+str[4]);
			nrow.appendChild(ncol);
            ncol.className = "value1";
			ncol.innerHTML = "<input type='radio' name='ssid_sel' id='ssid_sel_id' value='ssid_sel' onclick='getSSIDData()'/>";
			
			ncol=document.createElement('td');
			ncol.className = "value1";
			nrow.appendChild(ncol);
			ncol.innerHTML = str[1];
			
			ncol=document.createElement('td');
			ncol.className = "value1";
			nrow.appendChild(ncol);
			ncol.innerHTML = str[2];
		
			ncol=document.createElement('td');
			ncol.className = "value1";
			nrow.appendChild(ncol);
			ncol.innerHTML = str[0];
			
			ncol=document.createElement('td');
			ncol.className = "value1";
			nrow.appendChild(ncol);
			 if(str[4]=="UNKNOW") str[4]="OPEN";
		//	if(str[4]!="NONE")
		//	{
		//    	ncol.innerHTML = str[3]+"/"+ str[4];
		//	}
		//	else 
			{
			   ncol.innerHTML = str[3];
			}
 
			ncol=document.createElement('td');
			ncol.className = "value1";
			nrow.appendChild(ncol);
			ncol.innerHTML = str[5]+" "+str[4];
			
			nrow.align = "center";
			tbl.appendChild(nrow);
		}
 	}
 	else
  	{    
		// document.getElementById("wifiScanTable").style.display = "none";
		 document.getElementById("wlSurveyBtn").value=_("Rescan");
	//	 alert("can not  get scan_info");
	}
}
function getSSIDData()
{
	var wifiForm = document.getElementById("wanCfg2");
	if(!confirm(_("Do you ensure to connectting this AP?")))
	{
		return ;
	}
	var tbl = document.getElementById("wifiScanTable");
	var Slt = wifiForm.ssid_sel ;
	
//	alert(Slt.length)
	
	var mac,sc;
	var rowCount = tbl.rows.length;
	
	for (var r = rowCount; r > 1; r --)
	{
		if (rowCount == 2)
		 	sc = Slt.checked;
		 else
		 	sc = Slt[r - 2].checked;
		 
		if (sc)
		{  
		    var secu_mode;
			var wep_OPEN_SHARED = 1;
			var wpa_TKIP_AES = 1;
			var mac = tbl.rows[r - 1].cells[2].innerHTML;
			var cells_4 = tbl.rows[r - 1].cells[4].innerHTML;
			var secu_index;
			for(var i = 0;i<4;i++)
			{  
				if( (secu_index = cells_4.indexOf("Disable")) != -1)  
				{   
				  secu_mode = 0;
				  break;
				}
				if( (secu_index = cells_4.indexOf("WEP")) != -1)  
				{ 
				  secu_mode = 1;
				  if( (secu_index = cells_4.indexOf("OPEN")) != -1) 
				    wep_OPEN_SHARED = 0;
				  break;
				}
				if( (secu_index = cells_4.indexOf("WPAPSK")) != -1)  
				{ 
				  secu_mode = 2;
				    if( (secu_index = cells_4.indexOf("TKIP")) != -1) 
				    wpa_TKIP_AES = 0;
					break;
				}
				if( (secu_index = cells_4.indexOf("WPA2PSK")) != -1)  
				{ 
				  secu_mode = 3;
				    if( (secu_index = cells_4.indexOf("TKIP")) != -1) 
				    wpa_TKIP_AES = 0;
					break;
				}
			}
			wifiForm.sta_mac.value = mac;
			wifiForm.sta_ssid.value = tbl.rows[r - 1].cells[1].innerHTML;
			wifiForm.sta_channel.selectedIndex = tbl.rows[r - 1].cells[3].innerHTML - 1;
			wifiForm.sta_security_mode.options.selectedIndex = secu_mode;
		  	wifiForm.sta_wep_mode.options.selectedIndex = wep_OPEN_SHARED ;
		    wifiForm.sta_cipher[wpa_TKIP_AES*1].checked = true;
			onChangeSec();
		}
	}
}



function time_check(gettime)
{ 
   var time = gettime.value;
  if(isNaN(time)||time==" ")
  {
   alert(_("ERROR:This value must be a number"));
   gettime.value="";
   gettime.focus();
   }
   
}
function auto_fill_data( a, b,c, d)
{ 
	document.getElementById("Z3g_code").value= a;
	document.getElementById("Z3g_apn").value= b;
	document.getElementById("Z3g_username").value= c;
	document.getElementById("Z3g_passwd").value= d;
 }
	var clicktimes = 0 ;   
Butterlate.setTextDomain("internet");
   var lang =_("next");
   if (lang == "Next") lang="en";
  else lang="zh_cn";
   var page_name = "wansetting"
 
 function creat_http_request()
 {
      http_request = false;
    if (window.XMLHttpRequest) 
	{ // Mozilla, Safari,...
        http_request = new XMLHttpRequest();
        if (http_request.overrideMimeType) 
		{
            http_request.overrideMimeType('text/xml');
        }
    } 
	else if (window.ActiveXObject) 
	{ // IE
        try 
		{
            http_request = new ActiveXObject("Msxml2.XMLHTTP");
        }
		 catch (e) 
		{
            try 
			{
            http_request = new ActiveXObject("Microsoft.XMLHTTP");
            } 
			catch (e) 
			{
			alert('Cannot create an XMLHTTP instance'); 
			}
        }
    }
    if (!http_request) {
        alert('Cannot create an XMLHTTP instance');
        return false;
    }
 return http_request;
 }
   
function macCloneMacFillSubmit()
{
    http_request = creat_http_request();
    http_request.onreadystatechange = doFillMyMAC;
    http_request.open('POST', '/goform/getMyMAC', true);
    http_request.send('n\a');
}

function doFillMyMAC()
{
    if (http_request.readyState == 4) {
		if (http_request.status == 200) 
		{
		    var macaddr = http_request.responseText;
			document.getElementById("MACAddress_text").value = macaddr;
			if(macaddr.length != 0)
			{
				var mac_part = new Array();
				mac_part = macaddr.split(":")
				for(var i =0;i<6 ; i++)
				{
				 document.getElementById("dhcpStaticMac"+i).value = mac_part[i];
				}
			}
		 //	alert(document.getElementById("MACAddress_text").value);
		//	document.getElementById("mac_hidden0").value = macaddr;
	   //	document.getElementById("mac_hidden1").value = macaddr;
		} 
		else
		{
			alert("Can\'t get the mac address.");
		}
	}
}


function macCloneSwitch()
{
	if (document.getElementById("macCloneSel").options.selectedIndex == 1) {
		document.getElementById("macCloneMacRow").style.visibility = "visible";
		document.getElementById("macCloneMacRow").style.display = table_row_display_on();
			
	}
	else {
		document.getElementById("macCloneMacRow").style.visibility = "hidden";
		document.getElementById("macCloneMacRow").style.display = "none";
		
	}
}
function atoi(str, num)
{
	i = 1;
	if (num != 1) {
		while (i != num && str.length != 0) {
			if (str.charAt(0) == '.') {
				i++;
			}
			str = str.substring(1);
		}
		if (i != num)
			return -1;
	}

	for (i=0; i<str.length; i++) {
		if (str.charAt(i) == '.') {
			str = str.substring(0, i);
			break;
		}
	}
	if (str.length == 0)
		return -1;
	return parseInt(str, 10);
}

function checkRange(str, num, min, max)
{
	d = atoi(str, num);
	if (d > max || d < min)
		return false;
	return true;
}

function isAllNum(str)
{
	for (var i=0; i<str.length; i++) {
		if ((str.charAt(i) >= '0' && str.charAt(i) <= '9') || (str.charAt(i) == '.' ))
			continue;
		return 0;
	}
	return 1;
}

function checkIpAddr(field, ismask)
{
	if (field.value == "") {
		alert(_("Error.IP address is empty."));
		field.value = field.defaultValue;
		field.focus();
		return false;
	}

	if (isAllNum(field.value) == 0) {
		alert(_('It should be a [0-9] number.'));
		field.value = field.defaultValue;
		field.focus();
		return false;
	}

	if (ismask) {
		if ((!checkRange(field.value, 1, 0, 256)) ||
				(!checkRange(field.value, 2, 0, 256)) ||
				(!checkRange(field.value, 3, 0, 256)) ||
				(!checkRange(field.value, 4, 0, 256)))
		{
			 alert(_('IP format error.'));
			field.value = field.defaultValue;
			field.focus();
			return false;
		}
	}
	else {
		if ((!checkRange(field.value, 1, 0, 255)) ||
				(!checkRange(field.value, 2, 0, 255)) ||
				(!checkRange(field.value, 3, 0, 255)) ||
				(!checkRange(field.value, 4, 1, 254)))
		{
			 alert(_('IP format error.'));
			field.value = field.defaultValue;
			field.focus();
			return false;
		}
	}
	return true;
}

function CheckValue()
{       
        var Main_connectionMode = document.getElementById("connectionMode");
      	var wanCfgform = document.getElementById("wanCfg1");
	if (Main_connectionMode.value == "STATIC") {      //STATIC
		if (!checkIpAddr(wanCfgform.staticIp, false))
			return false;
		if (!checkIpAddr(wanCfgform.staticNetmask, true))
			return false;
		if (wanCfgform.staticGateway.value != "")
			if (!checkIpAddr(wanCfgform.staticGateway, false))
				return false;
		if (wanCfgform.staticPriDns.value != "")
			if (!checkIpAddr(wanCfgform.staticPriDns, false))
				return false;
		if (wanCfgform.staticSecDns.value != "")
			if (!checkIpAddr(wanCfgform.staticSecDns, false))
				return false;
				
	}
	else if (Main_connectionMode.value == "DHCP") { //DHCP
	//page_select_hidden(100);
		document.getElementById("macCloneSel").style.display="none";
	}
	else if (Main_connectionMode.value == "PPPOE") { //PPPOE
	    if (wanCfgform.pppoeUser.value == "") 
		{
		    alert(_("Account cannot be null!"));
			wanCfgform.pppoeUser.focus();
			wanCfgform.pppoeUser.select();
			return false;
		}
		if (wanCfgform.pppoePass.value != wanCfgform.pppoePass2.value) 
		{
		    alert(_("Password mismatched!"));
			return false;
		}
		if (wanCfgform.pppoeOPMode.value == "online")
		{
			if (wanCfgform.pppoeRedialPeriod.value == "")
			{   
			    alert(_("Please specify Redial Period."));
				wanCfgform.pppoeRedialPeriod.focus();
				wanCfgform.pppoeRedialPeriod.select();
				return false;
			}
		}
		else if (wanCfgform.pppoeOPMode.value == "trigger")
		{
			if (wanCfgform.pppoeIdleTime.value == "")
			{
				 alert(_("Please specify Idle Time."));
				wanCfgform.pppoeIdleTime.focus();
				wanCfgform.pppoeIdleTime.select();
				return false;
			}
		}
	}
	else if (Main_connectionMode.value == "L2TP") 
	{ //L2TP
	   if (wanCfgform.l2tpServer.value == "") 
		{ 
		    alert(_("Server IP cannot be null!"));
			return false;
		}
	   if (wanCfgform.l2tpUser.value == "") 
		{
		    alert(_("Account cannot be null!"));
			wanCfgform.l2tpUser.focus();
			wanCfgform.l2tpUser.select();
			return false;
		}
	     if (wanCfgform.l2tpPass.value != wanCfgform.l2tpPass2.value) 
		  {
		     alert(_('Password mismatched!'));
			 return false;
		  }
		 if (wanCfgform.l2tpMode.selectedIndex == 0) 
		{
			if (!checkIpAddr(wanCfgform.l2tpIp, false))
				return false;
			if (!checkIpAddr(wanCfgform.l2tpNetmask, true))
				return false;
			if (!checkIpAddr(wanCfgform.l2tpGateway, false))
				return false;
		}
		if (wanCfgform.l2tpOPMode.value == "online")
		{
			if (wanCfgform.l2tpRedialPeriod.value == "")
			{  
			   alert(_("Please specify Redial Period."));
				wanCfgform.l2tpRedialPeriod.focus();
				wanCfgform.l2tpRedialPeriod.select();
				return false;
			}
		}
		else if (wanCfgform.l2tpOPMode.value == "trigger")
		{
			if (wanCfgform.l2tpIdleTime.value == "")
			{
				alert(_("Please specify Idle Time."));
				wanCfgform.l2tpIdleTime.focus();
				wanCfgform.l2tpIdleTime.select();
				return false;
			}
		}
	}
	else if (Main_connectionMode.value == "PPTP") { //PPTP
		if (wanCfgform.pptpServer.value == "") 
		{ 
		    alert(_("Server IP cannot be null!"));
			return false;
		}
	    if (wanCfgform.pptpUser.value == "") 
		{
		    alert(_("Account cannot be null!"));
			 	wanCfgform.pptpUser.focus();
				wanCfgform.pptpUser.select();
			return false;
		}
		if (wanCfgform.pptpPass.value != wanCfgform.pptpPass2.value) 
		{
			alert(_('Password mismatched!'));
			return false;
		}
		
		if (wanCfgform.pptpMode.selectedIndex == 0) 
		{
			if (!checkIpAddr(wanCfgform.pptpIp, false))
				return false;
			if (!checkIpAddr(wanCfgform.pptpNetmask, true))
				return false;
			if (!checkIpAddr(wanCfgform.pptpGateway, false))
				return false;
		}
		if (wanCfgform.pptpOPMode.value == "online")
		{
			if (wanCfgform.pptpRedialPeriod.value == "")
			{
				alert(_("Please specify Redial Period."));
				wanCfgform.pptpRedialPeriod.focus();
				wanCfgform.pptpRedialPeriod.select();
				return false;
			}
		}
		else if(wanCfgform.pptpOPMode.value == "trigger")
		{
			if (wanCfgform.pptpIdleTime.value == "")
			{
				alert(_("Please specify Idle Time."));
				wanCfgform.pptpIdleTime.focus();
				wanCfgform.pptpIdleTime.select();
				return false;
			}
		}
	}
	else return false;

//	document.getElementById("wApply").disabled = true;
	return true;
}

function style_display_on()
{
	if (window.ActiveXObject)
		{ // IE
			return "block";
		}
     	else if (window.XMLHttpRequest)
		{ // Mozilla, Safari,...
			return "table";
		}
}

function table_row_display_on(){
	if (window.ActiveXObject)
		{ // IE
			return "block";
		}
     	else if (window.XMLHttpRequest)
		{ // Mozilla, Safari,...
			return "table-row";
		}
	}
	
var opratorInfo ;

function closeOpratorInfo()
{
	try {
		if (opratorInfo)
			opratorInfo.close();
	}
	catch(e) {}
}

function initTranslation()
{
    var  e = document.getElementById("connectionset");
	e.innerHTML = _("internet connection set");
	var en =_("next");
	//document.getElementById("help_list").innerHTML='<a href="../ISP-list.asp" name="help" target="_blank"class="wizard" style="color:#000000">ISP Info</a>';
	if (en == "Next")
	document.getElementById("help_list").innerHTML='<input type="button" name="ISP_Info" value="APN Info" style="width:110" onClick=\'opratorInfo = window.open("../ISP-list.asp","ISP_Info","top=500,left=600,width=400,height=400,resizable=yes,scrollbars=yes");\'>';
	else 
	{
		var run_business= '<% getZ3g_isp(); %>' ;
		//help_list_select(run_business, "help_list");
	 if(ensureVersion_Rousing())  //是否为荣讯版
	 {
	    if(run_business =="ChinaTelecom")
		{
		document.getElementById("help_list").innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="ISP_Info" value="3G上网卡" onClick=\'opratorInfo = window.open("../ISP2.asp","","top=500,left=600,width=400,height=400,resizable=yes,scrollbars=yes");\'>';
		}
		else if(run_business =="ChinaUnicom")
		{
		document.getElementById("help_list").innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="ISP_Info" value="3G上网卡" onClick=\'opratorInfo = window.open("../ISP3.asp","","top=500,left=600,width=400,height=400,resizable=yes,scrollbars=yes");\'>';
		}
		else
		{
 		document.getElementById("help_list").innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="ISP_Info" value="APN信息" onClick=\'opratorInfo = window.open("../ISP.asp","","top=500,left=600,width=400,height=400,resizable=yes,scrollbars=yes");\'>';
 		}
	}
	else
	{
		document.getElementById("help_list").innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="ISP_Info" value="APN信息" onClick=\'opratorInfo = window.open("../ISP.asp","","top=500,left=600,width=400,height=400,resizable=yes,scrollbars=yes");\'>';
	 }
	}
	var e = document.getElementById("connectionintro");
	e.innerHTML = _("Connection intro");
	e = document.getElementById("mode_radio");
	e.innerHTML = _("mode_radio");
	e = document.getElementById("3G_label");
	e.innerHTML = _("3G_label");
	e = document.getElementById("wire_label");
	e.innerHTML = _("wire_label");
    setInnerHTML("wifi_label","Wireless Client Mode");
	e = document.getElementById("wConnectionMode");	
	e.innerHTML = _("wan connection type");
	e = document.getElementById("run_business");
	e.innerHTML = _("run business"); 

	e = document.getElementById("wanselect");
	e.innerHTML = _("wanselect"); 
/*	e = document.getElementById("wanselect1");
	e.innerHTML = _("wanselect");    
	e = document.getElementById("wanselect2");
	e.innerHTML = _("wanselect"); */                    

      e = document.getElementById("3GSetting");
	  e.innerHTML = _("Connection GGSetting");
	/*e = document.getElementById("Mixed");
	e.innerHTML = _("Connection mixed");
	e = document.getElementById("Mixed1");
	e.innerHTML = _("Connection mixed");
	e = document.getElementById("Mixed2");
	e.innerHTML = _("Connection mixed");*/

	e = document.getElementById("callnumber");
	e.innerHTML = _("Connection callnumber");
	e = document.getElementById("apn");
	e.innerHTML = _("Connection Z3g_apn");
	
	e = document.getElementById("username");
	e.innerHTML = _("Connection username");
	e = document.getElementById("password");
	e.innerHTML = _("Connection password");

	
	e = document.getElementById("auth_type_select");
	e.innerHTML = _("auth type select");
	e = document.getElementById("w3GOPMode");	
	e.innerHTML = _("Z3g connect type");	
	
	
	e = document.getElementById("Redial_Period");
	e.innerHTML = _("Z3g connect RedialPeriod");
       e = document.getElementById("Idle_Time");
	e.innerHTML = _("Z3g connect IdleTime");
	
	e = document.getElementById("Timeunit");
	e.innerHTML = _("Time unit");
	e = document.getElementById("Timeunit1");
	e.innerHTML = _("Time unit");
	
	e = document.getElementById("wApply");
	e.value = _("Connection inet apply");
	e = document.getElementById("wCancel");
	e.value = _("Connection inet cancel");
	e = document.getElementById("Clone_Mac_title");
	e.innerHTML = _("Clone Mac title");
	
}

function initTranslation2()
{
	
		e = document.getElementById("wStaticMode");	
		e.innerHTML = _("wan static mode");	
		e = document.getElementById("wStaticIp");	
		e.innerHTML = _("inet ip");	
		e = document.getElementById("wStaticNetmask");	
		e.innerHTML = _("inet netmask");	
		e = document.getElementById("wStaticGateway");	
		e.innerHTML = _("inet gateway");
		e = document.getElementById("wStaticPriDns");	
		e.innerHTML = _("inet pri dns");	
		e = document.getElementById("wStaticSecDns");	
		e.innerHTML = _("inet sec dns");	
		e = document.getElementById("wDhcpMode");
		e.innerHTML = _("wan dhcp mode");	
		//e = document.getElementById("wDhcpHost");	
		//e.innerHTML = _("inet hostname");	
	
		e = document.getElementById("wPppoeMode");
	       e.innerHTML = _("wan pppoe mode");
		e = document.getElementById("wPppoeUser");
		e.innerHTML = _("inet user");	
		e = document.getElementById("wPppoePassword");	
		e.innerHTML = _("inet password");	
		e = document.getElementById("wPppoePass2");	
		e.innerHTML = _("inet pass2");	
		//e = document.getElementById("wPppoeOPMode");	
		//e.innerHTML = _("wan protocol opmode");
		
		e = document.getElementById("wL2tpMode");	
		e.innerHTML = _("wan l2tp mode");	
		e = document.getElementById("wL2tpServer");
		e.innerHTML = _("inet server");	
		e = document.getElementById("wL2tpUser");	
		e.innerHTML = _("inet user");	
		e = document.getElementById("wL2tpPassword");	
		e.innerHTML = _("inet password");	
	    e = document.getElementById("wL2tpPass2");	
		e.innerHTML = _("inet pass2");
		e = document.getElementById("wL2tpAddrMode");	
		e.innerHTML = _("wan address mode");
		e = document.getElementById("wL2tpAddrModeS");
		e.innerHTML = _("wan address mode static");	
		e = document.getElementById("wL2tpAddrModeD");
		e.innerHTML = _("wan address mode dynamic");
		e = document.getElementById("wL2tpIp");	
		e.innerHTML = _("inet ip");	
		e = document.getElementById("wL2tpNetmask");
		e.innerHTML = _("inet netmask");	
		e = document.getElementById("wL2tpGateway");	
		e.innerHTML = _("inet gateway");
		e = document.getElementById("wL2tpOPMode");
		e.innerHTML = _("Z3g connect type");
			
		e = document.getElementById("wPppoeOPMode");
	       e.innerHTML = _("Z3g connect type");	
		e = document.getElementById("wPptpMode");	
		e.innerHTML = _("wan pptp mode");	
		e = document.getElementById("wPptpServer");	
		e.innerHTML = _("inet server");	
		e = document.getElementById("wPptpUser");	
		e.innerHTML = _("inet user");	
		e = document.getElementById("wPptpPassword");
		e.innerHTML = _("inet password");
		e = document.getElementById("wPptpPass2");
		e.innerHTML = _("inet pass2");	
		e = document.getElementById("wPptpAddrMode");
		e.innerHTML = _("wan address mode");
		e = document.getElementById("wPptpAddrModeS");
		e.innerHTML = _("wan address mode static");	
		e = document.getElementById("wPptpAddrModeD");
		e.innerHTML = _("wan address mode dynamic");
		e = document.getElementById("wPptpIp");
		e.innerHTML = _("inet ip");	
		e = document.getElementById("wPptpNetmask");	
		e.innerHTML = _("inet netmask");
		e = document.getElementById("wPptpGateway");	
		e.innerHTML = _("inet gateway");	
		e = document.getElementById("wPptpOPMode");	
		e.innerHTML = _("Z3g connect type");	

       e = document.getElementById("pppoeTimeunit");
	e.innerHTML = _("Time unit");
	e = document.getElementById("pppoeTimeunit1");
	e.innerHTML = _("Time unit");
	e = document.getElementById("l2tpTimeunit");
	e.innerHTML = _("Time unit");
	e = document.getElementById("l2tpTimeunit1");
	e.innerHTML = _("Time unit");
	e = document.getElementById("pptpTimeunit");
	e.innerHTML = _("Time unit");
	e = document.getElementById("pptpTimeunit1");
	e.innerHTML = _("Time unit");

	e = document.getElementById("pppoeRedial_Period");
	e.innerHTML = _("Z3g connect RedialPeriod");
       e = document.getElementById("pppoeIdle_Time");
	e.innerHTML = _("Z3g connect IdleTime");
	e = document.getElementById("l2tpRedial_Period");
	e.innerHTML = _("Z3g connect RedialPeriod");
       e = document.getElementById("l2tpIdle_Time");
	e.innerHTML = _("Z3g connect IdleTime");
	e = document.getElementById("pptpRedial_Period");
	e.innerHTML = _("Z3g connect RedialPeriod");
       e = document.getElementById("pptpIdle_Time");
	e.innerHTML = _("Z3g connect IdleTime");
	
	e = document.getElementById("Clone_Mac_head");
	e.innerHTML = _("wan mac clone");
	e = document.getElementById("macCloneE");
	e.innerHTML = _("inet enable");
	e = document.getElementById("macCloneD");
	e.innerHTML = _("inet disable");
	
       e = document.getElementById("MAC_Address_head");
	e.innerHTML = _("inet mac");
	e = document.getElementById("Clone_button");
	  e.value = _("wan mac clone");
	   
}
/*function wantypefuc(){
     var ggType=document.getElementById("Z3g_type");
		  if(wantype=="1xCDMA")   
	      {
	  	 ggType.options.selectedIndex = 0 ;
	  	  }
	     else if(wantype=="EVDO")
		  {
			ggType.options.selectedIndex = 1 ;
			}
		 else if(wantype=="MIXED")
		  {
			ggType.options.selectedIndex = 2;
			}
		}
function wantypefuc1(){
     var ggType=document.getElementById("Z3g_type1");
		 	   if(wantype=="GPRS")   
	      {
	  	 ggType.options.selectedIndex = 0 ;
	  	  }
	     else if(wantype=="EDGE")
		    {
			ggType.options.selectedIndex = 1 ;
			  }
		 else if(wantype=="TD-SCDMA")
		    {
			ggType.options.selectedIndex = 2;
			  }
			else if(wantype=="Mixed_mode")
		    {
			ggType.options.selectedIndex = 3;
			  }
		}
function wantypefuc2(){
     var ggType=document.getElementById("Z3g_type2");
		 	if(wantype=="WCDMA")   
	      {
	  	 ggType.options.selectedIndex = 0 ;
	  	  }
	     else if(wantype=="CDMA")
		  {
			ggType.options.selectedIndex = 1 ;
			}
		 else if(wantype=="MIXED")
		  {
			ggType.options.selectedIndex = 2;
			}
		}
	function runBsChange(){
  document.getElementById("chinatel").style.display = "none";
    document.getElementById("chinamobile").style.display = "none";
   document.getElementById("chinaunicom").style.display = "none";
   document.getElementById("Z3g_type").setAttribute("name","Z3g_typexx");
   document.getElementById("Z3g_type1").setAttribute("name","Z3g_typexx");
   document.getElementById("Z3g_type2").setAttribute("name","Z3g_typexx");
  var runbsSelect=document.getElementById("run_bsSelect");
      if(runbsSelect.value=="ChinaTelecom"){
			 document.getElementById("chinatel").style.display =style_display_on();
			 document.getElementById("Z3g_type").setAttribute("name","Z3g_type");
			  wantypefuc();
	        }
     else if(runbsSelect.value=="ChinaMobile"){
			 document.getElementById("chinamobile").style.display =style_display_on();
			 document.getElementById("Z3g_type1").setAttribute("name","Z3g_type");
			  wantypefuc1();
	        }
     else if(runbsSelect.value=="ChinaUnicom"){
			 document.getElementById("chinaunicom").style.display =style_display_on();
			 document.getElementById("Z3g_type2").setAttribute("name","Z3g_type");
			  wantypefuc2();
			        }
		//	wantypefuc();
      } */
  
function runBusinessInit()
{        
      
 	var runbusinesslist;  
    if(ensureVersion_Rousing())  //是否为荣讯版
	{
	 	 runbusinesslist = '<% getZ3g_isp(); %>' ;
		 document.getElementById("rousing_business").innerHTML= _('<%getZ3g_isp();%>') ; //_("ChinaMobile");
	 }
	else
	{
		runbusinesslist = '<% listZ3g_isp(); %>';//  var runbusinesslist = 'ChinaMobile' ;listZ3g_isp()ChinaMobile,ChinaTelecom,ChinaUnicom
		document.getElementById("run_bsSelect").style.display = "";
	}  
	var en =_("next");
	if (en == "Next")  runbusinesslist="AUTO";
	var cur_bs = '<%getZ3g_isp();%>'; 
	var runbusiness = new Array(); 
	runbusiness = runbusinesslist.split(",");
	var runbsSelect = document.getElementById("run_bsSelect");
	runbsSelect.options.length = 0;
	var finded = 0;
	for(var i = 0;i < runbusiness.length; i++)
	{   
		runbsSelect.options[runbsSelect.length] = new Option(_(runbusiness[i]),runbusiness[i]);
		if(cur_bs == runbusiness[i])
		{
			runbsSelect.options.selectedIndex = i ;
			finded = 1;
			document.getElementById("HZ3g_isp").value = cur_bs;
			document.getElementById("Z3g_isp_type").value = cur_bs;
	    }
		else if(i == runbusiness.length-1 && finded == 0)
		{
			runbsSelect.options.selectedIndex = 0;
			document.getElementById("HZ3g_isp").value = runbusiness[0];
			document.getElementById("Z3g_isp_type").value = runbusiness[0];
		}
	}
		//  runBsChange();		
}

function connectionTPInit()
{
    var contype_list = '<% listZ3g_type(); %>';
  
    var en =_("next");
	if (en == "Next") contype_list="AUTO";
	  var cur_contype = '<% getZ3g_type(); %>'; 
	 // var cur_contype = ""
	  var contype = new Array(); 
		  contype = contype_list.split(",");
	  var Z3g_type_Sel = document.getElementById("Z3g_type");
		  Z3g_type_Sel.options.length = 0;
	  var finded = 0;
	   for( var i = 0;i < contype.length; i++ )
		   {   
		   Z3g_type_Sel.options[Z3g_type_Sel.length] = new Option(_(contype[i]),contype[i]);
		   if(cur_contype == contype[i]){
		   Z3g_type_Sel.options.selectedIndex = i ;
		    finded = 1;
			 document.getElementById("HZ3g_type").value = cur_contype;
		        }
		    else if( i == contype.length-1 && finded == 0){
		    	Z3g_type_Sel.options.selectedIndex = 0;
				document.getElementById("HZ3g_type").value = contype[0];
		    	}
		   }
}


function runBsChangeTJ()
{
	document.setZ3g_isp.submit();
//setTimeout("top.view.location=top.view.location;",2000);
}

function conTpChange()
{
	document.setZ3g_type.submit();
//setTimeout("top.view.location='../internet/connection_set.asp';",2000);
}


function GGOPModeSwitch()
{
	var wanCfg0form = document.getElementById("wanCfg0")
	wanCfg0form.Z3g_redail_interval.disabled = true;
	wanCfg0form.Z3g_idle_die.disabled = true;
	if (wanCfg0form.Z3g_connect_type.value == "online") 
		wanCfg0form.Z3g_redail_interval.disabled = false;
	else if (wanCfg0form.Z3g_connect_type.value == "trigger")
		wanCfg0form.Z3g_idle_die.disabled = false;
}

function initGGOPMode()
{
  /*<!--<option value="online" id="wPppoeKeepAlive">online</option>
      <option value="trigger" id="wPppoeOnDemand">trigger</option>
      <option value="manual1" id="wPppoeManual1">Manual1</option>
	  <option value="manual2" id="wPppoeManual2">Manual2</option>-->*/
	  
	     var  z3G_redail_interval_ = '<% getCfgGeneral(1,"Z3g_redail_interval");%>'
		var  z3G_idle_die_ = '<%getCfgGeneral(1,"Z3g_idle_die"); %>'
                var  z3G_simpin_ = '<%getCfgGeneral(1,"Z3g_simpin"); %>'

		var wanCfg0form = document.getElementById("wanCfg0")
		if(z3G_redail_interval_!="") 
		wanCfg0form.Z3g_redail_interval.value = z3G_redail_interval_;
		if(z3G_idle_die_!="") 
		wanCfg0form.Z3g_idle_die.value = z3G_idle_die_;

                wanCfg0form.Z3g_simpin.value = z3G_simpin_;
		// init redailperiod and idletile, 6 rows in up
		
	    var connect_type_list = "<% listZ3g_connect_type(); %>";
	    var curr_opmode ='<% getCfgGeneral(1,"Z3g_connect_type"); %>';
	    var connect_type = new Array();
	 	    connect_type = connect_type_list.split(",");
	   var connect_type_Sel = document.getElementById("Z3g_connect_type");
		    connect_type_Sel.options.length = 0;
	   var finded = 0;
	 for( var i = 0;i < connect_type.length; i++){   
		   connect_type_Sel.options[connect_type_Sel.length] = new Option(_(connect_type[i]),connect_type[i]);
		   if(curr_opmode == connect_type[i]){
		   connect_type_Sel.options.selectedIndex = i ;
		    finded = 1;
		     }
		    else if( i == connect_type.length-1 && finded == 0){
		    	connect_type_Sel.options.selectedIndex = 0;
		    }
	 }
	  GGOPModeSwitch(); 
}
function allWanConfTableHidden()
{   
    document.getElementById("wifiScanTable").style.display ="none" ;
	document.getElementById("scan_button").style.display ="none" ;
	document.getElementById("div_wpa").style.display ="none" ;
	document.getElementById("div_wep").style.display ="none" ;
    document.getElementById("div_apcli_basic").style.display ="none" ;
	document.getElementById("macCloneTable").style.display ="none" ;
    document.getElementById("connection_mode_sel_table").style.visibility = "hidden";
	document.getElementById("connection_mode_sel_table").style.display = "none";
    document.getElementById("static").style.visibility = "hidden";
	document.getElementById("static").style.display = "none";
	document.getElementById("dhcp").style.visibility = "hidden";
	document.getElementById("dhcp").style.display = "none";
	document.getElementById("pppoe").style.visibility = "hidden";
	document.getElementById("pppoe").style.display = "none";
	document.getElementById("l2tp").style.visibility = "hidden";
	document.getElementById("l2tp").style.display = "none";
	document.getElementById("pptp").style.visibility = "hidden";
	document.getElementById("pptp").style.display = "none";
	document.getElementById("3GS_Table").style.visibility = "hidden";
	document.getElementById("3GS_Table").style.display = "none";
}
function connectionModeSwitch()
{    
       allWanConfTableHidden();
	   document.getElementById("macCloneTable").style.display = "" ;
	   document.getElementById("connection_mode_sel_table").style.display = style_display_on();
	   document.getElementById("connection_mode_sel_table").style.visibility = "visible";
		var connectmode_sel = document.getElementById("connectionMode");
	/*if(connectmode_sel.value  == "3G"){
	   document.getElementById("HconnectionType").value = "3G";
		}
	else{
	 document.getElementById("HconnectionType").value = connectmode_sel.value;
		// document.getElementById("HconnectionType1").value = connectmode_sel.value;
		}*/
	
	/*if (connectmode_sel.value  == "3G") {
		document.getElementById("3GS_Table").style.visibility = "visible";
		document.getElementById("3GS_Table").style.display = style_display_on();
	}
    else*/ 
	if (connectmode_sel.value == "STATIC") {
		document.getElementById("static").style.visibility = "visible";
		document.getElementById("static").style.display = style_display_on();
	}
	else if (connectmode_sel.value  == "DHCP") {
	//	document.getElementById("dhcp").style.visibility = "visible";
	//	document.getElementById("dhcp").style.display = style_display_on();
	}
	else if (connectmode_sel.value == "PPPOE") {
		document.getElementById("pppoe").style.visibility = "visible";
		document.getElementById("pppoe").style.display = style_display_on();
		
	}
	else if (connectmode_sel.value  == "L2TP") {
		document.getElementById("l2tp").style.visibility = "visible";
		document.getElementById("l2tp").style.display = style_display_on();
		
	}
	else if (connectmode_sel.value  == "PPTP") {
		document.getElementById("pptp").style.visibility = "visible";
		document.getElementById("pptp").style.display = style_display_on();
		
	}
	else
	{
		document.getElementById("pppoe").style.visibility = "visible";
		document.getElementById("pppoe").style.display = style_display_on();
		
	}
	
	var  mode = connectmode_sel.value;       
	 /*if(mode == "3G"){
		 	threegg();
			 }
	 else*/
	  if (mode == "PPPOE"){
		var pppoe_opmode ='<% getCfgGeneral(1, "wan_pppoe_opmode"); %>'; //
		var pppoe_optime = '<% getCfgGeneral(1, "wan_pppoe_optime"); %>';
		if (pppoe_opmode == "trigger")
		{
			if (pppoe_optime != "")
				document.getElementById("wanCfg1").pppoeIdleTime.value = pppoe_optime;
		}
		else if (pppoe_opmode == "online")
		{
			if (pppoe_optime != "")
				document.getElementById("wanCfg1").pppoeRedialPeriod.value = pppoe_optime;
		}
		initpppoeOPMode();
	}
	else if (mode == "L2TP") {
		var l2tp_opmode =  '<% getCfgGeneral(1, "wan_l2tp_opmode"); %>';//"trigger
		var l2tp_optime = '<% getCfgGeneral(1, "wan_l2tp_optime"); %>';
		 var l2tpMode = '<% getCfgZero(1, "wan_l2tp_mode"); %>';  // 'Static'; 
			  document.getElementById("l2tpMode").selectedIndex = 1*l2tpMode;
		   l2tpModeSwitch();
		 if (l2tp_opmode == "trigger")
		{		
			if (l2tp_optime != "")
				document.getElementById("wanCfg1").l2tpIdleTime.value = l2tp_optime;
		}
		else if (l2tp_opmode == "online")
		{
			
			if (l2tp_optime != "")
				document.getElementById("wanCfg1").l2tpRedialPeriod.value = l2tp_optime;
		}
		initl2tpOPMode();
	}
	else if (mode == "PPTP") {
		var pptp_opmode = '<% getCfgGeneral(1, "wan_pptp_opmode"); %>';
              var pptp_optime='<% getCfgGeneral(1, "wan_pptp_optime"); %>';
			  var pptpMode = '<% getCfgZero(1, "wan_pptp_mode"); %>';  // 'Static'; 
			  document.getElementById("pptpMode").selectedIndex = 1*pptpMode;
		   pptpModeSwitch();
		if (pptp_opmode == "manual1" ||pptp_opmode == "manual2")
		{		
			if (pptp_optime != "")
				document.getElementById("wanCfg1").pptpIdleTime.value = pptp_optime;
		}
		if (pptp_opmode == "trigger")
		{
		
			if (pptp_optime != "")
				document.getElementById("wanCfg1").pptpIdleTime.value = pptp_optime;
		}
		else if (pptp_opmode == "online")
		{
		
			if (pptp_optime != "")
				document.getElementById("wanCfg1").pptpRedialPeriod.value = pptp_optime;
		}
		initpptpOPMode();
	}

	if (clicktimes ==1) {
		//   initTranslation2();
		   clicktimes++;
  		}
	  clicktimes++;
}

function l2tpModeSwitch()
{
	if (document.getElementById("wanCfg1").l2tpMode.selectedIndex == 0) {
		document.getElementById("l2tpIp").style.visibility = "visible";
		document.getElementById("l2tpIp").style.display = table_row_display_on();
		document.getElementById("l2tpNetmask").style.visibility = "visible";
		document.getElementById("l2tpNetmask").style.display = table_row_display_on();
		document.getElementById("l2tpGateway").style.visibility = "visible";
		document.getElementById("l2tpGateway").style.display = table_row_display_on();
	}
	else {
		document.getElementById("l2tpIp").style.visibility = "hidden";
		document.getElementById("l2tpIp").style.display = "none";
		document.getElementById("l2tpNetmask").style.visibility = "hidden";
		document.getElementById("l2tpNetmask").style.display = "none";
		document.getElementById("l2tpGateway").style.visibility = "hidden";
		document.getElementById("l2tpGateway").style.display = "none";
	}
}

function pptpModeSwitch()
{
	if (document.getElementById("wanCfg1").pptpMode.selectedIndex == 0) {
		document.getElementById("pptpIp").style.visibility = "visible";
		document.getElementById("pptpIp").style.display = table_row_display_on();
		document.getElementById("pptpNetmask").style.visibility = "visible";
		document.getElementById("pptpNetmask").style.display = table_row_display_on();
		document.getElementById("pptpGateway").style.visibility = "visible";
		document.getElementById("pptpGateway").style.display = table_row_display_on();
	}
	else {
		document.getElementById("pptpIp").style.visibility = "hidden";
		document.getElementById("pptpIp").style.display = "none";
		document.getElementById("pptpNetmask").style.visibility = "hidden";
		document.getElementById("pptpNetmask").style.display = "none";
		document.getElementById("pptpGateway").style.visibility = "hidden";
		document.getElementById("pptpGateway").style.display = "none";
	}
}

function pppoeOPModeSwitch()
{ var pppoeRP = document.getElementById("pppoeRedialPeriod");
	var pppoeIT = document.getElementById("pppoeIdleTime");
	pppoeRP.disabled = true;
	pppoeIT.disabled = true;
	var test=document.getElementById("wanCfg1");
	//alert(test.getAttribute("name"));
	//alert(document.wanCfg.pppoeOPMode.value);
	if (document.getElementById("wanCfg1").pppoeOPMode.value == "online") 
		pppoeRP.disabled = false;
	else if (document.getElementById("wanCfg1").pppoeOPMode.value == "trigger")
		pppoeIT.disabled = false;

}

function initpppoeOPMode()
{
  /*<!--<option value="online" id="wPppoeKeepAlive">online</option>
      <option value="trigger" id="wPppoeOnDemand">trigger</option>
      <option value="manual1" id="wPppoeManual1">Manual1</option>
	  <option value="manual2" id="wPppoeManual2">Manual2</option>-->*/
	
	    var connect_type_list = "<% listZ3g_connect_type(); %>";
	    var curr_opmode ='<% getCfgGeneral(1, "wan_pppoe_opmode"); %>';
	    var connect_type = new Array();
	 	    connect_type = connect_type_list.split(",");
	   var connect_type_Sel = document.getElementById("pppoeOPMode");
		    connect_type_Sel.options.length = 0;
	   var finded = 0;
	   for( var i = 0;i < connect_type.length; i++){   
		   connect_type_Sel.options[connect_type_Sel.length] = new Option(_(connect_type[i]),connect_type[i]);
		   if(curr_opmode == connect_type[i]){
		   connect_type_Sel.options.selectedIndex = i ;
		    finded = 1;
		     }
		    else if( i == connect_type.length-1 && finded == 0){
		    	connect_type_Sel.options.selectedIndex = 0;
		    }
	 }
	  pppoeOPModeSwitch(); 
}

function l2tpOPModeSwitch()
{
	document.getElementById("wanCfg1").l2tpRedialPeriod.disabled = true;
	document.getElementById("wanCfg1").l2tpIdleTime.disabled = true;
	if (document.getElementById("wanCfg1").l2tpOPMode.value == "online") 
		document.getElementById("wanCfg1").l2tpRedialPeriod.disabled = false;
	else if (document.getElementById("wanCfg1").l2tpOPMode.value == "trigger")
		document.getElementById("wanCfg1").l2tpIdleTime.disabled = false;
}

function initl2tpOPMode()
{
  /*<!--<option value="online" id="wPppoeKeepAlive">online</option>
      <option value="trigger" id="wPppoeOnDemand">trigger</option>
      <option value="manual1" id="wPppoeManual1">Manual1</option>
	  <option value="manual2" id="wPppoeManual2">Manual2</option>-->*/
	/*<!--<% listZ3g_connect_type(); %>-->*/
	    var connect_type_list = "online";
	    var curr_opmode ='<% getCfgGeneral(1, "wan_l2tp_opmode"); %>';
	    var connect_type = new Array();
	 	    connect_type = connect_type_list.split(",");
	   var connect_type_Sel = document.getElementById("l2tpOPMode");
		    connect_type_Sel.options.length = 0;
	   var finded = 0;
	   for( var i = 0;i < connect_type.length; i++){   
		   connect_type_Sel.options[connect_type_Sel.length] = new Option(_(connect_type[i]),connect_type[i]);
		   if(curr_opmode == connect_type[i]){
		   connect_type_Sel.options.selectedIndex = i ;
		    finded = 1;
		     }
		    else if( i == connect_type.length-1 && finded == 0){
		    	connect_type_Sel.options.selectedIndex = 0;
		    }
	 }
	  l2tpOPModeSwitch(); 
}

function pptpOPModeSwitch()
{
	document.getElementById("wanCfg1").pptpRedialPeriod.disabled = true;
	document.getElementById("wanCfg1").pptpIdleTime.disabled = true;
	if (document.getElementById("wanCfg1").pptpOPMode.value == "online") 
		document.getElementById("wanCfg1").pptpRedialPeriod.disabled = false;
	else if(document.getElementById("wanCfg1").pptpOPMode.value == "trigger")  
	document.getElementById("wanCfg1").pptpIdleTime.disabled = false;
}

function initpptpOPMode()
{
  /*<!--<option value="online" id="wPppoeKeepAlive">online</option>
      <option value="trigger" id="wPppoeOnDemand">trigger</option>
      <option value="manual1" id="wPppoeManual1">Manual1</option>
	  <option value="manual2" id="wPppoeManual2">Manual2</option>
	  <% listZ3g_connect_type(); %>
	  -->*/
	
	    var connect_type_list = "online";
	    var curr_opmode ='<% getCfgGeneral(1, "wan_pptp_opmode"); %>';
	    var connect_type = new Array();
	 	    connect_type = connect_type_list.split(",");
	   var connect_type_Sel = document.getElementById("pptpOPMode");
		    connect_type_Sel.options.length = 0;
	   var finded = 0;
	   for( var i = 0;i < connect_type.length; i++){   
		   connect_type_Sel.options[connect_type_Sel.length] = new Option(_(connect_type[i]),connect_type[i]);
		   if(curr_opmode == connect_type[i]){
		   connect_type_Sel.options.selectedIndex = i ;
		    finded = 1;
		     }
		    else if( i == connect_type.length-1 && finded == 0){
		    	connect_type_Sel.options.selectedIndex = 0;
		    }
	 }
	  pptpOPModeSwitch(); 
}

function initwan_ConM()
{    
     
     var connecMode_List = "<% listZwanConnectionMode();%>";// WAN list
    var internet_mode = '<% getZinternetMode();%>';   // //3G /WAN
	var cur_connecMode ='<%getCfgGeneral(1,"wanConnectionMode");%>'; //当前WAN 子类型
	var connec_Mode = new Array(); 
	    connec_Mode = connecMode_List.split(",");
	var connecType_Sel = document.getElementById("connectionMode");
		 connecType_Sel.options.length = 0;
	var finded = 0;
       for(var i = 0;i < connec_Mode.length; i++)
	    {   
	   	 connecType_Sel.options[connecType_Sel.length] = new Option(_(connec_Mode[i]),connec_Mode[i]);
		   if(cur_connecMode == connec_Mode[i])
		   {
		   connecType_Sel.options.selectedIndex = i ;
		    finded = 1;
	    	//  document.getElementById("HconnectionType").value = cur_connecMode;
		    }
		    else if(i == connec_Mode.length-1 && finded == 0)
			{
		    //	connecType_Sel.value = "PPPOE";
			}
		
         }
	
 document.getElementById("connection_mode_sel_table").style.display = "none";
 document.getElementById("connection_mode_sel_table").style.visibility ="hidden";	   
 
  if(internet_mode == "3G")
  {
    document.getElementById("3G_MODE").checked="checked";
       modedisplay(0);
	  
   }
   else if(internet_mode == "WAN")
  { 
    document.getElementById("wire_mode").checked="checked";
	  modedisplay(1);
  }
  else 
  { 
    document.getElementById("wifi_mode").checked="checked";
	 modedisplay(2);
	wifi_init();
  }
  //wifi_init();	 
}

function threegg()
{
        runBusinessInit();
	    connectionTPInit();			                  
	    initGGOPMode();
	 var en =_("next");
	if(en != "Next")
	if( document.getElementById("run_bsSelect").value=="AUTO" )
	//|| document.getElementById("Z3g_type").value=="AUTO"
	 {
	 document.getElementById("Z3g_code").disabled=true
	 document.getElementById("Z3g_apn").disabled=true;
	 document.getElementById("Z3g_username").disabled="disable";
	 document.getElementById("Z3g_passwd").disabled="disable";
	 document.getElementById("Z3g_code").style.backgroundColor="#E6E6E6";
	 document.getElementById("Z3g_apn").style.backgroundColor="#E6E6E6";
	 document.getElementById("Z3g_username").style.backgroundColor="#E6E6E6";
	 document.getElementById("Z3g_passwd").style.backgroundColor="#E6E6E6";
	 }
  
}

function initValue()
{
	initwan_ConM() ;
	initTranslation(); 
	initTranslation2();
    
	var clone = <% getCfgZero(1, "macCloneEnabled"); %> ;
	//   connectionModeSwitch();
    if (clone == 1)
		document.getElementById("macCloneSel").options.selectedIndex = 1;
	else
		document.getElementById("macCloneSel").options.selectedIndex = 0;
		macCloneSwitch();
		
	   var wanmac_ = "<% getWanMac();%>";
	   document.getElementById("MACAddress_text").value = wanmac_;
		if(wanmac_.length != 0)
		{
			var mac_part = new Array();
			mac_part = wanmac_.split(":")
			for(var i =0;i<6 ; i++)
			{
			 document.getElementById("dhcpStaticMac"+i).value = mac_part[i];
			}
		}
   all_page_init();
   
}

function domCfgtest()
{ 
//document.getElementById("3G_MODE").checked= "checked";
 //document.getElementById("wire_mode").checked=true;
var wan_mode=document.getElementsByTagName("input");
alert(wan_mode.length)
//wan_mode[1].checked="checked";
//wan_mode[1]="checked";
}

function checkMac(str){
	var len = str.length;
	if(len!=17)
		return false;

	for (var i=0; i<str.length; i++) {
		if((i%3) == 2){
			if(str.charAt(i) == ':')
				continue;
		}else{
			if (    (str.charAt(i) >= '0' && str.charAt(i) <= '9') ||
					(str.charAt(i) >= 'a' && str.charAt(i) <= 'f') ||
					(str.charAt(i) >= 'A' && str.charAt(i) <= 'F') )
			continue;
		}
		return false;
	}
	return true;
}

function domCfgSubmit()
{  
 if (_singleton ==1) return false;
  text_Trim();//declared at jsglobal
    var HconnectionType = document.getElementById("HconnectionType");
//	var connectmode_sel = document.getElementById("connectionMode");
	var Z3g_codeT = document.getElementById("Z3g_code");
	var Z3g_apnT = document.getElementById("Z3g_apn");
	var Z3g_usernameT = document.getElementById("Z3g_username");
	var Z3g_passwdT = document.getElementById("Z3g_passwd");
       
	var wancfg0sub = document.getElementById("wanCfg0");
	var wancfg1sub = document.getElementById("wanCfg1");
	var wancfg2sub = document.getElementById("wanCfg2");
		 /*if (document.getElementById("macCloneEnbl").options.selectedIndex == 1) 
		  {  
			 var macCloneMac=document.getElementById("macCloneMac").value;
			     wancfg0sub.macCloneMac.value=macCloneMac;
		   if(!checkMac(macCloneMac)) 
		    { 
			  alert(_"Your MAC Address is invalid")  ;
			  document.getElementById("macCloneMac").focus();
		        return false ;
			}
		 }*/
		 
        if (document.getElementById("macCloneSel").options.selectedIndex == 1) 
		if(HconnectionType.value  == "3G"||HconnectionType.value  == "WAN")
		{
			/*var re = /[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}/;  */
			
	var macAddr = combinMAC("mac0" ,"MACAddress_text" ) ;
	if(!checkMac_("MACAddress_text" ,false)) return false;
	 
	 /* var macCloneMac = document.getElementById("MACAddress_text") ;
	if (macCloneMac.value.length == 0) {
				alert(_("MAC Address should not be empty!"));
				macCloneMac.focus();
				return false;
			}
			if (!re.test(macCloneMac.value)) {
				alert(_("Please fill the MAC Address in correct format! (XX:XX:XX:XX:XX:XX)"));
				macCloneMac.focus();
				return false;
			}*/
			    wancfg0sub.macCloneMac.value = macAddr ;
			    wancfg1sub.macCloneMac.value = macAddr ;
				wancfg0sub.macCloneEnbl.value = 1;
			    wancfg1sub.macCloneEnbl.value = 1;
		}
		
       if(HconnectionType.value  == "3G")
	   {   
	       if (document.getElementById("HZ3g_isp").value!="AUTO" )
		   {
				   /*	if (  Z3g_codeT.value == "")
				{
					   alert(_("Code Number can'tbe null."));
					   return ;
					   } */  
					if( Z3g_codeT.value == "" )
					 {
						   if(Z3g_apnT.value != "" )
							{
							 alert(_("Code Number can't be null while APN not null."));
								 return ;
							}else if(Z3g_usernameT.value !="")
							{
							 alert(_("Code Number can't be null while  Username not null."));						                         return ;
							}else if(Z3g_passwdT.value !="")
							{
							 alert(_("Code Number can't be null while Password not null."));	
								return ;
							}
					  }
					 /* else
					  {
						  if(Z3g_apnT.value == "" )
							{
							 alert(_("APN can't be null while Code Number not null."));
								 return ;
							}
					  }	*/
		      }
			  
	      if (wancfg0sub.Z3g_connect_type.value == "online")
			{
				if (wancfg0sub.Z3g_redail_interval.value == "")
				{   
					alert(_("Please specify Redial Period."));
					wancfg0sub.Z3g_redail_interval.focus();
					wancfg0sub.Z3g_redail_interval.select();
					return false;
				}
			}
			else if (wancfg0sub.Z3g_connect_type.value == "trigger")
			 {
				if (wancfg0sub.Z3g_idle_die.value == "")
				{
					 alert(_("Please specify Idle Time."));
					wancfg0sub.Z3g_idle_die.focus();
					wancfg0sub.Z3g_idle_die.select();
					return false;
				}
			 }	
			   _singleton = 1;
			   document.getElementById("loading").style.display="block";
			   document.getElementById("run_bsSelect").style.display="none";
			   document.getElementById("Z3g_type").style.display="none";
			   document.getElementById("help_list").style.display="none";
			 parent.menu.setUnderFirmwareUpload(1);
		  	 setTimeout("parent.menu.setUnderFirmwareUpload(0);",20000);
			 setTimeout("top.view.location = '../internet/connection_set.asp';",23000);
			 setTimeout("top.view.location = '../internet/connection_set.asp';",27000);
			wancfg0sub.submit();
	   	}
	  else if(HconnectionType.value  == "WAN")
	  {  
	      //var OthersMode ;
	      // OthersMode = CheckValue();
	      	if(CheckValue())
			{    
			   setTimeout("parent.menu.setUnderFirmwareUpload(0);",20000);
			   setTimeout("top.view.location='../internet/connection_set.asp';",20000);
			   setTimeout("top.view.location='../internet/connection_set.asp';",25000);
			   parent.menu.setUnderFirmwareUpload(1);
			   _singleton = 1;
			   document.getElementById("loading").style.display="block";
		       page_select_hidden(5);
	  		   wancfg1sub.submit();
	          }
	 				   
	 }	
	 else  
	 {
	        if(CheckWifiValue())
			{    
				 setTimeout("parent.menu.setUnderFirmwareUpload(0);",20000);
				 setTimeout("top.view.location='../internet/connection_set.asp';",23000);
				  setTimeout("top.view.location='../internet/connection_set.asp';",27000);
			  	 parent.menu.setUnderFirmwareUpload(1);
				 _singleton = 1;
				 document.getElementById("loading").style.display="block";
		         page_select_hidden(7);
	  			 wancfg2sub.submit();
	        }
	 } 
}

function modedisplay(m)
{     
      allWanConfTableHidden();
	   if(m==0)
	   {  
		    document.getElementById("macCloneTable").style.display ="" ;
	   	     document.getElementById("HconnectionType").value = "3G";
			document.getElementById("3GS_Table").style.visibility = "visible";
			document.getElementById("3GS_Table").style.display = style_display_on();
	       threegg();
	   }
	  else if(m==1)
	  { 
	   document.getElementById("HconnectionType").value = "WAN";
	   connectionModeSwitch();
	  }
	  else  
	  {  
	    document.getElementById("HconnectionType").value = "WIFI";
        document.getElementById("div_apcli_basic").style.display = "";
	    document.getElementById("scan_button").style.display = "";
	   onChangeSec();
	   if(document.getElementById("wlSurveyBtn").value == _("Rescan"))
	   {
	    document.getElementById("wifiScanTable").style.display = "";
		}
	   else
	   {
		document.getElementById("wifiScanTable").style.display = "none";
		//SurveyClose();
		}
	  }
 }

</script>
</HEAD>
<BODY onLoad="initValue();" onUnload="closeOpratorInfo();closeIntro()">
<h2 class="btnl" id="connectionset"> </h2>
<table  width="100%" class="tintro">
  <tbody>
    <tr>
      <td class="intro" id="connectionintro" > This page displays  connection  to you .</td>
      <td align="right"   class="image_col"><script language="javascript" type="text/javascript" >
    	var lang =_("next");
	    if (lang == "Next") lang="en";
	    else lang="zh_cn";
	   var page_name = "wansetting"
	   help_display(page_name,lang);
	  </script>
      </td>
    </tr>
  </tbody>
</table>
<hr>
<table width="571" class="body" style="margin-bottom:0px">
  <tr>
    <td width="571"><CENTER>
        <table id="select_mode" cellspacing=1 cellpadding=3 width=450  border="1" bordercolor="#9BABBD" >
          <tbody>
            <tr>
              <td  class="title" id="mode_radio" colspan="3">wan mode</td>
            </tr>
            <tr>
              <td  ><input id="3G_MODE" type="radio" name="internet_mode" value="3G" onClick="modedisplay(0);"/>
                <label id="3G_label" for="3G_MODE">3G Mode</label></td>
              <td><input  id="wire_mode" type="radio" name="internet_mode"  value="WAN" onClick="modedisplay(1);"/>
                <label id="wire_label" for="wire_mode" >WAN Mode</label></td>
              <td><input id="wifi_mode" type="radio" name="internet_mode"   value="WIFI" onClick="modedisplay(2);"/>
                <label id="wifi_label" for="wifi_mode" >Client  Mode</label></td>
            </tr>
          </tbody>
        </table>
        <!--     </CENTER></td>
  </tr>
</table>
<table width="571" class="body" >
  <tr >
    <td width="563"><CENTER>-->
        <table id="3GS_Table" cellSpacing=1 cellPadding=3 width=450 class=text1 border="1" bordercolor="#9BABBD" style="margin-top:10px" >
          <tr>
            <td colspan=2 align="left" class=title id="3GSetting">Connection Settings </td>
          </tr>
          <form name="setZ3g_isp" action="/goform/setZ3g_isp" method="post" autocomplete="off" >
            <tr> 
              <td class=head  align="left" id="run_business">run business</td>
              <td class=value1 align="left" id="run_business2"><select name="Z3g_isp" id="run_bsSelect" size="1" onChange="runBsChangeTJ();" style="display:none">
                  <!--------initialize in javascript " runBusinessInit() "-------->
                </select>
                <span id="rousing_business"></span> <span id="help_list"></span></td>
            </tr>
          </form>
          <form  name="setZ3g_type" action="/goform/setZ3g_type" method="post" autocomplete="off">
            <tr id="chinatel" >
              <input type="hidden" name="Z3g_isp" id="Z3g_isp_type" value=""/>
              <td class=head  align="left" id="wanselect">connection type</td>
              <td class=value1 align="left"><select name="Z3g_type" id="Z3g_type" size="1" onChange="conTpChange();">
                </select>
              </td>
            </tr>
          </form>
          <form method=post name="wanCfgx" id="wanCfg0" action="/goform/setWan" autocomplete="off" target="hiddenframe_3g">
            <tr>
              <input type=hidden name="connectionType" id="HconnectionType" value="" >
              <input type="hidden" name="Z3g_isp"  id="HZ3g_isp" value="">
              <input type="hidden" name="Z3g_type"  id="HZ3g_type" value="">
              <input type="hidden" name="internet_mode"   value="3G">
              <td class=head  align="left" id="callnumber">code</td>
              <td class=value1  align="left"><input name="Z3g_code" id= "Z3g_code" maxlength=32 size=22 style="border:1 solid #9BABBD;" value='<% getZ3g_code(); %>'>
              </td>
            </tr>
            <tr>
              <td class=head  align="left" id="apn">apn</td>
              <td class=value1  align="left"><input name="Z3g_apn" id="Z3g_apn" maxlength=32 size=22 style="border:1 solid #9BABBD;" value='<% getZ3g_apn(); %>'>
              </td>
            </tr>
            <tr>
              <td class=head  align="left" id="username">username</td>
              <td class=value1  align="left"><input name="Z3g_username" id="Z3g_username" style="border:1 solid #9BABBD;"  maxlength=32 size=22
             value='<% getZ3g_username(); %>'>
              </td>
            </tr>
            <tr>
              <td class=head  align="left" id="password">password</td>
              <td class=value1  align="left"><input type="password" name="Z3g_passwd" id="Z3g_passwd"style="border:1 solid #9BABBD;" maxlength=32 size=22
             value='<% getZ3g_passwd(); %>'>
              </td>
            </tr>
   
             <tr>
              <td class=head  align="left" id="sim_pin">pin code</td>
              <td class=value1  align="left"><input  name="Z3g_simpin" id="Z3g_simpin"style="border:1 solid #9BABBD;" maxlength=32 size=22>
              </td>
            </tr>   

            <tr>
              <td class=head  align="left" id="auth_type_select">authtype</td>
              <td class=value1   align="left"><select name="Z3g_auth_type" size="1"  id="Z3g_auth_type" style="width:80px">
                  <script language="javascript" type="text/javascript">
 function initauthtype(){
       var auth_type_list = '<% listZ3g_auth_type(); %>';
	   var currauth_type ='<% getZ3g_auth_type(); %>';     //鉴权类型
	   var auth_type = new Array();
	 	    auth_type = auth_type_list.split(",");
	   var auth_type_Sel = document.getElementById("Z3g_auth_type");
		  auth_type_Sel.options.length = 0;
	  var finded = 0;
	   for( var i = 0;i < auth_type.length; i++){   
		   auth_type_Sel.options[auth_type_Sel.length] = new Option(_(auth_type[i]),auth_type[i]);
		   if(currauth_type == auth_type[i]){
		   auth_type_Sel.options.selectedIndex = i ;
		    finded = 1;
		        }
		    else if( i == auth_type.length-1 && finded == 0){
		    	auth_type_Sel.options.selectedIndex = 0;
		    	}
		   }
}
		   initauthtype();
		  </script>
                  <!--<option value="AUTO" id="type_auto">&nbsp;AUTO &nbsp;  </option>
      <option value="CHAP" id="type_chap">&nbsp;CHAP</option>
      <option value="PAP" id="type_pap">&nbsp;PAP</option>-->
                </select></td>
            </tr>
            <tr>
              <td class=head  align="left" id="w3GOPMode">Operation Mode</td>
              <td class=value1 align="left"><select name="Z3g_connect_type"  id="Z3g_connect_type" size="1" onChange="GGOPModeSwitch();">
                  <!-- init in javascript-->
                </select></td>
            </tr>
            <tr>
              <td class=head  align="left" id="Redial_Period"> online: Redial Period </td>
              <td align="left" class=value1><input type="text" name="Z3g_redail_interval" maxlength="5" size="4" value='5' onKeyUp="time_check(this);" />
                <font id="Timeunit"> senconds</font> </td>
            <tr>
              <td class=head  align="left" id="Idle_Time"> trigger: Idle Time </td>
              <td align="left" class=value1><input type="text" name="Z3g_idle_die" maxlength="5" size="4" value='600' disabled="disabled" onKeyUp="time_check(this);" />
                <font id="Timeunit1"> senconds</font> </td>
              <input type="hidden" name="macCloneMac" id="mac_hidden0" />
              <input type="hidden" name="macCloneEnbl" id="macCloneEnbl0" value="0" />
            </tr>
			<iframe name="hiddenframe_3g"  id="hiddenframe_3g" frameborder="0" border="0" style="display:none"></iframe>
          </form>
        </table>
        <!--<tr id="chinatel" style="display:none">
		  <td class=head  align="left" id="wanselect">chinatel</td>
		  <td class=value1 align="left">		 
               <select name="Z3g_type0" id="Z3g_type" size="1">
    	          <option value="1xCDMA">1xCDMA</option>
                  <option value="EVDO">EVDO</option>
                  <option value="MIXED" id="Mixed">Mixed_mode</option>
              </select> 
       </td>
	</tr>
	<tr id="chinamobile" style="display:none">
		  <td class=head  align="left" id="wanselect1">chinamobile </td>
		  <td class=value1   align="left">		 
               <select name="Z3g_type1" id="Z3g_type1"size="1">
    	          <option value="GPRS" >GPRS</option>
		 <option value="EDGE" >EDGE</option>
                  <option value="TD-SCDMA" >TD-SCDMA</option>
                  <option value="MIXED" id="Mixed1">Mixed_mode</option>
              </select> </td>
	</tr>
	<tr id="chinaunicom" style="display:none">
		  <td class=head  align="left" id="wanselect2">chinaunicom </td>
		  <td class=value1   align="left">
		  	<select name="Z3g_type2" id="Z3g_type2"size="1">
            <option value="WCDMA" >WCDMA</option>
            <option value="1xCDMA" >1xCDMA</option>
            <option value="MIXED" id="Mixed2">Mixed_mode</option>
          </select></td>
	</tr>                                            onSubmit="return CheckValue();"
-->
        <form  method=post name="wanCfgx" action="/goform/setWan" id="wanCfg1"  style="margin:0" target="hiddenframe0" autocomplete="off">
          <table cellSpacing=1 cellPadding=3 width=450 class=text1 id="connection_mode_sel_table" style="visibility:hidden; display:none; margin-bottom:10px" border="1" bordercolor="#9BABBD" >
            <tbody>
              <tr>
                <input type="hidden" name="internet_mode"   value="WAN">
                <td class="head" id="wConnectionMode"> WAN Connection Mode:</td>
                <td class="value1"><select name="connectionType" id="connectionMode" size="1" onChange="connectionModeSwitch();">
                    <!--<option value="STATIC" id="wConnTypeStatic">Static Mode (fixed IP)</option>
      		<option value="DHCP" id="wConnTypeDhcp">DHCP (Auto Config)</option>
      		<option value="PPPOE" id="wConnTypePppoe">PPPOE (ADSL)</option>
      		<option value="L2TP" id="wConnTypeL2tp">L2TP</option>
      		<option value="PPTP" id="wConnTypePptp">PPTP</option>-->
                  </select>
                </td>
              </tr>
            </tbody>
          </table>
          <table id="static" cellSpacing=1 cellPadding=3 width=450 class=text1 border="1" bordercolor="#9BABBD" style="display:none; visibility:hidden">
            <tbody>
              <tr>
                <!--input type=hidden name="connectionType" id="HconnectionType1" value="" -->
                <td class="title" colspan="2" id="wStaticMode">Static Mode</td>
              </tr>
              <tr>
                <td class="head" id="wStaticIp">IP Address</td>
                <td class=value1><input name="staticIp" maxlength=15 value="<% getWanIp(); %>"></td>
              </tr>
              <tr>
                <td class="head" id="wStaticNetmask">Subnet Mask</td>
                <td class=value1><input name="staticNetmask" maxlength=15 value="<% getWanNetmask(); %>"></td>
              </tr>
              <tr>
                <td class="head" id="wStaticGateway">Default Gateway</td>
                <td class=value1><input name="staticGateway" maxlength=15 value="<% getWanGateway(); %>">
                </td>
              </tr>
              <tr>
                <td class="head" id="wStaticPriDns">Primary DNS Server</td>
                <td class=value1><input name="staticPriDns" maxlength=15 value="<% getDns(1); %>"></td>
              </tr>
              <tr>
                <td class="head" id="wStaticSecDns">Secondary DNS Server</td>
                <td class=value1><input name="staticSecDns" maxlength=15 value="<% getDns(2); %>"></td>
              </tr>
            </tbody>
          </table>
          <!-- ================= DHCP Mode ================= -->
          <table id="dhcp" width="450" border="1" cellpadding="2" cellspacing="1" style="display:none; visibility:hidden">
            <tbody>
              <tr>
                <td class="title" colspan="2" id="wDhcpMode">DHCP Mode</td>
              </tr>
              <!--tr>
  <td class="head"><span id="wDhcpHost">Host Name</span> (optional)</td>
  <td class=value1><input type=text name="hostname" size=28 maxlength=32 value=""></td>
</tr-->
            </tbody>
          </table>
          <!-- ================= PPPOE Mode ================= -->
          <table id="pppoe" cellSpacing=1 cellPadding=3 width=450 class=text1 border="1" bordercolor="#9BABBD" style="display:none; visibility:hidden">
            <tbody>
              <tr>
                <td class="title" colspan="2" id="wPppoeMode">PPPoE Mode</td>
              </tr>
              <tr>
                <td class="head" id="wPppoeUser">User Name</td>
                <td class=value1><input name="pppoeUser" maxlength=32 size=32
             value="<% getCfgGeneral(1, "wan_pppoe_user"); %>"></td>
              </tr>
              <tr>
                <td class="head" id="wPppoePassword">Password</td>
                <td class=value1><input type="password" name="pppoePass" maxlength=32 size=32
             value="<% getCfgGeneral(1, "wan_pppoe_pass"); %>"></td>
              </tr>
              <tr>
                <td class="head" id="wPppoePass2">Verify Password</td>
                <td class=value1><input type="password" name="pppoePass2" maxlength=32 size=32
             value="<% getCfgGeneral(1, "wan_pppoe_pass"); %>"></td>
              </tr>
              <tr>
                <td class="head"  id="wPppoeOPMode">Operation Mode</td>
                <td class=value1><select name="pppoeOPMode" id="pppoeOPMode" size="1" onChange="pppoeOPModeSwitch()">
                  </select>
                </td>
              </tr>
              <tr>
                <td class=head  align="left" id="pppoeRedial_Period"> online: Redial Period </td>
                <td align="left" class=value1><input type="text" name="pppoeRedialPeriod" id="pppoeRedialPeriod" maxlength="5" size="4" value="5" onKeyUp="time_check(this);">
                  <font id="pppoeTimeunit"> senconds</font> </td>
              </tr>
              <tr>
                <td class=head  align="left" id="pppoeIdle_Time"> trigger: Idle Time </td>
                <td align="left" class=value1><input type="text" name="pppoeIdleTime"  id="pppoeIdleTime" maxlength="3" size="4" value="600" onKeyUp="time_check(this);">
                  <font id="pppoeTimeunit1"> senconds</font> </td>
              </tr>
            </tbody>
          </table>
          <!-- ================= L2TP Mode ================= -->
          <table id="l2tp" cellSpacing=1 cellPadding=3 width=450 class=text1 border="1" bordercolor="#9BABBD" style="display:none; visibility:hidden">
            <tbody>
              <tr>
                <td class="title" colspan="2" id="wL2tpMode">L2TP Mode</td>
              </tr>
              <tr>
                <td class="head" id="wL2tpServer">L2TP Server IP Address</td>
                <td class=value1><input name="l2tpServer" maxlength="15" size=15 value='<%
       getCfgGeneral(1, "wan_l2tp_server"); %>'></td>
              </tr>
              <tr>
                <td class="head" id="wL2tpUser">User Name</td>
                <td class=value1><input name="l2tpUser" maxlength="20" size=20 value="<%
       getCfgGeneral(1, "wan_l2tp_user"); %>"></td>
              </tr>
              <tr>
                <td class="head" id="wL2tpPassword">Password</td>
                <td class=value1><input type="password" name="l2tpPass" maxlength="32" size=32 value='<%
       getCfgGeneral(1, "wan_l2tp_pass"); %>'></td>
              </tr>
              <tr>
                <td class="head" id="wL2tpPass2">Password2</td>
                <td class=value1><input type="password" name="l2tpPass2" maxlength="32" size=32 value='<%
       getCfgGeneral(1, "wan_l2tp_pass"); %>'></td>
              </tr>
              <tr>
                <td class="head" id="wL2tpAddrMode">Address Mode</td>
                <td class=value1><select name="l2tpMode" id="l2tpMode" size="1" onChange="l2tpModeSwitch()">
                    <option value="0" id="wL2tpAddrModeS">Static</option>
                    <option value="1" id="wL2tpAddrModeD">Dynamic</option>
                  </select>
                </td>
              </tr>
              <tr id="l2tpIp">
                <td class="head" id="wL2tpIp">IP Address</td>
                <td class=value1><input name="l2tpIp" maxlength=15 size=15 value='<% getCfgGeneral(1, "wan_l2tp_ip"); %>'></td>
              </tr>
              <tr id="l2tpNetmask">
                <td class="head" id="wL2tpNetmask">Subnet Mask</td>
                <td class=value1><input name="l2tpNetmask" maxlength=15 size=15 value='<% getCfgGeneral(1, "wan_l2tp_netmask"); %>'>
                </td>
              </tr>
              <tr id="l2tpGateway">
                <td class="head" id="wL2tpGateway">Default Gateway</td>
                <td class=value1><input name="l2tpGateway" maxlength=15 size=15 value='<% getCfgGeneral(1, "wan_l2tp_gateway"); %>'>
                </td>
              </tr>
              <tr>
                <td class="head" id="wL2tpOPMode">Operation Mode</td>
                <td class=value1><select name="l2tpOPMode" id="l2tpOPMode" size="1" onChange="l2tpOPModeSwitch()">
                  </select>
                </td>
              </tr>
              <tr>
                <td class=head  align="left" id="l2tpRedial_Period"> online: Redial Period </td>
                <td align="left" class=value1><input type="text" name="l2tpRedialPeriod" maxlength="5" size="4" value="5" onKeyUp="time_check(this);">
                  <font id="l2tpTimeunit"> senconds</font> </td>
              <tr style="display:none; visibility:hidden">
                <td class=head  align="left" id="l2tpIdle_Time"> trigger: Idle Time </td>
                <td align="left" class=value1><input type="text" name="l2tpIdleTime" maxlength="5" size="4" value="600" onKeyUp="time_check(this);">
                  <font id="l2tpTimeunit1"> senconds</font> </td>
              </tr>
            </tbody>
          </table>
          <!-- ================= PPTP Mode ================= -->
          <table id="pptp" cellspacing=1 cellpadding=3 width=450 class=text1 border="1" bordercolor="#9BABBD" style="display:none; visibility:hidden">
            <tbody>
              <tr>
                <td class="title" colspan="2" id="wPptpMode">PPTP Mode</td>
              </tr>
              <tr>
                <td class="head" id="wPptpServer">PPTP Server IP Address</td>
                <td class=value1><input name="pptpServer" maxlength="15" size=15 value='<% getCfgGeneral(1, "wan_pptp_server"); %>'></td>
              </tr>
              <tr>
                <td class="head" id="wPptpUser">User Name</td>
                <td class=value1><input name="pptpUser" maxlength="20" size=20 value='<% getCfgGeneral(1,"wan_pptp_user"); %>'></td>
              </tr>
              <tr>
                <td class="head" id="wPptpPassword">Password</td>
                <td class=value1><input type="password" name="pptpPass" maxlength="32" size=32 value='<% getCfgGeneral(1, "wan_pptp_pass"); %>'></td>
              </tr>
              <tr>
                <td class="head" id="wPptpPass2">Password2</td>
                <td class=value1><input type="password" name="pptpPass2" maxlength="32" size=32 value='<% getCfgGeneral(1, "wan_pptp_pass"); %>'></td>
              </tr>
              <tr>
                <td class="head" id="wPptpAddrMode">Address Mode</td>
                <td class=value1><select name="pptpMode" id="pptpMode" size="1" onChange="pptpModeSwitch()">
                    <option value="0" id="wPptpAddrModeS">Static</option>
                    <option value="1" id="wPptpAddrModeD">Dynamic</option>
                  </select>
                </td>
              </tr>
              <tr id="pptpIp">
                <td class="head" id="wPptpIp">IP Address</td>
                <td class=value1><input name="pptpIp" maxlength=15 size=15 value='<% getCfgGeneral(1, "wan_pptp_ip"); %>'></td>
              </tr>
              <tr id="pptpNetmask">
                <td class="head" id="wPptpNetmask">Subnet Mask</td>
                <td class=value1><input name="pptpNetmask" maxlength=15 size=15 value="<% getCfgGeneral(1, "wan_pptp_netmask"); %>">
                </td>
              </tr>
              <tr id="pptpGateway">
                <td class="head" id="wPptpGateway">Default Gateway</td>
                <td class=value1><input name="pptpGateway" maxlength=15 size=15 value="<% getCfgGeneral(1, "wan_pptp_gateway"); %>">
                </td>
              </tr>
              <tr>
                <td class="head" id="wPptpOPMode">Operation Mode</td>
                <td class=value1><select name="pptpOPMode"  id="pptpOPMode" size="1" onChange="pptpOPModeSwitch()">
                  </select>
                </td>
              </tr>
              <tr>
                <td class=head  align="left" id="pptpRedial_Period"> online: Redial Period </td>
                <td align="left" class=value1><input type="text" name="pptpRedialPeriod" maxlength="5" size="4" value="5" onKeyUp="time_check(this);"/>
                  <font id="pptpTimeunit"> senconds</font> </td>
              <tr  style="display:none; visibility:hidden">
                <td class=head  align="left" id="pptpIdle_Time"> trigger: Idle Time </td>
                <td align="left" class=value1><input type="text" name="pptpIdleTime" maxlength="5" size="4" value="600" onKeyUp="time_check(this);"/>
                  <font id="pptpTimeunit1"> senconds</font> </td>
                <input type="hidden" name="macCloneMac" id="mac_hidden1" />
                <input type="hidden" name="macCloneEnbl" id="macCloneEnbl1" value="0" />
              </tr>
            </tbody>
          </table>
          <iframe name="hiddenframe0"  id="hiddenframe0" frameborder="0" border="0" style="display:none"></iframe>
        </form>
		     <table  cellspacing=1 cellpadding=3 width=450  border="1" bordercolor="#9BABBD" style="margin-top:16px" id="macCloneTable">
          <tbody>
            <tr>
              <td class="title"  colspan="2" id="Clone_Mac_title">WAN  Mac Address Clone</td>
            </tr>
          <form id="allpage_var" name="allpage_var" style="margin:0px;" autocomplete="off">
            <tr >
              <td class="head" id="Clone_Mac_head">Clone Mac Address</td>
              <td class=value1><select name="macCloneEnbl"  id="macCloneSel" size="1" onChange="macCloneSwitch()">
                  <option value="0" id="macCloneD">Disable</option>
                  <option value="1" id="macCloneE">Enable</option>
                </select>
              </td>
            </tr>
            <tr id="macCloneMacRow">
              <td class="head" id="MAC_Address_head">MAC Address</td>
              <td class=value1><input type="hidden" name="macCloneMac" id="MACAddress_text"  maxlength="17" size="17" value="<% getWanMac();%>" />
                <input type="text" size="2" maxlength="2" name="mac0"  id="dhcpStaticMac0" onKeyUp="check_value_long(2,this,'dhcpStaticMac1')">
                :
                <input type="text" size="2" maxlength="2" name="mac0"  id="dhcpStaticMac1" onKeyUp="check_value_long(2,this,'dhcpStaticMac2')">
                :
                <input type="text" size="2" maxlength="2" name="mac0"  id="dhcpStaticMac2"onKeyUp="check_value_long(2,this,'dhcpStaticMac3')">
                :
                <input type="text" size="2" maxlength="2" name="mac0"  id="dhcpStaticMac3"onKeyUp="check_value_long(2,this,'dhcpStaticMac4')">
                :
                <input type="text" size="2" maxlength="2" name="mac0"  id="dhcpStaticMac4"onKeyUp="check_value_long(2,this,'dhcpStaticMac5')">
                :
                <input type="text" size="2" maxlength="2" name="mac0"  id="dhcpStaticMac5"onKeyUp="check_value_long(2,this,'dhcpStaticMac5')">
                <input type="button" id="Clone_button" value="Clone"  style="width:80px" name="button" onClick="macCloneMacFillSubmit();" />
              </td>
            </tr>
          </form>
          </tbody>
        </table>
        <!-- ================= wifi Mode ================= -->
        <form  method=post name="wanCfgx" action="/goform/setWan" id="wanCfg2"   style="margin:0" target="hiddenframe1" autocomplete="off">
    <table id="div_apcli_basic" cellspacing=1 cellpadding=3 width=450 class=text1 border="1" bordercolor="#9BABBD" style="margin-top:10px;">
             <tr>
              <td colspan=2 align="left" class=title id="WIFISetting">无线客户端模式</td>
               
            </tr>
			<tr><input type="hidden" name="internet_mode"   value="WIFI">
              <td class=head  align="left" id="wifi_ssid">SSID:</td>
              <td align="left" class=value1><input type="text" maxlength=32 size="17" name="sta_ssid"></td>
            </tr>
            <tr>
              <td class=head  align="left" id="wifi_mac" >MAC:</td>   <!--color="#808080"-->
              <td align="left" class=value1>
			  <input type="text" name="sta_mac" size="17" maxlength="17"> 
			  <span id="mac_option" style="color:#FF0000"></span>
              </td>
            </tr>
            <tr>
              <td class=head  align="left" id="wifi_chanel">信道:</td>
              <td align="left" class=value1>
			  <select name="sta_channel">
                  <script>
							for(var i=1;i<14; i++)
								document.write('<<option value=' + i + '>' + i + '</option>');
		         </script>
                </select> 
				 
			</td>
            </tr>
            <tr>
              <td class=head  align="left" id="wifi_security_mode">安全模式:</td>
              <td align="left" class=value1>
			    <select name="sta_security_mode" id="sta_security_mode" style="width:100px"  onChange="onChangeSec()">
                  <option value="Disable">Disable</option>
                  <option value="WEP">WEP</option>
                  <option value="WPAPSK">WPA-PSK</option>
                  <option value="WPA2PSK">WPA2-PSK</option>
                </select>
              </td>
            </tr>
          </table>
          <table id="div_wep" cellspacing=1 cellpadding=3 width=450 class=text1 border="1" bordercolor="#9BABBD" style="display:none">
            <tbody>
              <tr>
                <td class=head  align="left" id="wep_mode_td">WEP 模式:</td>
                <td align="left" class=value1>
				 <select name="sta_wep_mode" size="1">
                    <option value=OPEN >OPEN   </option>
		            <option value=SHARED >SHARED  </option>
                  </select>                
			    </td>
              </tr>
              <tr> 
				<td class="head"  id="secureWEPDefaultKey">Default Key</td>
				<td colspan="2" class="value1">
					  <select name="sta_wep_default_key" id="sta_wep_default_key" size="1">
						<option value="1" id="secureWEPDefaultKey1">Key 1   </option>
						<option value="2" id="secureWEPDefaultKey2">Key 2</option>
						<option value="3" id="secureWEPDefaultKey3">Key 3</option>
						<option value="4" id="secureWEPDefaultKey4">Key 4</option>
					  </select>				</td>
			  </tr>
              <tr>
                <td class=head  align="left" id="wifi_wep_mode">密钥格式:</td>
                <td align="left" class=value1>
				<select id="sta_wep_select" name="sta_wep_select"> 
					<option value="1">ASCII </option>
					<option value="0">Hex</option>
				</select>                </td>
              </tr>
              <tr>
                <td class=head  align="left" id="secureWEPKey1">WEP密钥1 :</td>
                <td align="left" class=value1><input name="sta_wep_key_1" maxlength="26" value=""></td>
              </tr>
              <tr>
                <td class=head  align="left" id="secureWEPKey2">WEP密钥2 : </td>
                <td align="left" class=value1><input name="sta_wep_key_2" maxlength="26" value=""></td>
              </tr>
              <tr>
                <td class=head  align="left" id="secureWEPKey3">WEP密钥3 : </td>
                <td align="left" class=value1><input name="sta_wep_key_3" maxlength="26" value=""></td>
              </tr>
              <tr>
                <td class=head  align="left" id="secureWEPKey4">WEP密钥4 : </td>
                <td align="left" class=value1><input name="sta_wep_key_4" maxlength="26" value="" ></td>
              </tr>
            </tbody>
          </table>
          <!--///////////////////END WEP//////////////////////////////////-->
          <!--//////////////////////WPA//////////////////////////////////-->
          <table id="div_wpa" cellspacing=1 cellpadding=3 width=450 class=text1 border="1" bordercolor="#9BABBD" style="display:none">
            <tr>
              <td class=head  align="left" id="wifi_cipher_td">WPA/WPA2 算法:</td>
              <td align="left" class=value1><input name="sta_cipher" value="TKIP" type="radio" checked="checked">
                TKIP &nbsp;
                <input name="sta_cipher" value="AES" type="radio">
                AES &nbsp; </td>
            </tr>
            <tr>
              <td class=head  align="left" id="wifi_passphrase_td">密码:</td>
              <td align="left" class=value1><input name="sta_passphrase" size="28" maxlength="64" value="">
              </td>
            </tr>
          </table>
          <!--//////////////////////END WPA//////////////////////////////-->
          <table id="scan_button" width=450 >
            <tr>
              <td align="right">
			  <input name="wlSurveyBtn" id="wlSurveyBtn" type="button" class="button" onClick="SurveyClose()" value="开启扫描"/>
			  </td>
            </tr>
          </table>
		  <table width="450" cellSpacing=1 cellPadding=3 id="othercfg">
          <tbody>
            <tr align="center">
              <td><input type="button" style="{width:110px;}" value="Apply" id="wApply" onClick=" domCfgSubmit();" >
                &nbsp;&nbsp;
                <input type="reset"  style="{width:110px;}" value="Cancel" id="wCancel" onClick="reset_clicked()">
              </td>
            </tr>
          </tbody>
        </table>
          <table cellspacing=1 cellpadding=3 width=520 class=text1 border="1" bordercolor="#9BABBD" id="wifiScanTable" style="display:none;margin-bottom:25px;margin-top:10px" >
            <tr>
              <td width="10%" class=title><div align="center">选择</div></td>
              <td width="20%" class=title><div align="center">SSID</div></td>
              <td width="30%" class=title><div align="center">MAC地址</div></td>
              <td width="10%" class=title><div align="center">信道</div></td>
              <td width="15%" class=title><div align="center">安全</div></td>
              <td width="15%" class=title><div align="center">信号强度</div></td>
            </tr>
          </table>
          <iframe name="hiddenframe1"  id="hiddenframe1" frameborder="0" border="0" style="display:none"></iframe>
        </form>
      </CENTER></td>
  </tr>
</table>
</BODY>
<script language="JavaScript" type="text/javascript">document.write('<div id="loading"  style="display:none;z-index:9999;"><br><br><br><div align="center">'+_("Submitting settings")+
'</div><br><br>&nbsp;&nbsp;' +_("Submitting be patient")+'</div>');
</script>
</HTML>
