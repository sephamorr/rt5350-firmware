<HTML>
<head>
<TITLE>Index (Wizard | Host Settings)</TITLE>
<META http-equiv="Content-Type" content="text/html; charset=gb2312">
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
<meta content="MSHTML 6.00.2800.1106" name="GENERATOR">
<link href="../style/normal_ws.css" type="text/css" rel="stylesheet">
<link href="../style/style.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../lang/b28n.js"></script>
<script type="text/javascript" src="../js/global_js.js"></script>
<script type="text/javascript" src="../js/wifi_js.js"></script>
<SCRIPT language="javascript" type="text/javascript">
Butterlate.setTextDomain("admin");

var isEn = 1 ;
var ntpcurrenttime = "<% getCurrentTimeASP(); %>"
ntpcurrenttime = ntpcurrenttime*1000;
//Butterlate.setTextDomain("wireless");
parent.menu.setUnderFirmwareUpload(0);
var _singleton = 0;
var page_name = "wizard_asp"
function adm_init()
{
 Butterlate.setTextDomain("admin");
   var e = document.getElementById("wizardpt");
	e.innerHTML = _("wizard title");
	document.getElementById("cur_position").innerHTML=_("Administrator Settings");
	e = document.getElementById("wizard_intro");
	e.innerHTML = _("wizard intro");
	e = document.getElementById("manUserSet");
	e.innerHTML = _("man admin setting");
	e = document.getElementById("manUserAccount");
	e.innerHTML = _("user account");
	e = document.getElementById("manUserPasswd");
	e.innerHTML = _("user passwd");
	e = document.getElementById("manUserPasswd1");
	e.innerHTML = _("user passwd1");
	e = document.getElementById("manUserPasswd2");
	e.innerHTML = _("user passwd2");
	e = document.getElementById("manUserApply");
	e.value = _("next");
	all_page_init();
}

function wifi_init()
{ 
  init_wifi_trans();
var sta_ssid = '<% getCfgGeneral(1, "sta_ssid"); %>';
var sta_mac =  '<% getCfgGeneral(1, "sta_mac"); %>';
var sta_channel =  '<% getCfgGeneral(1, "sta_channel"); %>';
var sta_security_mode =  '<% getCfgGeneral(1, "sta_security_mode"); %>'; //Disable WEP WPAPSK WPA2PSK 
var sta_wep_mode  =  '<% getCfgGeneral(1, "sta_wep_mode"); %>';
      /*     a)value = "OPEN"  
          b)value ="SHARED" */
var sta_wep_default_key = '<% getCfgGeneral(1, "sta_wep_default_key"); %>'; 
         /* a) value = "1" 
          b) value = "2" 
          c) value = "3" 
          d) value = "4"*/
var sta_wep_select ='<% getCfgZero(1, "sta_wep_select"); %>'
         /* a)value = "1"   --ASCII  
          b)value = "0"   --Hex */
var sta_wep_key_1 =  '<% getCfgGeneral(1, "sta_wep_key_1"); %>';
var sta_wep_key_2 =  '<% getCfgGeneral(1, "sta_wep_key_2"); %>';
var sta_wep_key_3 =  '<% getCfgGeneral(1, "sta_wep_key_3"); %>';
var sta_wep_key_4 =  '<% getCfgGeneral(1, "sta_wep_key_4"); %>';
var sta_cipher = '<% getCfgZero(1, "sta_cipher"); %>';
        /* a)value =0       --TKIP
         b)value =1       --AES*/
var sta_passphrase =  '<% getCfgGeneral(1, "sta_passphrase"); %>';
var wifi_form = document.getElementById("wanCfg2");
  
	wifi_form.sta_ssid.value = sta_ssid;
	wifi_form.sta_mac.value = sta_mac;
	
	if(sta_channel!="")
	{
	  wifi_form.sta_channel.value = sta_channel;
	 }
	else 
	{
	  wifi_form.sta_channel.options.selectedIndex = 0;
	}
	
	if(sta_security_mode!="")
	{
	  wifi_form.sta_security_mode.value = sta_security_mode; 
	}
	else
	{
	  wifi_form.sta_security_mode.options.selectedIndex = 0;
	}
	if(sta_wep_mode!="")
	{
	   wifi_form.sta_wep_mode.value  = sta_wep_mode  ;
	}
	else  
	{
	   wifi_form.sta_wep_mode.options.selectedIndex = 0;
	}
	if(sta_wep_default_key!="")
	{
	wifi_form.sta_wep_default_key.value = sta_wep_default_key; 
	}
	else
	{
	  wifi_form.sta_wep_default_key.options.selectedIndex = 0;
	}
	 
	wifi_form.sta_wep_key_1.value = sta_wep_key_1 ;
	wifi_form.sta_wep_key_2.value = sta_wep_key_2;
	wifi_form.sta_wep_key_3.value = sta_wep_key_3;
	wifi_form.sta_wep_key_4.value = sta_wep_key_4;
	
	if(sta_cipher=="TKIP")
	    wifi_form.sta_cipher[0].checked = true;
	 else 
		wifi_form.sta_cipher[1].checked = true;
		
	wifi_form.sta_passphrase.value = sta_passphrase;
}
function runBusinessInit()
{ 
   var runbusinesslist;  
    if(ensureVersion_Rousing())  //是否为荣讯版
	{
	 	 runbusinesslist = '<% getZ3g_isp(); %>';//'ChinaMobile' ;
	 }
	else
	{
		runbusinesslist = '<% listZ3g_isp(); %>';//  var runbusinesslist = 'ChinaMobile' ;listZ3g_isp()ChinaMobile,ChinaTelecom,ChinaUnicom
	}
	var cur_bs = '<%getZ3g_isp();%>'; 
	var runbusiness = new Array(); 
	runbusiness = runbusinesslist.split(",");
	var runbsSelect = document.getElementById("run_bsSelect");
	runbsSelect.options.length = 0;
	var finded = 0;
	
	for(var i = 0;i < runbusiness.length; i++)
	{   
		runbsSelect.options[runbsSelect.length] = new Option(_(runbusiness[i]),runbusiness[i]);
		if(cur_bs == runbusiness[i]){
		runbsSelect.options.selectedIndex = i ;
		finded = 1;
		//document.getElementById("HZ3g_isp").value = cur_bs;
		//document.getElementById("Z3g_isp_type").value = cur_bs;
	}
	else if(i == runbusiness.length-1 && finded == 0)
	{
		runbsSelect.options.selectedIndex = 0;
		//		document.getElementById("HZ3g_isp").value = runbusiness[0];
		//		document.getElementById("Z3g_isp_type").value = runbusiness[0];
	}
}
		//  runBsChange();		
}

function initTranslate_ntp()
{
    e = document.getElementById("manNTPSet");
	e.innerHTML = _("man ntp setting");
	e = document.getElementById("manNTPTimeZone");
	e.innerHTML = _("man ntp timezone");
	e = document.getElementById("manNTPMidIsland");
	e.innerHTML = _("man ntp mid island");
	e = document.getElementById("manNTPHawaii");
	e.innerHTML = _("man ntp hawaii");
	e = document.getElementById("manNTPAlaska");
	e.innerHTML = _("man ntp alaska");
	e = document.getElementById("manNTPPacific");
	e.innerHTML = _("man ntp pacific");
	e = document.getElementById("manNTPMountain");
	e.innerHTML = _("man ntp mountain");
	e = document.getElementById("manNTPArizona");
	e.innerHTML = _("man ntp arizona");
	e = document.getElementById("manNTPCentral");
	e.innerHTML = _("man ntp central");
	e = document.getElementById("manNTPMidUS");
	e.innerHTML = _("man ntp mid us");
	e = document.getElementById("manNTPIndianaEast");
	e.innerHTML = _("man ntp indiana east");
	e = document.getElementById("manNTPEastern");
	e.innerHTML = _("man ntp eastern");
	e = document.getElementById("manNTPAtlantic");
	e.innerHTML = _("man ntp atlantic");
	e = document.getElementById("manNTPBolivia");
	e.innerHTML = _("man ntp bolivia");
	e = document.getElementById("manNTPGuyana");
	e.innerHTML = _("man ntp guyana");
	e = document.getElementById("manNTPBrazilEast");
	e.innerHTML = _("man ntp brazil east");
	e = document.getElementById("manNTPMidAtlantic");
	e.innerHTML = _("man ntp mid atlantic");
	e = document.getElementById("manNTPAzoresIslands");
	e.innerHTML = _("man ntp azores islands");
	e = document.getElementById("manNTPGambia");
	e.innerHTML = _("man ntp gambia");
	e = document.getElementById("manNTPEngland");
	e.innerHTML = _("man ntp england");
	e = document.getElementById("manNTPCzechRepublic");
	e.innerHTML = _("man ntp czech republic");
	e = document.getElementById("manNTPGermany");
	e.innerHTML = _("man ntp germany");
	e = document.getElementById("manNTPTunisia");
	e.innerHTML = _("man ntp tunisia");
	e = document.getElementById("manNTPGreece");
	e.innerHTML = _("man ntp greece");
	e = document.getElementById("manNTPSouthAfrica");
	e.innerHTML = _("man ntp south africa");
	e = document.getElementById("manNTPIraq");
	e.innerHTML = _("man ntp iraq");
	e = document.getElementById("manNTPMoscowWinter");
	e.innerHTML = _("man ntp moscow winter");
	e = document.getElementById("manNTPArmenia");
	e.innerHTML = _("man ntp armenia");
	e = document.getElementById("manNTPPakistan");
	e.innerHTML = _("man ntp pakistan");
	e = document.getElementById("manNTPBangladesh");
	e.innerHTML = _("man ntp bangladesh");
	e = document.getElementById("manNTPThailand");
	e.innerHTML = _("man ntp thailand");
	e = document.getElementById("manNTPChinaCoast");
	e.innerHTML = _("man ntp chinacoast");
	e = document.getElementById("manNTPTaipei");
	e.innerHTML = _("man ntp taipei");
	e = document.getElementById("manNTPSingapore");
	e.innerHTML = _("man ntp singapore");
	e = document.getElementById("manNTPAustraliaWA");
	e.innerHTML = _("man ntp australia wa");
	e = document.getElementById("manNTPJapan");
	e.innerHTML = _("man ntp japan");
	e = document.getElementById("manNTPKorean");
	e.innerHTML = _("man ntp korean");
	e = document.getElementById("manNTPGuam");
	e.innerHTML = _("man ntp guam");
	e = document.getElementById("manNTPAustraliaQLD");
	e.innerHTML = _("man ntp australia qld");
	e = document.getElementById("manNTPSolomonIslands");
	e.innerHTML = _("man ntp solomon islands");
	e = document.getElementById("manNTPFiji");
	e.innerHTML = _("man ntp fiji");
	e = document.getElementById("manNTPNewZealand");
	e.innerHTML = _("man ntp newzealand");
	e = document.getElementById("manNTPServer");
	e.innerHTML = _("man ntp server");
	e = document.getElementById("manNTPSync");
	e.innerHTML = _("man ntp sync");
	e = document.getElementById("NTP_Back");
	e.value = _("back");
	e = document.getElementById("NTP_Next");
	e.value = _("next");
	
	var time_result ;
	if(_("next")=="Next")
	 {
		time_result = new Date(ntpcurrenttime).toString();
		isEn = 1 ;
	 }
	 else
	 { 
		time_result = new Date(ntpcurrenttime).toLocaleString();
		isEn = 0 ;
	  }	
	document.getElementById("man_ntpcurrenttime").innerHTML = time_result ;
	
	e = document.getElementById("manNTPCurrentTime");
	e.innerHTML = _("man ntp current time");
	e = document.getElementById("manNTPSyncWithHost");
	e.value = _("man ntp sync with host");    
	}
	
function PageInit()
{  
  initTranslate_ntp();
  var tz = '<% getCfgGeneral(1, "TZ"); %>';
 var ntp_server = '<% getCfgGeneral(1, "NTPServerIP"); %>';
	if (tz == "UCT_-11")
		document.NTP.time_zone.options.selectedIndex = 0;
	else if (tz == "UCT_-10")
		document.NTP.time_zone.options.selectedIndex = 1;
	else if (tz == "NAS_-09")
		document.NTP.time_zone.options.selectedIndex = 2;
	else if (tz == "PST_-08")
		document.NTP.time_zone.options.selectedIndex = 3;
	else if (tz == "MST_-07")
		document.NTP.time_zone.options.selectedIndex = 4;
	else if (tz == "MST_-07")
		document.NTP.time_zone.options.selectedIndex = 5;
	else if (tz == "CST_-06")
		document.NTP.time_zone.options.selectedIndex = 6;
	else if (tz == "UCT_-06")
		document.NTP.time_zone.options.selectedIndex = 7;
	else if (tz == "UCT_-05")
		document.NTP.time_zone.options.selectedIndex = 8;
	else if (tz == "EST_-05")
		document.NTP.time_zone.options.selectedIndex = 9;
	else if (tz == "AST_-04")
		document.NTP.time_zone.options.selectedIndex = 10;
	else if (tz == "UCT_-04")
		document.NTP.time_zone.options.selectedIndex = 11;
	else if (tz == "UCT_-03")
		document.NTP.time_zone.options.selectedIndex = 12;
	else if (tz == "EBS_-03")
		document.NTP.time_zone.options.selectedIndex = 13;
	else if (tz == "NOR_-02")
		document.NTP.time_zone.options.selectedIndex = 14;
	else if (tz == "EUT_-01")
		document.NTP.time_zone.options.selectedIndex = 15;
	else if (tz == "UCT_000")
		document.NTP.time_zone.options.selectedIndex = 16;
	else if (tz == "GMT_000")
		document.NTP.time_zone.options.selectedIndex = 17;
	else if (tz == "MET_001")
		document.NTP.time_zone.options.selectedIndex = 18;
	else if (tz == "MEZ_001")
		document.NTP.time_zone.options.selectedIndex = 19;
	else if (tz == "UCT_001")
		document.NTP.time_zone.options.selectedIndex = 20;
	else if (tz == "EET_002")
		document.NTP.time_zone.options.selectedIndex = 21;
	else if (tz == "SAS_002")
		document.NTP.time_zone.options.selectedIndex = 22;
	else if (tz == "IST_003")
		document.NTP.time_zone.options.selectedIndex = 23;
	else if (tz == "MSK_003")
		document.NTP.time_zone.options.selectedIndex = 24;
	else if (tz == "UCT_004")
		document.NTP.time_zone.options.selectedIndex = 25;
	else if (tz == "UCT_005")
		document.NTP.time_zone.options.selectedIndex = 26;
	else if (tz == "UCT_006")
		document.NTP.time_zone.options.selectedIndex = 27;
	else if (tz == "UCT_007")
		document.NTP.time_zone.options.selectedIndex = 28;
	else if (tz == "CST_008")
		document.NTP.time_zone.options.selectedIndex = 29;
	else if (tz == "CCT_008")
		document.NTP.time_zone.options.selectedIndex = 30;
	else if (tz == "SST_008")
		document.NTP.time_zone.options.selectedIndex = 31;
	else if (tz == "AWS_008")
		document.NTP.time_zone.options.selectedIndex = 32;
	else if (tz == "JST_009")
		document.NTP.time_zone.options.selectedIndex = 33;
	else if (tz == "KST_009")
		document.NTP.time_zone.options.selectedIndex = 34;
	else if (tz == "UCT_010")
		document.NTP.time_zone.options.selectedIndex = 35;
	else if (tz == "AES_010")
		document.NTP.time_zone.options.selectedIndex = 36;
	else if (tz == "UCT_011")
		document.NTP.time_zone.options.selectedIndex = 37;
	else if (tz == "UCT_012")
		document.NTP.time_zone.options.selectedIndex = 38;
	else if (tz == "NZS_012")
		document.NTP.time_zone.options.selectedIndex = 39;

}

var _singleton = 0;
function AdmFormCheck()
{
	if(_singleton) return false;
	if(document.ImportSettings.filename.value == ""){
		alert(_("Export Settings: Please specify a file."));
		return false;
	}
	document.getElementById("ExportSettingsdiv").style.display="block";
	parent.menu.setUnderFirmwareUpload(1);
	_singleton = 1;
	return true;
}
function syncWithHost()
{
	var currentTime = new Date();
    var seconds = currentTime.getSeconds();
	var minutes = currentTime.getMinutes();
	var hours = currentTime.getHours();
	var month = currentTime.getMonth() + 1;
	var day = currentTime.getDate();
	var year = currentTime.getFullYear();

	var seconds_str = " ";
	var minutes_str = " ";
	var hours_str = " ";
	var month_str = " ";
	var day_str = " ";
	var year_str = " ";

	if(seconds < 10)
		seconds_str = "0" + seconds;
	else
		seconds_str = ""+seconds;

	if(minutes < 10)
		minutes_str = "0" + minutes;
	else
		minutes_str = ""+minutes;

	if(hours < 10)
		hours_str = "0" + hours;
	else
		hours_str = ""+hours;

	if(month < 10)
		month_str = "0" + month;
	else
		month_str = ""+month;

	if(day < 10)
		day_str = "0" + day;
	else
		day_str = day;

	var tmp = month_str + day_str + hours_str + minutes_str + year + "."+seconds;
	/*var Time_display = currentTime.toString();
	if(Time_display != "" || Time_display != NULL)
	{ 
	   var time_Index = Time_display.indexOf("+");
	   var time1 =  Time_display.substring(0,time_Index);
	   var time2 =  Time_display.substring(time_Index+5);
	   var Time_display = time1 + time2 ;
	  }*/
	  var Time_display ;
	  if (isEn) Time_display = currentTime.toString();
	  else       Time_display = currentTime.toLocaleString();
//	document.NTP.ntpcurrenttime.value = Time_display; 
	document.getElementById("man_ntpcurrenttime").innerHTML = Time_display; 
	makeRequest("/goform/NTPSyncWithHost",tmp,alertContents);
}

var MBSSID_MAX 				= 8;
var ACCESSPOLICYLIST_MAX	= 64;
var changed = 0;
var old_MBSSID;
var defaultShownMBSSID = 0;
var SSID = new Array();
var PreAuth = new Array();
var AuthMode = new Array();
var EncrypType = new Array();
var DefaultKeyID = new Array();
var Key1Type = new Array();
var Key1Str = new Array();
var Key2Type = new Array();
var Key2Str = new Array();
var Key3Type = new Array();
var Key3Str = new Array();
var Key4Type = new Array();
var Key4Str = new Array();
var WPAPSK = new Array();
var RekeyMethod = new Array();
var RekeyInterval = new Array();
var PMKCachePeriod = new Array();
var IEEE8021X = new Array();
var RADIUS_Server = new Array();
var RADIUS_Port = new Array();
var RADIUS_Key = new Array();
var session_timeout_interval = new Array();
var AccessPolicy = new Array();
var AccessControlList = new Array();

function checkMac(str){
	var len = str.length;
	if(len!=17)
		return false;

	for (var i=0; i<str.length; i++) {
		if((i%3) == 2){
			if(str.charAt(i) == ':')
				continue;
		}else{
			if (    (str.charAt(i) >= '0' && str.charAt(i) <= '9') ||
					(str.charAt(i) >= 'a' && str.charAt(i) <= 'f') ||
					(str.charAt(i) >= 'A' && str.charAt(i) <= 'F') )
			continue;
		}
		return false;
	}
	return true;
}

function checkRange(str, num, min, max)
{
    d = atoi(str,num);
    if(d > max || d < min)
        return false;
    return true;
}

function checkIpAddr(field)
{
    if(field.value == "")
        return false;

    if ( checkAllNum(field.value) == 0)
        return false;

    if( (!checkRange(field.value,1,0,255)) ||
        (!checkRange(field.value,2,0,255)) ||
        (!checkRange(field.value,3,0,255)) ||
        (!checkRange(field.value,4,1,254)) ){
        return false;
    }
   return true;
}

function checkHex(str){
	var len = str.length;

	for (var i=0; i<str.length; i++) {
		if ((str.charAt(i) >= '0' && str.charAt(i) <= '9') ||
			(str.charAt(i) >= 'a' && str.charAt(i) <= 'f') ||
			(str.charAt(i) >= 'A' && str.charAt(i) <= 'F') ){
				continue;
		}else
	        return false;
	}
    return true;
}

var http_request = false;
function makeRequest(url, content, handler) 
{
	http_request = creat_http_request();
	http_request.onreadystatechange = handler;
	http_request.open('POST', url, true);
	http_request.send(content);
}

function alertContents()
{
    if (http_request.readyState == 4) {
        if (http_request.status == 200) {
			// refresh
		//	window.location.reload();
        } else {
          //  alert('There was a problem with the request.');
        }
    }
}

function securityHandler() {
	if (http_request.readyState == 4) {
		if (http_request.status == 200) {
			parseAllData(http_request.responseText);
			UpdateMBSSIDList();
			LoadFields(defaultShownMBSSID);
			// load Access Policy for MBSSID[selected]
			//LoadAP();
			//ShowAP(defaultShownMBSSID);
		} else {
		   	alert(_("There was a problem with the request."));
		}
	}
}

function deleteAccessPolicyListHandler()
{
	window.location.reload(false);
}


function parseAllData(str)
{
	var all_str = new Array();
	all_str = str.split("\n");
	//defaultShownMBSSID = parseInt(all_str[0]);// all_str.length-2
	for (var i=0; i<2; i++)
	 {
		var fields_str = new Array();
		fields_str = all_str[i+1].split("\r");
		SSID[i] = fields_str[0];
		PreAuth[i] = fields_str[1];
		AuthMode[i] = fields_str[2];
		EncrypType[i] = fields_str[3];
		DefaultKeyID[i] = fields_str[4];
		Key1Type[i] = fields_str[5];
		Key1Str[i] = fields_str[6];
		Key2Type[i] = fields_str[7];
		Key2Str[i] = fields_str[8];
		Key3Type[i] = fields_str[9];
		Key3Str[i] = fields_str[10];
		Key4Type[i] = fields_str[11];
		Key4Str[i] = fields_str[12];
		WPAPSK[i] = fields_str[13];
		RekeyMethod[i] = fields_str[14];
		RekeyInterval[i] = fields_str[15];
		PMKCachePeriod[i] = fields_str[16];
		IEEE8021X[i] = fields_str[17];
		RADIUS_Server[i] = fields_str[18];
		RADIUS_Port[i] = fields_str[19];
		RADIUS_Key[i] = fields_str[20];
		session_timeout_interval[i] = fields_str[21];
		AccessPolicy[i] = fields_str[22];
		AccessControlList[i] = fields_str[23];

		/* !!!! IMPORTANT !!!!*/
		if(IEEE8021X[i] == "1")
			AuthMode[i] = "IEEE8021X";
		if(AuthMode[i] == "OPEN" && EncrypType[i] == "NONE" && IEEE8021X[i] == "0")
			AuthMode[i] = "Disable";
	}
}

function checkData()
{
	var securitymode;
//	var ssid = document.Cfg_wizard.Ssid.value;
 	securitymode = document.Cfg_wizard.security_mode.value;
	if (securitymode == "WEP")
	{ 
		if(!check_Wep() )	return false;
		return true;
	}
	else if (securitymode == "WPAPSK" || securitymode == "WPA2PSK" || securitymode == "WPAPSKWPA2PSK" /* || security_mode == 5 */)
	{
		var keyvalue = document.Cfg_wizard.passphrase.value;
 		if (keyvalue.length == 0){
		   alert(_("Please input wpapsk key!"));
 			return false;
		  }

		if (keyvalue.length < 8){
			alert(_("Please input at least 8 character of wpapsk key!"));
			return false;
		}
 		if(checkInjection(document.Cfg_wizard.passphrase.value) == false){
			alert(_("Invalid characters in Pass Phrase."));
			return false;
		}

		if(document.Cfg_wizard.cipher[0].checked != true && 
		   document.Cfg_wizard.cipher[1].checked != true &&
   		   document.Cfg_wizard.cipher[2].checked != true){
   		   alert(_("Please choose a WPA Algorithms."));
   		   return false;
		}

		if(checkAllNum(document.Cfg_wizard.keyRenewalInterval.value) == false){
			alert(_("Please input a valid key renewal interval."));
			return false;
		}
		if(document.Cfg_wizard.keyRenewalInterval.value < 60){
			alert(_("Warning: A short key renewal interval."));
					// return false;
		}
	//	if(check_wpa() == false)
		//	return false;
		return true;
	}
	/*else if (securitymode == "WPA" || securitymode == "WPA1WPA2") //     WPA or WPA1WP2 mixed mode
	{
		if(check_wpa() == false)
			return false;
		if(check_radius() == false)
			return false;
	}else if (securitymode == "WPA2") //         WPA2
	{
		if(check_wpa() == false)
			return false;
		if( document.Cfg_wizard.PreAuthentication[0].checked == false &&
			document.Cfg_wizard.PreAuthentication[1].checked == false){
			alert(_("Please choose the Pre-Authentication options."));
			
			return false;
		}

		if(!document.Cfg_wizard.PMKCachePeriod.value.length){
			alert(_("Please input the PMK Cache Period."));
			
			return false;
		}
		if(check_radius() == false)
			return false;
	} */
	return true;
}

function check_wpa()
{
		if(document.Cfg_wizard.cipher[0].checked != true && 
		   document.Cfg_wizard.cipher[1].checked != true &&
   		   document.Cfg_wizard.cipher[2].checked != true){
   		   alert(_("Please choose a WPA Algorithms."));
   		   return false;
		}

		if(checkAllNum(document.Cfg_wizard.keyRenewalInterval.value) == false){
			alert(_("Please input a valid key renewal interval."));
			return false;
		}
		if(document.Cfg_wizard.keyRenewalInterval.value < 60){
			alert(_("Warning: A short key renewal interval."));
			// return false;
		}
		return true;
}

function check_radius()
{
	if(!document.Cfg_wizard.RadiusServerIP.value.length){
		alert(_("Please input the radius server ip address."));
		return false;		
	}
	if(!document.Cfg_wizard.RadiusServerPort.value.length){
		alert(_("Please input the radius server port number."));
		return false;		
	}
	if(!document.Cfg_wizard.RadiusServerSecret.value.length){
		alert(_("Please input the radius server shared secret."));
		return false;		
	}

	if(checkIpAddr(document.Cfg_wizard.RadiusServerIP) == false){
		alert(_("Please input a valid radius server ip address."));
		return false;		
	}
	if( (checkRange(document.Cfg_wizard.RadiusServerPort.value, 1, 1, 65535)==false) ||
		(checkAllNum(document.Cfg_wizard.RadiusServerPort.value)==false)){
		alert(_("Please input a valid radius server port number."));
		return false;		
	}
	if(checkStrictInjection(document.Cfg_wizard.RadiusServerSecret.value)==false){
		alert(_("The shared secret contains invalid characters."));
		return false;		
	}

	if(document.Cfg_wizard.RadiusServerSessionTimeout.value.length){
		if(checkAllNum(document.Cfg_wizard.RadiusServerSessionTimeout.value)==false){
			alert(_("Please input a valid session timeout number or u may left it empty."));
			return false;	
		}	
	}

	return true;
}

function securityMode(c_f)
{
	var security_mode;
	changed = c_f;
	hideWep();
	document.getElementById("div_wep_mode").style.visibility = "hidden";
 	document.getElementById("div_wep_mode").style.display =  "none";
	document.getElementById("div_security_shared_mode").style.visibility = "hidden";
	document.getElementById("div_security_shared_mode").style.display = "none";
	document.getElementById("div_wpa").style.visibility = "hidden";
	document.getElementById("div_wpa").style.display = "none";
	document.getElementById("div_wpa_algorithms").style.visibility = "hidden";
	document.getElementById("div_wpa_algorithms").style.display = "none";
	document.getElementById("wpa_passphrase").style.visibility = "hidden";
	document.getElementById("wpa_passphrase").style.display = "none";
	document.getElementById("wpa_key_renewal_interval").style.visibility = "hidden";
	document.getElementById("wpa_key_renewal_interval").style.display = "none";
	document.getElementById("wpa_PMK_Cache_Period").style.visibility = "hidden";
	document.getElementById("wpa_PMK_Cache_Period").style.display = "none";
	document.getElementById("wpa_preAuthentication").style.visibility = "hidden";
	document.getElementById("wpa_preAuthentication").style.display = "none";
	document.Cfg_wizard.cipher[0].disabled = true;
	document.Cfg_wizard.cipher[1].disabled = true;
	document.Cfg_wizard.cipher[2].disabled = true;
	document.Cfg_wizard.passphrase.disabled = true;
	document.Cfg_wizard.keyRenewalInterval.disabled = true;
	document.Cfg_wizard.PMKCachePeriod.disabled = true;
	document.Cfg_wizard.PreAuthentication.disabled = true;
	// 802.1x
	//document.getElementById("div_radius_server").style.visibility = "hidden";
	//document.getElementById("div_radius_server").style.display = "none";
	//document.getElementById("div_8021x_wep").style.visibility = "hidden";
	//document.getElementById("div_8021x_wep").style.display = "none";
	//document.Cfg_wizard.ieee8021x_wep.disable = true;
	/*document.Cfg_wizard.RadiusServerIP.disable = true;
	document.Cfg_wizard.RadiusServerPort.disable = true;
	document.Cfg_wizard.RadiusServerSecret.disable = true;	
	document.Cfg_wizard.RadiusServerSessionTimeout.disable = true;
	document.Cfg_wizard.RadiusServerIdleTimeout.disable = true;	*/
	security_mode = document.Cfg_wizard.security_mode.value;

	if(security_mode == "OPEN" || security_mode == "SHARED" ||security_mode == "WEP"||security_mode == "WEPAUTO" )
	{
		showWep(security_mode);
	}else if (security_mode == "WPAPSK" || security_mode == "WPA2PSK" || security_mode == "WPAPSKWPA2PSK"){
		<!-- WPA -->
		document.getElementById("div_wpa").style.visibility = "visible";
		document.getElementById("div_wpa").style.display = display_on_table();
		document.getElementById("div_wpa_algorithms").style.visibility = "visible";
		document.getElementById("div_wpa_algorithms").style.display = display_on_row();
		document.Cfg_wizard.cipher[0].disabled = false;
		document.Cfg_wizard.cipher[1].disabled = false;

		// deal with TKIP-AES mixed mode
		if(security_mode == "WPAPSK" && document.Cfg_wizard.cipher[2].checked)
			document.Cfg_wizard.cipher[2].checked = false;
		// deal with TKIP-AES mixed mode
		if(security_mode == "WPA2PSK" || security_mode == "WPAPSKWPA2PSK")
			document.Cfg_wizard.cipher[2].disabled = false;

		document.getElementById("wpa_passphrase").style.visibility = "visible";
		document.getElementById("wpa_passphrase").style.display = display_on_row();
		document.Cfg_wizard.passphrase.disabled = false;

		document.getElementById("wpa_key_renewal_interval").style.visibility = "visible";
		document.getElementById("wpa_key_renewal_interval").style.display = display_on_row();
		document.Cfg_wizard.keyRenewalInterval.disabled = false;
	}else if (security_mode == "WPA" || security_mode == "WPA2" || security_mode == "WPA1WPA2") 	{
		document.getElementById("div_wpa").style.visibility = "visible";
		document.getElementById("div_wpa").style.display = display_on_table();

		document.getElementById("div_wpa_algorithms").style.visibility = "visible";
		document.getElementById("div_wpa_algorithms").style.display = display_on_row();
		document.Cfg_wizard.cipher[0].disabled = false;
		document.Cfg_wizard.cipher[1].disabled = false;
		document.getElementById("wpa_key_renewal_interval").style.visibility = "visible";
		document.getElementById("wpa_key_renewal_interval").style.display = display_on_row();
		document.Cfg_wizard.keyRenewalInterval.disabled = false;
	
		<!-- 802.1x -->
		/*document.getElementById("div_radius_server").style.visibility = "visible";
		document.getElementById("div_radius_server").style.display = display_on_table();
		document.Cfg_wizard.RadiusServerIP.disable = false;
		document.Cfg_wizard.RadiusServerPort.disable = false;
		document.Cfg_wizard.RadiusServerSecret.disable = false;	
		document.Cfg_wizard.RadiusServerSessionTimeout.disable = false;
		document.Cfg_wizard.RadiusServerIdleTimeout.disable = false;	*/

		// deal with TKIP-AES mixed mode
		if(security_mode == "WPA" && document.Cfg_wizard.cipher[2].checked)
			document.Cfg_wizard.cipher[2].checked = false;
		// deal with TKIP-AES mixed mode
		if(security_mode == "WPA2"){
			document.Cfg_wizard.cipher[2].disabled = false;
			document.getElementById("wpa_preAuthentication").style.visibility = "visible";
			document.getElementById("wpa_preAuthentication").style.display = display_on_table();
			document.Cfg_wizard.PreAuthentication.disabled = false;
			document.getElementById("wpa_PMK_Cache_Period").style.visibility = "visible";
			document.getElementById("wpa_PMK_Cache_Period").style.display = display_on_table();
			document.Cfg_wizard.PMKCachePeriod.disabled = false;
		}

		// deal with WPA1WPA2 mixed mode
		if(security_mode == "WPA1WPA2"){
			document.Cfg_wizard.cipher[2].disabled = false;
		}

	}else if (security_mode == "IEEE8021X"){ // 802.1X-WEP
		/*document.getElementById("div_8021x_wep").style.visibility = "visible";
		document.getElementById("div_8021x_wep").style.display = display_on_table();

		document.getElementById("div_radius_server").style.visibility = "visible";
		document.getElementById("div_radius_server").style.display = display_on_table();
		document.Cfg_wizard.ieee8021x_wep.disable = false;
		document.Cfg_wizard.RadiusServerIP.disable = false;
		document.Cfg_wizard.RadiusServerPort.disable = false;
		document.Cfg_wizard.RadiusServerSecret.disable = false;	
		document.Cfg_wizard.RadiusServerSessionTimeout.disable = false;*/
		//document.Cfg_wizard.RadiusServerIdleTimeout.disable = false;
	}
}


function hideWep()
{
	document.getElementById("div_wep").style.visibility = "hidden";
	document.getElementById("div_wep").style.display = "none";
}
function showWep(mode)
{
	<!-- WEP -->
	document.getElementById("div_wep").style.visibility = "visible";
	document.getElementById("div_wep").style.display = display_on_table();
	if(mode == "WEP")
	{
 		document.getElementById("div_wep_mode").style.visibility = "visible";
 		document.getElementById("div_wep_mode").style.display = display_on_row();
 	}
	//document.Cfg_wizard.wep_auth_type.disabled = false;
}


function check_Wep()
{
	var defaultid = document.Cfg_wizard.wep_default_key.value;
	var key_input;

	if ( defaultid == 1 )
		var keyvalue = document.Cfg_wizard.wep_key_1.value;
	else if (defaultid == 2)
		var keyvalue = document.Cfg_wizard.wep_key_2.value;
	else if (defaultid == 3)
		var keyvalue = document.Cfg_wizard.wep_key_3.value;
	else if (defaultid == 4)
		var keyvalue = document.Cfg_wizard.wep_key_4.value;
 	if (keyvalue.length == 0 )
	{ // shared wep  || md5
		alert(_("Please input wep key")+defaultid+'!');
		return false;
	}

	var keylength = document.Cfg_wizard.wep_key_1.value.length;
	if (keylength != 0)
	{
		if (document.Cfg_wizard.WEP1Select.options.selectedIndex == 0)
		{
			if(keylength != 5 && keylength != 13)
			{
				alert(_("Please input 5 or 13 characters of wep key1 !"));
				return false;
			}
			if(checkInjection(document.Cfg_wizard.wep_key_1.value)== false)
			{
				alert(_("Wep key1 contains invalid characters."));
				return false;
			}
		}
		if (document.Cfg_wizard.WEP1Select.options.selectedIndex == 1)
		{
			if(keylength != 10 && keylength != 26) 
			{
				alert(_("Please input 10 or 26 characters of wep key1 !"));
				return false;
			}
			if(checkHex(document.Cfg_wizard.wep_key_1.value) == false)
			{
				alert(_("Invalid Wep key1 format!"));
				return false;
			}
		}
	}

	keylength = document.Cfg_wizard.wep_key_2.value.length;
	if (keylength != 0){
		if (document.Cfg_wizard.WEP2Select.options.selectedIndex == 0){
			if(keylength != 5 && keylength != 13) {
				alert(_("Please input 5 or 13 characters of wep key2 !"));
				return false;
			}
			if(checkInjection(document.Cfg_wizard.wep_key_2.value)== false){
				alert(_("Wep key2 contains invalid characters."));
				return false;
			}			
		}
		if (document.Cfg_wizard.WEP2Select.options.selectedIndex == 1){
			if(keylength != 10 && keylength != 26) {
				alert(_("Please input 10 or 26 characters of wep key2 !"));
				return false;
			}
			if(checkHex(document.Cfg_wizard.wep_key_2.value) == false){
				alert(_("Invalid Wep key2 format!"));
				return false;
			}
		}
	}

	keylength = document.Cfg_wizard.wep_key_3.value.length;
	if (keylength != 0){
		if (document.Cfg_wizard.WEP3Select.options.selectedIndex == 0){
			if(keylength != 5 && keylength != 13) {
				alert(_("Please input 5 or 13 characters of wep key3 !"));
				return false;
			}
			if(checkInjection(document.Cfg_wizard.wep_key_3.value)== false){
				alert(_("Wep key3 contains invalid characters."));
				return false;
			}
		}
		if (document.Cfg_wizard.WEP3Select.options.selectedIndex == 1){
			if(keylength != 10 && keylength != 26) {
				alert(_("Please input 10 or 26 characters of wep key3 !"));
				return false;
			}
			if(checkHex(document.Cfg_wizard.wep_key_3.value) == false){
				alert(_("Invalid Wep key3 format!"));
				return false;
			}			
		}
	}

	keylength = document.Cfg_wizard.wep_key_4.value.length;
	if (keylength != 0){
		if (document.Cfg_wizard.WEP4Select.options.selectedIndex == 0){
			if(keylength != 5 && keylength != 13) {
				alert(_("Please input 5 or 13 characters of wep key4 !"));
				return false;
			}
			if(checkInjection(document.Cfg_wizard.wep_key_4.value)== false){
				alert(_("Wep key4 contains invalid characters."));
				return false;
			}			
		}
		if (document.Cfg_wizard.WEP4Select.options.selectedIndex == 1){
			if(keylength != 10 && keylength != 26) {
				alert(_("Please input 10 or 26 characters of wep key4 !"));
				return false;
			}

			if(checkHex(document.Cfg_wizard.wep_key_4.value) == false){
				alert(_("Invalid Wep key4 format!"));
				return false;
			}			
		}
	}
	return true;
}
	
function submit_apply()
{ 
    if(_singleton==1) return false; 
	if (!checkData())  return false; 
	  changed = 0;
	  form_finish();
}

function form_finish()
{
 	document.Cfg_wizard.admuser.value=document.User.admuser.value;
	document.Cfg_wizard.admpass.value=document.User.admpass.value;
	
	document.Cfg_wizard.ntpcurrenttime.value=document.NTP.ntpcurrenttime.value;
	document.Cfg_wizard.time_zone.value=document.NTP.time_zone.value;
	document.Cfg_wizard.NTPServerIP.value=document.NTP.NTPServerIP.value;
	document.Cfg_wizard.NTPSync.value=document.NTP.NTPSync.value;
	document.Cfg_wizard.Z3g_isp.value=document.mode3G.Z3g_isp.value;
	document.Cfg_wizard.Z3g_code.value=document.mode3G.Z3g_code.value;
	document.Cfg_wizard.Z3g_apn.value=document.mode3G.Z3g_apn.value;
	document.Cfg_wizard.Z3g_username.value=document.mode3G.Z3g_username.value;
	document.Cfg_wizard.Z3g_passwd.value=document.mode3G.Z3g_passwd.value;
	
	document.Cfg_wizard.pppoeUser.value=document.POE.pppoeUser.value;
	document.Cfg_wizard.pppoePass.value=document.POE.pppoePass.value;
	
	document.Cfg_wizard.staticIp.value=document.SIP.staticIp.value;
	document.Cfg_wizard.staticNetmask.value=document.SIP.staticNetmask.value;
	document.Cfg_wizard.staticGateway.value=document.SIP.staticGateway.value;
	document.Cfg_wizard.staticPriDns.value=document.SIP.staticPriDns.value;
	
	document.Cfg_wizard.pptpServer.value=document.PPTP.pptpServer.value;
	document.Cfg_wizard.pptpUser.value=document.PPTP.pptpUser.value;
	document.Cfg_wizard.pptpPass.value=document.PPTP.pptpPass.value;
	
	document.Cfg_wizard.l2tpServer.value=document.L2TP.l2tpServer.value;
	document.Cfg_wizard.l2tpUser.value=document.L2TP.l2tpUser.value;
	document.Cfg_wizard.l2tpPass.value=document.L2TP.l2tpPass.value;
	
	document.Cfg_wizard.l2tpPass.value=document.L2TP.l2tpPass.value;
	document.Cfg_wizard.l2tpPass.value=document.L2TP.l2tpPass.value;
	document.Cfg_wizard.l2tpPass.value=document.L2TP.l2tpPass.value;
	
	document.Cfg_wizard.sta_ssid.value = document.wanCfg2.sta_ssid.value; 
	document.Cfg_wizard.sta_mac.value = document.wanCfg2.sta_mac.value; 
	document.Cfg_wizard.sta_channel.value = document.wanCfg2.sta_channel.value; 
	document.Cfg_wizard.sta_security_mode.value = document.wanCfg2.sta_security_mode.value; 
	document.Cfg_wizard.sta_wep_mode.value = document.wanCfg2.sta_wep_mode.value;    
	document.Cfg_wizard.sta_wep_default_key.value = document.wanCfg2.sta_wep_default_key.value;    
	document.Cfg_wizard.sta_wep_select.value = document.wanCfg2.sta_wep_select.value; 
	document.Cfg_wizard.sta_wep_key_1.value = document.wanCfg2.sta_wep_key_1.value;  
	document.Cfg_wizard.sta_wep_key_2.value = document.wanCfg2.sta_wep_key_2.value; 
	document.Cfg_wizard.sta_wep_key_3.value = document.wanCfg2.sta_wep_key_3.value; 
	document.Cfg_wizard.sta_wep_key_4.value = document.wanCfg2.sta_wep_key_4.value; 
	
	if(document.wanCfg2.sta_cipher[0].checked)
	    document.Cfg_wizard.sta_cipher.value = document.wanCfg2.sta_cipher[0].value; 
	else
	    document.Cfg_wizard.sta_cipher.value = document.wanCfg2.sta_cipher[1].value
	
	document.Cfg_wizard.sta_passphrase.value = document.wanCfg2.sta_passphrase.value; 
	
	document.Cfg_wizard.lanIp.value=document.lanCfg.lanIp.value;
	document.Cfg_wizard.lanNetmask.value=document.lanCfg.lanNetmask.value;
	document.Cfg_wizard.lanDhcpType.value=document.lanCfg.lanDhcpType.value;
	document.Cfg_wizard.dhcpStart.value=document.lanCfg.dhcpStart.value;
	document.Cfg_wizard.dhcpEnd.value=document.lanCfg.dhcpEnd.value;
	
	document.Cfg_wizard.wireless_en.value=document.wireless_basic.wireless_en.value;
	document.Cfg_wizard.wirelessmode.value=document.wireless_basic.wirelessmode.value;
	document.Cfg_wizard.bssid_num.value=document.wireless_basic.bssid_num.value;
	document.Cfg_wizard.ssid.value=document.wireless_basic.ssid.value;
	
	//setTimeout("top.view.location='../status/equipinfo.asp';",8000);
	//setTimeout("top.view.location='../status/equipinfo.asp';",6000);
	//setTimeout("top.view.location='../status/equipinfo.asp';",10000);
     _singleton=1 ;
	 document.getElementById("security_mode").style.display="none";
	 document.getElementById("security_shared_mode").style.display="none";
	 document.getElementById("wep_default_key").style.display="none";
	 document.getElementById("loading").style.display="block";
	parent.menu.setUnderFirmwareUpload(1);
	setTimeout("parent.menu.setUnderFirmwareUpload(0);top.view.location='../status/equipinfo.asp';",20000);
 	document.Cfg_wizard.submit();
}
function LoadFields(MBSSID)
{
	var result;
	// Security Policy
	sp_select = document.getElementById("security_mode");
	sp_select.options.length = 0;
	//alert(AuthMode[MBSSID]);
	//alert(MBSSID)
       sp_select.options[sp_select.length] = new Option("Disable",	"Disable",	false, AuthMode[MBSSID] == "Disable");
  /*  sp_select.options[sp_select.length] = new Option("OPEN",	"OPEN",		false, AuthMode[MBSSID] == "OPEN");
    sp_select.options[sp_select.length] = new Option("SHARED",	"SHARED", 	false, AuthMode[MBSSID] == "SHARED");
	*/
    sp_select.options[sp_select.length] = new Option("WEP", "WEP",	false, AuthMode[MBSSID] == "OPEN"||AuthMode[MBSSID] == "SHARED"||AuthMode[MBSSID] == "WEPAUTO");
//sp_select.options[sp_select.length] = new Option("WPA",		"WPA",		false, AuthMode[MBSSID] == "WPA");
    sp_select.options[sp_select.length] = new Option("WPA-PSK", "WPAPSK",	false, AuthMode[MBSSID] == "WPAPSK");
//sp_select.options[sp_select.length] = new Option("WPA2",	"WPA2",		false, AuthMode[MBSSID] == "WPA2");
    sp_select.options[sp_select.length] = new Option("WPA2-PSK","WPA2PSK",	false, AuthMode[MBSSID] == "WPA2PSK");
    sp_select.options[sp_select.length] = new Option("WPA/WPA2-PSK","WPAPSKWPA2PSK",false, AuthMode[MBSSID] == "WPAPSKWPA2PSK");
//sp_select.options[sp_select.length] = new Option("WPA1WPA2","WPA1WPA2",	false, AuthMode[MBSSID] == "WPA1WPA2");
/*	
if(AuthMode[MBSSID] == "Disable")  sp_select.options.selectedIndex = 0;
if(AuthMode[MBSSID] == "OPEN")  sp_select.options.selectedIndex = 1;
if(AuthMode[MBSSID] == "SHARED")  sp_select.options.selectedIndex = 2;
if(AuthMode[MBSSID] == "WEPAUTO")  sp_select.options.selectedIndex = 3;
if(AuthMode[MBSSID] == "WPAPSK")  sp_select.options.selectedIndex = 4 ;
if(AuthMode[MBSSID] == "WPA2PSK")  sp_select.options.selectedIndex = 5 ;
if(AuthMode[MBSSID] == "WPAPSKWPA2PSK")  sp_select.options.selectedIndex = 6;
*/
	// WEP
	document.getElementById("WEP1").value = Key1Str[MBSSID];
	document.getElementById("WEP2").value = Key2Str[MBSSID];
	document.getElementById("WEP3").value = Key3Str[MBSSID];
	document.getElementById("WEP4").value = Key4Str[MBSSID];

	document.getElementById("WEP1Select").selectedIndex = (Key1Type[MBSSID] == "0" ? 1 : 0);
	document.getElementById("WEP2Select").selectedIndex = (Key2Type[MBSSID] == "0" ? 1 : 0);
	document.getElementById("WEP3Select").selectedIndex = (Key3Type[MBSSID] == "0" ? 1 : 0);
	document.getElementById("WEP4Select").selectedIndex = (Key4Type[MBSSID] == "0" ? 1 : 0);

	document.getElementById("wep_default_key").selectedIndex = parseInt(DefaultKeyID[MBSSID]) - 1 ;
// SHARED && NONE
	if(AuthMode[MBSSID] == "SHARED" && EncrypType[MBSSID] == "NONE")
		document.getElementById("security_shared_mode").selectedIndex = 1;
	else
		document.getElementById("security_shared_mode").selectedIndex = 0;
    
	if(AuthMode[MBSSID] == "WEP"||AuthMode[MBSSID] == "SHARED"||AuthMode[MBSSID] == "WEPAUTO")
	{
	    document.getElementById("wep_mode").value = AuthMode[MBSSID]; 
	}
	else
	{
	    document.getElementById("wep_mode").selectedIndex = 0;
	}
	// WPA
	if(EncrypType[MBSSID] == "TKIP")
		document.Cfg_wizard.cipher[0].checked = true;
	else if(EncrypType[MBSSID] == "AES")
		document.Cfg_wizard.cipher[1].checked = true;
	else if(EncrypType[MBSSID] == "TKIPAES")
		document.Cfg_wizard.cipher[2].checked = true;

	document.getElementById("passphrase").value = WPAPSK[MBSSID];
	document.getElementById("keyRenewalInterval").value = RekeyInterval[MBSSID];
	document.getElementById("PMKCachePeriod").value = PMKCachePeriod[MBSSID];
	//document.getElementById("PreAuthentication").value = PreAuth[MBSSID];
	if(PreAuth[MBSSID] == "0")
		document.Cfg_wizard.PreAuthentication[0].checked = true;
	else
		document.Cfg_wizard.PreAuthentication[1].checked = true;

	//802.1x wep
	if(IEEE8021X[MBSSID] == "1"){
		if(EncrypType[MBSSID] == "WEP")
			document.Cfg_wizard.ieee8021x_wep[1].checked = true;
		else
			document.Cfg_wizard.ieee8021x_wep[0].checked = true;
	}
	
	//document.getElementById("RadiusServerIP").value = RADIUS_Server[MBSSID];
	//document.getElementById("RadiusServerPort").value = RADIUS_Port[MBSSID];
	//document.getElementById("RadiusServerSecret").value = RADIUS_Key[MBSSID];			
	//document.getElementById("RadiusServerSessionTimeout").value = session_timeout_interval[MBSSID];
	securityMode(0);
}

function ShowAP(MBSSID)
{
	var i;
	for(i=0; i<MBSSID_MAX; i++){
		document.getElementById("apselect_"+i).selectedIndex	= AccessPolicy[i];
		document.getElementById("AccessPolicy_"+i).style.visibility = "hidden";
		document.getElementById("AccessPolicy_"+i).style.display = "none";
	}

	document.getElementById("AccessPolicy_"+MBSSID).style.visibility = "visible";
	if (window.ActiveXObject) {			// IE
		document.getElementById("AccessPolicy_"+MBSSID).style.display = "block";
	}else if (window.XMLHttpRequest) {	// Mozilla, Safari,...
		document.getElementById("AccessPolicy_"+MBSSID).style.display = "table";
	}
}

function LoadAP()
{
	for(var i=0; i<SSID.length; i++){
		var j=0;
		var aplist = new Array;

		if(AccessControlList[i].length != 0){
			aplist = AccessControlList[i].split(";");
			for(j=0; j<aplist.length; j++){
				document.getElementById("newap_"+i+"_"+j).value = aplist[j];
			}

			// hide the lastest <td class="value1">
			if(j%2){
				document.getElementById("newap_td_"+i+"_"+j).style.visibility = "hidden";
				document.getElementById("newap_td_"+i+"_"+j).style.display = "none";
				j++;
			}
		}

		// hide <tr> left
		for(; j<ACCESSPOLICYLIST_MAX; j+=2){
			document.getElementById("id_"+i+"_"+j).style.visibility = "hidden";
			document.getElementById("id_"+i+"_"+j).style.display = "none";
		}
	}
}

function selectMBSSIDChanged()
{
	// check if any security settings changed
	if(changed){
		ret = confirm("Are you sure to ignore changed?");
		if(!ret){
			document.Cfg_wizard.ssidIndex.options.selectedIndex = old_MBSSID;
			return false;
		}
		else
			changed = 0;
	}

	var selected = document.Cfg_wizard.ssidIndex.options.selectedIndex;

	// backup for user cancel action
	old_MBSSID = selected;

	MBSSIDChange(selected);
}

/*
 * When user select the different SSID, this function would be called.
 */ 
function MBSSIDChange(selected)
{
	// load wep/wpa/802.1x table for MBSSID[selected]
	LoadFields(selected);
	// update Access Policy for MBSSID[selected]
	//ShowAP(selected);
	// radio button special case
	WPAAlgorithms = EncrypType[selected];
	IEEE8021XWEP = IEEE8021X[selected];
	PreAuthentication = PreAuth[selected];
	//changeSecurityPolicyTableTitle(SSID[selected]);
	// clear all new access policy list field
	for(i=0; i<MBSSID_MAX; i++)
		document.getElementById("newap_text_"+i).value = "";
	return true;
}

function changeSecurityPolicyTableTitle(t)
{
	var title = document.getElementById("sp_title");
	title.innerHTML = "\"" + t + "\"";
}

function delap(mbssid, num)
{
	makeRequest("/goform/APDeleteAccessPolicyList", mbssid+ "," +num, deleteAccessPolicyListHandler);
}


function initAll()
{  
		
	makeRequest("/goform/wirelessGetSecurity", "n/a", securityHandler);
}

function UpdateMBSSIDList()
{
	//document.Cfg_wizard.ssidIndex.options.length = 0;
//	for(var i=0; i<SSID.length; i++){
		//var j = document.Cfg_wizard.ssidIndex.options.length;
		SSID[0]=document.wireless_basic.ssid.value;
		document.Cfg_wizard.ssidIndex.options[0] = new Option(SSID[0], 0, false, false);
	// }
	//document.Cfg_wizard.ssidIndex.options.selectedIndex = defaultShownMBSSID;
	old_MBSSID = defaultShownMBSSID;
	//changeSecurityPolicyTableTitle(SSID[defaultShownMBSSID]);
	changeSecurityPolicyTableTitle(SSID[0]);
}

function setChange(c){
	changed = c;
}

var WPAAlgorithms;
function onWPAAlgorithmsClick(type)
{
	if(type == 0 && WPAAlgorithms == "TKIP") return;
	if(type == 1 && WPAAlgorithms == "AES") return;
	if(type == 2 && WPAAlgorithms == "TKIPAES") return;
	setChange(1);
}

var IEEE8021XWEP;
function onIEEE8021XWEPClick(type)
{
	if(type == 0 && IEEE8021XWEP == false) return;
	if(type == 1 && IEEE8021XWEP == true) return;
	setChange(1);
}

var PreAuthentication;
function onPreAuthenticationClick(type)
{
	if(type == 0 && PreAuthentication == false) return;
	if(type == 1 && PreAuthentication == true) return;
	setChange(1);
}

function auto_fill_data( a, b,c, d)
{ 
	document.getElementById("Z3g_code").value= a;
	document.getElementById("Z3g_apn").value= b;
	document.getElementById("Z3g_username").value= c;
	document.getElementById("Z3g_passwd").value= d;
 }
var wan_type=0;
function setWanMode(m)
{ 
  Butterlate.setTextDomain("admin");
    if(m=="DHCP")
	{
	dhcpinit();
	document.getElementById("c_step").style.display = "none";
	document.getElementById("c_step").style.visibility ="hidden" ;
	document.getElementById("D").style.display = display_on_table();
	document.getElementById("D").style.visibility ="visible" ;
	wan_type = "DHCP";
	}
	else
	{
	document.getElementById("c_step").style.display = "none";
	document.getElementById("c_step").style.visibility ="hidden" ;
	document.getElementById("C"+m).style.display = display_on_table();
	document.getElementById("C"+m).style.visibility ="visible" ;
    }
	
   switch(m)
   {
    case "DHCP":
      document.Cfg_wizard.WAN_mode.value="DHCP";
	  document.getElementById("cur_position").innerHTML=_("LAN Setting");
	  break;
	case 1:
      document.Cfg_wizard.WAN_mode.value="3G";
	  runBusinessInit();
	  document.getElementById("cur_position").innerHTML=_("3G Setting");
	  document.getElementById("run_business").innerHTML=_("run business");
	  document.getElementById("title_3Gmode").innerHTML=_("3G Setting");
	  document.getElementById("callnumber").innerHTML=_("Dail Number");
	  var en =_("next");
	  if (en == "Next")
	  {
		document.getElementById("help_list1").innerHTML='<input type="button" name="ISP_Info" value="Operator Info" style="width:110" onClick=\'window.open("../ISP-list.asp","ISP_Info","top=500,left=600,width=400,height=400,resizable=yes,scrollbars=yes");\'>';
		document.getElementById("Dail_Number_tr").style.display = display_on_row();
		document.getElementById("Dail_Number_tr").style.visibility = "visible";
		document.getElementById("APN_tr").style.visibility = "visible";
		document.getElementById("Password_tr").style.visibility = "visible";
		document.getElementById("User_Name_tr").style.visibility = "visible";
		document.getElementById("APN_tr").style.display = display_on_row();
		document.getElementById("Password_tr").style.display = display_on_row();
		document.getElementById("User_Name_tr").style.display = display_on_row();
	
	 }
	 else
	 { 
		document.getElementById("run_business_tr").style.display = display_on_row();
		document.getElementById("run_business_tr").style.visibility = "visible";
       var run_business= '<% getZ3g_isp(); %>' ;
		//help_list_select(run_business, "help_list");
		 if(ensureVersion_Rousing())  //是否为荣讯版
		 {
			if(run_business =="ChinaTelecom")
			{
			document.getElementById("help_list").innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="ISP_Info" value="3G上网卡" onClick=\'opratorInfo = window.open("../ISP2.asp","","top=500,left=600,width=400,height=400,resizable=yes,scrollbars=yes");\'>';
			}
			else if(run_business =="ChinaUnicom")
			{
			document.getElementById("help_list").innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="ISP_Info" value="3G上网卡" onClick=\'opratorInfo = window.open("../ISP3.asp","","top=500,left=600,width=400,height=400,resizable=yes,scrollbars=yes");\'>';
			}
			else
			{
			document.getElementById("help_list").innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="ISP_Info" value="3G上网卡" onClick=\'opratorInfo = window.open("../ISP.asp","","top=500,left=600,width=400,height=400,resizable=yes,scrollbars=yes");\'>';
			}
		}
		else
		{
			document.getElementById("help_list").innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="ISP_Info" value="3G上网卡" onClick=\'opratorInfo = window.open("../ISP.asp","","top=500,left=600,width=400,height=400,resizable=yes,scrollbars=yes");\'>';
		 }
 	//	help_list_select(run_business, "help_list");
	
 	//	document.getElementById("help_list").innerHTML='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="ISP_Info" value="3G上网卡"  onClick=\'window.open("../ISP.asp","","top=500,left=600,width=400,height=400,resizable=yes,scrollbars=yes");\'>';
		
	 }
		document.getElementById("username").innerHTML=_("user account");
		document.getElementById("password").innerHTML=_("user passwd");
		document.getElementById("3G_Back").value=_("back");
		document.getElementById("3G_next").value=_("next");
	  break;
	case 2:
	   
	   document.getElementById("pppoe_title").innerHTML=_("PPPOE Setting");
      document.Cfg_wizard.WAN_mode.value="PPPOE";
	  document.getElementById("cur_position").innerHTML=_("PPPOE Setting");
	  document.getElementById("wPppoeUser").innerHTML=_("user account");
	  document.getElementById("wPppoePassword").innerHTML=_("user passwd");
	  document.getElementById("wPppoePass2").innerHTML=_("Verify Password");
	  document.getElementById("wPppoe_back").value=_("back");
	    document.getElementById("wPppoe_next").value=_("next");
	  break;
	case 3:   
       document.Cfg_wizard.WAN_mode.value="STATIC";
	   document.getElementById("cur_position").innerHTML=_("STATIC Setting");
	   document.getElementById("static_title").innerHTML=_("STATIC Setting");
       document.getElementById("wStaticIp").innerHTML=_("IP Address");
	   document.getElementById("wStaticNetmask").innerHTML=_("status subnet mask");
	   document.getElementById("wStaticGateway").innerHTML=_("status default gateway");
	   document.getElementById("wStaticPriDns").innerHTML=_("status primary dns");
	   document.getElementById("wStatic_back").value=_("back");
	    document.getElementById("wStatic_next").value=_("next");
	  break;
	 case 4: 
		document.Cfg_wizard.WAN_mode.value="PPTP";
		document.getElementById("pptp_title").innerHTML=_("PPTP Setting");
		document.getElementById("wPptpServer").innerHTML=_("PPTP Server IP Address");
		document.getElementById("wPptpUser").innerHTML=_("user account");
		document.getElementById("wPptpPassword").innerHTML=_("user passwd");
		document.getElementById("wPptpPass2").innerHTML=_("Verify Password");
		document.getElementById("cur_position").innerHTML=_("PPTP Setting");
		document.getElementById("wPptp_back").value=_("back");
		document.getElementById("wPptp_next").value=_("next");
	  break;
	 case 5:
		document.Cfg_wizard.WAN_mode.value="L2TP";
		document.getElementById("wL2tpMode").innerHTML=_("L2TP Setting");
		document.getElementById("cur_position").innerHTML=_("L2TP Setting");
		document.getElementById("wL2tpServer").innerHTML=_("L2TP Server IP Address");
		document.getElementById("wL2tpUser").innerHTML=_("user account");
		document.getElementById("wL2tpPassword").innerHTML=_("user passwd");
		document.getElementById("wL2tpPass2").innerHTML=_("Verify Password");
		document.getElementById("wL2tp_back").value=_("back");
		document.getElementById("wL2tp_next").value=_("next");
     case 6:
	   Butterlate.setTextDomain("internet");
	   wifi_init();
	   onChangeSec();
	    document.getElementById("wifi_Back").value = _("back");;
	    document.getElementById("wifi_Next").value = _("next");
	    document.Cfg_wizard.WAN_mode.value="WIFI";
 	  break;
     }
	//alert(document.Cfg_wizard.WAN_mode.value) 
}


function to_D_Step(m)
{   Butterlate.setTextDomain("admin");
	if(m*1==1)
	{
		var Z3g_codeT = document.getElementById("Z3g_code");
		var Z3g_apnT = document.getElementById("Z3g_apn");
		var Z3g_usernameT = document.getElementById("Z3g_username");
		var Z3g_passwdT = document.getElementById("Z3g_passwd");
		
		if( Z3g_codeT.value == "" )
		{
			if(Z3g_apnT.value != "" )
			{
			 alert(_("You Code Number can't be null while APN  isnot null."));
				 return ;
			}else if(Z3g_usernameT.value !="")
			{
			 alert(_("You Code Number can't be null while  Username isnot null."));						                        return ;
			}else if(Z3g_passwdT.value !="")
			{
			 alert(_("You Code Number can't be null while Password  isnot null."));	
				return ;
			}
						
		  }	
	}
	else if(m*1 == 6)
	{ 
	Butterlate.setTextDomain("internet");
	  if(!CheckWifiValue())
	  return;
	}
	else if(!CheckValue())
	{ 
	//alert("ss2");
	    return;
	}
	
    for (var m=1; m<=6;m++ )
	{
		if ( document.getElementById("C"+m).style.visibility == "visible")
		{ 
		wan_type = m;
		document.getElementById("C"+m).style.display ="none";
		document.getElementById("C"+m).style.visibility ="hidden" ;
		}
		
	}
	document.getElementById("D").style.display = display_on_table();
	document.getElementById("D").style.visibility ="visible" ;     
	 document.getElementById("cur_position").innerHTML=_("LAN Setting");
	dhcpinit();
}

function CheckValue()
{    // alert("ss1");
        var Main_connectionMode = document.getElementById("WAN_mode");
	//	 alert(Main_connectionMode.value);
      	var SIP_form = document.getElementById("SIP");
		var PPPOE_form = document.getElementById("POE");
		var PPTP_form = document.getElementById("PPTP");
		var L2TP_form = document.getElementById("L2TP");
	if (Main_connectionMode.value == "STATIC") 
	{      //STATIC
//	alert("STATIC");
		if (!checkIpAddr(SIP_form.staticIp, false))
			return false;
		if (!checkIpAddr(SIP_form.staticNetmask, true))
			return false;
		if (SIP_form.staticGateway.value != "")
			if (!checkIpAddr(SIP_form.staticGateway, false))
				return false;
		if (SIP_form.staticPriDns.value != "")
			if (!checkIpAddr(SIP_form.staticPriDns, false))
				return false;
	}
	else if (Main_connectionMode.value == "PPPOE") { //PPPOE
	    if (PPPOE_form.pppoeUser.value == "") 
		{
		    alert(_("Account cannot be null!"));
			PPPOE_form.pppoeUser.focus();
			PPPOE_form.pppoeUser.select();
			return false;
		}
		if (PPPOE_form.pppoePass.value != PPPOE_form.pppoePass2.value) 
		{
		    alert(_("Password mismatched!"));
			return false;
		}
		
	}
	else if (Main_connectionMode.value == "L2TP") 
	{ //L2TP
	      if (L2TP_form.l2tpServer.value == "") 
			{ 
				alert(_("Server IP cannot be null!"));
				return false;
			}
			
	   if (L2TP_form.l2tpUser.value == "") 
		{
		    alert(_("Account cannot be null!"));
			L2TP_form.l2tpUser.focus();
			L2TP_form.l2tpUser.select();
			return false;
		}
	     if (L2TP_form.l2tpPass.value != L2TP_form.l2tpPass2.value) 
		  {
		     alert(_('Password mismatched!'));
			 return false;
		  }
	}
	else if (Main_connectionMode.value == "PPTP") { //PPTP
		   if (PPTP_form.pptpServer.value == "") 
			{ 
				alert(_("Server IP cannot be null!"));
				return false;
			}
		
		 if (PPTP_form.pptpUser.value == "") 
		{
		    alert(_("Account cannot be null!"));
			 	PPTP_form.pptpUser.focus();
				PPTP_form.pptpUser.select();
			return false;
		}
		if (PPTP_form.pptpPass.value != PPTP_form.pptpPass2.value) 
		{
			alert(_('Password mismatched!'));
			return false;
		}
						
	}
	return true;
}

function DtoCX_step()
{  Butterlate.setTextDomain("admin");
    document.getElementById("D").style.display = "none";
	document.getElementById("D").style.visibility ="hidden" ;
	if(wan_type=="DHCP")
	{   
	    document.getElementById("c_step").style.display =display_on_table();
		document.getElementById("c_step").style.visibility ="visible" ;
		 document.getElementById("cur_position").innerHTML=_("WAN Setting"); 
	}
	else
	{   document.getElementById("C"+wan_type).style.display =display_on_table();
		document.getElementById("C"+wan_type).style.visibility ="visible" ;
		
	  switch(wan_type) 
	  {
	   case 1:
	    document.getElementById("cur_position").innerHTML=_("3G Setting");
	    break;
	   case 2:
     	  document.getElementById("cur_position").innerHTML=_("PPPOE Setting");
	    break;
	   case 3:
        document.getElementById("cur_position").innerHTML=_("STATIC Setting");
	    break;
	   case 4:
        document.getElementById("cur_position").innerHTML=_("PPTP Setting");
	    break;
	   case 5:
        document.getElementById("cur_position").innerHTML=_("L2TP Setting");
      }
   }
	
}

function basic_wireless_init()
{
var radio_off = '<% getCfgZero(1, "RadioOff"); %>';
var PhyMode  = '<% getCfgZero(1, "WirelessMode"); %>';
    PhyMode  = 1*PhyMode;
	 if (1*radio_off == 1)
		{
		document.getElementById("wireless_en").options.selectedIndex=0;
		}
	else{
       document.getElementById("wireless_en").options.selectedIndex=1;
		}
	
	if ((PhyMode == 0) || (PhyMode == 4) || (PhyMode == 9))
	{
		if (PhyMode == 0)
			document.wireless_basic.wirelessmode.options.selectedIndex = 0;
		else if (PhyMode == 4)
			document.wireless_basic.wirelessmode.options.selectedIndex = 2;
		else if (PhyMode == 9)
			document.wireless_basic.wirelessmode.options.selectedIndex = 3;
	}
	else if (PhyMode == 1)
	{
		document.wireless_basic.wirelessmode.options.selectedIndex = 1;
	}
	else 
	{
		document.wireless_basic.wirelessmode.options.selectedIndex = 3;
	}
}

function to_WB_Step(from,to)
{  
   if (DHCP_CheckValue()){
 // try {  initTranslation_wb();}
//  catch(Err )
 // {  alert("initTranslation_wb"); }
     initTranslation_wb();
	 Butterlate.setTextDomain("admin");
	  document.getElementById("cur_position").innerHTML=_("Wireless Basic Setting");
    document.getElementById(from).style.display = "none";
	document.getElementById(from).style.visibility = "hidden"
    document.getElementById(to).style.display = display_on_table();
	document.getElementById(to).style.visibility = "visible";
	basic_wireless_init();
	}
}

function dhcpinit()
{
 var dhcp = '<% getCfgZero(1, "dhcpEnabled"); %>';
 document.lanCfg.lanDhcpType.options.selectedIndex = 1*dhcp;
	dhcpTypeSwitch();
	   
	Butterlate.setTextDomain("internet");
	e = document.getElementById("lSetup");
	e.innerHTML = _("lan setup");
	e = document.getElementById("lIp");
	e.innerHTML = _("inet ip");
	e = document.getElementById("lNetmask");
	e.innerHTML = _("inet netmask");
	e = document.getElementById("lDhcpType");
	e.innerHTML = _("lan dhcp type");
	e = document.getElementById("lDhcpTypeD");
	e.innerHTML = _("inet disable");
	e = document.getElementById("lDhcpTypeS");
	e.innerHTML = _("lan dhcp type server");
	e = document.getElementById("lDhcpStart");
	e.innerHTML = _("lan dhcp start");
	e = document.getElementById("lDhcpEnd");
	e.innerHTML = _("lan dhcp end");
	e = document.getElementById("LAN_Back");
	e.value = _("back");
	e = document.getElementById("LAN_Next");
	e.value = _("next");
	 
}
function dhcpTypeSwitch()
{
	document.getElementById("start").style.visibility = "hidden";
	document.getElementById("start").style.display = "none";
	document.lanCfg.dhcpStart.disabled = true;
	document.getElementById("end").style.visibility = "hidden";
	document.getElementById("end").style.display = "none";
	document.lanCfg.dhcpEnd.disabled = true;
	if (document.lanCfg.lanDhcpType.options.selectedIndex == 1)
	{
		document.getElementById("start").style.visibility = "visible";
		document.getElementById("start").style.display = display_on_row();
		document.lanCfg.dhcpStart.disabled = false;
		document.getElementById("end").style.visibility = "visible";
		document.getElementById("end").style.display = display_on_row();
		document.lanCfg.dhcpEnd.disabled = false;
	}
}

function DHCP_CheckValue()
{
	if (!checkIpAddr(document.lanCfg.lanIp, false))
		return false;
	if (!checkIpAddr(document.lanCfg.lanNetmask, true))
		return false;
	
	if (document.lanCfg.lanDhcpType.options.selectedIndex == 1)
	{
		if (!checkIpAddr(document.lanCfg.dhcpStart, false))
			return false;
		if (!checkIpAddr(document.lanCfg.dhcpEnd, false))
			return false;
		
	}
	return true;
}

var oldIp;
function recIpCfg()
{
	oldIp = document.lanCfg.lanIp.value;
}

function modDhcpCfg()
{
	var i, j;
	var mask = document.lanCfg.lanNetmask.value;
	var newNet = document.lanCfg.lanIp.value;

	//support simple subnet mask only
	if (mask == "255.255.255.0")
		mask = 3;
	else if (mask == "255.255.0.0")
		mask = 2;
	else if (mask == "255.0.0.0")
		mask = 1;
	else
		return;

	//get the old subnet
	for (i=0, j=0; i<oldIp.length; i++) {
		if (oldIp.charAt(i) == '.') {
			j++;
			if (j != mask)
				continue;
			oldIp = oldIp.substring(0, i);
			break;
		}
	}

	//get the new subnet
	for (i=0, j=0; i<newNet.length; i++) {
		if (newNet.charAt(i) == '.') {
			j++;
			if (j != mask)
				continue;
			newNet = newNet.substring(0, i);
			break;
		}
	}

	document.lanCfg.dhcpStart.value = document.lanCfg.dhcpStart.value.replace(oldIp, newNet);
	document.lanCfg.dhcpEnd.value = document.lanCfg.dhcpEnd.value.replace(oldIp, newNet);
}

function initTranslation_wb(){
   
Butterlate.setTextDomain("wireless");
	var e = document.getElementById("basicWirelessNet");
	e.innerHTML = _("basic wireless network");
	e = document.getElementById("basicRadioButton");
	e.innerHTML = _("basic radio button");
	e = document.getElementById("basicNetMode");
	e.innerHTML = _("basic network mode");
	e = document.getElementById("basicSSID");
	e.innerHTML = _("basic ssid");
	
	e = document.getElementById("WB_Next");
	e.value =_("next");
	e = document.getElementById("WB_Back");
	e.value =_("back");
	}
	
function initTranslate_WSecrity(){
 
 Butterlate.setTextDomain("wireless");
	var e = document.getElementById("secureSelectSSID");
	e.innerHTML = _("secure select ssid");
	e = document.getElementById("secureSSIDChoice");
	e.innerHTML = _("secure ssid choice");
	
	e = document.getElementById("secureSecureMode");
	e.innerHTML = _("secure security mode");
	
	setInnerHTML("wep_mode_td","security options")
	e = document.getElementById("secureEncrypType");
	e.innerHTML = _("secure encryp type");
	e = document.getElementById("secureEncrypTypeNone");
	e.innerHTML = _("wireless none");

	e = document.getElementById("secureWEP");
	e.innerHTML = _("secure wep");
	e = document.getElementById("secureWEPDefaultKey");
	e.innerHTML = _("secure wep default key");
	e = document.getElementById("secureWEPDefaultKey1");
	e.innerHTML = _("secure wep default key1");
	e = document.getElementById("secureWEPDefaultKey2");
	e.innerHTML = _("secure wep default key2");
	e = document.getElementById("secureWEPDefaultKey3");
	e.innerHTML = _("secure wep default key3");
	e = document.getElementById("secureWEPDefaultKey4");
	e.innerHTML = _("secure wep default key4");
	e = document.getElementById("secureWEPKey");
	e.innerHTML = _("secure wep key");
	e = document.getElementById("secureWEPKey1");
	e.innerHTML = _("secure wep key1");
	e = document.getElementById("secureWEPKey2");
	e.innerHTML = _("secure wep key2");
	e = document.getElementById("secureWEPKey3");
	e.innerHTML = _("secure wep key3");
	e = document.getElementById("secureWEPKey4");
	e.innerHTML = _("secure wep key4");
	
	e = document.getElementById("secreWPA");
	e.innerHTML = _("secure wpa");
	e = document.getElementById("secureWPAAlgorithm");
	e.innerHTML = _("secure wpa algorithm");
	e = document.getElementById("secureWPAPassPhrase");
	e.innerHTML = _("secure wpa pass phrase");
	e = document.getElementById("secureWPAKeyRenewInterval");
	e.innerHTML = _("secure wpa key renew interval");
	e = document.getElementById("secureWPAPMKCachePeriod");
	e.innerHTML = _("secure wpa pmk cache period");
	e = document.getElementById("secureWPAPreAuth");
	e.innerHTML = _("secure wpa preauth");
	e = document.getElementById("secureWPAPreAuthDisable");
	e.innerHTML = _("wireless disable");
	e = document.getElementById("secureWPAPreAuthEnable");
	e.innerHTML = _("wireless enable");
	
	e = document.getElementById("Back_to_WB");
	e.value = _("back");
	e = document.getElementById("Finish");
	e.value = _("finish");

}
	
function to_WSecrity_Step(from,to)
{  
   if(document.wireless_basic.ssid==""||document.wireless_basic.ssid==null)
   {
   alert("SSID cannot be use,please input again.");
    }
	else
	{
	    initAll();
		Butterlate.setTextDomain("admin");
	    document.getElementById("cur_position").innerHTML=_("Wireless Secrity Setting");
	 document.getElementById(from).style.display = "none";
	 document.getElementById(from).style.visibility = "hidden"
     document.getElementById(to).style.display = display_on_table();
	 document.getElementById(to).style.visibility = "visible";
	 initTranslate_WSecrity();
	}
	
}
function UserFormCheck()
{ 
	 var Username = '<% getCfgGeneral(1, "Login"); %>'; 
	 var Userpass = '<% getCfgGeneral(1, "Password"); %>'; 
	 document.User.oldadmpass.value = document.User.oldadmpass.value.Trim();
	 document.User.admpass2.value = document.User.admpass2.value.Trim();
	 document.User.admpass.value = document.User.admpass.value.Trim();
	 document.User.admuser.value = document.User.admuser.value.Trim();
	 
	 if (document.User.oldadmpass.value == ""&&document.User.admpass2.value == ""&&document.User.admpass.value == "")
	 {
	  return true;
	 }
	if (document.User.admuser.value == "") {
		alert(_("Your username is null,please input again."));
		return false;
	}
    /*	
	if (document.User.oldadmpass.value == "") {
		alert(_("Please specify the Userinistrator password."));
		return false;
	}
	*/
	if (document.User.admpass2.value == ""||document.User.admpass.value == "") {
		alert(_("Your new password is null,please input again."));
		return false;
	}
	
	if ( document.User.oldadmpass.value != Userpass) {
		alert(_("Your password erro,please input again."));
	       document.User.admuser.value="";
		   document.User.oldadmpass.value="";
		return false;
	}
	
	if (document.User.admpass.value!=document.User.admpass2.value) {
		alert(_("Your double new password isnot equil,please input again."));
		document.User.admpass.value="";
		document.User.admpass2.value="";
		document.User.admpass.focus();
		return false;
	}
	return true;
}

function to_second_Step()
{
	 if(UserFormCheck())
	 {
	 Butterlate.setTextDomain("admin");
	//   help_href_modi("sntp")
	  document.getElementById("cur_position").innerHTML=_("NTP Setting");
	 document.getElementById("first_step").style.display ="none"
	 document.getElementById("first_step").style.visibility ="hidden"
	 document.getElementById("second_step").style.display = "block";
	 document.getElementById("second_step").style.visibility ="visible";
	 PageInit();
	}

}
function atoi(str, num)
{
	i = 1;
	if (num != 1) {
		while (i != num && str.length != 0) {
			if (str.charAt(0) == '.') {
				i++;
			}
			str = str.substring(1);
		}
		if (i != num)
			return -1;
	}

	for (i=0; i<str.length; i++) {
		if (str.charAt(i) == '.') {
			str = str.substring(0, i);
			break;
		}
	}
	if (str.length == 0)
		return -1;
	return parseInt(str, 10);
}

function checkRange(str, num, min, max)
{
	d = atoi(str, num);
	if (d > max || d < min)
		return false;
	return true;
}

function isAllNum(str)
{
	for (var i=0; i<str.length; i++) {
		if ((str.charAt(i) >= '0' && str.charAt(i) <= '9') || (str.charAt(i) == '.' ))
			continue;
		return 0;
	}
	return 1;
}

function checkIpAddr(field, ismask)
{
	if (field.value == "") {
		alert(_("Error.IP address is empty."));
		field.value = field.defaultValue;
		field.focus();
		return false;
	}

	if (isAllNum(field.value) == 0) {
		alert(_('It should be a [0-9] number.'));
		field.value = field.defaultValue;
		field.focus();
		return false;
	}

	if (ismask) {
		if ((!checkRange(field.value, 1, 0, 256)) ||
				(!checkRange(field.value, 2, 0, 256)) ||
				(!checkRange(field.value, 3, 0, 256)) ||
				(!checkRange(field.value, 4, 0, 256)))
		{
			 alert(_('IP format error.'));
			field.value = field.defaultValue;
			field.focus();
			return false;
		}
	}
	else {
		if ((!checkRange(field.value, 1, 0, 255)) ||
				(!checkRange(field.value, 2, 0, 255)) ||
				(!checkRange(field.value, 3, 0, 255)) ||
				(!checkRange(field.value, 4, 1, 254)))
		{
			 alert(_('IP format error.'));
			field.value = field.defaultValue;
			field.focus();
			return false;
		}
	}
	return true;
}
function NTPFormCheck()
{
	if( document.NTP.NTPServerIP.value != "" && 
		document.NTP.NTPSync.value == ""){
		alert(_("Please specify a value for the interval of synchroniztion."));
		return false;
	}
	if(isAllNum( document.NTP.NTPSync.value ) == 0){
		alert(_("Invalid NTP synchronization value."));
		return false;
	}
	if( atoi(document.NTP.NTPSync.value, 1) > 300){
		alert(_("The synchronization value is too big.(1~300)"));
		return false;
	}
	 Butterlate.setTextDomain("admin");
	 	document.getElementById("cur_position").innerHTML=_("WAN Setting");
		document.getElementById("wan_select").innerHTML=_("Select WAN Mode:"); 
		document.getElementById("gg_mode").innerHTML=_("gg_mode"); 
		document.getElementById("pppoe_mode").innerHTML=_("pppoe_mode"); 
		document.getElementById("dhcp_mode").innerHTML=_("dhcp_mode"); 
		document.getElementById("static_mode").innerHTML=_("static_mode"); 
		document.getElementById("pptp_mode").innerHTML=_("pptp_mode"); 
		document.getElementById("l2tp_mode").innerHTML=_("l2tp_mode");
		document.getElementById("wifi_label").innerHTML=_("Wireless Client Mode");
		
		document.getElementById("wan_back").value=_("back");  
	document.getElementById("second_step").style.display = "none";
	document.getElementById("second_step").style.visibility = "hidden"
    document.getElementById("c_step").style.display = display_on_table();
	document.getElementById("c_step").style.visibility = "visible";
}

function toBack_step(from,to){ 
  if(_singleton==1) return false;
  Butterlate.setTextDomain("admin");
       document.getElementById(from).style.display = "none";
	document.getElementById(from).style.visibility = "hidden"
       document.getElementById(to).style.display = display_on_table();
	document.getElementById(to).style.visibility = "visible";
	switch(to)
	{
	 case "D":
	 document.getElementById("cur_position").innerHTML=_("LAN Setting");break;
	  case "WB":
	  document.getElementById("cur_position").innerHTML=_("Wireless Basic Setting"); break;
	 case "WSecrity":
	  document.getElementById("cur_position").innerHTML=_("Wireless Secrity Setting"); break;
	 case "c_step":
	 document.getElementById("cur_position").innerHTML=_("WAN Setting"); break;
	 case "first_step":
	 document.getElementById("cur_position").innerHTML=_("Administrator Settings"); break;
	 case "second_step":
	 document.getElementById("cur_position").innerHTML=_("NTP Setting");  break;
	  
     }       
	 
}



</SCRIPT>
</HEAD>
<BODY  onLoad="adm_init();">
<h2 class="btnl"> <span id="wizardpt"> Wizard Information</span> > <span id="cur_position">Administrator Settings</span> </h2>
<table  width="100%" class="tintro" >
  <tbody>
    <tr>
      <td class="intro" id="wizard_intro" ></td>
      <td class ="image_col" align="right" ><script language="javascript" type="text/javascript" >
	  var lang =_("next");
	   if (lang == "Next") lang="en";
	  else lang="zh_cn";
	   help_display("wizard_asp",lang);
	  </script>
      </td>
    </tr>
  </tbody>
</table>
<hr>
<div id="first_step">
  <form name="User">
    <table width="450" border="1" cellspacing="1" cellpadding="3" bordercolor="#9BABBD" style="margin-left:100px">
      <tr>
        <td class="title" colspan="2" id="manUserSet">Adminstrator Settings</td>
      </tr>
      <tr>
        <td class="head" id="manUserAccount">Username</td>
        <td class="value1"><input  type="text" name="admuser" size="16" maxlength="16" value='<% getCfgGeneral(1, "Login"); %>'></td>
      </tr>
      <tr>
        <td class="head" id="manUserPasswd">Password</td>
        <td class="value1"><input type="password" name="oldadmpass" size="16" maxlength="32" value=""></td>
      </tr>
      <tr>
        <td class="head" id="manUserPasswd1">NewPassword</td>
        <td class="value1"><input type="password" name="admpass" size="16" maxlength="32" value=""></td>
      </tr>
      <tr>
        <td class="head" id="manUserPasswd2">Verify NewPassword</td>
        <td class="value1"><input type="password" name="admpass2" size="16" maxlength="32" value="">
        </td>
      </tr>
    </table>
    <table width="450" height="32" border="0" cellpadding="2" cellspacing="1" style="margin-left:100">
      <tr align="center">
        <td><input name="button" type="button" id="manUserApply" style="{width:110px;}" onClick="to_second_Step()" value="Next ">
        </td>
      </tr>
    </table>
  </form>
</div>
<!-- step A host settings -->
<div id="second_step" style="visibility:hidden;display:none">
  <form name="NTP"  style="margin-left:100">
    <table width="450" border="1" cellspacing="1" cellpadding="3" bordercolor="#9BABBD">
      <tbody>
        <tr>
          <td class="title" colspan="2" id="manNTPSet">NTP Settings</td>
        </tr>
        <tr>
          <td class="head"  id="manNTPCurrentTime">Current Time</td>
          <td class="value1"><input type="hidden" size="24" name="ntpcurrenttime" value="" readonly="1">
            <span  id="man_ntpcurrenttime" style="width:200px" > </span>
            <!--% getCurrentTimeASP(); %-->
            <input type="button" value="Sync with host"  style="{width:80px;}"id="manNTPSyncWithHost" name="manNTPSyncWithHost" onClick="syncWithHost()">
          </td>
        </tr>
        <tr>
          <td class="head" id="manNTPTimeZone">Time Zone:</td>
          <td class="value1"><select name="time_zone">
              <option value="UCT_-11" id="manNTPMidIsland">(GMT-11:00) Midway Island, Samoa</option>
              <option value="UCT_-10" id="manNTPHawaii">(GMT-10:00) Hawaii</option>
              <option value="NAS_-09" id="manNTPAlaska">(GMT-09:00) Alaska</option>
              <option value="PST_-08" id="manNTPPacific">(GMT-08:00) Pacific Time</option>
              <option value="MST_-07" id="manNTPMountain">(GMT-07:00) Mountain Time</option>
              <option value="MST_-07" id="manNTPArizona">(GMT-07:00) Arizona</option>
              <option value="CST_-06" id="manNTPCentral">(GMT-06:00) Central Time</option>
              <option value="UCT_-06" id="manNTPMidUS">(GMT-06:00) Middle America</option>
              <option value="UCT_-05" id="manNTPIndianaEast">(GMT-05:00) Indiana East,Colombia</option>
              <option value="EST_-05" id="manNTPEastern">(GMT-05:00) Eastern Time</option>
              <option value="AST_-04" id="manNTPAtlantic">(GMT-04:00) Atlantic Time,Brazil West</option>
              <option value="UCT_-04" id="manNTPBolivia">(GMT-04:00) Bolivia, Venezuela</option>
              <option value="UCT_-03" id="manNTPGuyana">(GMT-03:00) Guyana</option>
              <option value="EBS_-03" id="manNTPBrazilEast">(GMT-03:00) Brazil East,Greenland</option>
              <option value="NOR_-02" id="manNTPMidAtlantic">(GMT-02:00) Mid-Atlantic</option>
              <option value="EUT_-01" id="manNTPAzoresIslands">(GMT-01:00) Azores Islands</option>
              <option value="UCT_000" id="manNTPGambia">(GMT) Gambia, Liberia, Morocco</option>
              <option value="GMT_000" id="manNTPEngland">(GMT) England</option>
              <option value="MET_001" id="manNTPCzechRepublic">(GMT+01:00) Czech Republic,N</option>
              <option value="MEZ_001" id="manNTPGermany">(GMT+01:00) Germany</option>
              <option value="UCT_001" id="manNTPTunisia">(GMT+01:00) Tunisia</option>
              <option value="EET_002" id="manNTPGreece">(GMT+02:00) Greece, Ukraine,Turkey</option>
              <option value="SAS_002" id="manNTPSouthAfrica">(GMT+02:00) South Africa</option>
              <option value="IST_003" id="manNTPIraq">(GMT+03:00) Iraq, Jordan, Kuwait</option>
              <option value="MSK_003" id="manNTPMoscowWinter">(GMT+03:00) Moscow Winter Time</option>
              <option value="UCT_004" id="manNTPArmenia">(GMT+04:00) Armenia</option>
              <option value="UCT_005" id="manNTPPakistan">(GMT+05:00) Pakistan, Russia</option>
              <option value="UCT_006" id="manNTPBangladesh">(GMT+06:00) Bangladesh,Russia</option>
              <option value="UCT_007" id="manNTPThailand">(GMT+07:00) Thailand, Russia</option>
              <option value="CST_008" id="manNTPChinaCoast">(GMT+08:00) China Coast,Hong Kong</option>
              <option value="CCT_008" id="manNTPTaipei">(GMT+08:00) Taipei</option>
              <option value="SST_008" id="manNTPSingapore">(GMT+08:00) Singapore</option>
              <option value="AWS_008" id="manNTPAustraliaWA">(GMT+08:00) Australia (WA)</option>
              <option value="JST_009" id="manNTPJapan">(GMT+09:00) Japan, Korea</option>
              <option value="KST_009" id="manNTPKorean">(GMT+09:00) Korean</option>
              <option value="UCT_010" id="manNTPGuam">(GMT+10:00) Guam, Russia</option>
              <option value="AES_010" id="manNTPAustraliaQLD">(GMT+10:00) Australia </option>
              <option value="UCT_011" id="manNTPSolomonIslands">(GMT+11:00) Solomon Islands</option>
              <option value="UCT_012" id="manNTPFiji">(GMT+12:00) Fiji</option>
              <option value="NZS_012" id="manNTPNewZealand">(GMT+12:00) New Zealand</option>
            </select>
          </td>
        </tr>
        <tr>
          <td class="head" id="manNTPServer">NTP Server</td>
          <td class="value1"><input size="32" maxlength="64" name="NTPServerIP" value='<% getCfgGeneral(1, "NTPServerIP"); %>' type="text">
            <br>
            &nbsp;&nbsp;<font color="#808080">ex:&nbsp;time.nist.gov</font> <br>
            &nbsp;&nbsp;<font color="#808080">&nbsp;&nbsp;&nbsp;&nbsp;ntp0.broad.mit.edu</font> <br>
            &nbsp;&nbsp;<font color="#808080">&nbsp;&nbsp;&nbsp;&nbsp;time.stdtime.gov.tw</font> </td>
        </tr>
        <tr>
          <td class="head" id="manNTPSync">NTP synchronization</td>
          <td class="value1"><input size="4" maxlength="3" name="NTPSync" value='<% getCfgGeneral(1, "NTPSync"); %>' type="text">
          </td>
        </tr>
      </tbody>
    </table>
    <table width="425" border="0" cellpadding="2" cellspacing="1">
      <tr >
        <td width="419"  align="center"><input type="button" style="{width:110px;}" onClick="toBack_step('second_step','first_step');" value="Back" id="NTP_Back">
          &nbsp; &nbsp;
          <input type="button" style="{width:110px;}" value="Next" id="NTP_Next" onClick="NTPFormCheck()">
        </td>
      </tr>
    </table>
  </form>
</div>
<div id="c_step" style="visibility:hidden;display:none;margin-left:100"> <BR>
  <!--title="请选择WAN口的连接类型。点选不同连接方式，会进入相应的设置页面供您进行WAN口参数设置。"-->
  <table  width="450" border="1" cellspacing="1" cellpadding="3" bordercolor="#9BABBD"  >
    <tr>
      <td class="title" id="wan_select" > Select WAN Mode: </td>
    </tr>
    <tr>
      <td ><li style="color: orange"> <a href="javascript:setWanMode(1);" id="gg_mode">&nbsp;<b>3G</b> </a></li></td>
    </tr>
    <tr>
      <td ><li style="color: orange"><a href="javascript:setWanMode(2);" id="pppoe_mode">&nbsp;<b>PPPoE</b> </a></li></td>
    </tr>
    <tr>
      <td><li style="color: orange"> <a href="javascript:setWanMode('DHCP');" id="dhcp_mode">&nbsp;<b>DHCP</b> </a></li>
        <!--(xDSL线路上常用的连接方式) (通过ISP自动获得IP地址) (使用ISP分配的静态IP地址) (点对点隧道协议VPN通信)-->
      </td>
    </tr>
    <tr>
      <td ><li style="color: orange"> <a href="javascript:setWanMode(3);" id="static_mode">&nbsp;STATIC </a></li></td>
    </tr>
    <tr>
      <td><li style="color: orange"> <a href="javascript:setWanMode(4);" id="pptp_mode">&nbsp;<b>PPTP</b> </a></li></td>
    </tr>
    <tr>
      <td><li style="color: orange"><a href="javascript:setWanMode(5);" id="l2tp_mode">&nbsp;<b>L2TP</b> </a></li></td>
    </tr>
	<tr>
      <td><li style="color: orange"><a href="javascript:setWanMode(6);" id="wifi_label">&nbsp;<b>Wireless Client Mode</b> </a></li></td>
    </tr>
  </table>
  <table  width="450">
    <tr>
      <td align="center"><input type="button" style="{width:110px;}" onClick="toBack_step('c_step','second_step');" value="Back"  id="wan_back">
      </td>
    </tr>
  </table>
</div>
<!-- step C1 -->
<div id=C1 style="visibility:hidden;display:none; margin-left:100"> <BR>
  <table width="450" border="1" cellspacing="1" cellpadding="3" bordercolor="#9BABBD" >
    <tr>
      <td class=title colspan="2" id="title_3Gmode">3G Mode</td>
    </tr>
    <FORM name="mode3G" id="mode3G">
      <tr id="run_business_tr" style="display:none; visibility:hidden">
        <td class=head  align="left" id="run_business">run business</td>
        <td class=value1 align="left" id="run_business2"><select name="Z3g_isp" id="run_bsSelect" size="1">
            <!--------initialize in javascript " runBusinessInit() "-------->
          </select>
          <span id="help_list"></span></td>
      </tr>
      <tr id="Dail_Number_tr" style="display:none; visibility:hidden">
        <td class=head  align="left" id="callnumber">Dail Number</td>
        <td class=value1  align="left"><input name="Z3g_code" id= "Z3g_code" maxlength=32 size=16  value='<% getZ3g_code(); %>'>
          <span id="help_list1"></span></td>
        </td>
      </tr>
      <tr id="APN_tr" style="display:none; visibility:hidden">
        <td class=head  align="left" id="apn">APN</td>
        <td class=value1  align="left"><input name="Z3g_apn" id="Z3g_apn" maxlength=32 size=22
             value='<% getZ3g_apn(); %>'>
        </td>
      </tr>
      <tr id="User_Name_tr" style="display:none; visibility:hidden">
        <td class=head  align="left" id="username">User Name</td>
        <td class=value1  align="left"><input name="Z3g_username" id="Z3g_username" maxlength=32 size=22
             value='<% getZ3g_username(); %>'>
        </td>
      </tr>
      <tr id="Password_tr" style="display:none; visibility:hidden">
        <td class=head  align="left" id="password">Password</td>
        <td class=value1  align="left"><input type="password" name="Z3g_passwd" id="Z3g_passwd" maxlength=32 size=22   value='<% getZ3g_passwd(); %>'>
        </td>
      </tr>
    </FORM>
  </table>
  <table width="450" border="0" cellpadding="2" cellspacing="1">
    <tr >
      <td  align="center"><input type="button" style="{width:110px;}" onClick="toBack_step('C1','c_step');" value="Back" id="3G_Back">
        &nbsp; &nbsp;
        <input name="button" type=submit id="3G_next" style="{width:110px;}" onClick="to_D_Step(1);" value="Next ">
      </td>
    </tr>
  </table>
</div>
<!-- step C2 pppoe -->
<div id=C2 style="visibility:hidden;display:none; margin-left:100"> <BR>
  <table cellspacing=1 cellpadding=3 width=450  class=text1 border="1">
    <tr>
      <td class=title colspan="2" id="pppoe_title">PPPOE Mode </td>
    </tr>
    <FORM name="POE" id="POE">
      <tr>
        <td class="head" id="wPppoeUser">User Name</td>
        <td class=value1><input name="pppoeUser" maxlength=32 size=32 value='<% getCfgGeneral(1,"wan_pppoe_user"); %>'></td>
      </tr>
      <tr>
        <td class="head" id="wPppoePassword">Password</td>
        <td class=value1><input type="password" name="pppoePass" maxlength=32 size=32 value='<% getCfgGeneral(1, "wan_pppoe_pass"); %>'></td>
      </tr>
      <tr>
        <td class="head" id="wPppoePass2">Verify Password</td>
        <td class=value1><input type="password" name="pppoePass2" maxlength=32 size=32
             value="<% getCfgGeneral(1, 'wan_pppoe_pass'); %>"></td>
      </tr>
    </FORM>
  </table>
  <table width="450" border="0" cellpadding="2" cellspacing="1">
    <tr >
      <td  align="center"><input type="button" style="{width:110px;}" onClick="toBack_step('C2','c_step');" value="Back" id="wPppoe_back">
        &nbsp; &nbsp;
        <input type="button" id="wPppoe_next" style="{width:110px;}" onClick="to_D_Step(2);" value="Next">
      </td>
    </tr>
  </table>
</div>
<div id=C3 style="visibility:hidden;display:none; margin-left:100"> <BR>
  <table width="450" border="1" cellspacing="1" cellpadding="3" bordercolor="#9BABBD" >
    <tr>
      <td class=title colspan="2" id="static_title">STATIC Mode</td>
    </tr>
    <FORM name="SIP" id="SIP">
      <tr>
        <td class="head" id="wStaticIp">IP Address</td>
        <td class=value1><input name="staticIp" maxlength=15 value="<% getWanIp(); %>"></td>
      </tr>
      <tr>
        <td class="head" id="wStaticNetmask">Subnet Mask</td>
        <td class=value1><input name="staticNetmask" maxlength=15 value="<% getWanNetmask(); %>"></td>
      </tr>
      <tr>
        <td class="head" id="wStaticGateway">Default Gateway</td>
        <td class=value1><input name="staticGateway" maxlength=15 value="<% getWanGateway(); %>">
        </td>
      </tr>
      <tr>
        <td class="head" id="wStaticPriDns">Primary DNS Server</td>
        <td class=value1><input name="staticPriDns" maxlength=15 value="<% getDns(1); %>"></td>
      </tr>
    </FORM>
  </table>
  <table width="450" border="0" cellpadding="2" cellspacing="1">
    <tr >
      <td  align="center"><input type="button" style="{width:110px;}" onClick="toBack_step('C3','c_step');" value="Back" id="wStatic_back">
        &nbsp; &nbsp;
        <input type="button" style="{width:110px;}" value="Next"id="wStatic_next" onClick="to_D_Step(3);">
      </td>
    </tr>
  </table>
</div>
<!-- step C4 pptp -->
<div id=C4 style="visibility:hidden;display:none; margin-left:100"> <BR>
  <table cellspacing=1 cellpadding=5 width=450  class=text1 border="1">
    <tr>
      <td class=title colspan="2" id="pptp_title">PPTP Mode </td>
    </tr>
    <FORM name="PPTP" id="PPTP">
      <tr>
        <td class="head" id="wPptpServer">PPTP Server IP Address</td>
        <td class=value1><input name="pptpServer" maxlength="15" size=20 value='<%
       getCfgGeneral(1, "wan_pptp_server"); %>'></td>
      </tr>
      <tr>
        <td class="head" id="wPptpUser">User Name</td>
        <td class=value1><input name="pptpUser" maxlength="20" size=20 value='<%
       getCfgGeneral(1, "wan_pptp_user"); %>'></td>
      </tr>
      <tr>
        <td class="head" id="wPptpPassword">Password</td>
        <td class=value1><input type="password" name="pptpPass" maxlength="32" size=32 value='<%
       getCfgGeneral(1, "wan_pptp_pass"); %>'></td>
      </tr>
      <tr>
        <td class="head" id="wPptpPass2">Verify Password</td>
        <td class=value1><input type="password" name="pptpPass2" maxlength="32" size=32 value='<%
       getCfgGeneral(1, "wan_pptp_pass"); %>'></td>
      </tr>
    </FORM>
  </table>
  <table width="450" border="0" cellpadding="2" cellspacing="1">
    <tr >
      <td  align="center"><input type="button" style="{width:110px;}" onClick="toBack_step('C4','c_step');" value="Back" id="wPptp_back">
        &nbsp; &nbsp;
        <input type="button"  style="{width:110px;}" value="Next"id="wPptp_next" onClick="to_D_Step(4);">
      </td>
    </tr>
  </table>
</div>
<!-- step C5 l2tp -->
<div id=C5 style="visibility:hidden;display:none; margin-left:100"> <BR>
  <table cellspacing=1 cellpadding=5 width=450  class=text1 border="1">
    <FORM name="L2TP" id="L2TP">
      <tr>
        <td class="title" colspan="2" id="wL2tpMode">L2TP Mode</td>
      </tr>
      <tr>
        <td class="head" id="wL2tpServer">L2TP Server IP Address</td>
        <td class=value1><input name="l2tpServer" maxlength="15" size=15 value='<% getCfgGeneral(1, "wan_l2tp_server"); %>'></td>
      </tr>
      <tr>
        <td class="head" id="wL2tpUser">User Name</td>
        <td class=value1><input name="l2tpUser" maxlength="20" size=20 value='<%
       getCfgGeneral(1, "wan_l2tp_user"); %>'></td>
      </tr>
      <tr>
        <td class="head" id="wL2tpPassword">Password</td>
        <td class=value1><input type="password" name="l2tpPass" maxlength="32" size=32 value='<%
       getCfgGeneral(1, "wan_l2tp_pass"); %>'></td>
      </tr>
      <tr>
        <td class="head" id="wL2tpPass2">Verify Password</td>
        <td class=value1><input type="password" name="l2tpPass2" maxlength="32" size=32 value='<%
       getCfgGeneral(1, "wan_l2tp_pass"); %>'></td>
      </tr>
    </FORM>
  </table>
  <table width="450" border="0" cellpadding="2" cellspacing="1">
    <tr >
      <td  align="center"><input type="button" style="{width:110px;}" onClick="toBack_step('C5','c_step');" value="Back" id="wL2tp_back">
        &nbsp; &nbsp;
        <input type="button"  style="{width:110px;}" value="Next"id="wL2tp_next" onClick="to_D_Step(5);">
      </td>
    </tr>
  </table>
</div>


  
	  
	  
<div id=C6 style="visibility:hidden;display:none; margin-left:100"> <BR>
 <FORM name="wanCfg2" id="wanCfg2">
	  <table cellspacing=1 cellpadding=5 width=450  class=text1 border="1">
            <tr>
              <td colspan=2 align="left" class=title id="WIFISetting">无线客户端模式</td>
            </tr>
			<tr><input type="hidden" name="internet_mode"   value="WIFI">
              <td class=head  align="left" id="ssid_wifi">SSID:</td>
              <td align="left" class=value1><input type="text" maxlength=32 size="17" name="sta_ssid"></td>
            </tr>
            <tr>
              <td class=head  align="left" id="mac_wifi" >MAC:</td>
              <td align="left" class=value1><input type="text" name="sta_mac" size="17" maxlength="17"> 
			  <span id="mac_option" style="color:#FF0000">(option)</span>
			  </td>
            </tr>
            <tr>
              <td class=head  align="left" id="chanel_wifi">信道:</td>
              <td align="left" class=value1>
			  <select name="sta_channel">
                  <script>
							for(var i=1;i<14; i++)
								document.write('<<option value=' + i + '>' + i + '</option>');
		         </script>
                </select> 
				 
			</td>
            </tr>
            <tr>
              <td class=head  align="left" id="security_mode_wifi">安全模式:</td>
              <td align="left" class=value1>
			    <select name="sta_security_mode" id="sta_security_mode" style="width:100px"  onChange="onChangeSec()">
                  <option value="Disable" selected="selected">Disable</option>
                  <option value="WEP"    >WEP</option>
                  <option value="WPAPSK">WPA-PSK</option>
                  <option value="WPA2PSK">WPA2-PSK</option>
                </select>
              </td>
            </tr>
          </table>
		  
          <table id="div_wep_wifi" cellspacing=1 cellpadding=5 width=450  class=text1 border="1" style="display:none">
            <tbody>
              <tr>
                <td class=head  align="left" id="wep_mode_wifi">WEP 模式:</td>
                <td align="left" class=value1>
				 <select name="sta_wep_mode" size="1">
                    <option value=OPEN  selected="selected">OPEN   </option>
		            <option value=SHARED >SHARED  </option>
                  </select>                
			    </td>
              </tr>
              <tr> 
				<td class="head"  id="secureWEPDefaultKey_wifi">Default Key</td>
				<td colspan="2" class="value1">
					  <select name="sta_wep_default_key" id="sta_wep_default_key" size="1">
						<option value="1" id="secureWEPDefaultKey1_wifi" selected="selected">Key 1   </option>
						<option value="2" id="secureWEPDefaultKey2_wifi">Key 2</option>
						<option value="3" id="secureWEPDefaultKey3_wifi">Key 3</option>
						<option value="4" id="secureWEPDefaultKey4_wifi">Key 4</option>
					  </select>				</td>
			  </tr>
              <tr>
                <td class=head  align="left" id="wifi_wep_mode_wifi">密钥格式:</td>
                <td align="left" class=value1>
				<select id="sta_wep_select" name="sta_wep_select"> 
					<option value="1">ASCII </option>
					<option value="0">Hex</option>
				</select>                </td>
              </tr>
              <tr>
                <td class=head  align="left" id="secureWEPKey1_wifi">WEP密钥1 :</td>
                <td align="left" class=value1><input name="sta_wep_key_1" maxlength="26" value=""></td>
              </tr>
              <tr>
                <td class=head  align="left" id="secureWEPKey2_wifi">WEP密钥2 : </td>
                <td align="left" class=value1><input name="sta_wep_key_2" maxlength="26" value=""></td>
              </tr>
              <tr>
                <td class=head  align="left" id="secureWEPKey3_wifi">WEP密钥3 : </td>
                <td align="left" class=value1><input name="sta_wep_key_3" maxlength="26" value=""></td>
              </tr>
              <tr>
                <td class=head  align="left" id="secureWEPKey4_wifi">WEP密钥4 : </td>
                <td align="left" class=value1><input name="sta_wep_key_4" maxlength="26" value="" ></td>
              </tr>
            </tbody>
          </table>
          <!--///////////////////END WEP//////////////////////////////////-->
          <!--//////////////////////WPA//////////////////////////////////-->
          <table id="div_wpa_wifi" cellspacing=1 cellpadding=3 width=450 class=text1 border="1" bordercolor="#9BABBD" style="display:none">
            <tr>
              <td class=head  align="left" id="wifi_cipher_wifi">WPA/WPA2 算法:</td>
              <td align="left" class=value1><input name="sta_cipher" value="TKIP" type="radio" checked="checked">
                TKIP &nbsp;
                <input name="sta_cipher" value="AES" type="radio">
                AES &nbsp; </td>
            </tr>
            <tr>
              <td class=head  align="left" id="wifi_passphrase_wifi">密码:</td>
              <td align="left" class=value1><input name="sta_passphrase" size="28" maxlength="64" value="">
              </td>
            </tr>
          </table>
          <!--//////////////////////END WPA//////////////////////////////-->
          <table id="scan_button" width=450 >
            <tr>
              <td align="right">
			  <input name="wlSurveyBtn" id="wlSurveyBtn" type="button" class="button" onClick="SurveyClose()" value="开启扫描"/>
			  </td>
            </tr>
          </table>
		  <table width="450" cellSpacing=1 cellPadding=3 id="othercfg">
          <tbody>
            <tr align="center">
              <td> <input type="button"  style="{width:110px;}" onClick="toBack_step('C6','c_step');" value="Back" id="wifi_Back">
        &nbsp; &nbsp;
        <input type="button"  style="{width:110px;}" value="Next"id="wifi_Next" onClick="to_D_Step(6);">
              </td>
            </tr>
          </tbody>
        </table>
          <table cellspacing=1 cellpadding=3 width=520 class=text1 border="1" bordercolor="#9BABBD" id="wifiScanTable" style="display:none;margin-bottom:25px;margin-top:10px; margin-left:-40px" >
            <tr>
              <td width="10%" class=title><div align="center">选择</div></td>
              <td width="20%" class=title><div align="center">SSID</div></td>
              <td width="30%" class=title><div align="center">MAC地址</div></td>
              <td width="10%" class=title><div align="center">信道</div></td>
              <td width="15%" class=title><div align="center">安全</div></td>
              <td width="15%" class=title><div align="center">信号强度</div></td>
            </tr>
          </table>
           
        </form>
		</div>
  	  
	  
	  
<div id=D style="visibility:hidden;display:none; margin-left:100">
  <form name="lanCfg" id="lanCfg" >
    <table border="1" cellpadding="2" cellspacing="1" width="450" bordercolor="#9BABBD">
      <tbody>
        <tr>
          <td colspan=2 align="left" class=title id="lSetup">LAN Setting</td>
        </tr>
        <tr>
          <td class=head  align="left" id="lIp">IP Address</td>
          <td width="60%" align="left" class=value1><input name="lanIp" maxlength=15 value='<% getCfgGeneral(1,"lan_ipaddr"); %>' onFocus="recIpCfg()" onBlur="modDhcpCfg()" /></td>
        </tr>
        <tr>
          <td class=head  align="left" id="lNetmask">Subnet Mask</td>
          <td align="left" class=value1><input name="lanNetmask" maxlength=15 value="<% getCfgGeneral(1,"lan_netmask"); %>" /></td>
        </tr>
        <tr>
          <td class=head  align="left" id="lDhcpType">DHCP Type</td>
          <td align="left" class=value1><select name="lanDhcpType" size="1" onChange="dhcpTypeSwitch();">
              <option value="DISABLE" id="lDhcpTypeD">Disable</option>
              <option value="SERVER" id="lDhcpTypeS">Server</option>
            </select>
          </td>
        </tr>
        <tr id="start">
          <td class=head  align="left" id="lDhcpStart" >DHCP Start IP</td>
          <td align="left" class=value1><input name="dhcpStart" maxlength=15
             value="<% getCfgGeneral(1, "dhcpStart"); %>" /></td>
        </tr>
        <tr id="end">
          <td class=head  align="left" id="lDhcpEnd" >DHCP End IP</td>
          <td align="left" class=value1><input name="dhcpEnd" maxlength=15
             value="<% getCfgGeneral(1, "dhcpEnd"); %>" /></td>
        </tr>
      </tbody>
    </table>
    <table width="450" border="0" cellpadding="2" cellspacing="1">
      <tr >
        <td  align="center"><input type="button" style="{width:110px;}" onClick="DtoCX_step();" value="Back" id="LAN_Back">
          &nbsp; &nbsp;
          <input type="button"  style="{width:110px;}" value="Next"id="LAN_Next" onClick="to_WB_Step('D','WB');">
        </td>
      </tr>
    </table>
  </form>
</div>
<!-- step WB  -->
<div id=WB style="margin-left:100;visibility:hidden;">
  <form name="wireless_basic" id="wireless_basic">
    <table border="1" cellpadding="2" cellspacing="1" width="450" bordercolor="#9BABBD" >
      <tbody>
        <tr>
          <td class="title" colspan="2" id="basicWirelessNet">Wireless Network</td>
        </tr>
        <tr>
          <td class="head" id="basicRadioButton">Radio On/Off</td>
          <td class="value1"><select name="wireless_en" id="wireless_en"  size="1">
              <option value="0">Disable</option>
              <option value="1">Enable</option>
            </select></td>
        </tr>
        <tr>
          <td class="head" id="basicNetMode">Network Mode</td>
          <td class="value1"><select name="wirelessmode" id="wirelessmode" size="1" style="width:80px">
              <option value=0>11b/g </option>
              <option value=1>11b only</option>
              <option value=4>11g only</option>
              <!--<option value=2>11a only</option>
              <option value=8>11a/n mixed mode</option>-->
              <option value=9>11b/g/n </option>
            </select>
          </td>
        </tr>
      <input type="hidden" name="bssid_num" value="1">
      <tr>
        <td class="head" id="basicSSID">Network Name(SSID)</td>
        <td class="value1"><input type=text name="ssid" size=20 maxlength=32 value="<% getCfgGeneral(1, "SSID1"); %>"></td>
      </tr>
      </tbody>
      
    </table>
    <table width="450" border="0" cellpadding="2" cellspacing="1">
      <tr >
        <td  align="center"><input type="button" style="{width:110px;}" onClick="toBack_step('WB','D');" value="Back" id="WB_Back">
          &nbsp; &nbsp;
          <input type="button"  style="{width:110px;}" value="Next"id="WB_Next" onClick="to_WSecrity_Step('WB','WSecrity');">
        </td>
      </tr>
    </table>
  </form>
</div>
<div id=WSecrity style="visibility:hidden;display:none;margin-left:100">
  <!---->
  <BR>
  <form name="Cfg_wizard" id="Cfg_wizard" action="/goform/Cfg_wizard"  method="post" target="hiddenframeS">
    <!-- ---------------------  MBSSID Selection  --------------------- -->
    <table border="1" cellpadding="2" cellspacing="1" width="450" bordercolor="#9BABBD" style=" visibility:hidden;display:none">
      <tbody>
        <tr>
          <!--
<input type="hidden" name="ssidIndex">
<input type="hidden" name="security_mode">
<input type="hidden" name="security_shared_mode">
<input type="hidden" name="wep_default_key">
<input type="hidden" name="wep_key_1">
<input type="hidden" name="WEP1Select">
<input type="hidden" name="wep_key_2">
<input type="hidden" name="WEP2Select">
<input type="hidden" name="wep_key_3">
<input type="hidden" name="WEP3Select">
<input type="hidden" name="wep_key_4">
<input type="hidden" name="WEP4Select">
<input type="hidden" name="cipher">
<input type="hidden" name="passphrase">
<input type="hidden" name="keyRenewalInterval">
<input type="hidden" name="PMKCachePeriod">
<input type="hidden" name="PreAuthentication">-->
          <td class="title" colspan="2" id="secureSelectSSID">&nbsp;</td>
        </tr>
        <tr>
          <td class="head" id="secureSSIDChoice">&nbsp;</td>
          <td class="value1"><select name="ssidIndex" size="1" onChange="selectMBSSIDChanged()">
              <!-- ....Javascript will update options.... -->
            </select>
          </td>
        </tr>
      </tbody>
    </table>
    <br>
    <table border="1" bordercolor="#9babbd" cellpadding="3" cellspacing="1" hspace="2" vspace="2" width="450">
      <tbody>
        <tr>
          <input type="hidden" name="admuser">
          <input type="hidden" name="admpass" >
          <input type="hidden" name="ntpcurrenttime" >
          <input type="hidden" name="time_zone">
          <input type="hidden" name="NTPServerIP" >
          <input type="hidden" name="NTPSync">
          <input type="hidden" name="WAN_mode" id="WAN_mode" value="novalue">
          <input type="hidden" name="Z3g_code">
          <input type="hidden" name="Z3g_isp">
          <input type="hidden" name="Z3g_apn">
          <input type="hidden" name="Z3g_username">
          <input type="hidden" name="Z3g_passwd">
          <input type="hidden" name="pppoeUser">
          <input type="hidden" name="pppoePass">
          <input type="hidden" name="staticIp">
          <input type="hidden" name="staticNetmask">
          <input type="hidden" name="staticGateway">
          <input type="hidden" name="staticPriDns">
          <input type="hidden" name="pptpServer">
          <input type="hidden" name="pptpUser">
          <input type="hidden" name="pptpPass">
          <input type="hidden" name="l2tpServer">
          <input type="hidden" name="l2tpUser">
          <input type="hidden" name="l2tpPass">
		  
		<input type="hidden" name="sta_ssid"> 
		<input type="hidden" name="sta_mac">
		<input type="hidden" name="sta_channel">
		<input type="hidden" name="sta_security_mode">
		<input type="hidden" name="sta_wep_mode">   
		<!--//     a)value = "OPEN"  
		//     b)value ="SHARED"-->
		<input type="hidden" name="sta_wep_default_key">   
		<!--  a) value = "1" 
		b) value = "2" 
		c) value = "3" 
		d) value = "4"-->
		<input type="hidden" name="sta_wep_select">
		<!-- a)value = "1"    ASCII  
		b)value = "0"    Hex -->
		<input type="hidden" name="sta_wep_key_1"> 
		<input type="hidden" name="sta_wep_key_2">
		<input type="hidden" name="sta_wep_key_3">
		<input type="hidden" name="sta_wep_key_4">
		<input type="hidden" name="sta_cipher">
		<!--  a)value =0       TKIP
		b)value =1        AES-->
		<input type="hidden" name="sta_passphrase">
		  
		  
		  
          <input type="hidden" name="lanIp">
          <input type="hidden" name="lanNetmask">
          <input type="hidden" name="lanDhcpType">
          <input type="hidden" name="dhcpStart">
          <input type="hidden" name="dhcpEnd">
          <input type="hidden" name="wireless_en">
          <input type="hidden" name="wirelessmode">
          <input type="hidden" name="bssid_num">
          <input type="hidden" name="ssid">
          <td class="title" colspan="2"><span id="sp_title"></span>&nbsp;</td>
        </tr>
        <tr id="div_security_infra_mode" name="div_security_infra_mode">
          <td class="head" id="secureSecureMode">&nbsp;</td>
          <td class="value1"><select name="security_mode" id="security_mode" size="1"  style="width:110px" onChange="securityMode(1)">
              <!-- ....  in LoadFields .... -->
            </select>
          </td>
        </tr>
		
		<tr id="div_wep_mode" name="div_wep_mode" style="visibility: hidden;display:none"> 
			<td class="head" id="wep_mode_td">&nbsp;</td>
			<td class="value1">
				  <select name="wep_mode" id="wep_mode" size="1" >
					<option value=OPEN selected="selected">OPEN   </option>
					<option value=SHARED >SHARED  </option>
					<option value=WEPAUTO >AUTO   </option>
				  </select>
			</td>
		</tr>
        <tr id="div_security_shared_mode" name="div_security_shared_mode" style="visibility: hidden; display:none">
          <td class="head" id="secureEncrypType">&nbsp;</td>
          <td class="value1"><select name="security_shared_mode" id="security_shared_mode" size="1" onChange="securityMode(1)">
              <option value=WEP>WEP</option>
              <option value=None id="secureEncrypTypeNone">None</option>
            </select>
          </td>
        </tr>
      </tbody>
    </table>
    <br>
    <!-- WEP -->
    <table id="div_wep" name="div_wep" border="1" bordercolor="#9babbd" cellpadding="3" cellspacing="1" hspace="2" vspace="2" width="450" style="visibility:hidden;display:none;">
      <tbody>
        <tr>
          <td class="title" colspan="4" id="secureWEP">&nbsp;</td>
        </tr>
        <tr>
          <td class="head" colspan="2" id="secureWEPDefaultKey">Default Key</td>
          <td colspan="2" class="value1"><select name="wep_default_key" id="wep_default_key" size="1" onChange="setChange(1)">
              <option value="1" id="secureWEPDefaultKey1">Key 1</option>
              <option value="2" id="secureWEPDefaultKey2">Key 2</option>
              <option value="3" id="secureWEPDefaultKey3">Key 3</option>
              <option value="4" id="secureWEPDefaultKey4">Key 4</option>
            </select>
          </td>
        </tr>
        <tr>
          <td width="9%" rowspan="4" class="head1" id="secureWEPKey">WEP Keys</td>
          <td width="29%" class="head2" id="secureWEPKey1">WEP Key 1 :</td>
          <td width="43%" class="value1"><input name="wep_key_1" id="WEP1" maxlength="26" value="" onKeyUp="setChange(1)"></td>
          <td width="19%" class="value1"><select id="WEP1Select" name="WEP1Select" onChange="setChange(1)">
              <option value="1">ASCII</option>
              <option value="0">Hex</option>
            </select></td>
        </tr>
        <tr>
          <td class="head2" id="secureWEPKey2">WEP Key 2 : </td>
          <td class="value1"><input name="wep_key_2" id="WEP2" maxlength="26" value="" onKeyUp="setChange(1)"></td>
          <td class="value1"><select id="WEP2Select" name="WEP2Select" onChange="setChange(1)">
              <option value="1">ASCII</option>
              <option value="0">Hex</option>
            </select></td>
        </tr>
        <tr>
          <td class="head2" id="secureWEPKey3">WEP Key 3 : </td>
          <td class="value1"><input name="wep_key_3" id="WEP3" maxlength="26" value="" onKeyUp="setChange(1)"></td>
          <td class="value1"><select id="WEP3Select" name="WEP3Select" onChange="setChange(1)">
              <option value="1">ASCII</option>
              <option value="0">Hex</option>
            </select></td>
        </tr>
        <tr>
          <td class="head2" id="secureWEPKey4">WEP Key 4 : </td>
          <td class="value1"><input name="wep_key_4" id="WEP4" maxlength="26" value="" onKeyUp="setChange(1)"></td>
          <td class="value1"><select id="WEP4Select" name="WEP4Select" onChange="setChange(1)">
              <option value="1">ASCII</option>
              <option value="0">Hex</option>
            </select></td>
        </tr>
      </tbody>
    </table>
    <!-- WPA -->
    <table id="div_wpa" name="div_wpa" border="1" bordercolor="#9babbd" cellpadding="3" cellspacing="1" hspace="2" vspace="2" width="450" style="visibility: hidden;display:none">
      <tbody>
        <tr>
          <td class="title" colspan="2" id="secreWPA">WPA</td>
        </tr>
        <tr id="div_wpa_algorithms" name="div_wpa_algorithms" style="visibility:hidden;display:none;">
          <td class="head" id="secureWPAAlgorithm">WPA Algorithms</td>
          <td class="value1"><input name="cipher" id="cipher" value="0" type="radio" onClick="onWPAAlgorithmsClick(0)">
            TKIP &nbsp;
            <input name="cipher" id="cipher" value="1" type="radio" onClick="onWPAAlgorithmsClick(1)">
            AES &nbsp;
            <input name="cipher" id="cipher" value="2" type="radio" onClick="onWPAAlgorithmsClick(2)">
            AUTO &nbsp; </td>
        </tr>
        <tr id="wpa_passphrase" name="wpa_passphrase" style="visibility:hidden;">
          <td class="head" id="secureWPAPassPhrase">Pass Phrase</td>
          <td class="value1"><input name="passphrase" id="passphrase" size="28" maxlength="64" value="" onKeyUp="setChange(1)">
          </td>
        </tr>
        <tr id="wpa_key_renewal_interval" name="wpa_key_renewal_interval" style="visibility: hidden;">
          <td class="head" id="secureWPAKeyRenewInterval">Key Renewal Interval</td>
          <td class="value1"><input name="keyRenewalInterval" id="keyRenewalInterval" size="4" maxlength="4" value="3600" onKeyUp="setChange(1)">
            seconds </td>
        </tr>
        <tr id="wpa_PMK_Cache_Period" name="wpa_PMK_Cache_Period" style="visibility: hidden;">
          <td class="head" id="secureWPAPMKCachePeriod">PMK Cache Period</td>
          <td class="value1"><input name="PMKCachePeriod" id="PMKCachePeriod" size="4" maxlength="4" value="" onKeyUp="setChange(1)">
            minute </td>
        </tr>
        <tr id="wpa_preAuthentication" name="wpa_preAuthentication" style="visibility:hidden;display:none">
          <td class="head" id="secureWPAPreAuth">Pre-Authentication</td>
          <td class="value1"><input name="PreAuthentication" id="PreAuthentication" value="0" type="radio" onClick="onPreAuthenticationClick(0)">
            <font id="secureWPAPreAuthDisable">Disable &nbsp;</font>
            <input name="PreAuthentication" id="PreAuthentication" value="1" type="radio" onClick="onPreAuthenticationClick(1)">
            <font id="secureWPAPreAuthEnable">Enable &nbsp;</font> </td>
        </tr>
      </tbody>
    </table>
    <table width="450" border="0" cellpadding="2" cellspacing="1">
      <tr>
        <td  align="center"><input type="button" style="{width:110px;}" value="Back" id="Back_to_WB" onClick="toBack_step('WSecrity','WB');">
          &nbsp; &nbsp;
          <input type="button"  style="{width:110px;}" value="Finish"id="Finish" onClick="submit_apply();">
        </td>
      </tr>
    </table>
    <iframe name="hiddenframeS"  id="hiddenframeS" frameborder="0" border="0" style="display:none"></iframe>
  </form>
</div>
</BODY>
<script language="JavaScript" type="text/javascript">
Butterlate.setTextDomain("admin");
document.write('<div id="loading" style="display: none;z-index:9999"><br><br><br><div align="center">'+_("Submitting settings")+
'</div><br><br>&nbsp;&nbsp;' +_("Submitting be patient")+'</div>');
</script>
</HTML>
