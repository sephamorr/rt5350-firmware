 <HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=gb2312">
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
<meta http-equiv="Pragma" content="no-cache">
<meta content="MSHTML 6.00.2800.1106" name="GENERATOR">
<link href="../style/normal_ws.css" type="text/css" rel="stylesheet">
<link href="../style/style.css" type="text/css" rel="stylesheet">
<style type="text/css" >
 #upgradingDiv {
       position: absolute;
       left: 30%;
        top: 30%;
        margin-top: -50px;
       margin-left: -230px;
	   }
</style>
<script type="text/javascript" src="../lang/b28n.js"></script>
<script type="text/javascript" src="/js/global_js.js"></script>
<script language="JavaScript" type="text/javascript">
Butterlate.setTextDomain("admin");
var _singleton = 0;
var UpgradeConfirm = _("UpgradeConfirm");
function uploadFirmwareCheck()
{

	if(_singleton) return false;
	if(document.UploadFirmware.filename.value == ""){
		alert(_("Firmware Upgrade: Please specify a file."));
		return false;
	}
	//document.getElementById("loading").style.display="block";
	if( confirm(UpgradeConfirm) ){
	    
		   	 parent.menu.setUnderFirmwareUpload(1);
			
        	_singleton = 1;
		 	document.UploadFirmware.submit(); 
			
			document.getElementById("upgrad_table").style.display = "none"
			document.getElementById("upgradingDiv").style.display = "block";
			document.getElementById("upgradingDiv").style.display = "block";
            document.getElementById("upgradingNote").innerHTML = _("UploadingNote") ;
			
			start_upgrading();
			//top.view.location='../admin/upgrading.asp'
			   return true;
	  }

}


isEn= 0 ;
 if(_("lang")=="english") isEn =1 ;
 var lanip =  '<% getCfgGeneral(1,"lan_ipaddr"); %>' ;
var time = 0;
var time1 = 0;
var pc = 0;
var pc1 = 0;


function start_upgrading(f)
{   
   	if(isEn)	document.getElementById("lpc").style.backgroundColor="#FF0000"
	 else document.getElementById("lpc").style.backgroundColor="RGB(19,97,184)"
	 upgrade();
}

function upgrade()
{
	pc+=1; 
	if (pc > 100) 
	{ 
		document.getElementById("upgradeTab").disabled = true;
		document.getElementById("rebootTab").disabled = false;
		clearTimeout(time); 
		reboot();
		return;
	} 
	else
	{
		document.getElementById("upgradeTab").disabled = false;
		document.getElementById("rebootTab").disabled = true;
	}
	setWidth(self, "lpc", pc + "%");
	document.getElementById("percent").innerHTML =_("Uploading firmware") + pc + "%"
	time = setTimeout("upgrade()",950);
}

function setWidth(windowObj, el, newwidth)
{
    if (document.all)
	{
	  if (windowObj.document.all(el) )
        windowObj.document.all(el).style.width = newwidth ;
	}
	else if (document.getElementById)
	{
	  if (windowObj.document.getElementById(el) )
	    windowObj.document.getElementById(el).style.width = newwidth;
	}
}


function reboot()
{
	pc1+=1; 

	if (pc1 > 100) 
	{ 
	   //alert("upgrading over")
		window.top.location = "http://" + lanip;
		clearTimeout(time1); 
		
		return;
	} 
	setWidth(self, "lpcReboot", pc1 + "%");
	document.getElementById("perReboot").innerHTML=_("Reboot router") + pc1 + "%"
	time1 = setTimeout("reboot()",550);
}



function initTranslation()
{
	var e = document.getElementById("uploadTitle");
	e.innerHTML = _("upload title");
	e = document.getElementById("uploadIntroduction1");
	e.innerHTML = _("upload introduction1");
	e = document.getElementById("uploadIntroduction2");
	e.innerHTML = _("upload introduction2");
	e = document.getElementById("uploadFW");
	e.innerHTML = _("upload firmware");
	e = document.getElementById("uploadFWLocation");
	e.innerHTML = _("upload firmware location");
	e = document.getElementById("uploadFWApply");
	e.value = _("admin apply");
	e = document.getElementById("setmanImpSetCancel");
	e.value = _("admin cancel");
}
function pageInit(){
	initTranslation();
	all_page_init();
}
</script>
</head>
<body onLoad="pageInit()">
<h2 class="btnl" id="uploadTitle" >Upgrade Firmware</h2>
<table  width="100%" class="tintro" >
  <tbody>
     <tr>
      <td class="intro"> 
      <div><font id="uploadIntroduction1">Upgrade the router firmware to obtain new functionality. </font>
      <font id="uploadIntroduction2" color="#ff0000">It takes about 1 minute to upload &amp; upgrade flash and be patient please. Caution! A corrupted image will hang up the system.</font>
      </div> 
      </td>
      <td class ="image_col" align="right">
	     <script language="javascript" type="text/javascript" >
	     var lang =_("next");
	   if (lang == "Next") lang="en";
	  else lang="zh_cn";
	   var page_name = "softwaremanage"
	    help_display(page_name,lang);
	  </script>
	  </td>
    </tr>
  </tbody>
</table>
<!-- ----------------- Upload firmware Settings ----------------- -->
<hr>
<table width="571" class="body" id="upgrad_table">
  <tbody><tr><td width="563">
  <form method="post" name="UploadFirmware" action="/cgi-bin/upload.cgi" enctype="multipart/form-data" target="hiddenframe0">

<CENTER>
 <table border="1" cellpadding="2" cellspacing="1" width="450" bordercolor="#9BABBD">
<tbody><tr>
  <td class="title" colspan="2" id="uploadFW">Update Firmware</td>
</tr>
<tr>
  <td  class="head" id="uploadFWLocation">Location:</td>
	<td  class="value1" > <input type="file" name="filename" size="30" maxlength="256" lang="ts"> </td>
</tr>
</tbody></table>
<table  cellpadding="2" cellspacing="1" width="450" >
<tbody>
<tr align="center">
  <td colspan="2">
<input value="Apply" class="button" id="uploadFWApply" name="UploadFirmwareSubmit" type="button" onClick="return uploadFirmwareCheck();"onMouseOut="style.color='#000000'" onMouseOver="style.color='#FF9933'"> &nbsp; &nbsp;
<input type=reset class="button" value="Cancel" id="setmanImpSetCancel"onmouseout="style.color='#000000'" onMouseOver="style.color='#FF9933'">
 
</td>
</tr>
</tbody></table>
</center>
<iframe name="hiddenframe0"  id="hiddenframe0" frameborder="0" border="0" style="display:none"></iframe>
</form>
</td></tr></tbody></table>

<div  style="display:none; width:100%" id="upgradingDiv" >
<TABLE width=70%  border=0 cellPadding=5 cellSpacing=0>
  <TR>
    
    <TD vAlign=top align=center>
       
       
        <table   width=80% id="upgradeTab">
          <tr>
            <td id="percent" style="color:#FF0000"></td>
          </tr>
          <tr>
            <TD align="left" width="100%" colspan="2">
			<table width="100%" align="left" bgcolor="#ffffff" cellpadding="0" cellspacing="0" bordercolor="#999999" id="AutoNumber19" style="border-style: solid; border-width: 1px">
                <tr>
                  <td align=left><table id=lpc  height=20>
                      <tr>
                        <td></TD>
                      </TR>
                    </table></TD>
                </TR>
              </table></TD>
          </tr>
        </table>
        <table  width=80% id="rebootTab">
          <tr>
            <td id="perReboot" style="color:#FF0000"></td>
          </tr>
          <tr>
            <TD align="left" width="100%" colspan="2">
			<table width="100%" align="left" bgcolor="#ffffff" cellpadding="0" cellspacing="0" bordercolor="#999999" id="AutoNumber19" style="border-style: solid; border-width: 1px">
                <tr>
                  <td align=left>
				  <table id="lpcReboot" bgcolor="#CCCCCC" height=20 >
                      <tr>
                        <td></TD>
                      </TR>
                    </table></TD>
                </TR>
              </table></TD>
          </tr>
		  
        </table>
		
		<table width="80%" >
		<tr>
		<td >
		<p id="upgradingNote" style="color:#FF0000"></p>
		</td>
		</tr>
		</table>
      </TD>
  </TR>
</TABLE>
</div>
</body> 
<script language="JavaScript" type="text/javascript">document.write('<div id="loading"  style="display: none;"><br><br><br><div align="center">'+_("Uploading firmware")+
'</div><br><br>&nbsp;&nbsp;' +_("Uploading be patient")+'<span style="color:#ff0000" > '+_("Uploading be caution")+'</span></div>');
</script>
</html>
