<html>
<head>
<title>equipment manage</title>
<META http-equiv="Content-Type" content="text/html; charset=gb2312">
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
<!--meta http-equiv="Pragma" content="no-cache"-->
<meta content="MSHTML 6.00.2800.1106" name="GENERATOR">
<link href="../style/normal_ws.css" type="text/css" rel="stylesheet">
<link href="../style/style.css" type="text/css" rel="stylesheet">
<style type="text/css" >
 #reboot_div_ {
   position: absolute;
       left: 30%;
       top: 30%;
       margin-top: -50px;
       margin-left: -230px;
	   }
</style>

<script type="text/javascript" src="/lang/b28n.js"></script>
<script type="text/javascript" src="/js/global_js.js"></script>
<script language="JavaScript" type="text/javascript">

Butterlate.setTextDomain("admin");
var ntpcurrenttime = "<% getCurrentTimeASP(); %>"
ntpcurrenttime = ntpcurrenttime*1000;
parent.menu.setUnderFirmwareUpload(0);
var lanip =  '<% getCfgGeneral(1,"lan_ipaddr"); %>' ;
var _singleton = 0;
var time = 0;
var time1 = 0;
var pc = 0;
var pc1 = 0;
function reboot_func()
{ 
 document.getElementById("reboot_div_").style.display="block";
 document.getElementById("equip_tables").style.display="none";

	pc1+=1; 
	if (pc1 > 100) 
	{ 
	   //alert("upgrading over")
	    window.top.location = "http://" + lanip;
		clearTimeout(time1); 
		return;
	} 
	setWidth(self, "lpcReboot", pc1 + "%");
	document.getElementById("perReboot").innerHTML = _("Reboot router") + pc1 + "%"
	time1 = setTimeout("reboot_func()",540);
}

function loadDefault_func()
{ 
document.getElementById("reboot_div_").style.display="block";
document.getElementById("equip_tables").style.display="none";

	pc1+=1; 
	if (pc1 > 100) 
	{ 
	   //alert("upgrading over")
		window.top.location.reload();
		clearTimeout(time1); 
		return;
	} 
	setWidth(self, "lpcReboot", pc1 + "%");
	document.getElementById("perReboot").innerHTML = _("load_fact_default") + pc1 + "%"
	time1 = setTimeout("loadDefault_func()",580);
}

function setWidth(windowObj, el, newwidth)
{
    if (document.all)
	{
	  if (windowObj.document.all(el) )
        windowObj.document.all(el).style.width = newwidth ;
	}
	else if (document.getElementById)
	{
	  if (windowObj.document.getElementById(el) )
	    windowObj.document.getElementById(el).style.width = newwidth;
	}
}

function uploadFirmwareCheck()
{  
   	if(_singleton) return false;
	
       if(confirm(_("Do you ensure to reboot router."))){
	    
		 document.frmSetup.submit();	   
	     parent.menu.setUnderFirmwareUpload(1);
	//	 document.getElementById("time_zone").style.display="none";
		 _singleton = 1;
	//      document.getElementById("rebooting").style.display="block";
	
	  	document.getElementById("reboot_note").innerHTML=_("rebootting be patient")  
	     reboot_func();
	
	   //  setTimeout("top.location='../home.asp';",35000);
			
      //    return true;
		}
		else { 
		return false;
		}
	// return true;
}


function LoadFactDefaultCheck()
{
 
	if(_singleton) return false;
	
	if(confirm(_("Do you ensure to load factory default configuration."))){
			//document.getElementById("loading_default").style.display="block";
			document.LoadDefaultSettings.submit();
			 parent.menu.setUnderFirmwareUpload(1);
			 document.getElementById("time_zone").style.display="none";
			 _singleton = 1;
		 document.getElementById("reboot_note").innerHTML=_("load_fact_defaultNote")
			 loadDefault_func();
		    
		//	setTimeout("top.location='../home.asp';",39000);
			 
		}
		else {
			return false;
	   }
	  
}

var http_request = false;
function makeRequest(url, content) {
    http_request = creat_http_request();
    http_request.onreadystatechange = alertContents;
    http_request.open('POST', url, true);
    http_request.send(content);
}

function alertContents() {
    if (http_request.readyState == 4) {
        if (http_request.status == 200) {
			// refresh
		//	window.location.reload();
        } else {
            alert('There was a problem with the request.');
        }
    }
}

function atoi(str, num)
{
    i=1;
    if(num != 1 ){
        while (i != num && str.length != 0){
            if(str.charAt(0) == '.'){
                i++;
            }
            str = str.substring(1);
        }
        if(i != num )
            return -1;
    }

    for(i=0; i<str.length; i++){
        if(str.charAt(i) == '.'){
            str = str.substring(0, i);
            break;
        }
    }
    if(str.length == 0)
        return -1;
    return parseInt(str, 10);
}

function isAllNum(str)
{
	for (var i=0; i<str.length; i++){
	    if((str.charAt(i) >= '0' && str.charAt(i) <= '9') || (str.charAt(i) == '.' ))
			continue;
		return 0;
	}
	return 1;
}


function NTPFormCheck()
{  
 if(_singleton) return false;
	if( document.NTP.NTPServerIP.value != "" && 
		document.NTP.NTPSync.value == ""){
		alert(_("Please specify a value for the interval of synchroniztion."));
		return false;
	}
	if(isAllNum( document.NTP.NTPSync.value ) == 0){
		alert(_("Invalid NTP synchronization value."));
		return false;
	}
	if( atoi(document.NTP.NTPSync.value, 1) > 300){
		alert(_("The synchronization value is too big.(1~300)"));
		return false;
	}
	
	parent.menu.setUnderFirmwareUpload(1);
	setTimeout("top.view.location='../admin/equip_manage.asp';",2000);		
	return true;
}


function initTranslation()
{
	var e = document.getElementById("manLangSet");
	e.innerHTML = _("man language setting");
	e = document.getElementById("manSelectLang");
	e.innerHTML = _("man select language");
	e = document.getElementById("english");
	e.innerHTML = _("english");
	e = document.getElementById("chinese");
	e.innerHTML = _("chinese");
 	e = document.getElementById("manLangApply");
	e.value = _("lang apply");
	e = document.getElementById("manLangCancel");
	e.value = _("lang cancel");
	
	e = document.getElementById("setmanTitle");
	e.innerHTML = _("setman title");
	e = document.getElementById("setmanIntroduction");
	e.innerHTML = _("setman introduction");

		e = document.getElementById("manNTPSet");
	e.innerHTML = _("man ntp setting");
	e = document.getElementById("manNTPTimeZone");
	e.innerHTML = _("man ntp timezone");
	e = document.getElementById("manNTPMidIsland");
	e.innerHTML = _("man ntp mid island");
	e = document.getElementById("manNTPHawaii");
	e.innerHTML = _("man ntp hawaii");
	e = document.getElementById("manNTPAlaska");
	e.innerHTML = _("man ntp alaska");
	e = document.getElementById("manNTPPacific");
	e.innerHTML = _("man ntp pacific");
	e = document.getElementById("manNTPMountain");
	e.innerHTML = _("man ntp mountain");
	e = document.getElementById("manNTPArizona");
	e.innerHTML = _("man ntp arizona");
	e = document.getElementById("manNTPCentral");
	e.innerHTML = _("man ntp central");
	e = document.getElementById("manNTPMidUS");
	e.innerHTML = _("man ntp mid us");
	e = document.getElementById("manNTPIndianaEast");
	e.innerHTML = _("man ntp indiana east");
	e = document.getElementById("manNTPEastern");
	e.innerHTML = _("man ntp eastern");
	e = document.getElementById("manNTPAtlantic");
	e.innerHTML = _("man ntp atlantic");
	e = document.getElementById("manNTPBolivia");
	e.innerHTML = _("man ntp bolivia");
	e = document.getElementById("manNTPGuyana");
	e.innerHTML = _("man ntp guyana");
	e = document.getElementById("manNTPBrazilEast");
	e.innerHTML = _("man ntp brazil east");
	e = document.getElementById("manNTPMidAtlantic");
	e.innerHTML = _("man ntp mid atlantic");
	e = document.getElementById("manNTPAzoresIslands");
	e.innerHTML = _("man ntp azores islands");
	e = document.getElementById("manNTPGambia");
	e.innerHTML = _("man ntp gambia");
	e = document.getElementById("manNTPEngland");
	e.innerHTML = _("man ntp england");
	e = document.getElementById("manNTPCzechRepublic");
	e.innerHTML = _("man ntp czech republic");
	e = document.getElementById("manNTPGermany");
	e.innerHTML = _("man ntp germany");
	e = document.getElementById("manNTPTunisia");
	e.innerHTML = _("man ntp tunisia");
	e = document.getElementById("manNTPGreece");
	e.innerHTML = _("man ntp greece");
	e = document.getElementById("manNTPSouthAfrica");
	e.innerHTML = _("man ntp south africa");
	e = document.getElementById("manNTPIraq");
	e.innerHTML = _("man ntp iraq");
	e = document.getElementById("manNTPMoscowWinter");
	e.innerHTML = _("man ntp moscow winter");
	e = document.getElementById("manNTPArmenia");
	e.innerHTML = _("man ntp armenia");
	e = document.getElementById("manNTPPakistan");
	e.innerHTML = _("man ntp pakistan");
	e = document.getElementById("manNTPBangladesh");
	e.innerHTML = _("man ntp bangladesh");
	e = document.getElementById("manNTPThailand");
	e.innerHTML = _("man ntp thailand");
	e = document.getElementById("manNTPChinaCoast");
	e.innerHTML = _("man ntp chinacoast");
	e = document.getElementById("manNTPTaipei");
	e.innerHTML = _("man ntp taipei");
	e = document.getElementById("manNTPSingapore");
	e.innerHTML = _("man ntp singapore");
	e = document.getElementById("manNTPAustraliaWA");
	e.innerHTML = _("man ntp australia wa");
	e = document.getElementById("manNTPJapan");
	e.innerHTML = _("man ntp japan");
	e = document.getElementById("manNTPKorean");
	e.innerHTML = _("man ntp korean");
	e = document.getElementById("manNTPGuam");
	e.innerHTML = _("man ntp guam");
	e = document.getElementById("manNTPAustraliaQLD");
	e.innerHTML = _("man ntp australia qld");
	e = document.getElementById("manNTPSolomonIslands");
	e.innerHTML = _("man ntp solomon islands");
	e = document.getElementById("manNTPFiji");
	e.innerHTML = _("man ntp fiji");
	
 if(_("next")=="Next")
 {
    ntpcurrenttime = new Date(ntpcurrenttime).toString();
 }
 else
 { 
    ntpcurrenttime = new Date(ntpcurrenttime).toLocaleString();
  }	
	document.getElementById("ntpcurrenttime").innerHTML = ntpcurrenttime ;
	
	
	e = document.getElementById("manNTPNewZealand");
	e.innerHTML = _("man ntp newzealand");
	e = document.getElementById("manNTPServer");
	e.innerHTML = _("man ntp server");
	e = document.getElementById("manNTPSync");
	e.innerHTML = _("man ntp sync");
	e = document.getElementById("manNTPApply");
	e.value = _("admin apply");
	e = document.getElementById("manNTPCancel");
	e.value = _("admin cancel");
	
	e = document.getElementById("manNTPCurrentTime");
	e.innerHTML = _("man ntp current time");
	e = document.getElementById("manNTPSyncWithHost");
	e.value = _("man ntp sync with host");
	
	e = document.getElementById("setmanExpSet");
	e.innerHTML = _("setman export setting");
	e = document.getElementById("setmanExpSetButton");
	e.innerHTML = _("setman export setting button");
	e = document.getElementById("setmanExpSetExport");
	e.value = _("setman export setting export");

	e = document.getElementById("setmanImpSet");
	e.innerHTML = _("setman import setting");
	e = document.getElementById("setmanImpSetFileLocation");
	e.innerHTML = _("setman import setting file location");
	e = document.getElementById("setmanImpSetImport");
	e.value = _("setman import setting import");
	e = document.getElementById("setmanImpSetCancel");
	e.value = _("admin cancel");

	e = document.getElementById("setmanLoadFactDefault");
	e.innerHTML = _("setman load factory default");
	e = document.getElementById("setmanLoadFactDefaultButton");
	e.innerHTML = _("setman load factory default button");
	e = document.getElementById("setmanLoadDefault");
	e.value = _("setman load default");

	e = document.getElementById("rebootwebserver");
	e.innerHTML = _("reboot webserver");
	e = document.getElementById("rebootButton");
	e.innerHTML = _("reboot Button");
	e = document.getElementById("reboot");
	e.value = _("reboot");
	}

function PageInit()
{
  if (parent.menu.isFimwareUpload == 1){
    document.getElementById("loading_default").style.display="block";
   }

 var tz = '<% getCfgGeneral(1, "TZ"); %>';
 var ntp_server = '<% getCfgGeneral(1, "NTPServerIP"); %>';
	initTranslation();
	all_page_init();
	//var lang_Selection =document.getElementById("langSelection");
	//var Lang= '<% getCfgGeneral(1,"Language");%>'
	//if (Lang=="zhcn") lang_Selection.options.selectedIndex=1;    
     //   else lang_Selection.options.selectedIndex=0;
 var lang_element = document.getElementById("langSelection");
	
	if (document.cookie.length > 0) {
		var s = document.cookie.indexOf("language=");
		var e = document.cookie.indexOf(";", s);
		var lang = "en";
		var i;

		if (s != -1) {
			if (e == -1)
				lang = document.cookie.substring(s+9);
			else
				lang = document.cookie.substring(s+9, e);
		}
		for (i=0; i<lang_element.options.length; i++) {
			   if (lang == lang_element.options[i].value) {
				  lang_element.options.selectedIndex = i;
				  break;
			   }
		}
	}
	
	if (tz == "UCT_-11")
		document.NTP.time_zone.options.selectedIndex = 0;
	else if (tz == "UCT_-10")
		document.NTP.time_zone.options.selectedIndex = 1;
	else if (tz == "NAS_-09")
		document.NTP.time_zone.options.selectedIndex = 2;
	else if (tz == "PST_-08")
		document.NTP.time_zone.options.selectedIndex = 3;
	else if (tz == "MST_-07")
		document.NTP.time_zone.options.selectedIndex = 4;
	else if (tz == "MST_-07")
		document.NTP.time_zone.options.selectedIndex = 5;
	else if (tz == "CST_-06")
		document.NTP.time_zone.options.selectedIndex = 6;
	else if (tz == "UCT_-06")
		document.NTP.time_zone.options.selectedIndex = 7;
	else if (tz == "UCT_-05")
		document.NTP.time_zone.options.selectedIndex = 8;
	else if (tz == "EST_-05")
		document.NTP.time_zone.options.selectedIndex = 9;
	else if (tz == "AST_-04")
		document.NTP.time_zone.options.selectedIndex = 10;
	else if (tz == "UCT_-04")
		document.NTP.time_zone.options.selectedIndex = 11;
	else if (tz == "UCT_-03")
		document.NTP.time_zone.options.selectedIndex = 12;
	else if (tz == "EBS_-03")
		document.NTP.time_zone.options.selectedIndex = 13;
	else if (tz == "NOR_-02")
		document.NTP.time_zone.options.selectedIndex = 14;
	else if (tz == "EUT_-01")
		document.NTP.time_zone.options.selectedIndex = 15;
	else if (tz == "UCT_000")
		document.NTP.time_zone.options.selectedIndex = 16;
	else if (tz == "GMT_000")
		document.NTP.time_zone.options.selectedIndex = 17;
	else if (tz == "MET_001")
		document.NTP.time_zone.options.selectedIndex = 18;
	else if (tz == "MEZ_001")
		document.NTP.time_zone.options.selectedIndex = 19;
	else if (tz == "UCT_001")
		document.NTP.time_zone.options.selectedIndex = 20;
	else if (tz == "EET_002")
		document.NTP.time_zone.options.selectedIndex = 21;
	else if (tz == "SAS_002")
		document.NTP.time_zone.options.selectedIndex = 22;
	else if (tz == "IST_003")
		document.NTP.time_zone.options.selectedIndex = 23;
	else if (tz == "MSK_003")
		document.NTP.time_zone.options.selectedIndex = 24;
	else if (tz == "UCT_004")
		document.NTP.time_zone.options.selectedIndex = 25;
	else if (tz == "UCT_005")
		document.NTP.time_zone.options.selectedIndex = 26;
	else if (tz == "UCT_006")
		document.NTP.time_zone.options.selectedIndex = 27;
	else if (tz == "UCT_007")
		document.NTP.time_zone.options.selectedIndex = 28;
	else if (tz == "CST_008")
		document.NTP.time_zone.options.selectedIndex = 29;
	else if (tz == "CCT_008")
		document.NTP.time_zone.options.selectedIndex = 30;
	else if (tz == "SST_008")
		document.NTP.time_zone.options.selectedIndex = 31;
	else if (tz == "AWS_008")
		document.NTP.time_zone.options.selectedIndex = 32;
	else if (tz == "JST_009")
		document.NTP.time_zone.options.selectedIndex = 33;
	else if (tz == "KST_009")
		document.NTP.time_zone.options.selectedIndex = 34;
	else if (tz == "UCT_010")
		document.NTP.time_zone.options.selectedIndex = 35;
	else if (tz == "AES_010")
		document.NTP.time_zone.options.selectedIndex = 36;
	else if (tz == "UCT_011")
		document.NTP.time_zone.options.selectedIndex = 37;
	else if (tz == "UCT_012")
		document.NTP.time_zone.options.selectedIndex = 38;
	else if (tz == "NZS_012")
		document.NTP.time_zone.options.selectedIndex = 39;
}

function syncWithHost()
{   
if(_singleton) return false;
	var currentTime = new Date();
    //   alert(currentTime.string);
	var seconds = currentTime.getSeconds();
	var minutes = currentTime.getMinutes();
	var hours = currentTime.getHours();
	var month = currentTime.getMonth() + 1;
	var day = currentTime.getDate();
	var year = currentTime.getFullYear();

	var seconds_str = " ";
	var minutes_str = " ";
	var hours_str = " ";
	var month_str = " ";
	var day_str = " ";
	var year_str = " ";

	if(seconds < 10)
		seconds_str = "0" + seconds;
	else
		seconds_str = ""+seconds;

	if(minutes < 10)
		minutes_str = "0" + minutes;
	else
		minutes_str = ""+minutes;

	if(hours < 10)
		hours_str = "0" + hours;
	else
		hours_str = ""+hours;

	if(month < 10)
		month_str = "0" + month;
	else
		month_str = ""+month;

	if(day < 10)
		day_str = "0" + day;
	else
		day_str = day;

	var tmp = month_str + day_str + hours_str + minutes_str + year + "."+seconds;
	if(_("next")=="Next")	document.getElementById("ntpcurrenttime").innerHTML = currentTime.toString();
	else  document.getElementById("ntpcurrenttime").innerHTML = currentTime.toLocaleString();
		makeRequest("/goform/NTPSyncWithHost", tmp);
}


function validateForm(f){
        if(f.CCMD[0].checked) 
                f.CMD.value = '0';
        else if(f.CCMD[2].checked) 
       {        f.CMD.value = '2';
				f.enctype="multipart/form-data";
		//      f.action="AP61.CFG" ;alert("Select ");
				window.location="AP61.DAT";//alert("Select ");
				f.webpage.value ="sys_config.htm";
				return false;
        }
        else if(f.CCMD[3].checked) 
        {
                        f.CMD.value = '3';
                        /*f.files.value=="" ?alert("Select one file!"):"";
                        return f.files.value=="" ? false:true;*/
                        f.webpage.value ="sys_config.htm";
                      
                        return Reconfirm_config_load(f);
        }
        else if(f.CCMD[4].checked) 
        {
                        f.CMD.value = '4';
                        f.which_cgi.value ="write_flash";
                        f.webpage.value ="sys_config.htm";
                        f.submit();
                        return ReconfirmSave();
        }
        else
        {
                        f.CMD.value = '1';
                        /*f.files.value=="" ?alert("Select one file!"):"";*/
                        //alert( f.CMD.value);
                        f.submit();
                        return ReconfirmRestore();
        }
                //      alert( f.CMD.value);
        f.submit();
}

function setLanguage()
{
	document.cookie="language="+document.Lang.langSelection.value+"; path=/";
	//parent.menu.location.reload();
      top.location.reload();
	return true;
}


function AdmFormCheck()
{   
      
	if(_singleton) return false;
	if(document.ImportSettings.filename.value == ""){
		alert(_("Export Settings: Please specify a file."));
		return false;
	}
	document.getElementById("ExportSettingsdiv").style.display="block";
	parent.menu.setUnderFirmwareUpload(1);
	setTimeout("top.location='../home.asp';",29000);
	_singleton = 1;
	return true;
}

function reset_clicked()
 { 
   if(_singleton==1) return false;
  window.location.reload();
 }
 
 function submit_clicked()
 {
 if(_singleton==1) return false;
 return true;
 }
</script>
</head>
<body onLoad="PageInit()">
<h2 class="btnl" id="setmanTitle">&nbsp;</h2>
<table  width="100%" class="tintro" >
  <tbody>
    <tr>
      <td class="intro"   id="setmanIntroduction">&nbsp;</td>
      <td class ="image_col" align="right"><script language="javascript" type="text/javascript" >
	    var lang =_("next");
	   if (lang == "Next") lang="en";
	  else lang="zh_cn";
	   var page_name = "equipmanage"
	    help_display(page_name,lang);
	  </script>
      </td>
    </tr>
  </tbody>
</table>
<hr>
<table width="100%" class="body" id="equip_tables">
  <tbody>
    <tr>
      <td width="100%"><!-- ================= Langauge Settings ================= -->
        <center>
          <form method="post" name="Lang" action="/goform/setSysLang" style="display:none; visibility:hidden; margin:0" >
            <table width="86%" border="1" cellspacing="1" cellpadding="3" bordercolor="#9BABBD" style="display:none; visibility:hidden; margin:0">
              <tr>
                <td class="title" colspan="2" id="manLangSet">Language Settings</td>
              </tr>
              <tr>
                <td class="head" id="manSelectLang">Select Language</td>
                <td  class="value1" ><select name="langSelection" id="langSelection">
                    <option id="english" value="en">english</option>
                    <option id="chinese" value="zhcn">chinese</option>
                  </select>
                </td>
              </tr>
            </table>
            <table width="86%" border="0" cellpadding="2" cellspacing="1" >
              <tr align="center">
                <td><input type=submit style="{width:110px;}" value="Apply" id="manLangApply" onClick="return setLanguage()">
                  &nbsp; &nbsp;
                  <input type=reset  style="{width:110px;}" value="Cancel" id="manLangCancel" onClick="window.location.reload()">
                </td>
              </tr>
            </table>
          </form>
          <!-- ================= Export ================= -->
          <form method="get" name="ExportSettings" action="/cgi-bin/ExportSettings.sh">
            <table width="86%" border="1" cellspacing="1" cellpadding="3" bordercolor="#9BABBD">
              <tr>
                <td class="title" colspan="2" id="setmanExpSet"></td>
              </tr>
              <tr>
                <td  class="head" id="setmanExpSetButton"></td>
                <td  class="value1"><input value="Export" id="setmanExpSetExport" name="Export" style="{width:110px;}" type="submit" onClick="submit_clicked()"></td>
              </tr>
            </table>
          </form>
          <!-- ================= Import ================= -->
          <form method="post" name="ImportSettings" action="/cgi-bin/upload_settings.cgi" enctype="multipart/form-data" target="hiddenframex">
            <table width="86%" border="1" cellspacing="1" cellpadding="3" bordercolor="#9BABBD">
              <tr>
                <td class="title" colspan="2" id="setmanImpSet"></td>
              </tr>
              <tr>
                <td  class="head" id="setmanImpSetFileLocation"></td>
                <td  class="value1"><input type="File" name="filename" size="30" maxlength="256"></td>
              </tr>
            </table>
            <table width="86%"  cellpadding="2" cellspacing="1">
              <tr align="center">
                <td ><input type=submit style="{width:110px;}" value="Import" id="setmanImpSetImport" onClick="return AdmFormCheck()">
                  &nbsp; &nbsp;
                  <input type=reset  style="{width:110px;}" value="Cancel" id="setmanImpSetCancel">
                </td>
              </tr>
            </table>
            <iframe name="hiddenframex"  id="hiddenframex" frameborder="0" border="0" style="display:none"></iframe>
          </form>
          <!-- ================= Load FactoryDefaults  ================= -->
          <form method="post" id="LoadDefaultSettings" name="LoadDefaultSettings" action="/goform/LoadDefaultSettings"  target="hiddenframe0" onSubmit="return LoadFactDefaultCheck();">
            <table width="86%" border="1" cellspacing="1" cellpadding="3" bordercolor="#9BABBD">
              <tr>
                <td class="title" colspan="2" id="setmanLoadFactDefault"></td>
              </tr>
              <tr>
                <td  class="head" id="setmanLoadFactDefaultButton"></td>
                <td  class="value1"><input value="Load Default" id="setmanLoadDefault" name="LoadDefault" style="{width:110px;}" type="submit"  ></td>
              </tr>
            </table>
            <iframe name="hiddenframe0"  id="hiddenframe0" frameborder="0" border="0" style="display:none"></iframe>
          </form>
          <!-- ================= reboot  ================= -->
          <form name="frmSetup" id="frmSetup" method="post" action="/goform/setZreboot"  target="hiddenframe" onSubmit="return uploadFirmwareCheck();">
            <!--
				 input type=hidden name="which_cgi" value="sys_config">
                 <input type=hidden name="webpage" value="restoreProgress.htm">
                <INPUT type=hidden name=CMD  value="0"-->
            <table width="86%" border="1" cellspacing="1" cellpadding="3" bordercolor="#9BABBD">
              <tr>
                <td class="title" colspan="2" id="rebootwebserver"></td>
              </tr>
              <tr>
                <td  class="head" id="rebootButton"></td>
                <td  class="value1"><input value="reboot" id="reboot" name="reboot" style="{width:110px;}"  type="submit"></td>
              </tr>
            </table>
            <iframe name="hiddenframe"  id="hiddenframe" frameborder="0" border="0" style="display:none"></iframe>
          </form>
          <form method="post" name="NTP" action="/goform/NTP" target="hiddenframeS">
            <table width="86%" border="1" cellspacing="1" cellpadding="3" bordercolor="#9BABBD">
              <tbody>
                <tr>
                  <td class="title" colspan="2" id="manNTPSet"></td>
                </tr>
                <tr>
                  <td class="head"  id="manNTPCurrentTime"></td>
                  <td class="value1"><span id="ntpcurrenttime"></span>
                    <!--<input size="20" name="ntpcurrenttime" value="" type="text" readonly="1">-->
                    <input type="button" value="Sync with host" id="manNTPSyncWithHost" name="manNTPSyncWithHost" onClick="syncWithHost()"></td>
                </tr>
                <tr>
                  <td class="head" id="manNTPTimeZone">&nbsp;</td>
                  <td class="value1"><select name="time_zone" id="time_zone">
                      <option value="UCT_-11" id="manNTPMidIsland">(GMT-11:00) Midway Island, Samoa</option>
                      <option value="UCT_-10" id="manNTPHawaii">(GMT-10:00) Hawaii</option>
                      <option value="NAS_-09" id="manNTPAlaska">(GMT-09:00) Alaska</option>
                      <option value="PST_-08" id="manNTPPacific">(GMT-08:00) Pacific Time</option>
                      <option value="MST_-07" id="manNTPMountain">(GMT-07:00) Mountain Time</option>
                      <option value="MST_-07" id="manNTPArizona">(GMT-07:00) Arizona</option>
                      <option value="CST_-06" id="manNTPCentral">(GMT-06:00) Central Time</option>
                      <option value="UCT_-06" id="manNTPMidUS">(GMT-06:00) Middle America</option>
                      <option value="UCT_-05" id="manNTPIndianaEast">(GMT-05:00) Indiana East,Colombia</option>
                      <option value="EST_-05" id="manNTPEastern">(GMT-05:00) Eastern Time</option>
                      <option value="AST_-04" id="manNTPAtlantic">(GMT-04:00) Atlantic Time,Brazil West</option>
                      <option value="UCT_-04" id="manNTPBolivia">(GMT-04:00) Bolivia, Venezuela</option>
                      <option value="UCT_-03" id="manNTPGuyana">(GMT-03:00) Guyana</option>
                      <option value="EBS_-03" id="manNTPBrazilEast">(GMT-03:00) Brazil East,Greenland</option>
                      <option value="NOR_-02" id="manNTPMidAtlantic">(GMT-02:00) Mid-Atlantic</option>
                      <option value="EUT_-01" id="manNTPAzoresIslands">(GMT-01:00) Azores Islands</option>
                      <option value="UCT_000" id="manNTPGambia">(GMT) Gambia, Liberia, Morocco</option>
                      <option value="GMT_000" id="manNTPEngland">(GMT) England</option>
                      <option value="MET_001" id="manNTPCzechRepublic">(GMT+01:00) Czech Republic,N</option>
                      <option value="MEZ_001" id="manNTPGermany">(GMT+01:00) Germany</option>
                      <option value="UCT_001" id="manNTPTunisia">(GMT+01:00) Tunisia</option>
                      <option value="EET_002" id="manNTPGreece">(GMT+02:00) Greece, Ukraine,Turkey</option>
                      <option value="SAS_002" id="manNTPSouthAfrica">(GMT+02:00) South Africa</option>
                      <option value="IST_003" id="manNTPIraq">(GMT+03:00) Iraq, Jordan, Kuwait</option>
                      <option value="MSK_003" id="manNTPMoscowWinter">(GMT+03:00) Moscow Winter Time</option>
                      <option value="UCT_004" id="manNTPArmenia">(GMT+04:00) Armenia</option>
                      <option value="UCT_005" id="manNTPPakistan">(GMT+05:00) Pakistan, Russia</option>
                      <option value="UCT_006" id="manNTPBangladesh">(GMT+06:00) Bangladesh,Russia</option>
                      <option value="UCT_007" id="manNTPThailand">(GMT+07:00) Thailand, Russia</option>
                      <option value="CST_008" id="manNTPChinaCoast">(GMT+08:00) China Coast,Hong Kong</option>
                      <option value="CCT_008" id="manNTPTaipei">(GMT+08:00) Taipei</option>
                      <option value="SST_008" id="manNTPSingapore">(GMT+08:00) Singapore</option>
                      <option value="AWS_008" id="manNTPAustraliaWA">(GMT+08:00) Australia (WA)</option>
                      <option value="JST_009" id="manNTPJapan">(GMT+09:00) Japan, Korea</option>
                      <option value="KST_009" id="manNTPKorean">(GMT+09:00) Korean</option>
                      <option value="UCT_010" id="manNTPGuam">(GMT+10:00) Guam, Russia</option>
                      <option value="AES_010" id="manNTPAustraliaQLD">(GMT+10:00) Australia </option>
                      <option value="UCT_011" id="manNTPSolomonIslands">(GMT+11:00) Solomon Islands</option>
                      <option value="UCT_012" id="manNTPFiji">(GMT+12:00) Fiji</option>
                      <option value="NZS_012" id="manNTPNewZealand">(GMT+12:00) New Zealand</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td class="head" id="manNTPServer"></td>
                  <td class="value1"><input size="32" maxlength="64" name="NTPServerIP" value="<% getCfgGeneral(1, "NTPServerIP"); %>" type="text">
                    <br>
                    &nbsp;&nbsp;<font color="#808080">ex:&nbsp;time.nist.gov</font> <br>
                    &nbsp;&nbsp;<font color="#808080">&nbsp;&nbsp;&nbsp;&nbsp;ntp0.broad.mit.edu</font> <br>
                    &nbsp;&nbsp;<font color="#808080">&nbsp;&nbsp;&nbsp;&nbsp;time.stdtime.gov.tw</font> </td>
                </tr>
                <tr>
                  <td class="head" id="manNTPSync">&nbsp;</td>
                  <td class="value1"><input size="4" maxlength="3" name="NTPSync" value="<% getCfgGeneral(1, "NTPSync"); %>" type="text">
                  </td>
                </tr>
              </tbody>
            </table>
            <table width="86%" border="0" cellpadding="2" cellspacing="1">
              <tr align="center">
                <td ><input type="submit" style="{width:120px;}" value="Apply" id="manNTPApply" onClick="return NTPFormCheck()">
                  &nbsp; &nbsp;
                  <input type=reset  style="{width:120px;}" value="Cancel"id="manNTPCancel" onClick="reset_clicked()">
                </td>
              </tr>
            </table>
            <iframe name="hiddenframeS"  id="hiddenframeS" frameborder="0" border="0" style="display:none"></iframe>
          </form>
          <!--table width="85%" bordercolor="#9BABBD">
<tbody><tr><td >
<p class="value1">
Reboot Router:Click submission button��system will reboot <br>
Restore Factory Default Settings :Click submission button��system will reboot and restore factory setup <br>
Backup configuration Settings :Click the right key to save the configuration information on PC <br>
Restore configuration Settings :It is possible to restore the previously backup configuration information file and boot into the wireless router,please choose the previously backup file in advance. 
<br>
Notice: If the operation system run abnormally,please restore factory setup. 
</table-->
        </center></td>
    </tr>
  </tbody>
</table>
<script language="JavaScript" type="text/javascript">document.write('<div id="rebooting" style="display:none;z-index:9999"><br><br><br><div align="center">'+_("Reboot router")+
'</div><br><br>&nbsp;&nbsp;' +_("rebootting be patient")+'</div>');
document.write('<div id="loading_default" style="display:none;z-index:9999"><br><br><br><div align="center">'+_("load_fact_default")+
'</div><br><br> &nbsp;&nbsp;' +_("load_fact_default be patient")+_("load_fact_default be caution")+'</div>');
document.write('<div id="ExportSettingsdiv" style="display:none;z-index:9999"><br><br><br><div align="center">'+_("Export Settings")+
'</div><br><br> &nbsp;&nbsp;' +_("Export Settings be patient")+_("Export Settings be caution")+'</div>');
</script>
<!--<iframe name="hiddenframett" src="../sssss.asp"    frameborder="0" border="0" style=" display:none;">

</iframe>-->
<div id="reboot_div_" style="display:none;width:100%">
<TABLE width="70%" border=0 cellPadding=5 cellSpacing=0>
  <TR>
    <td  vAlign=top align=center> 
   <table class=space width="80%" id="rebootTab">
    <tr>
      <td id="perReboot" style="color:#FF0000"></td>
    </tr>
    <tr>
      <TD align="left" width="100%" colspan="2">
	  <table width="100%" align="left" bgcolor="#ffffff" cellpadding="0" cellspacing="0" bordercolor="#999999" id="AutoNumber19" style="border-style:solid; border-width:1px">
          <tr>
            <td align=left>
			<table id="lpcReboot" bgcolor="RGB(19,97,184)" height=20 >
                <tr>
                  <td> </TD>
                </TR>
              </table>
			</TD>
          </TR>
	  </table></TD>
    </tr>
	
	 <tr><td ><p id="reboot_note" style="color:#FF0000"></p></td></tr>
  </table>
  
  </td></TR></TABLE>
</div>
</body>
</html>
