<html>
<head>
<title>System Log</title>
<META http-equiv="Content-Type" content="text/html; charset=gb2312">
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
<meta http-equiv="Pragma" content="no-cache">
<meta content="MSHTML 6.00.2800.1106" name="GENERATOR">
<link href="../style/normal_ws.css" type="text/css" rel="stylesheet">
<link href="../style/style.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="/lang/b28n.js"></script>
<script type="text/javascript" src="/js/global_js.js"></script>
<script language="JavaScript" type="text/javascript">
Butterlate.setTextDomain("admin");
 parent.menu.setUnderFirmwareUpload(0);
   var  _singleton = 0;
function initTranslation()
{
	var e = document.getElementById("syslogTitle");
	e.innerHTML = _("syslog title");
	e = document.getElementById("syslogIntroduction");
	e.innerHTML = _("syslog introduction");
	
	e = document.getElementById("Remote_Log_Manage");
	e.innerHTML = _("Remote Log Manage");
	e = document.getElementById("remotelog_e_d");
	e.innerHTML = _("remotelog ed");
	e = document.getElementById("remote_disable");
	e.innerHTML = _("admin disable");
	e = document.getElementById("remote_enable");
	e.innerHTML = _("admin enable");
	e = document.getElementById("remote_ip");
	e.innerHTML = _("remote ip");
	e = document.getElementById("Apply");
	e.value = _("admin apply");
	e = document.getElementById("Cancel");
	e.value = _("admin cancel");
	e = document.getElementById("syslogSysLog");
	e.innerHTML = _("syslog system log");
	e = document.getElementById("syslogSysLogClear");
	e.value = _("syslog clear");
	e = document.getElementById("syslogSysLogRefresh");
	e.value = _("syslog refresh");
	
}

var http_request = false;
function makeRequest(url, content) {
    http_request = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_request = new XMLHttpRequest();
        if (http_request.overrideMimeType) {
            http_request.overrideMimeType('text/xml');
        }
    } else if (window.ActiveXObject) { // IE
        try {
            http_request = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
            http_request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
        }
    }
    if (!http_request) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    http_request.onreadystatechange = alertContents;
    http_request.open('POST', url, true);
    http_request.send(content);
}

function alertContents() 
{
    if (http_request.readyState == 4) {
        if (http_request.status == 200) {
			uploadLogField(http_request.responseText);
        } else {
            alert('There was a problem with the request.');
        }
    }
}

function uploadLogField(str)
{
	if(str == "-1"){
		document.getElementById("syslog").value =
		"Not support.\n(Busybox->\n  System Logging Utilitie ->\n    syslogd\n    Circular Buffer\n    logread"
	}else
		document.getElementById("syslog").value = str;
}

function updateLog()
{
	makeRequest("/goform/syslog", "n/a", false);
}

function clearlogclick()
{
	document.getElementById("syslog").value = "";
	return true;
}

function refreshlogclick()
{
	updateLog();
	return true;
}
function atoi(str, num)
{
	i = 1;
	if (num != 1) {
		while (i != num && str.length != 0) {
			if (str.charAt(0) == '.') {
				i++;
			}
			str = str.substring(1);
		}
		if (i != num)
			return -1;
	}

	for (i=0; i<str.length; i++) {
		if (str.charAt(i) == '.') {
			str = str.substring(0, i);
			break;
		}
	}
	if (str.length == 0)
		return -1;
	return parseInt(str, 10);
}

function checkRange(str, num, min, max)
{
	d = atoi(str, num);
	if (d > max || d < min)
		return false;
	return true;
}

function isAllNum(str)
{
	for (var i=0; i<str.length; i++) {
		if ((str.charAt(i) >= '0' && str.charAt(i) <= '9') || (str.charAt(i) == '.' ))
			continue;
		return 0;
	}
	return 1;
}

function checkIpAddr(field, ismask)
{
	if (field.value == "") {
		alert(_("Error.IP address is empty."));
		field.value = field.defaultValue;
		field.focus();
		return false;
	}

	if (isAllNum(field.value) == 0) {
		alert(_('It should be a [0-9] number.'));
		field.value = field.defaultValue;
		field.focus();
		return false;
	}

	if (ismask) {
		if ((!checkRange(field.value, 1, 0, 256)) ||
				(!checkRange(field.value, 2, 0, 256)) ||
				(!checkRange(field.value, 3, 0, 256)) ||
				(!checkRange(field.value, 4, 0, 256)))
		{
			alert(_('IP adress format error.'));
			field.value = field.defaultValue;
			field.focus();
			return false;
		}
	}
	else {
		if ((!checkRange(field.value, 1, 0, 255)) ||
				(!checkRange(field.value, 2, 0, 255)) ||
				(!checkRange(field.value, 3, 0, 255)) ||
				(!checkRange(field.value, 4, 1, 254)))
		{
			alert(_('IP adress format error.'));
			field.value = field.defaultValue;
			field.focus();
			return false;
		}
	}
	return true;
}

function CheckValue()
{ 
  if(_singleton==1) return false; 
	if( ! document.getElementById("remote").options.selectedIndex )
	{  parent.menu.setUnderFirmwareUpload(1);
     _singleton = 1;
	setTimeout("top.view.location='../admin/syslog.asp';",2000);
	return true;
	}
	if (!checkIpAddr(document.setZlog.ip, false)) 	return false;
	parent.menu.setUnderFirmwareUpload(1);
     _singleton = 1;
	  setTimeout("top.view.location='../admin/syslog.asp';",2000);
	return true;
}

function style_display_on()
	{
	if (window.ActiveXObject)
		{ // IE
			return "block";
		}
     	else if (window.XMLHttpRequest)
		{ // Mozilla, Safari,...
			return "table-row";
		}
	}

function remotelogchange()
{   
	var remote_select=document.getElementById("remote");
	document.getElementById("ip_text").style.display = "none";
	
 	if(remote_select.options.selectedIndex == 1){
	document.getElementById("ip_text").style.display = style_display_on();
	
	}
 	
 }

function pageInit()
{
	initTranslation();
	all_page_init();
	updateLog();
 	var remotelog = '<% getCfgGeneral(1, "syslogc.remote"); %>';
	var remoteSel = document.getElementById("remote");
	if (remotelog == "enable")	remoteSel.options.selectedIndex = 1;
	else remoteSel.options.selectedIndex = 0;
	remotelogchange();
}
</script>
</head>
<body onLoad="pageInit()">
<h2 id="syslogTitle" class="btnl">System Log</h2>
<table  width="100%" class="tintro" >
  <tbody>
    <tr>
      <td class="intro" id="syslogIntroduction" >Syslog: </td>
      <td class ="image_col" align="right">
           <script language="javascript" type="text/javascript" >
	    var lang =_("next");
	   if (lang == "Next") lang="en";
	  else lang="zh_cn";
	   var page_name = "syslogmanage"
	    help_display(page_name,lang);
	  </script>
	  	</td>
    </tr>
  </tbody>
</table>
<hr>
<table class="body" >
      <tbody>
        <tr>
          <td width="563"><center>
<form method="post" name ="setZlog" action="/goform/setZlog" onSubmit="return CheckValue()" target="hiddenframe0">
  <table  cellpadding=3 cellspacing=1 class=text1 width="450"  border="1" bordercolor="#9BABBD">
    <tbody>
      <tr>
        <td class="title" id="Remote_Log_Manage" colspan="2">Remote Log Manage</td>
      </tr>
      <tr>
        <td class="head" id="remotelog_e_d">remote log </td>
        <td class="value1"><select name="remote" id="remote" size="1"onChange="remotelogchange()">
            <option value="disable" id="remote_disable">Disable&nbsp;</option>
            <option value="enable" id="remote_enable">Enable</option>
          </select></td>
      </tr>
      <tr style="display:none" id="ip_text">
        <td class="head" id="remote_ip">remote ip</td>
        <td class="value1"><input type="text" size="18"maxlength="15" name="ip" id="ip" value="<% getCfgGeneral(1, "syslogc.ip"); %>"/>
        </td>
      </tr>
    </tbody>
  </table>
  <table width="450" cellpadding="2" cellspacing="1">
    <tr align="center">
      <td width="450"><input type=submit style="{width:110px;}" value="Apply" id="Apply">
        &nbsp;&nbsp;
 <input type=reset  style="{width:110px;}" value="Cancel" id="Cancel" onClick="reset_clicked()">
      </td>
    </tr>
  </table>
  <iframe name="hiddenframe0"  id="hiddenframe0" frameborder="0" border="0" style="display:none"></iframe>
</form>
<!-- ================= System log ================= -->
<table  cellpadding=3 cellspacing=1 class=text1 width="700"  border="1" bordercolor="#9BABBD" style="margin-left:20">
  <tbody>
    <tr>
      <td class="title" id="syslogSysLog">System Log: </td>
    </tr>
    <tr>
      <td><textarea style=font-size:9pt name="syslog" id="syslog" cols="114" rows="20" wrap="off" readonly="1">
					</textarea></td>
    </tr>
  </tbody>
</table>
<table>
  <tr>
    <td align="center"  cellpadding=3 cellspacing=1 class=text1 width="885"  border="1" bordercolor="#9BABBD"><form method="post" name ="SubmitClearLog" action="/goform/clearlog">
        <input type="button" value="Refresh" id="syslogSysLogRefresh" name="refreshlog" onClick="refreshlogclick();">
        &nbsp;&nbsp;
        <input type="submit" value="Clear" id="syslogSysLogClear" name="clearlog" onClick="clearlogclick();">
      </form></td>
</tr>
</table>
</center></td></tr></tbody></table>
</body>
</html>
