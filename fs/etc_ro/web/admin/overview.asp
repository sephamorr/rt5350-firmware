<html>
<head>
<title>System Log</title>
<META http-equiv="Content-Type" content="text/html; charset=gb2312">
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
<meta http-equiv="Pragma" content="no-cache">
<meta content="MSHTML 6.00.2800.1106" name="GENERATOR">
<link href="../style/normal_ws.css" type="text/css" rel="stylesheet">
<link href="../style/style.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="/lang/b28n.js"></script>
<script type="text/javascript" src="/js/global_js.js"></script>
<script language="JavaScript" type="text/javascript">
Butterlate.setTextDomain("admin");

function initTranslation()
{
	
	var e = document.getElementById("ovIntroduction");
	e.innerHTML = _("overview introduction");
	var e = document.getElementById("ovSelectLang");
	e.innerHTML = _("overview select language");
	
	var e = document.getElementById("Select_language");
	e.innerHTML = _("Select language versions");
	var e = document.getElementById("Language_versions");
	e.innerHTML = _("Language versions");
	
	e = document.getElementById("ovLangApply");
	e.value = _("admin apply");
//	e = document.getElementById("ovLangCancel");
//	e.value = _("admin cancel");
	
		

//	e = document.getElementById("ovStatus");
//	e.innerHTML = _("overview status link");
//	e = document.getElementById("ovStatistic");
//	e.innerHTML = _("overview statistic link");
//	e = document.getElementById("ovManagement");
//	e.innerHTML = _("overview management link");
}

function initValue() {
	var lang_element = document.getElementById("langSelection");
	var lang_en = "<% getLangBuilt("en"); %>";
	var lang_zhtw = "<% getLangBuilt("zhtw"); %>";
	var lang_zhcn = "<% getLangBuilt("zhcn"); %>";

	initTranslation();
//	initTranslation();
   all_page_init();
	//lang_element.options.length = 0;
	if (lang_en == "1")
		lang_element.options[lang_element.length] = new Option('English', 'en');
	if (lang_zhtw == "1")
		lang_element.options[lang_element.length] = new Option('Traditional Chinese', 'zhtw');
	if (lang_zhcn == "1")
		lang_element.options[lang_element.length] = new Option('Simple Chinese', 'zhcn');

	if (document.cookie.length > 0) {
		var s = document.cookie.indexOf("language=");
		var e = document.cookie.indexOf(";", s);
		var lang = "en";
		var i;

		if (s != -1) {
			if (e == -1)
				lang = document.cookie.substring(s+9);
			else
				lang = document.cookie.substring(s+9, e);
		}
		for (i=0; i<lang_element.options.length; i++) {
			if (lang == lang_element.options[i].value) {
				lang_element.options.selectedIndex = i;
				break;
			}
		}
	}
}

function setLanguage()
{
	document.cookie="language="+document.Lang.langSelection.value+"; path=/";
	parent.menu.location.reload();
        location.reload();
	return true;
}
</script>
</HEAD>

<BODY onLoad="initValue()">
<h2 class="btnl" id="ovIntroduction">Langauge Settings</h2>
<table  width="100%" class="tintro" >
  <tbody>
     <tr>
      <td class="intro"> 
      <div><font id="ovSelectLang">Select langauge version. </font>
      </div> 
      </td>
      <td class ="image_col" align="right">
	     
	  </td>
    </tr>
  </tbody>
</table>

<hr>
<table class="body" >
      <tbody>
        <tr>
          <td width="563"><center>
<form method="post" name="Lang" action="/goform/setSysLang" target="hiddenframe0">
  <table  cellpadding=3 cellspacing=1 class=text1 width="450"  border="1" bordercolor="#9BABBD">
    <tbody>
      <tr>
        <td class="title" id="Select_language" colspan="2">Select language versions</td>
      </tr>
      <tr>
        <td class="head" id="Language_versions"> Language versions</td>
        <td class="value1"><select name="langSelection" id="langSelection">

</select>&nbsp;&nbsp;</td>
      </tr>
    </tbody>
  </table>
  <table width="450" cellpadding="2" cellspacing="1">
    <tr align="center">
      <td width="450"><input type=submit style="{width:110px;}" value="Apply" id="ovLangApply" onClick="return setLanguage()">
        &nbsp;&nbsp;
      </td>
    </tr>
  </table>
  <iframe name="hiddenframe0"  id="hiddenframe0" frameborder="0" border="0" style="display:none"></iframe>
</form>

</center></td></tr></tbody></table>


<!--blockquote><fieldset><p>
<a href="/adm/status.asp" id="ovStatus">Status</a><br />
<a href="/adm/statistic.asp" id="ovStatistic">Statistic</a><br />
<a href="/adm/management.asp" id="ovManagement">Management</a><br />
<br />
</p></fieldset></blockquote-->


</BODY>
</HTML>
