
/****************************************************************************************************



*****************************************************************************************************/
var Butterlate = new Butterlation();
window._ = function(key) { return Butterlate.gettext(key); };
window.__ = function(key,replacements) { return Butterlate.vgettext(key,replacements); };

function Butterlation() {
  this.dict = new ButterDictionary();
  this.getLang = function() {
    var one, two, end;
  
    if((one=document.cookie.indexOf("language"))==-1) {
      //return ((navigator.language) ? navigator.language : navigator.browserLanguage).substring(0,2);  
     //var SysLang= '<% getCfgGeneral(1,"Language");%>';
	//  if (SysLang !="en" && Syslang !="zhcn") 
	  	return "en" ;
   //     else  return Syslang;
           
    }
    end = (document.cookie.indexOf(';',one)!=-1) ? document.cookie.indexOf(';',one) : document.cookie.length;
    return unescape(document.cookie.substring(one+9,end));
  };
  this.lang = this.getLang();
  this.setTextDomain = function(domain) { 
  	this.po=window.location.protocol+"//"+window.location.host+"/lang/"+this.lang+"/"+domain+".xml"; 
  	//this.po="../lang/"+this.lang+"/"+domain+".xml"; 
  	this.initializeDictionary(); }
  this.initializeDictionary = function() {
    var request;
    try { request = new XMLHttpRequest(); } catch(e1) {
      try { request = new ActiveXObject("Msxml2.XMLHTTP"); } catch(e2) {
        try { request = new ActiveXObject("Microsoft.XMLHTTP"); } catch(e3) { return; }}};
    request.open("GET",this.po,false); 
    request.send(null);
    if(request.status==200) { 
    
 //  alert(this.po);
 //  window.open(this.po);
	  var pos = request.responseXML.documentElement.getElementsByTagName("message");
	  
	 
      for(var i=0; i<pos.length; i++) this.dict.set(pos[i].getAttribute("msgid"),pos[i].getAttribute("msgstr"));
    }
  };
  this.gettext = function(key) { return this.dict.get(key); };
  this.vgettext = function(key,replacements) { 
    var nkey=this.gettext(key); var index; var count=0;
    if(replacements.length==0) return nkey;
    while((index=nkey.indexOf('%s'))!=-1) { 
      nkey=nkey.substring(0,index)+replacements[count]+nkey.substring(index+2,nkey.length); 
      count = ((count+1)==replacements.length) ? count : (count+1);
    }
    return nkey;
  };
}

function ButterDictionary() {
  this.keys = new Array();
  this.values = new Array();
  this.set = function(key,value) { 
    var index = this.getIndex(key);
    if(index==-1) { this.keys.push(key); this.values.push(value); }
    else this.values[index]=value;
  };
  this.get = function(key) {
    var index;
    if((index=this.getIndex(key))!=-1) return this.values[index];
    return key;
  };
  this.getIndex = function(key) {
    var index=-1;
    for(var i=0; i<this.keys.length; i++) if(this.keys[i]==key) { index=i; break; }
    return index;
  };
  this.keyExists = function(key) { return (this.getIndex(key)!=1); };
  this.deleteKey = function(key) { 
    var index = getIndex(key);
    if(index!=-1) { this.keys.splice(index,1); this.values.splice(index,1); }
  };
}

function have_submited()
{
var isie = (document.all) ? true:false; 
 function mouseClicked(e)
	{  
	//if(window.event.button==1) return false;  
	   var key; 
			var ev; 
		if(isie){ 
			key = window.event.button; 
			ev = window.event; 
			}else{ 
			key = e.which; 
			ev = e; 
		}
		if(key==1) alert("left click");
		return false;
	}  
 function somekeyClicked(e){ 
	 //if(event.keyCode==13) return false;
			 
			var key; 
			var ev; 
		if(isie){ 
			key = window.event.keyCode; 
			ev = window.event; 
			}else{ 
			key = e.which; 
			ev = e; 
		}
	 if(key==13) alert("enter press");
	 return false;
	}
document.onmousedown = mouseClicked; 
document.onkeydown = somekeyClicked; 
}

