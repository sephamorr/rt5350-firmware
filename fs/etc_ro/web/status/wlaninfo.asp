 
<HEAD><TITLE>equipment infomation</TITLE>
<META http-equiv="Content-Type" content="text/html; charset=gb2312">
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
<meta http-equiv="Pragma" content="no-cache">
<meta content="MSHTML 6.00.2800.1106" name="GENERATOR">
<link href="../style/normal_ws.css" type="text/css" rel="stylesheet">
<link href="../style/style.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../lang/b28n.js"></script>
<script type="text/javascript" src="../js/global_js.js"></script>
<script language="JavaScript" type="text/javascript">
var start_time= new Date().getTime();
start_time=parseInt(start_time/1000);
Butterlate.setTextDomain("admin");
function initTranslation(){
var e = document.getElementById("wlanpt");	
e.innerHTML = _("wlan Title");
e = document.getElementById("wlanintro");	
e.innerHTML = _("wlan introduction");
e = document.getElementById("Wlaninformation");	
e.innerHTML = _("Wlan information");

e = document.getElementById("wlan_status");	
e.innerHTML = _("wlan status");
e = document.getElementById("channel");	
e.innerHTML = _("channel");
e = document.getElementById("SSID_name");	
e.innerHTML = _("ssid name");
e = document.getElementById("securemode");	
e.innerHTML = _("secure mode");


e = document.getElementById("stalistWirelessNet");	
e.innerHTML = _("stalist wireless network");
e = document.getElementById("stalistMacAddr");
e.innerHTML = _("stalist macaddr");
e = document.getElementById("wirelessmode");	
e.innerHTML = _("net mode");
}

function init()
{   
    all_page_init();
	initTranslation();
	var radio_off = '<% getCfgZero(1, "RadioOff"); %>';
		if (radio_off*1==1)
		{
		document.getElementById("wlan_status1").innerHTML= _("WLAN OFF");
		}
		else
		{
		document.getElementById("wlan_status1").innerHTML= _("WLAN ON");
		}
}
</script>

<BODY onLoad="init();">
<h2 class="btnl" id="wlanpt">&nbsp;</h2>

<table  width="100%" class="tintro" >
  <tbody>
     <tr>
      <td class="intro" id="wlanintro" >&nbsp;</td>
      <td class ="image_col" align="right">
	        <script language="javascript" type="text/javascript" >
	     var lang =_("next");
	   if (lang == "Next") lang="en";
	  else lang="zh_cn";
	   var page_name = "wlaninfo"
	    help_display(page_name,lang);
	  </script>
	  </td>
    </tr>
  </tbody>
</table>
<hr>
<table width="571" class="body" bordercolor="#9BABBD">
 <tr>
 <td width="563"> 
  <CENTER>
<table width=450 border="1" cellPadding=3 cellSpacing=1 class=text1 bordercolor="#9BABBD" >
  
		<tr>
		  <td colspan=2 align="left" class=title id="Wlaninformation">&nbsp;</td>
		</tr>
    	<tr>
		  <td class=head  align="left"id="wlan_status">&nbsp;</td>
		  <td class=value1><span id="wlan_status1"></span>&nbsp;	 </td>	
	</tr>
    	<tr>
		  <td class=head  align="left"id="channel">&nbsp;</td>
		  <td class=value1><% getWlanChannel();%>&nbsp;	 </td>	
	</tr>
		
	<tr>
		  <td class=head  align="left" id="SSID_name">&nbsp;</td>
		  <td class=value1> <% getCfgGeneral(1,"SSID1"); %>&nbsp;	</td>	
	</tr>
	<tr>
		  <td class=head  align="left" id="wirelessmode">&nbsp;</td>
		  <td class=value1 id="wirelessmode2">
	 <script language="JavaScript" type="text/javascript">
		var PhyMode  = '<% getCfgZero(1, "WirelessMode"); %>';
            PhyMode =PhyMode *1;
        if ((PhyMode == 0) || (PhyMode == 4) || (PhyMode == 9)){   
	       if (PhyMode == 0)
			document.write("11b/g mixed mode");
		  else if (PhyMode == 4)
			document.write("11g only");
		  else if (PhyMode == 9)
			document.write("11b/g/n mixed");
     	  }
		else if (PhyMode == 1){
		    document.write("11b only");
		  }
		else if ((PhyMode == 2) || (PhyMode == 8))	{
	       if (PhyMode == 2)
			document.write("11a only");
		   else if (PhyMode == 8)
			document.write("11a/n mixed");
			}
		</script>&nbsp;	
		</td>	
	</tr>
	<tr>
		  <td class=head  align="left" id="securemode">&nbsp;</td>
		  <td class=value1> <% getCfgGeneral(1,"AuthMode"); %> &nbsp;	 </td>	
	</tr>
	</table>
	<br>
<table width="100%" border="1" cellPadding=3 cellSpacing=1 class=text1 bordercolor="#9BABBD" >
  
		<tr>
		  <td colspan=8 align="left" class="title" id="stalistWirelessNet">&nbsp;</td>
		</tr>
		<tr>
    <td class="head11" id="stalistMacAddr">MAC Address</td>
    <td class="head11">Aid</td>
    <td class="head11">PSM</td>
    <td class="head11">MimoPS</td>
    <td class="head11">MCS</td>
    <td class="head11">BW</td>
    <td class="head11">SGI</td>
    <td class="head11">STBC</td>
  </tr>
  <% getWlanStaInfo(); %>
   </table> 
   
</CENTER></td></tr></table>

</BODY>
</HTML>