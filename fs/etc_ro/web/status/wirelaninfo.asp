 <HEAD>
<META http-equiv="Content-Type" content="text/html; charset=gb2312">
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
<meta http-equiv="Pragma" content="no-cache">
<meta content="MSHTML 6.00.2800.1106" name="GENERATOR">
<link href="../style/normal_ws.css" type="text/css" rel="stylesheet">
<link href="../style/style.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../lang/b28n.js"></script>
<script type="text/javascript" src="../js/global_js.js"></script>
<TITLE>equipment infomation</TITLE>
<script language="JavaScript" type="text/javascript">
var start_time= new Date().getTime();
start_time=parseInt(start_time/1000);
Butterlate.setTextDomain("admin");
function initTranslation(){
var e = document.getElementById("intrannet");	
e.innerHTML = _("intrannet Title");
e = document.getElementById("intrannetintro");	
e.innerHTML = _("intrannet introduction");
e = document.getElementById("laninformation");	
e.innerHTML = _("lan interface information");	
e = document.getElementById("statusLANIPAddr");	
e.innerHTML = _("status lan ipaddr");	
e = document.getElementById("statusLANMAC");	
e.innerHTML = _("status mac");
e = document.getElementById("statusconnection");	
  e.innerHTML = _("status ethernet port status");
}
var secs
var timerID = null
var timerRunning = false
var timeout = 2
var delay = 200

function InitializeTimer(){
	// Set the length of the timer, in seconds
	secs = timeout
	StopTheClock()
	StartTheTimer()
}

function StopTheClock(){
	if(timerRunning)
		clearTimeout(timerID)
	timerRunning = false
}

function StartTheTimer(){
	if (secs==0){
		timerHandler();
		  secs = timeout
		StartTheTimer()
    }else{
		self.status = secs
		secs = secs - 1
		timerRunning = true
		timerID = self.setTimeout("StartTheTimer()", delay)
	}
}

var  PortStatus; 
function timerHandler()
{
  PortStatus = window.hiddenframe1.PortStatus;
	if(isNaN(PortStatus))
	{return ;}
	else if(PortStatus == "-1"){		
		document.getElementById("portstatus").innerHTML ="not support";		
		return ;	}
		else{
	showPortStatus();
	timeout = 2;
    delay = 1000;
	}
}

function showPortStatus()
{	
	 PortStatus = window.hiddenframe1.PortStatus;
	  var all = new Array();	
	 	all = PortStatus.split(",");	
 document.getElementById("portstatus1").innerHTML="";	
	for(i=12; i< all.length-1; i+=3)
	{	
		if(all[i] == "1")
		{			
			if(all[i+1] == "10")	
			 {    
			        var newnode = document.createElement("<img>");
					var newnodeAttr = document.createAttribute("src");
					newnodeAttr.nodeValue = "../graphics/10.gif";
					newnode.setAttributeNode(newnodeAttr);
					document.getElementById("portstatus1").appendChild(newnode);			
			      // document.write("<img src=../graphics/10.gif> ");		
			}	
			else if(all[i+1] == "100")	
			{ 
			        var newnode = document.createElement("<img>");
					var newnodeAttr = document.createAttribute("src");
					newnodeAttr.nodeValue = "../graphics/100.gif";
					newnode.setAttributeNode(newnodeAttr);
					document.getElementById("portstatus1").appendChild(newnode);	
			}			
		}
		else if(all[i] == "0")
		{				
		   var newnode = document.createElement("<img>");
			var newnodeAttr = document.createAttribute("src");
			newnodeAttr.nodeValue = "../graphics/empty.gif";
			newnode.setAttributeNode(newnodeAttr);
			document.getElementById("portstatus1").appendChild(newnode);	
		                        
		}			
	}
	
}

function init()
{ 
initTranslation();
//all_page_init();
   }
</script>
<BODY   onLoad="init();" onUnload="closeIntro()">
<h2 class="btnl" id="intrannet" >  </h2>

  <table  width="100%" class="tintro" >
  <tbody>
     <tr><iframe name="hiddenframe1" src="hiddenframe1.asp"id="hiddenframe1"  frameborder="0" border="0"  target="_self" style="display:none">
 </iframe>
      <td class="intro" id="intrannetintro" ></td>
      <td class ="image_col" align="right">
      <script language="javascript" type="text/javascript" >
	     var lang =_("next");
	   if (lang == "Next") lang="en";
	  else lang="zh_cn";
	   var page_name = "wirelaninfo"
	    help_display(page_name,lang);
	  </script>
	  </td>
    </tr>
  </tbody>
</table>
<hr>
<table width="571" class="body" bordercolor="#9BABBD">
 <tr>
 <td width="563">
  <CENTER>
  <table width=450 border="1" cellPadding=3 cellSpacing=1 class=text1 bordercolor="#9BABBD">
		 <tr>
		  <td colspan=2 align="left" class=title id="laninformation"></td>
		</tr>
		<tr>
		  <td class=head  align="left" id="statusLANIPAddr">&nbsp;</td>
		  <td class=value1 > 	 <% getLanIp(); %>	&nbsp;	  </td>
		</tr>
	<tr>
		  <td class=head  align="left" id="statusLANMAC">&nbsp;</td>
		  <td class=value1 >		  <% getLanMac(); %>	&nbsp;	 </td>	
	</tr>
	<tr>
		  <td class=head  align="left" id="statusconnection"></td>
		  <td class=value1 ><span id="portstatus"></span> <span id="portstatus1" style="font-weight:800"></span>
		  <script type="text/javascript"> 
		  		  setTimeout("showPortStatus();",1000);
				if(  lang=="en" )
				  document.write("<span>");
				  else document.write("<span><b>");
				  
				  document.write(_("right_port_type"))
				  
				  if(  lang=="en" ) document.write("</span>");
				  else 
				  document.write("</b></span>");
				  
		   </script>  
	 &nbsp;	</td>	
	</tr>
</table>
  </CENTER></td></tr>
   
 </table>
</BODY>
</HTML>
