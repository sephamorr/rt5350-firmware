function init_wifi_trans()
{
	setInnerHTML("wifi_label","Wireless Client Mode")
	setInnerHTML("chanel_wifi","Chanel")
	setInnerHTML("mac_wifi","MAC")
	setInnerHTML("mac_option","MAC Option");
	setInnerHTML("WIFISetting","Wireless Client Mode")
	setInnerHTML("ssid_wifi","SSID")
	setInnerHTML("security_mode_wifi","Security Mode")
	setInnerHTML("wep_mode_wifi","WEP")
	setInnerHTML("secureWEPDefaultKey_wifi","Default Key")
	setInnerHTML("secureWEPDefaultKey1_wifi","Key 1")
	setInnerHTML("secureWEPDefaultKey2_wifi","Key 2")
	setInnerHTML("secureWEPDefaultKey3_wifi","Key 3")
	setInnerHTML("secureWEPDefaultKey4_wifi","Key 4")
	setInnerHTML("wifi_wep_mode_wifi","KEY Form")
	setInnerHTML("secureWEPKey1_wifi","WEP Key 1 :")
	setInnerHTML("secureWEPKey2_wifi","WEP Key 2 :")
	setInnerHTML("secureWEPKey3_wifi","WEP Key 3 :")
	setInnerHTML("secureWEPKey4_wifi","WEP Key 4 :")
	setInnerHTML("wifi_cipher_wifi","WPA Algorithms")
	setInnerHTML("wifi_passphrase_wifi","Password")
    document.getElementById("wlSurveyBtn").value=_("Open Scan");
}

function CheckWifiValue()
{
var wifi_form = document.getElementById("wanCfg2");
   alert_cfg_gard(wifi_form.sta_security_mode.value);
	if( wifi_form.sta_security_mode.value == "WEP" ) 
	{
		 if(!check_Wep_wifi())
		 {
			 return false;
		 }
	}
    if(wifi_form.sta_security_mode.value =="WPAPSK" || wifi_form.sta_security_mode.value=="WPA2PSK" )
     {
	      var keyvalue = wifi_form.sta_passphrase.value;
			if (keyvalue.length == 0)
			{
			   alert(_("Please input wpapsk key!"));
				return false;
			}
			if (keyvalue.length < 8)
			{
				alert(_("Please input at least 8 character of wpapsk key!"));
				return false;
			}
			
			if(checkInjection(wifi_form.sta_passphrase.value) == false)
			{
				alert(_("Invalid characters in Pass Phrase."));
				return false;
			}
		 	if(wifi_form.sta_cipher[0].checked != true && 
		        wifi_form.sta_cipher[1].checked != true)
		   {
			   alert(_("Please choose a WPA Algorithms."));
			   return false;
		   }
	 }
	 return true;
}

   
function check_Wep_wifi()
{   
   alert_cfg_gard("at check wep")
	var wifi_form = document.getElementById("wanCfg2");
	var defaultid = wifi_form.sta_wep_default_key.value;
	var key_input;
	var keyvalue ;   
	if ( defaultid == 1 )
		  keyvalue = wifi_form.sta_wep_key_1.value;
	else if (defaultid == 2)
		  keyvalue = wifi_form.sta_wep_key_2.value;
	else if (defaultid == 3)
		  keyvalue = wifi_form.sta_wep_key_3.value;
	else if (defaultid == 4)
		  keyvalue = wifi_form.sta_wep_key_4.value;
 	if (keyvalue.length == 0 ){ // shared wep  || md5
		alert(_("Please input wep key")+defaultid+'!');
		return false;
	}
   alert_cfg_gard(keyvalue)
	var keylength = wifi_form.sta_wep_key_1.value.length;
	if (keylength != 0)
	{
		if (wifi_form.sta_wep_select.options.selectedIndex == 0)
		{
			if(keylength != 5 && keylength != 13) 
			{
				alert(_("Please input 5 or 13 characters of wep key1 !"));
				return false;
			}
			if(checkInjection(wifi_form.sta_wep_key_1.value)== false){
				alert(_("Wep key1 contains invalid characters."));
				return false;
			}
		}          
		if (wifi_form.sta_wep_select.options.selectedIndex == 1)
		{
			if(keylength != 10 && keylength != 26) 
			{
				alert(_("Please input 10 or 26 characters of wep key1 !"));
				return false;
			}
			if(checkHex(wifi_form.sta_wep_key_1.value) == false){
				alert(_("Invalid Wep key1 format!"));
				return false;
			}
		}
	}

	keylength = wifi_form.sta_wep_key_2.value.length;
	if (keylength != 0){
		if (wifi_form.sta_wep_select.options.selectedIndex == 0){
			if(keylength != 5 && keylength != 13) {
				alert(_("Please input 5 or 13 characters of wep key2 !"));
				return false;
			}
			if(checkInjection(wifi_form.sta_wep_key_2.value)== false){
				alert(_("Wep key2 contains invalid characters."));
				return false;
			}			
		}
		if (wifi_form.sta_wep_select.options.selectedIndex == 1){
			if(keylength != 10 && keylength != 26) {
				alert(_("Please input 10 or 26 characters of wep key2 !"));
				return false;
			}
			if(checkHex(wifi_form.sta_wep_key_2.value) == false){
				alert(_("Invalid Wep key2 format!"));
				return false;
			}
		}
	}

	keylength = wifi_form.sta_wep_key_3.value.length;
	if (keylength != 0){
		if (wifi_form.sta_wep_select.options.selectedIndex == 0)
		{
			if(keylength != 5 && keylength != 13) {
				alert(_("Please input 5 or 13 characters of wep key3 !"));
				return false;
			}
			if(checkInjection(wifi_form.sta_wep_key_3.value)== false)
			{
				alert(_("Wep key3 contains invalid characters."));
				return false;
			}
		}
		if (wifi_form.sta_wep_select.options.selectedIndex == 1)
		{
			if(keylength != 10 && keylength != 26) 
			{
				alert(_("Please input 10 or 26 characters of wep key3 !"));
				return false;
			}
			if(checkHex(wifi_form.sta_wep_key_3.value) == false)
			{
				alert(_("Invalid Wep key3 format!"));
				return false;
			}			
		}
	}

	keylength = wifi_form.sta_wep_key_4.value.length;
	if (keylength != 0){
		if (wifi_form.sta_wep_select.options.selectedIndex == 0)
		{
			if(keylength != 5 && keylength != 13) {
				alert(_("Please input 5 or 13 characters of wep key4 !"));
				return false;
			}
			if(checkInjection(wifi_form.sta_wep_key_4.value)== false)
			{
				alert(_("Wep key4 contains invalid characters."));
				return false;
			}			
		}
		if (wifi_form.sta_wep_select.options.selectedIndex == 1){
			if(keylength != 10 && keylength != 26) {
				alert(_("Please input 10 or 26 characters of wep key4 !"));
				return false;
			}

			if(checkHex(wifi_form.sta_wep_key_4.value) == false){
				alert(_("Invalid Wep key4 format!"));
				return false;
			}			
		}
	}
	return true;
}
function onChangeSec()
{
	var  wep_table = document.getElementById("div_wep_wifi").style;
	var  wpa_table = document.getElementById("div_wpa_wifi").style;
	var sec_mode_index = document.getElementById("sta_security_mode").selectedIndex *1 ;
   
	if(sec_mode_index == 1)
	{
		wep_table.display = "";
		wpa_table.display = "none";
	}
	else if(sec_mode_index == 2 || sec_mode_index == 3) 
	{
		wep_table.display = "none";
		wpa_table.display = "";
	}
	else
	{
		wep_table.display = "none";
		wpa_table.display = "none";
	}
}

/*function SurveyClose()
{
	var tbl = document.getElementById("wifiScanTable").style;
 	if (tbl.display == "")
	{
		tbl.display = "none";
		document.getElementById("wlSurveyBtn").value=_("Open Scan");
	}
	else
	{
		tbl.display = "";
		document.getElementById("wlSurveyBtn").value=_("Close Scan");
		 http_request = creat_http_request();
 	    var url = "/goform/ssid_scan";
 		http_request.open("POST", url, true);
	//	alert("http_request.open ok")
 		http_request.onreadystatechange = RequestRes;
	// 	http_request.setRequestHeader("If-Modified-Since","0");
 	//	http_request.send(null);
    	 http_request.send('n\a');	
	//	alert("http_request.send end")	
	}	
}*/
function SurveyClose()
{
	var tbl = document.getElementById("wifiScanTable").style;
 //	if (tbl.display == "")
//	{
//		tbl.display = "none";
//		document.getElementById("wlSurveyBtn").value=_("Open Scan");
//	}
//	else
//	{
		tbl.display = "";
	//	document.getElementById("wlSurveyBtn").value=_("Rescan");
	    document.getElementById("wlSurveyBtn").value=_("Scaning...");
		 http_request = creat_http_request();
 	    var url = "/goform/ssid_scan";
 		http_request.open("POST", url, true);
	//	alert("http_request.open ok")
 		http_request.onreadystatechange = RequestRes;
	// 	http_request.setRequestHeader("If-Modified-Since","0");
 	//	http_request.send(null);
    	 http_request.send('n\a');	
	//	alert("http_request.send end")	
//	}	
}
function RequestRes()
{  //  alert("onreadystatechange"+http_request.readyState)
	if (http_request.readyState == 4)
	{  
	   if (http_request.status == 200) 
		{  
	 //    alert(http_request.responseText);
		initScan(http_request.responseText);
		}
		else
		{
		 alert("can not  get scan_info")
		 }
	}
}

/*
//initilize scan table
function initScan(scan_info)
{	
//   alert(scan_info)
	if(scan_info != '')
	{
		var str1 = scan_info.split(";");
		var len = str1.length;
		document.getElementById("wifiScanTable").style.display = "";
		document.getElementById("wlSurveyBtn").value=_("Close Scan");
	//	 alert(document.getElementById("wlSurveyBtn").value)
		var tbl = document.getElementById("wifiScanTable").getElementsByTagName('tbody')[0];
		//delete
		var maxcell = tbl.rows.length;
		for (var j = maxcell; j > 1; j --)
		{
			tbl.deleteRow(j - 1);
		}	
	
		var count = parseInt(len);
		for (i = 0; i < count; i ++)
		{   
		    if(str1[i]=="") continue;
			var str = str1[i].split(",");
			var nrow=document.createElement('tr');
			var ncol=document.createElement('td');
			 
			nrow.appendChild(ncol);
            ncol.className = "value1";
			ncol.innerHTML = "<input type='radio' name='ssid_sel' id='ssid_sel_id' value='ssid_sel' onclick='getSSIDData()'/>";
			
			ncol=document.createElement('td');
			ncol.className = "value1";
			nrow.appendChild(ncol);
			ncol.innerHTML = str[1];
			
			ncol=document.createElement('td');
			ncol.className = "value1";
			nrow.appendChild(ncol);
			ncol.innerHTML = str[2];
		
			ncol=document.createElement('td');
			ncol.className = "value1";
			nrow.appendChild(ncol);
			ncol.innerHTML = str[0];
			
			ncol=document.createElement('td');
			ncol.className = "value1";
			nrow.appendChild(ncol);
			 if(str[4]=="UNKNOW") str[4]="OPEN";
			if(str[4]!="NONE")
			{
		    	ncol.innerHTML = str[3]+"/"+ str[4];
			}
			else 
			{
			   ncol.innerHTML = str[3];
			}
			
			ncol=document.createElement('td');
			ncol.className = "value1";
			nrow.appendChild(ncol);
			ncol.innerHTML = str[6]+" "+str[5];
			
			nrow.align = "center";
			tbl.appendChild(nrow);
		}
	}
	else
	{
		 document.getElementById("wifiScanTable").style.display = "none";
		 document.getElementById("wlSurveyBtn").value=_("Open Scan");
	}
} */

function initScan(scan_info)
{	
//   alert(scan_info)
 	if(scan_info != '')
 	{
		var str1 = scan_info.split(";");
		var len = str1.length;
		document.getElementById("wifiScanTable").style.display = "";
		document.getElementById("wlSurveyBtn").value=_("Rescan");
	//	 alert(document.getElementById("wlSurveyBtn").value)
		var tbl = document.getElementById("wifiScanTable").getElementsByTagName('tbody')[0];
		//delete
		var maxcell = tbl.rows.length;
		for (var j = maxcell; j > 1; j --)
		{
			tbl.deleteRow(j - 1);
		}	
 		var count = parseInt(len);
		for (i = 0; i < count; i ++)
		{   
		    if(str1[i]=="") continue;
		//	alert(str1[i])
			var str = str1[i].split(",");
			var nrow=document.createElement('tr');
			var ncol=document.createElement('td');
		//	 alert(str[2]+str[3]+str[4]);
			nrow.appendChild(ncol);
            ncol.className = "value1";
			ncol.innerHTML = "<input type='radio' name='ssid_sel' id='ssid_sel_id' value='ssid_sel' onclick='getSSIDData()'/>";
			
			ncol=document.createElement('td');
			ncol.className = "value1";
			nrow.appendChild(ncol);
			ncol.innerHTML = str[1];
			
			ncol=document.createElement('td');
			ncol.className = "value1";
			nrow.appendChild(ncol);
			ncol.innerHTML = str[2];
		
			ncol=document.createElement('td');
			ncol.className = "value1";
			nrow.appendChild(ncol);
			ncol.innerHTML = str[0];
			
			ncol=document.createElement('td');
			ncol.className = "value1";
			nrow.appendChild(ncol);
			 if(str[4]=="UNKNOW") str[4]="OPEN";
			if(str[4]!="NONE")
			{
		    	ncol.innerHTML = str[3]+"/"+ str[4];
			}
			else 
			{
			   ncol.innerHTML = str[3];
			}
 
			ncol=document.createElement('td');
			ncol.className = "value1";
			nrow.appendChild(ncol);
			ncol.innerHTML = str[6]+" "+str[5];
			
			nrow.align = "center";
			tbl.appendChild(nrow);
		}
 	}
 	else
  	{    
		// document.getElementById("wifiScanTable").style.display = "none";
		 document.getElementById("wlSurveyBtn").value=_("Rescan");
	//	 alert("can not  get scan_info");
	}
}

function getSSIDData()
{
	var wifiForm = document.getElementById("wanCfg2");
	if(!confirm(_("Do you ensure to connectting this AP?")))
	{
		return ;
	}
	var tbl = document.getElementById("wifiScanTable");
	var Slt = wifiForm.ssid_sel ;
	
//	alert(Slt.length)
	
	var mac,sc;
	var rowCount = tbl.rows.length;
	
	for (var r = rowCount; r > 1; r --)
	{
		if (rowCount == 2)
		 	sc = Slt.checked;
		 else
		 	sc = Slt[r - 2].checked;
		 
		if (sc)
		{  
		    var secu_mode;
			var wep_OPEN_SHARED = 1;
			var wpa_TKIP_AES = 1;
			var mac = tbl.rows[r - 1].cells[2].innerHTML;
			var cells_4 = tbl.rows[r - 1].cells[4].innerHTML;
			var secu_index;
			for(var i = 0;i<4;i++)
			{  
				if( (secu_index = cells_4.indexOf("Disable")) != -1)  
				{   
				  secu_mode = 0;
				  break;
				}
				if( (secu_index = cells_4.indexOf("WEP")) != -1)  
				{ 
				  secu_mode = 1;
				  if( (secu_index = cells_4.indexOf("OPEN")) != -1) 
				    wep_OPEN_SHARED = 0;
				  break;
				}
				if( (secu_index = cells_4.indexOf("WPAPSK")) != -1)  
				{ 
				  secu_mode = 2;
				    if( (secu_index = cells_4.indexOf("TKIP")) != -1) 
				    wpa_TKIP_AES = 0;
					break;
				}
				if( (secu_index = cells_4.indexOf("WPA2PSK")) != -1)  
				{ 
				  secu_mode = 3;
				    if( (secu_index = cells_4.indexOf("TKIP")) != -1) 
				    wpa_TKIP_AES = 0;
					break;
				}
			}
			wifiForm.sta_mac.value = mac;
			wifiForm.sta_ssid.value = tbl.rows[r - 1].cells[1].innerHTML;
			wifiForm.sta_channel.selectedIndex = tbl.rows[r - 1].cells[3].innerHTML - 1;
			wifiForm.sta_security_mode.options.selectedIndex = secu_mode;
		  	wifiForm.sta_wep_mode.options.selectedIndex = wep_OPEN_SHARED ;
		    wifiForm.sta_cipher[wpa_TKIP_AES*1].checked = true;
			onChangeSec();
		}
	}
}