<html>
<head>
<title>DTree</title>
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1" />
 <meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache,must-revalidate">
<meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
<link rel="stylesheet" href="style/normal_ws.css" type="text/css" />
<script type="text/javascript" src="dtree/dtree.js"></script>
<script type="text/javascript" src="/js/global_js.js"></script>
<script type="text/javascript" src="lang/b28n.js"></script>
<script language="JavaScript">
var isFimwareUpload = 0;
Butterlate.setTextDomain("main");
function initValue()
{
//	var e = document.getElementById("openall");
//  e.innerHTML = _("treeapp openall");
//	e = document.getElementById("closeall");
//	e.innerHTML = _("treeapp closeall");
	 var e = document.getElementById("wizard");
	e.innerHTML = _("treeapp wizard");
	
}
function setUnderFirmwareUpload(flag){
	isFimwareUpload = flag;
}
function go(zz) {
	if(!isFimwareUpload)
		top.view.location=zz;
}
function refresh(){
	window.location.reload(false);
}
</script>
</head>
<body  onLoad="initValue()" class="treecss" oncontextmenu="return false">

<p style="font-size:9pt;color:#FFFFFF"> &nbsp; &nbsp; <!--<a style="font-size:9pt;color:#FFFFFF" href="javascript: a.openAll();"id="openall" >open</a> | <a style="font-size:9pt;color:#FFFFFF" href="javascript: a.closeAll();" id="closeall">close</a>--></p>
<p > &nbsp; &nbsp; 
<a class="wizard" href="admin/wizard.asp"target="view" id="wizard">wizard</a>
</p>

<script type="text/javascript">
var opmode = '<% getCfgZero(1, "OperationMode"); %>';
var dhcpen = '<% getCfgZero(1, "dhcpEnabled"); %>';
var dpbsta = '<% getDpbSta(); %>';
var ethconv = '<% getCfgZero(1, "ethConvert"); %>';
var meshb = '<% getMeshBuilt(); %>';
var storageb = '<% getStorageBuilt(); %>';
var ftpb = '<% getFtpBuilt(); %>';
var smbb = '<% getSmbBuilt(); %>';
var mediab = '<% getMediaBuilt(); %>';
var swqos = '<% getSWQoSBuilt(); %>';
var ad = '<% isAntennaDiversityBuilt(); %>';
a = new dTree('a');
a.config.useStatusText=true;
a.config.useCookies=false;
//  nodeID, parent nodeID,  Name,  URL
a.add(000,  -1, _("3G ROUTER")  );
//a.add(200,  0, _("wizard"),                "javascript:go('admin/wizard.asp');");
 a.add(300,   0, _("treeapp status"),                   "javascript:a.oo(300);go('status/waninfo.asp');");
         a.add(301, 300, _("treeapp waninfo"),                   "javascript:go('status/waninfo.asp');");
        a.add(302, 300, _("treeapp equipinfo"),                 "javascript:go('status/equipinfo.asp');");
           //a.add(303, 300, _("treeapp usersideinfo"),     "javascript:a.oo(303);go('status/wlaninfo.asp');");
                a.add(303, 300, _("treeapp usersideinfo"),                      "javascript:a.oo(303);go('status/wlaninfo.asp');");
                a.add(304, 303, _("treeapp wlaninfo"),                  "javascript:go('status/wlaninfo.asp');");
                a.add(305, 303, _("treeapp wirelaninfo"),               "javascript:go('status/wirelaninfo.asp');");
                a.add(306, 303, _("treeapp laninfo"),                   "javascript:go('status/laninfo.asp');");

a.add(400,   0, _("treeapp internet settings"),   			"javascript:a.oo(400);go('internet/connection_set.asp');");
	a.add(401, 400, _("treeapp connection settings"),       "javascript:go('internet/connection_set.asp');");
	a.add(402, 400, _("treeapp dhcp settings"),             "javascript:go('internet/dhcp_set.asp');");
	a.add(403, 400, _("treeapp ip_mac_binding"),             "javascript:go('internet/ip_mac_binding.asp');");
    a.add(404, 400, _("treeapp static route settings"),       "javascript:go('internet/static_route_set.asp');");
//	a.add(405, 400, _("treeapp dynamic route settings"),      "javascript:go('internet/dynamic_route_set.asp');");
	
	
//	a.add(407, 400, _("treeapp qos1"),             "javascript:go('internet/qosaf.asp');");
//	a.add(408, 400, _("treeapp qos2"),             "javascript:go('internet/qosclassifier.asp');");
	
a.add(500,   0, _("treeapp wireless settings"),     "javascript:a.oo(500);go('wireless/basic.asp');");
	a.add(501, 500, _("treeapp basic"),                 "javascript:go('wireless/basic.asp');");
	a.add(502, 500, _("treeapp security"),              "javascript:go('wireless/security.asp');");
	a.add(503, 500, _("treeapp advanced"),              "javascript:go('wireless/advanced.asp');");
//	a.add(504, 500, _("treeapp wds"),                   "javascript:go('wireless/wds.asp');");
//	a.add(505, 500, _("treeapp wps"),                   "javascript:go('wps/wps.asp');");
 //安全设置
a.add(600,   0, _("treeapp secret settinging"),     "javascript:a.oo(600);go('secret/filterSwitch.asp');");
  //  a.add(602, 600, _("treeapp filter Switch"),       "javascript:go('secret/filterSwitch.asp');");
	a.add(601, 600, _("treeapp mac ip port filtering"),       "javascript:go('secret/port_filtering.asp');");
	a.add(602, 600, _("treeapp content filtering"),           "javascript:go('secret/content_filtering.asp');");
	a.add(603, 600, _("treeapp other filtering"),             "javascript:go('secret/others.asp');");

 //应用设置
a.add(700,   0, _("treeapp appli"),                "javascript:a.oo(700);go('application/virtual_server.asp');");

//a.add(702, 700, _("treeapp advance nat settings"),        "javascript:a.oo(702)");
//a.add(701, 700, _("treeapp alg settings"),               "javascript:go('application/alg_set.asp');");
a.add(701, 700, _("treeapp virtual server"),        "javascript:go('application/virtual_server.asp');");
//a.add(405, 700, _("treeapp QoS settings"),               "javascript:go('application/qos.asp');");
//a.add(703, 700, _("treeapp igmp settings"),              "javascript:go('application/igmp_setting.asp');");
//a.add(702, 700, _("treeapp upnp settings"),              "javascript:go('application/upnp_setting.asp');");
a.add(702, 700, _("treeapp port_trigger settings"),      "javascript:go('application/port_trigger_set.asp');");
a.add(703, 700, _("treeapp dmz settings"),               "javascript:go('application/dmz_set.asp');");
a.add(704, 700, _("treeapp ddns settings"),              "javascript:go('application/ddns_set.asp');");
//a.add(711, 700, _("treeapp ippd settings"),              "javascript:go('application/ippd_setting.asp');");

 //管理设置
a.add(800,   0, _("treeapp manage"),                  "javascript:a.oo(800);go('admin/user_manage.asp');");
a.add(801, 800, _("treeapp user manage"),                  "javascript:go('admin/user_manage.asp');");
//a.add(802, 800, _("treeapp language select"),            "javascript:go('admin/overview.asp');");
a.add(802, 800, _("treeapp equip manage"),                 "javascript:go('admin/equip_manage.asp');");
a.add(803, 800, _("treeapp syslog manage"),                 "javascript:go('admin/syslog.asp');");
a.add(804, 800, _("treeapp interface upgrade"),            "javascript:go('admin/interface_upgrade.asp');");
//a.add(900,   0, _("treeapp logout"),                  "javascript:go('logout.asp');");


document.write(a);
</script>
<p> &nbsp; &nbsp;</p>
<p> &nbsp; &nbsp;</p>
</body>
<head>
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache,must-revalidate">
<meta http-equiv="expires" content="Wed, 26 Feb 1997 08:21:57 GMT">
</head>
</html>
