<html>
<head>
<title>MAC Filtering Settings</title>
<META http-equiv="Content-Type" content="text/html; charset=gb2312">
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
<meta http-equiv="Pragma" content="no-cache">
<meta content="MSHTML 6.00.2800.1106" name="GENERATOR">
<link href="../style/normal_ws.css" type="text/css" rel="stylesheet">
<link href="../style/style.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="/lang/b28n.js"></script>
<script type="text/javascript" src="/js/global_js.js"></script>
<script language="JavaScript" type="text/javascript">
parent.menu.setUnderFirmwareUpload(0);
var _singleton = 0;
Butterlate.setTextDomain("firewall");

function deleteClick()
{
    return true;
}


function checkRange(str, num, min, max)
{
	d = atoi(str,num);
	if(d > max || d < min)
		return false;
	return true;
}


function atoi(str, num)
{
	i=1;
	if(num != 1 ){
		while (i != num && str.length != 0){
			if(str.charAt(0) == '.'){
				i++;
			}
			str = str.substring(1);
		}
	  	if(i != num )
			return -1;
	}
	
	for(i=0; i<str.length; i++){
		if(str.charAt(i) == '.'){
			str = str.substring(0, i);
			break;
		}
	}
	if(str.length == 0)
		return -1;
	return parseInt(str, 10);
}


function isAllNum(str)
{
	for (var i=0; i<str.length; i++){
	    if((str.charAt(i) >= '0' && str.charAt(i) <= '9') || (str.charAt(i) == '.' ))
			continue;
		return 0;
	}
	return 1;
}

function checkIpAddr(field)
{
    if(field.value == ""){
        alert(_("Error.IP address is empty."));
        field.value = field.defaultValue;
        field.focus();
        return false;
    }

    if ( isAllNum(field.value) == 0) {
        alert(_('It should be a [0-9] number.'));
        field.value = field.defaultValue;
        field.focus();
        return false;
    }

    if( (!checkRange(field.value,1,0,255)) ||
        (!checkRange(field.value,2,0,255)) ||
        (!checkRange(field.value,3,0,255)) ||
        (!checkRange(field.value,4,1,254)) ){
        alert(_('IP format error.'));
        field.value = field.defaultValue;
        field.focus();
        return false;
    }

   return true;
}

function formCheck()
{  
    if (_singleton ==1) return false;
	 
	if(document.DMZ.DMZIPAddress.value == ""){
		alert(_("Not set a ip address."));
		document.DMZ.DMZIPAddress.focus();
		return false;
	}

	if(!checkIpAddr(document.DMZ.DMZIPAddress) ){
	//	alert(_("IP address format error."));
		document.DMZ.DMZIPAddress.focus();
		return false;
	}
    _singleton=1;
//	if(document.DMZ.DMZEnabled.options.selectedIndex==0){
		// user choose disable
		
		document.getElementById("loading").style.display="block";
		document.getElementById("DMZEnabled").style.display="none";
//		parent.menu.setUnderFirmwareUpload(1);
		setTimeout("top.view.location='../application/dmz_set.asp';",500);
	     return true;
//	}
//    setTimeout("top.view.location='../application/dmz_set.asp';",2000);
//	return true;
}


function display_on()
{
  if(window.XMLHttpRequest){ // Mozilla, Firefox, Safari,...
    return "table-row";
  } else if(window.ActiveXObject){ // IE
    return "block";
  }
}

function disableTextField (field)
{
  if(document.all || document.getElementById)
    field.disabled = true;
  else {
    field.oldOnFocus = field.onfocus;
    field.onfocus = skip;
  }
}

function enableTextField (field)
{
  if(document.all || document.getElementById)
    field.disabled = false;
  else {
    field.onfocus = field.oldOnFocus;
  }
}

function initTranslation()
{
	var e = document.getElementById("dmzTitle");
	e.innerHTML = _("dmz title");
	e = document.getElementById("dmzIntroduction");
	e.innerHTML = _("dmz introduction");

	e = document.getElementById("dmzSetting");
	e.innerHTML = _("dmz setting");
	e = document.getElementById("dmzSet");
	e.innerHTML = _("dmz setting");
	e = document.getElementById("dmzDisable");
	e.innerHTML = _("firewall disable");
	e = document.getElementById("dmzEnable");
	e.innerHTML = _("firewall enable");
	e = document.getElementById("dmzIPAddr");
	e.innerHTML = _("dmz ipaddr");
	e = document.getElementById("dmzApply");
	e.value = _("firewall apply");
	e = document.getElementById("dmzReset");
	e.value = _("firewall reset");
}


	
function DMZinit()
{
		if(document.DMZ.DMZEnabled.options.selectedIndex == 1)
		{
			enableTextField(document.DMZ.DMZIPAddress);
			document.getElementById("dmzIP_Display").style.display = display_on_row();
			document.getElementById("dmzIP_Display").style.visibility = "visible";
		 }else
		 {  
			disableTextField(document.DMZ.DMZIPAddress);
			document.getElementById("dmzIP_Display").style.display = "none";
			document.getElementById("dmzIP_Display").style.visibility = "hidden";
		}
}

function updateState()
{  
  all_page_init();
	initTranslation();
	
	DMZinit();
}

</script>
</head>

<body onLoad="updateState()">
<h2 class="btnl" id="dmzTitle">&nbsp;</h2>
<% checkIfUnderBridgeModeASP(); %>
<table  width="100%" class="tintro">
  <tbody>
    <tr>
      <td class="intro" id="dmzIntroduction" >  </td>
      <td align="right"  class="image_col">
                <script language="javascript" type="text/javascript" >
	      var lang =_("next");
	   if (lang == "Next") lang="en";
	  else lang="zh_cn";
	   var page_name = "dmzsetting"
	    help_display(page_name,lang);
	  </script>
      </td>
    </tr>
  </tbody>
</table> 

<hr>
<table width="571" class="body" bordercolor="#9BABBD">
  <tbody><tr><td width="563">
<form method=post name="DMZ" action=/goform/DMZ   target="hiddenframe0">
<center>
<table width="450" border="1" cellpadding="2" cellspacing="1" bordercolor="#9BABBD" >
<tr>
  <td class="title" colspan="2" id="dmzSetting">&nbsp;</td>
</tr>
<tr>
	<td class="head" id="dmzSet">&nbsp;</td>
	<td  class="value1" >
	<select name="DMZEnabled" id="DMZEnabled" size="1"  onChange="DMZinit()">
	<option value=0 <% getDMZEnableASP(0); %> id="dmzDisable">Disable</option>
    <option value=1 <% getDMZEnableASP(1); %> id="dmzEnable">Enable</option>
    </select>    </td>
</tr>

<tr id="dmzIP_Display" style="display:none;visibility:hidden">
	<td class="head" id="dmzIPAddr">&nbsp;</td>
	<td  class="value1" >
  		<input type="text" size="18" maxlength="15" name="DMZIPAddress" value=<% showDMZIPAddressASP(); %> >	</td>
</tr>
</table>
<table  cellpadding="2" cellspacing="1" width="358" >
<tbody>
<tr align="center"> <td width="450" colspan="2" >
	<input type="submit"style="width:110px;"  value="Apply" id="dmzApply" name="addDMZ" onClick="return formCheck()"> &nbsp;&nbsp;
	<input type="reset" style="width:110px;" value="Reset" id="dmzReset" name="reset" onClick=" reset_clicked()"></td>
</tr>
</table>
</center>
<iframe name="hiddenframe0"  id="hiddenframe0" frameborder="0" border="0" style="display:none"></iframe>
</form></td></tr></tbody></table>
 <!--table width="85%" bordercolor="#9BABBD">
<tbody><tr><td >
<p class="value1">
Notice��<br>
 In some special condition, it is necessary to make one computer in the LAN divulged completely to the WAN, to realize intercommunication, at this time, you can set up the computer as DMZ Host.<br> 
(Notice: After setting up DMZ Host, the firewall setup which is correlative to this IP will not take any effect.).<br>
 Note:<Br> DMZ IP Address: Please set up the IP address which you would like to display in the Internet. </p>
</table-->

</body>
<script language="JavaScript" type="text/javascript">document.write('<div id="loading"  style="display: none;z-index = 9999"><br><br><br><div align="center">'+_("Submitting settings")+
'</div><br><br>&nbsp;&nbsp;' +_("Submitting be patient")+'</div>');
</script>
</html>