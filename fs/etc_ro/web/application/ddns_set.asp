<html>
<head>
<title>DDNS Settings</title>
<META http-equiv="Content-Type" content="text/html; charset=gb2312">
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
<meta http-equiv="Pragma" content="no-cache">
<meta content="MSHTML 6.00.2800.1106" name="GENERATOR">
<link href="../style/normal_ws.css" type="text/css" rel="stylesheet">
<link href="../style/style.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="/lang/b28n.js"></script>
<script type="text/javascript" src="/js/global_js.js"></script>
<script language="JavaScript" type="text/javascript">
parent.menu.setUnderFirmwareUpload(0);
var _singleton = 0;
Butterlate.setTextDomain("firewall");
function formCheck()
{ 
	if (_singleton ==1) return false;
	
	document.getElementById("hostname").value = document.getElementById("hostname").value.Trim();
	document.getElementById("Account").value = document.getElementById("Account").value.Trim();
	document.getElementById("Password").value = document.getElementById("Password").value.Trim();
	
	if(document.getElementById("ddnsEnable").options.selectedIndex == 1)
	{
		if(document.getElementById("hostname").value=="")
		{
			alert(_("Please input Registered Domain Name"));
			return false;
		}
		
 		if(document.getElementById("Account").value=="")
		{
			alert(_("Please input Account"));
			return false;
		}
		
 		if(document.getElementById("Password").value=="")
		{
			alert(_("Please input Password"));
		    return false;
		}
		
	}
  	_singleton=1;
	page_select_hidden(100);
	document.getElementById("loading").style.display="block";
	
 setTimeout("top.view.location='../application/ddns_set.asp';",1500);
	return true;
}
function initTranslation()
{
var e = document.getElementById("ddns_Title");
 
	e.innerHTML = _("ddns title");
	e = document.getElementById("ddns_Introduction");
	e.innerHTML = _("ddns introduction");

	e = document.getElementById("ddns_Setting");
	e.innerHTML = _("ddns setting");
	e = document.getElementById("ddnsEnable_head");
	e.innerHTML = _("ddns Enable head");
	e = document.getElementById("ddns_Disable");
	e.innerHTML = _("firewall disable");
	e = document.getElementById("ddns_Enable");
	e.innerHTML = _("firewall enable");
	
	e = document.getElementById("DDNSProvider_head");
	e.innerHTML = _("firewall DDNSProvider head");
	e = document.getElementById("DDNS_head");
	e.innerHTML = _("firewall DDNS head");
	e = document.getElementById("Account_head");
	e.innerHTML = _("firewall Account head");
	e = document.getElementById("Password_head");
	e.innerHTML = _("firewall Password head");
		
	e = document.getElementById("ddns_Apply");
	e.value = _("firewall apply");
	e = document.getElementById("ddns_Reset");
	e.value = _("firewall reset");
}

function style_display_on()
{
	if (window.ActiveXObject)
		{ // IE
			return "block";
		}
     	else if (window.XMLHttpRequest)
		{ // Mozilla, Safari,...
			return "table-row";
		}
}
	
function debug(parm)
{
// alert(parm);
}

function ddnsEn_changed()
{  
      var tr_array = new Array();
	   tr_array = document.getElementsByTagName("tr");
	   debug(tr_array.length);
	 
	if( document.getElementById("ddnsEnable").options.selectedIndex == 1 )
	 {
	  for (var i =4; i<8; i++)
	   {
	     tr_array[i].style.display = style_display_on(); 
	     tr_array[i].style.visibility = "visible"; 
		 }
	 }
	 else
	 {
	  for (var i =4; i<8; i++)
	   {
	     tr_array[i].style.display = "none"; 
	     tr_array[i].style.visibility = "hidden"; 
		}
	}
}

function initvalue()
{  
	initTranslation();
	all_page_init();
	var ddnsEnable = "<% getCfgGeneral(1,"ddnsEnable");%>";
	var DDNSProvider = "<% getCfgGeneral(1,"DDNSProvider");%>";
	var DDNS = "<% getCfgGeneral(1,"DDNS");%>";
	var DDNSAccount = "<% getCfgGeneral(1,"DDNSAccount");%>";
	var DDNSPassword = "<% getCfgGeneral(1,"DDNSPassword");%>";
	ddnsEnable = ddnsEnable * 1;
	document.getElementById("ddnsEnable").options.selectedIndex = ddnsEnable;
	document.getElementById("hostname").value = DDNS;
	document.getElementById("Account").value = DDNSAccount;
	document.getElementById("Password").value = DDNSPassword;
	
	var DDNSProvider_Sel = document.getElementById("DDNSProvider")
	 for (var i=0 ; i < DDNSProvider_Sel.options.length; i++ )
	 {
	    if (DDNSProvider == DDNSProvider_Sel.options[i].value) 
	    { 
			  DDNSProvider_Sel.options.selectedIndex = i;
			  break ;
		 }
	  }
	ddnsEn_changed();
}

</script>
</head>

<body onLoad="initvalue()">
<h2 class="btnl" id="ddns_Title">&nbsp;</h2>
<table  width="100%" class="tintro">
  <tbody>
    <tr>
      <td class="intro" id="ddns_Introduction" >  </td>
      <td align="right"  class="image_col">
      <script language="javascript" type="text/javascript" >
	    var lang =_("next");
	   if (lang == "Next") lang="en";
	  else lang="zh_cn";
	   var page_name = "ddns_set"
	    help_display(page_name,lang);
	  </script>
      </td>
    </tr>
  </tbody>
</table> 

<hr>
<table width="571" class="body" bordercolor="#9BABBD">
  <tbody><tr><td width="563">
<form method=post name="DDNS" action=/goform/DDNS   target="hiddenframe0">
<center>
<table width="450" border="1" cellpadding="2" cellspacing="1" bordercolor="#9BABBD" >
<tr>
  <td class="title" colspan="2" id="ddns_Setting">&nbsp;</td>
</tr>
<tr>
	<td class="head" id="ddnsEnable_head">&nbsp;</td>
	<td  class="value1" >
	<select name="ddnsEnable" id="ddnsEnable" size="1" onChange="ddnsEn_changed()">
	<option value=0 id="ddns_Disable">Disable</option>
    <option value=1  id="ddns_Enable">Enable</option>
    </select>    </td>
</tr>

<tr id="DDNSProvider_Display" style="display:none;visibility:hidden">
	<td class="head" id="DDNSProvider_head">&nbsp;</td>
	<td  class="value1" >
  		<select name="DDNSProvider"  id="DDNSProvider" size="1">
		     <option VALUE=qdns > 3322.org </option>
			<option VALUE=qdns-static > 3322.org(static) </option>
			<option VALUE=dyndns > dyndns.org </option>
			<option VALUE=dyndns-static > dyndns.org(static) </option>
			<option VALUE=dyndns-custom > dyndns.org(custom) </option>
			<option VALUE=ezip > EZ-IP.Net </option>
			<option VALUE=pgpow > penguinpowered.com </option>
			<option VALUE=dhs > dhs.org </option>
			<option VALUE=ods > ods.org </option>
			<option VALUE=tzo > tzo.com </option>
			<option VALUE=easydns > easydns.com </option>
			<option VALUE=easydns-partner > easydns.com(partner) </option>
			<option VALUE=justlinux > justlinux.com </option>
			<option VALUE=dyns > dyns.cx </option>
			<option VALUE=hn > hn.org </option>
			<option VALUE=zoneedit > zoneedit.com </option>
			<option VALUE=heipv6tb > ipv6tb.he.net </option>
			</select>	</td>
</tr>

<tr id="DDNS_Display" style="display:none;visibility:hidden">
	<td class="head" id="DDNS_head">&nbsp;</td>
	<td  class="value1" ><input type="text" name="DDNS"  id="hostname" maxlength="40" size="22" /></td>
</tr>
<tr id="Account_Display" style="display:none;visibility:hidden">
	<td class="head" id="Account_head">&nbsp;</td>
	<td  class="value1" >
  		<input name="Account" id="Account"  type="text" maxlength="32" size="22">	</td>
</tr>

<tr id="Password_Display" style="display:none;visibility:hidden">
	<td class="head" id="Password_head">&nbsp;</td>
	<td  class="value1" >
  		<input name="Password" id="Password" type="password"maxlength="32" size="22">	</td>
</tr>
</table>
<table  cellpadding="2" cellspacing="1" width="358" >
<tbody>
<tr align="center"> <td width="450" colspan="2" >
	<input type="submit"style="width:110px;"  value="Apply" id="ddns_Apply" name="submit" onClick="return formCheck()"> &nbsp;&nbsp;
	<input type="reset" style="width:110px;" value="Reset" id="ddns_Reset" name="reset" onClick=" reset_clicked()"></td>
</tr>
</table>
</center>
<iframe name="hiddenframe0"  id="hiddenframe0" frameborder="0" border="0" style="display:none"></iframe>
</form></td></tr></tbody></table>
</body>
<script language="JavaScript" type="text/javascript">
document.write('<div id="loading"  style="display: none;z-index = 9999"><br><br><br><div align="center">'+_("Submitting settings")+
'</div><br><br>&nbsp;&nbsp;' +_("Submitting be patient")+'</div>');
</script>
</html>