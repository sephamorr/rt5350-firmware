<html>
<head>
<title>IP/Port Filtering Settings</title>
<META http-equiv="Content-Type" content="text/html; charset=gb2312">
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
<meta http-equiv="Pragma" content="no-cache">
<meta content="MSHTML 6.00.2800.1106" name="GENERATOR">
<link href="../style/normal_ws.css" type="text/css" rel="stylesheet">
<link href="../style/style.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../lang/b28n.js"></script>
<script type="text/javascript" src="../js/global_js.js"></script>
<script language="JavaScript" type="text/javascript">
Butterlate.setTextDomain("firewall");
parent.menu.setUnderFirmwareUpload(0);
var _singleton = 0;
var MAX_RULES = 32;
var secs
var timerID = null
var timerRunning = false
var timeout = 3
var delay = 1000
var rules_num = "<% getPortTriggerRuleNumsASP(); %>";
     rules_num = rules_num*1;
var pt_En_sys= "<% getCfgGeneral(1,"portTriggerEnabled");%>";  
    pt_En_sys = pt_En_sys*1;

function deleteClick()
{  
	if (_singleton ==1)  return false; 
	page_select_hidden(100);
	document.getElementById("loading").style.display="block";
	setTimeout("top.view.location='../application/port_trigger_set.asp';",800);
	_singleton = 1;
	return true;
}
 
function submit_Click()
{  
   if (_singleton ==1) return false;
	if(rules_num >= (MAX_RULES-1) ){
		alert(_("The rule number is exceeded")+ MAX_RULES +".");
		return false;
	}
	var doc_forms1 = document.forms[1]; 
	var protocol_= doc_forms1.protocol.value;
//	debug("this rule's protocol is "+protocol_)
	var protocol_NUM ;
 switch(protocol_)
  {
   case "TCP":
   protocol_NUM=1;
   break;
   case "UDP":
   protocol_NUM=2;
   break;
   case "TCP&UDP":
   protocol_NUM=3;
   break;
   case "ICMP":
   protocol_NUM=4;
   break;
   case "None":
   protocol_NUM=5;
   break;
   }
 //  debug("this rule's protocol_NUM is "+protocol_NUM)
	var tri_port_start_= doc_forms1.tri_port_start.value.Trim(); 
	var tri_port_end_ = doc_forms1.tri_port_end.value.Trim();
	var open_prot_start_ = doc_forms1.open_prot_start.value.Trim();
	var open_port_end_ = doc_forms1.open_port_end.value.Trim();
		
 	doc_forms1.tri_port_start.value = tri_port_start_
	doc_forms1.tri_port_end.value = tri_port_end_
	doc_forms1.open_prot_start.value = open_prot_start_
	doc_forms1.open_port_end.value = open_port_end_
 	
//doc_forms1.tri_port_end.value == "" &&doc_forms1.open_port_end.value == "" &&		
	if( doc_forms1.tri_port_start.value == ""||	doc_forms1.open_prot_start.value == "") //正确的，起始都不能为空
	{
		alert(_("The start port of port range is invalid."));
		return false;
	}
	
	if(doc_forms1.tri_port_start.value != "")
	{
		d1 = atoi(doc_forms1.tri_port_start.value, 1);
		if(isNumOnly( doc_forms1.tri_port_start.value ) == 0)
		{
			alert(_("Invalid port number: Trigger Port."));
			doc_forms1.tri_port_start.focus();
			return false;
		}
		if(d1 > 65535 || d1 < 1)
		{
			alert(_("Invalid port number: Trigger Port."));
			doc_forms1.tri_port_start.focus();
			return false;
		}
		
		if(doc_forms1.tri_port_end.value=="")  doc_forms1.tri_port_end.value = d1;  //结束端口为空时，将以开始端口赋值
	
		if(doc_forms1.tri_port_end.value != "")
		{
			if(isNumOnly( doc_forms1.tri_port_end.value ) == 0)
			{
				alert(_("Invalid port number: Trigger Port."));
				doc_forms1.tri_port_end.focus();
				return false;
			}		
			d2 = atoi(doc_forms1.tri_port_end.value, 1);
			if(d2 > 65535 || d2 < 1)
			{
				alert(_("Invalid port number: Trigger Port."));
				doc_forms1.tri_port_end.focus();
				return false;
			}
			if(d1 > d2)
			{
				alert(_("Invalid port range: Trigger Port Range."));
				return false;
			}
		}
	}

	if(doc_forms1.open_prot_start.value != "")
	{
		d1 = atoi(doc_forms1.open_prot_start.value, 1);
		if(isNumOnly( doc_forms1.open_prot_start.value ) == 0)
		{
			alert(_("Invalid port number: Open Port."));
			doc_forms1.open_prot_start.focus();
			return false;
		}
		if(d1 > 65535 || d1 < 1){
			alert(_("Invalid port number: Open Port."));
			doc_forms1.open_prot_start.focus();
			return false;
		}
		
		if(doc_forms1.open_port_end.value =="") doc_forms1.open_port_end.value = d1; //结束端口为空时，将以开始端口赋值
		
		if(doc_forms1.open_port_end.value != "")
		{
			if(isNumOnly( doc_forms1.open_port_end.value ) == 0)
			{
				alert(_("Invalid port number: Open Port."));
				doc_forms1.open_port_end.focus();
				return false;
			}		
			d2 = atoi(doc_forms1.open_port_end.value, 1);
			if(d2 > 65535 || d2 < 1)
			{
				alert(_("Invalid port number: Open Port."));
				doc_forms1.open_port_end.focus();
				return false;
			}
			if(d1 > d2)
			{
			alert(_("Invalid port range: Open Port Range."));
			doc_forms1.open_port_end.focus();
			return false;
			}
		}
	}
	var PortTriggerRules = '<% getCfgGeneral(1,"PortTriggerRules");%>' ;
	var cur_rule = new Array();
	var port_in_currule = new Array();
	if(PortTriggerRules.length)
	{
	cur_rule = PortTriggerRules.split(";")
	for(var i=0;i<cur_rule.length;i++)
	{ 
	port_in_currule = cur_rule[i].split(",")
	if(port_in_currule[0]==doc_forms1.tri_port_start.value&&port_in_currule[1]==doc_forms1.tri_port_end.value&&port_in_currule[2]==doc_forms1.open_prot_start.value&&port_in_currule[3]==doc_forms1.open_port_end.value&&port_in_currule[4]==protocol_NUM)
	 { 
	  alert(_("There is a same rule in system,please input a new one."));
	  return false;
	  }
	 
	 }
 	}
	 
	// check ip address format
	page_select_hidden(100);
	document.getElementById("loading").style.display="block";
 setTimeout("top.view.location='../application/port_trigger_set.asp';",800);
 var portTrigger1 = document.getElementById("portTrigger1");
 _singleton = 1;
 portTrigger1.submit()
 	//return true;
}
function pt_formCheck() //检查端口 
{ 
	if (_singleton ==1)  return false;
	page_select_hidden(100);
	document.getElementById("loading").style.display="block";
	setTimeout("top.view.location='../application/port_trigger_set.asp';",800);
	var portTrigger0 = document.getElementById("portTrigger0");
	_singleton = 1;
	portTrigger0.submit();
	//return true;
}

function initTranslation()
{
	var e = document.getElementById("portTriggerTitle");
	e.innerHTML = _("portTriggerTitle");
	e = document.getElementById("portTriggerIntroduction");
	e.innerHTML = _("portTriggerIntroduction");
	e = document.getElementById("portTrigger_title2");
	e.innerHTML = _("portTrigger_title2");
	
	
	e = document.getElementById("portTrigger_head");
	e.innerHTML = _("portTrigger_head");
	e = document.getElementById("portTriggerD");
	e.innerHTML = _("firewall disable");
	e = document.getElementById("portTriggerE");
	e.innerHTML = _("firewall enable");
	e = document.getElementById("portTriggerApply");
	e.value = _("firewall apply");
	e = document.getElementById("portTriggerReset");
	e.value = _("firewall reset");
	
	
    e = document.getElementById("portTriggerSet");
	e.innerHTML = _("portTriggerSet");
	e = document.getElementById("portFilterProtocol");
	e.innerHTML = _("trigger_protocol");
	e = document.getElementById("tri_port_Range"); 
	e.innerHTML = _("tri_port_Range");
	e = document.getElementById("open_prot_Range");
	e.innerHTML = _("open_prot_Range");
	e = document.getElementById("portFilterComment");
	e.innerHTML = _("firewall comment");
	e = document.getElementById("portSetApply");
	e.value = _("firewall add");
	e = document.getElementById("portSetReset");
	e.value = _("firewall reset");
   
	
	e = document.getElementById("Current_rules_title");
	e.innerHTML = _("Current_rules_title")        
	e = document.getElementById("Current_Comment");
	e.innerHTML = _("firewall comment");
	e = document.getElementById("Current_opRange");
	e.innerHTML = _("open_prot_Range")
	e = document.getElementById("Current_tpRange");
	e.innerHTML = _("tri_port_Range");
	e = document.getElementById("Current_Protocol");
	e.innerHTML = _("trigger_protocol")
	e = document.getElementById("portCurrentFilterNo");
	e.innerHTML = _("firewall no");
	
	
	e = document.getElementById("deleteSelRule");
	e.value = _("firewall del select");
	e = document.getElementById("del_pt_Reset");
	e.value = _("firewall reset")
	
	/* if(document.getElementById("portCurrentFilterDefaultDrop")){
		e = document.getElementById("portCurrentFilterDefaultDrop");
		e.innerHTML = _("firewall default drop");
	}
	if(document.getElementById("portCurrentFilterDefaultAccept")){
		e = document.getElementById("portCurrentFilterDefaultAccept");
		e.innerHTML = _("firewall default accept");
	}*/

	var i=0;
	while( document.getElementById("portFilterActionDrop"+i) ||
			document.getElementById("portFilterActionAccept"+i) ){
		if(document.getElementById("portFilterActionDrop"+i)){
			e = document.getElementById("portFilterActionDrop"+i);
			e.innerHTML = _("port filter action drop");
		}

		if(document.getElementById("portFilterActionAccept"+i)){
			e = document.getElementById("portFilterActionAccept"+i);
			e.innerHTML = _("port filter action accept");
		}

		i++;
	}
}
function init()
{    
	// all_page_init();
	document.forms[0].portTriggerEnabled.options.selectedIndex = pt_En_sys ;
	 if(!rules_num ){
 		disableTextField(document.portTriggerDelete.deleteSelRule);
 		disableTextField(document.portTriggerDelete.del_pt_Reset);
	}else{ 
        enableTextField(document.portTriggerDelete.deleteSelRule);
        enableTextField(document.portTriggerDelete.del_pt_Reset);
	}
     updateState();
	 initTranslation();
	 all_page_init();
}
function display_on_table2(){
   if (window.XMLHttpRequest) { // Mozilla, Firefox, Safari,...
        return "table";
    }
    else 
        if (window.ActiveXObject) { // IE
            return "block";
        } 
}
function updateState()
{   
     	 
	if( document.forms[0].portTriggerEnabled.options.selectedIndex == 1)
		{  
		  document.getElementById("set_table").style.visibility= "visible";
		  document.getElementById("set_table").style.display= display_on_table(); 
		  document.getElementById("set_table_submit").style.visibility= "visible";
		  document.getElementById("set_table_submit").style.display= display_on_table(); 
		  document.getElementById("cur_rule_table").style.visibility= "visible";
		  document.getElementById("cur_rule_table").style.display=display_on_table(); 
		  document.getElementById("del_rule_table").style.visibility= "visible";
		  document.getElementById("del_rule_table").style.display= display_on_table();  
        }
		else
		{ 
	  document.getElementById("set_table").style.visibility = "hidden";
	  document.getElementById("set_table").style.display = "none"; 
	  document.getElementById("set_table_submit").style.visibility= "hidden";
	  document.getElementById("set_table_submit").style.display= "none"; 
	  document.getElementById("cur_rule_table").style.visibility= "hidden";
	  document.getElementById("cur_rule_table").style.display="none"; 
	  document.getElementById("del_rule_table").style.visibility= "hidden";
	  document.getElementById("del_rule_table").style.display="none"; }
}
</script>
</head>
                         <!--     body      -->
<body onLoad="init()">
<h2 id="portTriggerTitle" class="btnl"></h2>
<table  width="100%" class="tintro" >
  <tbody>
     <tr>
      <td class="intro" id="portTriggerIntroduction" >   </td>
      <td class ="image_col" align="right">
	<script language="javascript" type="text/javascript" >
	    var lang =_("next");
	   if (lang == "Next") lang="en";
	  else lang="zh_cn";
	   var page_name = "port_trigger_set"
	    help_display(page_name,lang);
	  </script>
	  </td>
    </tr>
  </tbody>
</table><hr>
<table width="571" class="body" bordercolor="#9BABBD">
<tbody>  <tr><td width="563"><center>
<!-- ====================   BASIC  form  ==================== -->
<form method=post name="portTrigger" id="portTrigger0" action=/goform/portTrigger target="hiddenframe" style="margin:0">
<table width="450"  cellpadding="2" cellspacing="1"  border="1" bordercolor="#9BABBD">
<tbody>
<tr>
	<td class="title" colspan="2" id="portTrigger_title2">port Trigger</td>
</tr>
<tr style="border:1">
	<td  class="head" id="portTrigger_head">port Trigger</td>
	<td class=value1 >
	<select name="portTriggerEnabled" size="1" id="portTriggerEnabled" onChange="updateState()" >
	<option value=0  id="portTriggerD">Disable&nbsp;</option>
    <option value=1  id="portTriggerE">Enable</option>
    </select> </td>
</tr>
</tbody></table>
<table  cellpadding="2" cellspacing="1" width="450" >

<tr align="center">
  <td>
	<input type="button" style="width:110px;" value="Apply" id="portTriggerApply" name="addDMZ" onClick="return pt_formCheck()"> &nbsp;&nbsp;
	<input type="button" style="width:110px;" value="Reset" id="portTriggerReset" name="reset" onClick="reset_clicked()">  </td>
</tr>
</table>
<iframe name="hiddenframe"  id="hiddenframe" frameborder="0" border="0" style="display:none; visibility:hidden; margin:0"></iframe>
</form>
<form method=post name="portTrigger" id="portTrigger1" action=/goform/portTrigger target="hiddenframe1" style="margin:0">
<table width="450" border="1" cellpadding="2" cellspacing="1" bordercolor="#9BABBD" id="set_table" style="display:none" >
  <tr>
    <td class="title" colspan="2" id="portTriggerSet">port Trigger Settings </td>
  </tr>
   <tr><input type="hidden" name="portTriggerEnabled" value="1" id="hidden_portTriggerEnabled"/>
    <td class="head" id="portFilterProtocol"> Protocol </td>
    <td  class="value1" ><select name="protocol" id="protocol">
      <option value="TCP">TCP</option>
      <option value="UDP">UDP</option>
      <option value="TCP&UDP">TCP&UDP</option>
    </select>
      &nbsp;&nbsp; </td>
  </tr>
  <tr>
    <td class="head"  id="tri_port_Range">tri_port</td>
    <td  class="value1" ><input type="text" size="5" maxlength="5" name="tri_port_start" id="tri_port_start">
      ---
      <input type="text" size="5" maxlength="5" name="tri_port_end" id="tri_port_end">    </td>
  </tr>
  <tr>
    <td class="head"  id="open_prot_Range"> open_prot </td>
    <td  class="value1" ><input type="text" size="5" maxlength="5" name="open_prot_start" id="open_prot_start">
      ---
      <input type="text" size="5"  maxlength="5"name="open_port_end" id="open_port_end">    </td>
  </tr>
    <tr>
    <td class="head"  id="portFilterComment">Comment</td>
    <td  class="value1" ><input type="text" name="comment" size="18" maxlength="32" >    </td>
  </tr>
</table>
<table  cellpadding="2" cellspacing="1" width="446" id="set_table_submit" style="display:none">
	<tr align="left" id="prompt_maximum">
		<td id="maximum_rule"> 
			<script>
				document.write("("+_("The maximum rule count is")+ MAX_RULES +".)");
			</script>
		</td>
	</tr>
<tr align="center">
  <td>
	<input type="button"  class="button" value="Apply" id="portSetApply" name="addDMZ" onClick="return submit_Click()"> &nbsp;&nbsp;
	<input type="reset"  class="button" value="Reset" id="portSetReset" name="reset">  </td>
</tr>
</table>
<iframe name="hiddenframe1"  id="hiddenframe1" frameborder="0" border="0" style="display:none; visibility:hidden; margin:0"></iframe>
</form>

<form action=/goform/portTriggerDelete method=POST name="portTriggerDelete" target="hiddenframe1">
  <table width="550" border="1" cellpadding="2" cellspacing="1" bordercolor="#9BABBD" align="center" id="cur_rule_table" style="display:none">
    <tbody >
      <tr>
        <td class="title" colspan="10" id="Current_rules_title">Current port trigger rules</td>
      </tr>
      <tr>
        <td width="14%" class="head11" id="portCurrentFilterNo"> No.</td>
        <td width="25%" align=center class="head11" id="Current_tpRange"> trigger port Range</td>
           <td width="25%" align=center class="head11" id="Current_opRange">open prot Range</td>
		   <td width="18%" align=center class="head11" id="Current_Protocol">Protocol</td>
          <td width="18%" align=center class="head11" id="Current_Comment"> Comment</td>
         </tr>
      <% showPortTriggerRulesASP(); %>
    </table>
  <table  cellpadding="2" cellspacing="1" width="450" id="del_rule_table" style="display:none">
<tbody>
<tr align="center">
  <td >
<input type="submit" style="width:110px;" value="Delete Selected" id="deleteSelRule" name="deleteSelRule" onClick="return deleteClick()">&nbsp;&nbsp;
<input type="reset" style="width:110px;" value="Reset" id="del_pt_Reset" name="del_pt_Reset"></td> 
</tr> 
</tbody>
</table>
<iframe name="hiddenframe1"  id="hiddenframe1" frameborder="0" border="0" style="display:none"></iframe>
</form>
</center>
</td></tr></tbody></table>
</BODY>
<script language="JavaScript" type="text/javascript">document.write('<div id="loading"  style="display:none;z-index:9999"><br><br><br><div align="center">'+_("Submitting settings")+
'</div><br><br>&nbsp;&nbsp;' +_("Submitting be patient")+'</div>');
</script>
</HTML>
