<html>
<head>
<title>Virtual Settings</title>
<META http-equiv="Content-Type" content="text/html; charset=gb2312">
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
<meta http-equiv="Pragma" content="no-cache">
<meta content="MSHTML 6.00.2800.1106" name="GENERATOR">
<link href="../style/normal_ws.css" type="text/css" rel="stylesheet">
<link href="../style/style.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../lang/b28n.js"></script>
<script type="text/javascript" src="../js/global_js.js"></script>
<script language="JavaScript" type="text/javascript">
parent.menu.setUnderFirmwareUpload(0);
var _singleton = 0;
Butterlate.setTextDomain("firewall");
var MAX_RULES = 32;
var rules_num =<% getPortForwardRuleNumsASP(); %> ;
function deleteClick()
{ 
  if (_singleton==1) return false;
       _singleton=1
  lastEnsure();
    return true;
}
/*
 function checkIpAddr(field)
{
    if(field.value == ""){
        alert(_("Please input IP address."));
        field.value = field.defaultValue;
        field.focus();
        return false;
    }

    if ( isAllNum_(field.value) == 0) {
        alert(_('It should be a [0-9] number.'));
        field.value = field.defaultValue;
        field.focus();
        return false;
    }

    if( (!checkRange(field.value,1,0,255)) ||
        (!checkRange(field.value,2,0,255)) ||
        (!checkRange(field.value,3,0,255)) ||
        (!checkRange(field.value,4,1,254)) ){
        alert(_('IP format error.'));
        field.value = field.defaultValue;
        field.focus();
        return false;
    }

   return true;
}

*/
function formCheck()
{  
   if(_singleton==1) return false;
	if(rules_num >= (MAX_RULES-1) ){
	 	alert(_("The rule number is exceeded")+MAX_RULES);
		return false;
	}

	/*if(!document.portForward.portForwardEnabled.options.selectedIndex){
		// user choose disable
		_singleton=1;
		parent.menu.setUnderFirmwareUpload(1);
		 document.getElementById("loading").style.display="block";
		setTimeout("top.view.location='../application/virtual_server.asp';",500);
		return true;
	}*/
	var form1 = document.forms[1];

    var ip_address_ = form1.ip_address.value.Trim() ;
	var fromPort_= form1.fromPort.value.Trim()  ;
	var toPort_	= form1.toPort.value.Trim()  ;  
	
 	var	protocol_ = form1.protocol.value.Trim() ; 
	var protocol_num;
switch(protocol_)
  {
   case "TCP":
   protocol_num=1;
   break;
   case "UDP":
   protocol_num=2;
   break;
   case "TCP&UDP":
   protocol_num=3;
   break;
   case "ICMP":
   protocol_num=4;
   break;
   case "None":
   protocol_num=5;
   break;
   }
		form1.ip_address.value = ip_address_;
		form1.fromPort.value = fromPort_;
		form1.toPort.value   = toPort_;
		
	if(	form1.ip_address.value == "" ||
		form1.fromPort.value == "" ||
		form1.toPort.value   == "")     //正确的，都不能为空。  
	{
		alert(_("Please input IP address and port range."));
		return false;
 	}
 	  // checkIpAddr_(sId, ismask, setfocus) // atoi,checkRange_,isAllNumAndSlash_[./0-9],isAllNum_[.0-9]
    if(!checkIpAddr_("ip_address",0,1) ){  
      //  alert("IP address format error."); 已有提示，多余
      // form1.ip_address.focus();
        return false;
    }
      
	// exam Port
	if(form1.fromPort.value == ""){
		alert(_("The start port of port range is invalid."));
		form1.fromPort.focus();
		return false;
	}

	if(isNumOnly( form1.fromPort.value ) == 0){
		alert(_("Invalid port number."));
		form1.fromPort.focus();
		return false;
	}

	d1 = atoi(form1.fromPort.value,1);
	if(d1 > 65535 || d1 < 1){
		alert(_("Invalid port number."));
		form1.fromPort.focus();
		return false;
	}
	
    if(form1.toPort.value=="")  form1.toPort.value = d1; //结束端口为空时，将以开始端口赋值
 	
	if(form1.toPort.value != ""){
		if(isNumOnly( form1.toPort.value ) == 0){
		alert(_("Invalid port number."));
			form1.toPort.focus();
			return false;
		}
		d2 = atoi(form1.toPort.value, 1);
		if(d2 > 65535 || d2 < 1){
			alert(_("Invalid port number."));
			form1.toPort.focus();
			return false;
		}
 		if(d1 > d2){
			alert(_("Invalid port range setting."));
			form1.fromPort.focus();
			return false;
		}
   }
  var PortForwardRules = "<%getCfgGeneral(1,"PortForwardRules");%>";
  if(PortForwardRules.length!=0)
  { 
  var cue_rules = new Array();
      cue_rules = PortForwardRules.split(";");
	  debug("当前规则：" + PortForwardRules +"数量："+ cue_rules.length);
	  for(var i =0 ; i < cue_rules.length ;i++)
	  {
	  debug("current Rule is"+cue_rules[i]);
	  
	  var cur_rule = new Array();
	    cur_rule = cue_rules[i].split(",");
	/*	debug("this rule have number of date is"+cur_rule.length);
		if(cur_rule[0]== ip_address_ ) debug(cur_rule[0]+"=="+ip_address_)
		else debug(cur_rule[0]+"!="+ip_address_)
		
		if(cur_rule[1]==fromPort_ ) debug(cur_rule[1]+"=="+fromPort_)
		else debug(cur_rule[1]+"!="+fromPort_)
		
		if(cur_rule[2]==toPort_ ) debug(cur_rule[2]+"=="+toPort_)
		else debug(cur_rule[2]+"!="+toPort_)
		
		if(cur_rule[3]==protocol_num ) debug(cur_rule[3]+"=="+protocol_num)
		else debug(cur_rule[3]+"!="+protocol_num)
		 */
 		if(cur_rule[0]== ip_address_ &&cur_rule[1]==fromPort_ &&cur_rule[2]==toPort_ &&cur_rule[3]==protocol_num )
		{ 
		 alert(_("There is a same rule in system."));
		 return false;
		 }
		
	   }
  }
    _singleton=1;
	 lastEnsure();
   return true;
}

function disableTextField (field)
{
  if(document.all || document.getElementById)
    field.disabled = true;
  else {
    field.oldOnFocus = field.onfocus;
    field.onfocus = skip;
   }
}

function enableTextField (field)
{
  if(document.all || document.getElementById)
    field.disabled = false;
  else
  {
    field.onfocus = field.oldOnFocus;
  }
}

function initTranslation()
{
	var e = document.getElementById("forwardTitle");
	e.innerHTML = _("forward title");
	e = document.getElementById("forwardIntroduction");
	e.innerHTML = _("forward introduction");
 	   
	e = document.getElementById("basic_seting_title");
	e.innerHTML = _("forward basic_seting_title");
	e = document.getElementById("basic_Apply");
	e.value = _("firewall apply");
	e = document.getElementById("basic_Reset");
	e.value = _("firewall reset");
	
	e = document.getElementById("forwardVirtualSrv");
	e.innerHTML = _("forward virtual server");
	e = document.getElementById("forwardVirtualSrvSet");
	e.innerHTML = _("forward virtual server setting");
	e = document.getElementById("forwardVirtualSrvDisable");
	e.innerHTML = _("firewall disable");
	e = document.getElementById("forwardVirtualSrvEnable");
	e.innerHTML = _("firewall enable");
	e = document.getElementById("forwardVirtualSrvIPAddr");
	e.innerHTML = _("forward virtual server ipaddr");
	e = document.getElementById("forwardVirtualSrvPortRange");
	e.innerHTML = _("forward virtual server port range");
	e = document.getElementById("forwardVirtualSrvProtocol");
	e.innerHTML = _("firewall protocol");
	e = document.getElementById("forwardVirtualSrvComment");
	e.innerHTML = _("firewall comment");
	e = document.getElementById("forwardVirtualSrvApply");
	e.value = _("firewall add");
	e = document.getElementById("forwardVirtualSrvReset");
	e.value = _("firewall reset");

	e = document.getElementById("forwardCurrentVirtualSrv");
	e.innerHTML = _("forward current virtual server");
	e = document.getElementById("forwardCurrentVirtualSrvNo");
	e.innerHTML = _("firewall no");
	e = document.getElementById("forwardCurrentVirtualSrvIP");
	e.innerHTML = _("forward virtual server ipaddr");
	e = document.getElementById("forwardCurrentVirtualSrvPort");
	e.innerHTML = _("forward virtual server port range");
	e = document.getElementById("forwardCurrentVirtualSrvProtocol");
	e.innerHTML = _("firewall protocol");
	e = document.getElementById("forwardCurrentVirtualSrvComment");
	e.innerHTML = _("firewall comment");
	e = document.getElementById("forwardCurrentVirtualSrvDel");
	e.value = _("firewall del select");
	e = document.getElementById("forwardCurrentVirtualSrvReset");
	e.value = _("firewall reset");
}

function updateState()
{  
    all_page_init();
	initTranslation();
	
    if(! rules_num )
    {
 		disableTextField(document.portForwardDelete.deleteSelPortForward);
 		disableTextField(document.portForwardDelete.reset);
	}
	else
	{
        enableTextField(document.portForwardDelete.deleteSelPortForward);
        enableTextField(document.portForwardDelete.reset);
	}

	if(document.getElementById("portForwardEnabled").options.selectedIndex == 1){
	document.getElementById("setting_table").style.display = display_on_table();
	document.getElementById("set_submit_table").style.display = display_on_table();
	document.getElementById("del_submit_table").style.display = display_on_table();
	document.getElementById("cur_rules_table").style.display = display_on_table();
	//document.getElementById("prompt_maximum").style.display = "";
		
	}else{
//	document.getElementById("prompt_maximum").style.display = "none";
	document.getElementById("setting_table").style.display = "none";
	document.getElementById("set_submit_table").style.display = "none";
	document.getElementById("del_submit_table").style.display = "none";
	document.getElementById("cur_rules_table").style.display = "none";
	}
}

function bs_formCheck() 
{ 
  lastEnsure();
 document.forms[0].submit();
 
// var portForward0 = document.getElementById("portForward0");
// portForward0.submit();
	//return true;
}

function lastEnsure()
{
  document.getElementById("portForwardEnabled").style.display="none";
  document.getElementById("protocol").style.display="none";
  document.getElementById("loading").style.display="block";
 setTimeout("top.view.location='../application/virtual_server.asp';",500);
 }
</script>
</head>
                         <!--     body      -->
<body onLoad="updateState()">

<h2 id="forwardTitle" class="btnl">&nbsp;</h2>
<% checkIfUnderBridgeModeASP(); %>
<table  width="100%" class="tintro" >
  <tbody>
     <tr>
      <td class="intro" id="forwardIntroduction" >&nbsp;</td>
      <td class ="image_col" align="right">
	      <script language="javascript" type="text/javascript" >
	    var lang =_("next");
	   if (lang == "Next") lang="en";
	  else lang="zh_cn";
	   var page_name = "virserver"
	    help_display(page_name,lang);
	  </script>
	  </td>
    </tr>
  </tbody>
</table>
<hr>

<table width="571" class="body" bordercolor="#9BABBD" >
  <tbody><tr><td width="563">
  <form method=post name="portForward" id="portForward0" action=/goform/portForward target="hiddenframe1">
<center>

<table border="1" cellpadding="2" cellspacing="1" width="450" bordercolor="#9BABBD">
<tbody>
<tr>
  <td class="title" colspan="2" id="basic_seting_title">&nbsp;</td>
</tr>
  <tr>
	<td class="head" id="forwardVirtualSrvSet">&nbsp;</td>
	<td class="value1">
	
	<select name="portForwardEnabled" id="portForwardEnabled" size="1" onChange="updateState()">
	<option value=0  <% getPortForwardEnableASP(0); %> id="forwardVirtualSrvDisable">Disable</option>
    <option value=1 <% getPortForwardEnableASP(1); %> id="forwardVirtualSrvEnable">Enable</option> 
	 </select>
    </td>
</tr>
</tbody>
</table>
<table  cellpadding="2" cellspacing="1" width="450" >
<tbody>
<tr align="center">
  <td >
    <input type="button"style="width:110px;"  value="Apply" id="basic_Apply" name="addFilterPort" onClick="bs_formCheck()"> &nbsp;&nbsp;
	<input type="reset"style="width:110px;"  value="Reset" id="basic_Reset" name="reset" onClick="reset_clicked()">
</td>
</tr>
</tbody></table>
<iframe name="hiddenframe1"  id="hiddenframe1" frameborder="0" border="0" style="display:none"></iframe>
</center>
</form>
<form method=post name="portForward" id="portForward1" action=/goform/portForward target="hiddenframe0">
<center>
<table border="1" cellpadding="2" cellspacing="1" width="450" bordercolor="#9BABBD" id="setting_table" style="display:none">
<tbody>
<tr>
  <td class="title" colspan="2" id="forwardVirtualSrv">&nbsp;</td>
</tr>
<tr><input type="hidden" name="portForwardEnabled" id="portForwardEnabled" value="1" />
	<td class="head" id="forwardVirtualSrvIPAddr">&nbsp;</td>
	<td class="value1">
  		<input type="text" size="16" name="ip_address" maxlength="15" id="ip_address">
	</td>
</tr>

<tr>
	<td class="head" id="forwardVirtualSrvPortRange">&nbsp;</td>
	<td class="value1">
  		<input type="text" size="5" maxlength="5" name="fromPort">-<input type="text" size="5" maxlength="5" name="toPort"> &nbsp;&nbsp;
	</td>
</tr>

<tr>
	<td class="head" id="forwardVirtualSrvProtocol">&nbsp;</td>
	<td class="value1">
		<select name="protocol" id="protocol">
   		<option select value="TCP&UDP">TCP&UDP</option>
		<option value="TCP">TCP</option>
   		<option value="UDP">UDP</option>
   		</select>
	</td>
</tr>

<tr>
	<td class="head" id="forwardVirtualSrvComment">&nbsp;</td>
	<td class="value1">
		<input type="text" name="comment" size="16" maxlength="32">
	</td>
</tr>
</tbody></table>

<table  cellpadding="2" cellspacing="1" width="450" id="set_submit_table"  style="display:none">
<tbody>
<tr align="left" id="prompt_maximum"><td id="maximum_rule"> 
<script>
	document.write("("+_("The maximum rule count is")+ MAX_RULES +".)");
</script>
</td>
</tr>
<tr align="center">
  <td >
    <input type="submit"style="width:110px;"  value="Apply" id="forwardVirtualSrvApply" name="addFilterPort" onClick="return formCheck()"> &nbsp;&nbsp;
	<input type="reset"style="width:110px;"  value="Reset" id="forwardVirtualSrvReset" name="reset">
</td>
</tr>
</tbody></table></center>
<iframe name="hiddenframe0"  id="hiddenframe0" frameborder="0" border="0" style="display:none"></iframe>
</form>

<!--  delete rules -->
<form action=/goform/portForwardDelete method=POST name="portForwardDelete" target="hiddenframe1">
<table border="1" cellpadding="2" cellspacing="1" width="550" bordercolor="#9BABBD" id="cur_rules_table"  style="display:none">
<tbody>
	<tr>
		<td class="title" colspan="5" id="forwardCurrentVirtualSrv">&nbsp;</td>
	</tr>
<tr>
	<td width="10%" align=center class="head11"  id="forwardCurrentVirtualSrvNo">&nbsp;</td>
	  <td width="25%"  align=center class="head11" id="forwardCurrentVirtualSrvIP">&nbsp;</td>
		<td width="25%"   align=center class="head11"  id="forwardCurrentVirtualSrvPort">&nbsp;</td>
		<td width="20%"  align=center  class="head11" id="forwardCurrentVirtualSrvProtocol">&nbsp;</td>
		<td width="20%"   align=center  class="head11" id="forwardCurrentVirtualSrvComment">&nbsp;</td>
	</tr>
   <% showPortForwardRulesASP(); %>
</tbody></table>
<table  cellpadding="2" cellspacing="1" width="552" id="del_submit_table"  style="display:none" >
<tbody>
<tr align="center">
  <td colspan="2">
<input type="submit" style="width:110px;" value="Delete Selected" id="forwardCurrentVirtualSrvDel" name="deleteSelPortForward" onClick="return deleteClick()">&nbsp;&nbsp;
<input type="reset" style="width:110px;" value="Reset" id="forwardCurrentVirtualSrvReset" name="reset" >
</td></tr>
</tbody></table>
<iframe name="hiddenframe1"  id="hiddenframe1" frameborder="0" border="0" style="display:none"></iframe>
</form>
</td></tr></tbody></table>
</body>
<script language="JavaScript" type="text/javascript">
document.write('<div id="loading"  style="display: none;z-index = 9999"><br><br><br><div align="center">'+_("Submitting settings")+
'</div><br><br>&nbsp;&nbsp;' +_("Submitting be patient")+'</div>');
</script>
</html>