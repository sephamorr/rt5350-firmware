 <HTML>
<HEAD><title> Wireless Security Settings</title>
<META http-equiv="Content-Type" content="text/html; charset=gb2312">
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
<meta http-equiv="Pragma" content="no-cache">
<meta content="MSHTML 6.00.2800.1106" name="GENERATOR">
<link href="../style/normal_ws.css" type="text/css" rel="stylesheet">
<link href="../style/style.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../lang/b28n.js"></script>
<script type="text/javascript" src="../js/global_js.js"></script>
<script language="JavaScript" type="text/javascript">

var http_request = false;
var _singleton = 0;
Butterlate.setTextDomain("wireless");
   
var changed = 0;
var old_MBSSID;
var defaultShownMBSSID = 0;
var SSID = new Array();
var PreAuth = new Array();
var AuthMode = new Array();
var EncrypType = new Array();
var DefaultKeyID = new Array();
var Key1Type = new Array();
var Key1Str = new Array();
var Key2Type = new Array();
var Key2Str = new Array();
var Key3Type = new Array();
var Key3Str = new Array();
var Key4Type = new Array();
var Key4Str = new Array();
var WPAPSK = new Array();
var RekeyMethod = new Array();
var RekeyInterval = new Array();
var PMKCachePeriod = new Array();
var IEEE8021X = new Array();
var RADIUS_Server = new Array();
var RADIUS_Port = new Array();
var RADIUS_Key = new Array();
var session_timeout_interval = new Array();
var AccessPolicy = new Array();
var AccessControlList = new Array();
function makeRequest(url, content, handler) {
	http_request = creat_http_request();
	http_request.onreadystatechange = handler;
	http_request.open('POST', url, true);
	http_request.send(content);
}

function securityHandler() {
	if (http_request.readyState == 4) {
		if (http_request.status == 200) {
			parseAllData(http_request.responseText);
			UpdateMBSSIDList();
			LoadFields(defaultShownMBSSID);
				// load Access Policy for MBSSID[selected]
			LoadAP();
			ShowAP(defaultShownMBSSID);
		} else {
		   	alert(_("There was a problem with the request."));
		}
	}
}
function parseAllData(str)
{
	var all_str = new Array();
	all_str = str.split("\n");
	defaultShownMBSSID = parseInt(all_str[0]);
	for (var i=0; i<all_str.length-2; i++) {
		var fields_str = new Array();
		fields_str = all_str[i+1].split("\r");
		SSID[i] = fields_str[0];
		PreAuth[i] = fields_str[1];
		AuthMode[i] = fields_str[2];
		EncrypType[i] = fields_str[3];
		DefaultKeyID[i] = fields_str[4];
		Key1Type[i] = fields_str[5];
		Key1Str[i] = fields_str[6];
		Key2Type[i] = fields_str[7];
		Key2Str[i] = fields_str[8];
		Key3Type[i] = fields_str[9];
		Key3Str[i] = fields_str[10];
		Key4Type[i] = fields_str[11];
		Key4Str[i] = fields_str[12];
		WPAPSK[i] = fields_str[13];
		RekeyMethod[i] = fields_str[14];
		RekeyInterval[i] = fields_str[15];
		PMKCachePeriod[i] = fields_str[16];
		IEEE8021X[i] = fields_str[17];
		RADIUS_Server[i] = fields_str[18];
		RADIUS_Port[i] = fields_str[19];
		RADIUS_Key[i] = fields_str[20];
		session_timeout_interval[i] = fields_str[21];
		AccessPolicy[i] = fields_str[22];
		AccessControlList[i] = fields_str[23];

		/* !!!! IMPORTANT !!!!*/
		if(IEEE8021X[i] == "1")
			AuthMode[i] = "IEEE8021X";
		if(AuthMode[i] == "OPEN" && EncrypType[i] == "NONE" && IEEE8021X[i] == "0")
			AuthMode[i] = "Disable";
	}
}

parent.menu.setUnderFirmwareUpload(0);
 //var MBSSID_MAX 		  = 1;	//= 8;
var cur_ssids = '<% getCfgGeneral(1,"BssidNum"); %>';
cur_ssids = parseInt(cur_ssids);
    debug("SSID quantity " + cur_ssids);
var ACCESSPOLICYLIST_MAX  = 64;
var ACCESS_POLICY = new Array();

for(var i = 0 ; i < cur_ssids; i++)
{   
debug("start the cycle for get ACCESS_POLICY[i]");
ACCESS_POLICY[i] = 0;
var access_list ;
	 switch(i)
	 {
	  case 0:
	 access_list = "<% getCfgGeneral(1,'AccessControlList0'); %>";
	 break;
	  case 1:
	 access_list = "<% getCfgGeneral(1,'AccessControlList1'); %>";
	 break;
	  case 2:
	 access_list = "<% getCfgGeneral(1,'AccessControlList2'); %>";
	 break;
	  case 3:
	 access_list = "<% getCfgGeneral(1,'AccessControlList3'); %>";
	 break;
	 }
  debug("AccessControlList"+i+"is"+access_list);
  if(access_list.length!=0)
  {
	  var Access_ = new Array();
		  Access_ = access_list.split(";")
		 var num = Access_.length ;
	  debug("AccessControlList"+i+"hava Access numbers is"+num);
		 ACCESS_POLICY[i] = num;
	 
  }
     debug(ACCESS_POLICY[i])
/*
	 if( AccessControlList[i].length != 0 )
	{ 
	 var Access_ = new Array();
	  Access_ = AccessControlList[i].split(";")
	 var num = Access.length ;
	 debug("while AccessControlList[i] is not empty")
	   debug(num);
	 ACCESS_POLICY[i] = num;
	}
	else{ ACCESS_POLICY[i] = 0;
	debug("while AccessControlList[i] is empty")
	debug(ACCESS_POLICY[i]);
	}*/
  }

 function checkMac(str){
	var len = str.length;
	if(len!=17)
		return false;

	for (var i=0; i<str.length; i++) {
		if((i%3) == 2){
			if(str.charAt(i) == ':')
				continue;
		}else{
			if (    (str.charAt(i) >= '0' && str.charAt(i) <= '9') ||
					(str.charAt(i) >= 'a' && str.charAt(i) <= 'f') ||
					(str.charAt(i) >= 'A' && str.charAt(i) <= 'F') )
			continue;
		}
		return false;
	}
	return true;
}

function checkRange(str, num, min, max)
{
    d = atoi(str,num);
    if(d > max || d < min)
        return false;
    return true;
}

function checkIpAddr(field)
{
    if(field.value == "")
        return false;

    if ( checkAllNum(field.value) == 0)
        return false;

    if( (!checkRange(field.value,1,0,255)) ||
        (!checkRange(field.value,2,0,255)) ||
        (!checkRange(field.value,3,0,255)) ||
        (!checkRange(field.value,4,1,254)) ){
        return false;
    }
   return true;
}


/*
function display_on_row()
{
	if (window.ActiveXObject) { // IE
		return "block";
	}
	else if (window.XMLHttpRequest) { // Mozilla, Safari,...
		return "table-row";
	}
}
*/


function deleteAccessPolicyListHandler()
{
if (http_request.readyState == 4) {
		if (http_request.status == 200) {
			window.location.reload(false);
		} else {
		   	alert(_("There was a problem with the request."));
		}
	}
	
}



function checkData()
{
	 var securitymode;
//	var ssid = document.security_form.Ssid.value;
	 securitymode = document.security_form.security_mode.value;
	 //securitymode == "OPEN" || securitymode == "SHARED" ||securitymode == "WEPAUTO"||
        if (securitymode == "WEP")
		{ 
		   	if(!check_Wep(securitymode) )
				return false;
		}              /* || security_mode == 5 */
	    else if(securitymode == "WPAPSK" || securitymode == "WPA2PSK" || securitymode == "WPAPSKWPA2PSK")
	   {
		    var keyvalue = document.security_form.passphrase.value;
			if (keyvalue.length == 0)
			{
			   alert(_("Please input wpapsk key!"));
				return false;
			}
			if (keyvalue.length < 8)
			{
				alert(_("Please input at least 8 character of wpapsk key!"));
				return false;
			}
			
			if(checkInjection(document.security_form.passphrase.value) == false)
			{
				alert(_("Invalid characters in Pass Phrase."));
				return false;
			}
		 	if(document.security_form.cipher[0].checked != true && 
		   document.security_form.cipher[1].checked != true &&
   		   document.security_form.cipher[2].checked != true)
		   {
			   alert(_("Please choose a WPA Algorithms."));
			   return false;
		   }

			if(checkAllNum(document.security_form.keyRenewalInterval.value) == false)
			{
				alert(_("Please input a valid key renewal interval."));
				return false;
			}
			if(document.security_form.keyRenewalInterval.value < 60)
			{
				alert(_("Warning: A short key renewal interval."));
						// return false;
			}
			if(check_wpa() == false)
				return false;
	  }
	else if (securitymode == "IEEE8021X") // 802.1x
	{
		if( document.security_form.ieee8021x_wep[0].checked == false &&
			document.security_form.ieee8021x_wep[1].checked == false)
			{
		     	alert(_("Please choose the 802.1x WEP option."));
				return false;
		    }
		if(check_radius() == false)
			return false;
	}
	else if (securitymode == "WPA" || securitymode == "WPA1WPA2") //     WPA or WPA1WP2 mixed mode
	{
		if(check_wpa() == false)
			return false;
		if(check_radius() == false)
			return false;
	}
	else if (securitymode == "WPA2") //         WPA2
	{
		if(check_wpa() == false)
			return false;
		if( document.security_form.PreAuthentication[0].checked == false &&
			document.security_form.PreAuthentication[1].checked == false)
		{
			alert(_("Please choose the Pre-Authentication options."));
			
			return false;
		}

		if(!document.security_form.PMKCachePeriod.value.length)
		{
			alert(_("Please input the PMK Cache Period."));
			
			return false;
		}
		if(check_radius() == false)
			return false;
	}

	// check Access Policy
	for(i=0; i<cur_ssids; i++)
	{
	     var newap_text_mac = combinMAC("mac0"+i,"newap_text_"+i);
		 debug("you mac is " + newap_text_mac);
        if( newap_text_mac!= "")
		{
			/*if(!checkMac(newap_text_mac))
			{
				alert(_("The mac address in Access Policy form is invalid."));
				return false;
			}*/  //changged at 2009.9.21 by cairong
		  if(!checkMac_("newap_text_" + i,false)) return false; //the arg is element's ID
		  	debug(AccessControlList[i]);
			if(AccessControlList[i].length!=0)
			if( !sameRuleCheck(AccessControlList[i],newap_text_mac) ) return false;
			/*if(AccessControlList[i].indexOf(newap_text_mac)!= -1)
			{
			alert(_("There is a same mac address in system ,please enter a new one."));
				return false;
			}*/
		}
	}
    
	return true;
}

function check_wpa()
{
		if(document.security_form.cipher[0].checked != true && 
		   document.security_form.cipher[1].checked != true &&
   		   document.security_form.cipher[2].checked != true){
   		   alert(_("Please choose a WPA Algorithms."));
   		   return false;
		}

		if(checkAllNum(document.security_form.keyRenewalInterval.value) == false){
			alert(_("Please input a valid key renewal interval."));
			return false;
		}
		if(document.security_form.keyRenewalInterval.value < 60){
			alert(_("Warning: A short key renewal interval."));
			// return false;
		}
		return true;
}

function check_radius()
{
	if(!document.security_form.RadiusServerIP.value.length){
		alert(_("Please input the radius server ip address."));
		return false;		
	}
	if(!document.security_form.RadiusServerPort.value.length){
		alert(_("Please input the radius server port number."));
		return false;		
	}
	if(!document.security_form.RadiusServerSecret.value.length){
		alert(_("Please input the radius server shared secret."));
		return false;		
	}

	if(checkIpAddr(document.security_form.RadiusServerIP) == false){
		alert(_("Please input a valid radius server ip address."));
		return false;		
	}
	if( (checkRange(document.security_form.RadiusServerPort.value, 1, 1, 65535)==false) || (checkAllNum(document.security_form.RadiusServerPort.value)==false)){
		alert(_("Please input a valid radius server port number."));
		return false;		
	}
	if(checkStrictInjection(document.security_form.RadiusServerSecret.value)==false){
		alert(_("The shared secret contains invalid characters."));
		return false;		
	}

	if(document.security_form.RadiusServerSessionTimeout.value.length){
		if(checkAllNum(document.security_form.RadiusServerSessionTimeout.value)==false){
			alert(_("Please input a valid session timeout number or u may left it empty."));
			return false;	
		}	
	}

	return true;
}

function securityMode(c_f)
{
	var security_mode;
	changed = c_f;
	hideWep();
	document.getElementById("div_wep_mode").style.visibility = "hidden";
	document.getElementById("div_wep_mode").style.display = "none";
	document.getElementById("div_security_shared_mode").style.visibility = "hidden";
	document.getElementById("div_security_shared_mode").style.display = "none";
	document.getElementById("div_wpa").style.visibility = "hidden";
	document.getElementById("div_wpa").style.display = "none";
	document.getElementById("div_wpa_algorithms").style.visibility = "hidden";
	document.getElementById("div_wpa_algorithms").style.display = "none";
	document.getElementById("wpa_passphrase").style.visibility = "hidden";
	document.getElementById("wpa_passphrase").style.display = "none";
	document.getElementById("wpa_key_renewal_interval").style.visibility = "hidden";
	document.getElementById("wpa_key_renewal_interval").style.display = "none";
	document.getElementById("wpa_PMK_Cache_Period").style.visibility = "hidden";
	document.getElementById("wpa_PMK_Cache_Period").style.display = "none";
	document.getElementById("wpa_preAuthentication").style.visibility = "hidden";
	document.getElementById("wpa_preAuthentication").style.display = "none";
	document.security_form.cipher[0].disabled = true;
	document.security_form.cipher[1].disabled = true;
	document.security_form.cipher[2].disabled = true;
	document.security_form.passphrase.disabled = true;
	document.security_form.keyRenewalInterval.disabled = true;
	document.security_form.PMKCachePeriod.disabled = true;
	document.security_form.PreAuthentication.disabled = true;
	// 802.1x
	document.getElementById("div_radius_server").style.visibility = "hidden";
	document.getElementById("div_radius_server").style.display = "none";
	document.getElementById("div_8021x_wep").style.visibility = "hidden";
	document.getElementById("div_8021x_wep").style.display = "none";
	document.security_form.ieee8021x_wep.disable = true;
	document.security_form.RadiusServerIP.disable = true;
	document.security_form.RadiusServerPort.disable = true;
	document.security_form.RadiusServerSecret.disable = true;	
	document.security_form.RadiusServerSessionTimeout.disable = true;
	document.security_form.RadiusServerIdleTimeout.disable = true;	
	security_mode = document.security_form.security_mode.value;

	if (security_mode == "OPEN" || security_mode == "SHARED" ||security_mode == "WEP"||security_mode == "WEPAUTO" )
	{
		showWep(security_mode);
	}else if (security_mode == "WPAPSK" || security_mode == "WPA2PSK" || security_mode == "WPAPSKWPA2PSK"){
		<!-- WPA -->
		document.getElementById("div_wpa").style.visibility = "visible";
		document.getElementById("div_wpa").style.display = display_on_table();
		
		document.getElementById("div_wpa_algorithms").style.visibility = "visible";
		document.getElementById("div_wpa_algorithms").style.display = display_on_row();
		document.security_form.cipher[0].disabled = false;
		document.security_form.cipher[1].disabled = false;

		// deal with TKIP-AES mixed mode
		if(security_mode == "WPAPSK" && document.security_form.cipher[2].checked)
			document.security_form.cipher[2].checked = false;
		// deal with TKIP-AES mixed mode
		if(security_mode == "WPA2PSK" || security_mode == "WPAPSKWPA2PSK")
			document.security_form.cipher[2].disabled = false;

		document.getElementById("wpa_passphrase").style.visibility = "visible";
		document.getElementById("wpa_passphrase").style.display = display_on_row();
		document.security_form.passphrase.disabled = false;

		document.getElementById("wpa_key_renewal_interval").style.visibility = "visible";
		document.getElementById("wpa_key_renewal_interval").style.display = display_on_row();
		document.security_form.keyRenewalInterval.disabled = false;
	}else if (security_mode == "WPA" || security_mode == "WPA2" || security_mode == "WPA1WPA2") //wpa enterprise
	{
		document.getElementById("div_wpa").style.visibility = "visible";
		document.getElementById("div_wpa").style.display = display_on_table();

		document.getElementById("div_wpa_algorithms").style.visibility = "visible";
		document.getElementById("div_wpa_algorithms").style.display = display_on_row();
		document.security_form.cipher[0].disabled = false;
		document.security_form.cipher[1].disabled = false;
		document.getElementById("wpa_key_renewal_interval").style.visibility = "visible";
		document.getElementById("wpa_key_renewal_interval").style.display = display_on_row();
		document.security_form.keyRenewalInterval.disabled = false;
	
		<!-- 802.1x -->
		/*document.getElementById("div_radius_server").style.visibility = "visible";
		document.getElementById("div_radius_server").style.display = display_on_row();
		document.security_form.RadiusServerIP.disable = false;
		document.security_form.RadiusServerPort.disable = false;
		document.security_form.RadiusServerSecret.disable = false;	
		document.security_form.RadiusServerSessionTimeout.disable = false;
		document.security_form.RadiusServerIdleTimeout.disable = false;	*/

		// deal with TKIP-AES mixed mode
		if(security_mode == "WPA" && document.security_form.cipher[2].checked)
			document.security_form.cipher[2].checked = false;
		// deal with TKIP-AES mixed mode
		if(security_mode == "WPA2"){
			document.security_form.cipher[2].disabled = false;
			document.getElementById("wpa_preAuthentication").style.visibility = "visible";
			document.getElementById("wpa_preAuthentication").style.display = display_on_row();
			document.security_form.PreAuthentication.disabled = false;
			document.getElementById("wpa_PMK_Cache_Period").style.visibility = "visible";
			document.getElementById("wpa_PMK_Cache_Period").style.display = display_on_row();
			document.security_form.PMKCachePeriod.disabled = false;
		}

		// deal with WPA1WPA2 mixed mode
		if(security_mode == "WPA1WPA2"){
			document.security_form.cipher[2].disabled = false;
		}

	}else if (security_mode == "IEEE8021X"){ // 802.1X-WEP
		/*document.getElementById("div_8021x_wep").style.visibility = "visible";
		document.getElementById("div_8021x_wep").style.display = display_on_row();

		document.getElementById("div_radius_server").style.visibility = "visible";
		document.getElementById("div_radius_server").style.display = display_on_row();
		document.security_form.ieee8021x_wep.disable = false;
		document.security_form.RadiusServerIP.disable = false;
		document.security_form.RadiusServerPort.disable = false;
		document.security_form.RadiusServerSecret.disable = false;	
		document.security_form.RadiusServerSessionTimeout.disable = false;*/
		//document.security_form.RadiusServerIdleTimeout.disable = false;
	}
}


function hideWep()
{
	document.getElementById("div_wep").style.visibility = "hidden";
	document.getElementById("div_wep").style.display = "none";
}
function showWep(mode)
{
	<!-- WEP -->
	document.getElementById("div_wep").style.visibility = "visible";
	document.getElementById("div_wep").style.display = display_on_table();
		
	if(mode == "WEP")
	{
 		document.getElementById("div_wep_mode").style.visibility = "visible";
 		document.getElementById("div_wep_mode").style.display = display_on_row();
 	}

//	if(mode == "SHARED"){
//		document.getElementById("div_security_shared_mode").style.visibility = "visible";
//		document.getElementById("div_security_shared_mode").style.display = display_on_row();
//	}
	//document.security_form.wep_auth_type.disabled = false;
}


function check_Wep(securitymode)
{
	var defaultid = document.security_form.wep_default_key.value;
	var key_input;

	if ( defaultid == 1 )
		var keyvalue = document.security_form.wep_key_1.value;
	else if (defaultid == 2)
		var keyvalue = document.security_form.wep_key_2.value;
	else if (defaultid == 3)
		var keyvalue = document.security_form.wep_key_3.value;
	else if (defaultid == 4)
		var keyvalue = document.security_form.wep_key_4.value;
//&&  (securitymode == "WEP" )|| securitymode == "SHARED" || securitymode == "OPEN" || securitymode == "WEPAUTO"
	if (keyvalue.length == 0 ){ // shared wep  || md5
		alert(_("Please input wep key")+defaultid+'!');
		return false;
	}

	var keylength = document.security_form.wep_key_1.value.length;
	if (keylength != 0)
	{
		if (document.security_form.WEP1Select.options.selectedIndex == 0)
		{
			if(keylength != 5 && keylength != 13) 
			{
				alert(_("Please input 5 or 13 characters of wep key1 !"));
				return false;
			}
			if(checkInjection(document.security_form.wep_key_1.value)== false)
			{
				alert(_("Wep key1 contains invalid characters."));
				return false;
			}
		}
		if (document.security_form.WEP1Select.options.selectedIndex == 1)
		{
			if(keylength != 10 && keylength != 26) 
			{
				alert(_("Please input 10 or 26 characters of wep key1 !"));
				return false;
			}
			if(checkHex(document.security_form.wep_key_1.value) == false)
			{
				alert(_("Invalid Wep key1 format!"));
				return false;
			}
		}
	}

	keylength = document.security_form.wep_key_2.value.length;
	if (keylength != 0){
		if (document.security_form.WEP2Select.options.selectedIndex == 0){
			if(keylength != 5 && keylength != 13) {
				alert(_("Please input 5 or 13 characters of wep key2 !"));
				return false;
			}
			if(checkInjection(document.security_form.wep_key_2.value)== false){
				alert(_("Wep key2 contains invalid characters."));
				return false;
			}			
		}
		if (document.security_form.WEP2Select.options.selectedIndex == 1){
			if(keylength != 10 && keylength != 26) {
				alert(_("Please input 10 or 26 characters of wep key2 !"));
				return false;
			}
			if(checkHex(document.security_form.wep_key_2.value) == false){
				alert(_("Invalid Wep key2 format!"));
				return false;
			}
		}
	}

	keylength = document.security_form.wep_key_3.value.length;
	if (keylength != 0){
		if (document.security_form.WEP3Select.options.selectedIndex == 0)
		{
			if(keylength != 5 && keylength != 13) {
				alert(_("Please input 5 or 13 characters of wep key3 !"));
				return false;
			}
			if(checkInjection(document.security_form.wep_key_3.value)== false)
			{
				alert(_("Wep key3 contains invalid characters."));
				return false;
			}
		}
		if (document.security_form.WEP3Select.options.selectedIndex == 1)
		{
			if(keylength != 10 && keylength != 26) 
			{
				alert(_("Please input 10 or 26 characters of wep key3 !"));
				return false;
			}
			if(checkHex(document.security_form.wep_key_3.value) == false)
			{
				alert(_("Invalid Wep key3 format!"));
				return false;
			}			
		}
	}

	keylength = document.security_form.wep_key_4.value.length;
	if (keylength != 0){
		if (document.security_form.WEP4Select.options.selectedIndex == 0)
		{
			if(keylength != 5 && keylength != 13) {
				alert(_("Please input 5 or 13 characters of wep key4 !"));
				return false;
			}
			if(checkInjection(document.security_form.wep_key_4.value)== false)
			{
				alert(_("Wep key4 contains invalid characters."));
				return false;
			}			
		}
		if (document.security_form.WEP4Select.options.selectedIndex == 1){
			if(keylength != 10 && keylength != 26) {
				alert(_("Please input 10 or 26 characters of wep key4 !"));
				return false;
			}

			if(checkHex(document.security_form.wep_key_4.value) == false){
				alert(_("Invalid Wep key4 format!"));
				return false;
			}			
		}
	}
	return true;
}
	
function submit_apply()
{
   if(_singleton==1) return false;   
	if (checkData() ==true) //
	{	   
		changed = 0;
        _singleton=1; 
		 parent.menu.setUnderFirmwareUpload(1);
		  alert_("security_form submit ");
		
 		if(document.getElementById("security_mode").options.selectedIndex==0)
		  {
		   page_select_hidden(100);
		   }
		else 
		 { 
		 
		  document.getElementById("loading").style.display="block"; 
		  document.getElementById("wep_mode").style.display="none";
		   document.getElementById("wep_default_key").style.display="none";
		  document.getElementById("security_shared_mode").style.display="none";
		  document.getElementById("security_mode").style.display="none";
		  document.getElementById("ssidIndex").style.display="none";
	 	}
	 setTimeout("parent.menu.setUnderFirmwareUpload(0);top.view.location='../wireless/security.asp';",25000);
	return true;
	 }
	 return false;  
}

function LoadFields(MBSSID)
{
	var result;
	// Security Policy
	sp_select = document.getElementById("security_mode");
	sp_select.options.length = 0;
    sp_select.options[sp_select.length] = new Option("Disable",	"Disable",	false, AuthMode[MBSSID] == "Disable");
  /*  sp_select.options[sp_select.length] = new Option("OPEN",	"OPEN",		false, AuthMode[MBSSID] == "OPEN");
    sp_select.options[sp_select.length] = new Option("SHARED",	"SHARED", 	false, AuthMode[MBSSID] == "SHARED");
	*/
    sp_select.options[sp_select.length] = new Option("WEP", "WEP",	false, AuthMode[MBSSID] == "OPEN"||AuthMode[MBSSID] == "SHARED"||AuthMode[MBSSID] == "WEPAUTO");
	
//sp_select.options[sp_select.length] = new Option("WPA",		"WPA",		false, AuthMode[MBSSID] == "WPA");
    sp_select.options[sp_select.length] = new Option("WPA-PSK", "WPAPSK",	false, AuthMode[MBSSID] == "WPAPSK");
//sp_select.options[sp_select.length] = new Option("WPA2",	"WPA2",		false, AuthMode[MBSSID] == "WPA2");
    sp_select.options[sp_select.length] = new Option("WPA2-PSK","WPA2PSK",	false, AuthMode[MBSSID] == "WPA2PSK");
    sp_select.options[sp_select.length] = new Option("WPA/WPA2-PSK","WPAPSKWPA2PSK",false, AuthMode[MBSSID] == "WPAPSKWPA2PSK");
//sp_select.options[sp_select.length] = new Option("WPA1WPA2","WPA1WPA2",	false, AuthMode[MBSSID] == "WPA1WPA2");
	/* 
	 * until now we only support 8021X WEP for MBSSID[0]
	 */
//	if(MBSSID == 0)
//	sp_select.options[sp_select.length] = new Option("802.1X",	"IEEE8021X",false, AuthMode[MBSSID] == "IEEE8021X");

	// WEP
	document.getElementById("WEP1").value = Key1Str[MBSSID];
	document.getElementById("WEP2").value = Key2Str[MBSSID];
	document.getElementById("WEP3").value = Key3Str[MBSSID];
	document.getElementById("WEP4").value = Key4Str[MBSSID];
	document.getElementById("WEP1Select").selectedIndex = (Key1Type[MBSSID] == "0" ? 1 : 0);
	document.getElementById("WEP2Select").selectedIndex = (Key2Type[MBSSID] == "0" ? 1 : 0);
	document.getElementById("WEP3Select").selectedIndex = (Key3Type[MBSSID] == "0" ? 1 : 0);
	document.getElementById("WEP4Select").selectedIndex = (Key4Type[MBSSID] == "0" ? 1 : 0);

	document.getElementById("wep_default_key").selectedIndex = parseInt(DefaultKeyID[MBSSID]) - 1 ;
	// SHARED && NONE
	if(AuthMode[MBSSID] == "SHARED" && EncrypType[MBSSID] == "NONE")
		document.getElementById("security_shared_mode").selectedIndex = 1;
	else
		document.getElementById("security_shared_mode").selectedIndex = 0;
		
    if(AuthMode[MBSSID] == "OPEN"||AuthMode[MBSSID] == "SHARED"||AuthMode[MBSSID] == "WEPAUTO")
	{
	    document.getElementById("wep_mode").value = AuthMode[MBSSID]; 
	}
	else
	{
	    document.getElementById("wep_mode").value = "OPEN"; 
	}
		 
 	// WPA
	document.security_form.cipher[0].checked = true;
	if(EncrypType[MBSSID] == "TKIP")
		document.security_form.cipher[0].checked = true;
	else if(EncrypType[MBSSID] == "AES")
		document.security_form.cipher[1].checked = true;
	else if(EncrypType[MBSSID] == "TKIPAES")
		document.security_form.cipher[2].checked = true;

	document.getElementById("passphrase").value = WPAPSK[MBSSID];
	document.getElementById("keyRenewalInterval").value = RekeyInterval[MBSSID];
	document.getElementById("PMKCachePeriod").value = PMKCachePeriod[MBSSID];
	//document.getElementById("PreAuthentication").value = PreAuth[MBSSID];
	if(PreAuth[MBSSID] == "0")
		document.security_form.PreAuthentication[0].checked = true;
	else
		document.security_form.PreAuthentication[1].checked = true;

	//802.1x wep
	if(IEEE8021X[MBSSID] == "1"){
		if(EncrypType[MBSSID] == "WEP")
			document.security_form.ieee8021x_wep[1].checked = true;
		else
			document.security_form.ieee8021x_wep[0].checked = true;
	}
	
	document.getElementById("RadiusServerIP").value = RADIUS_Server[MBSSID];
	document.getElementById("RadiusServerPort").value = RADIUS_Port[MBSSID];
	document.getElementById("RadiusServerSecret").value = RADIUS_Key[MBSSID];			
	document.getElementById("RadiusServerSessionTimeout").value = session_timeout_interval[MBSSID];
	securityMode(0);
}
/*
function display_on()
{
	if(window.ActiveXObject) 
	{ // IE
		return "block";
	}
	else if(window.XMLHttpRequest) 
	{ // Mozilla, Firefox, Safari,...
		return "table";
	}
}
*/
function ShowAP(MBSSID)
{
	var i;
	for( i = 0; i < cur_ssids; i++)
	{   
		document.getElementById("apselect_"+i).selectedIndex = AccessPolicy[i];
		document.getElementById("AccessPolicy_"+i).style.visibility = "hidden";
		document.getElementById("AccessPolicy_"+i).style.display = "none";
	}

	document.getElementById("AccessPolicy_"+MBSSID).style.visibility = "visible";
	document.getElementById("AccessPolicy_"+MBSSID).style.display = display_on_table();
	
}

function LoadAP()
{
	for(var i=0; i<SSID.length; i++)
	{
		var j=0;
		var aplist = new Array();
        aplist = AccessControlList[i].split(";");
		if(AccessControlList[i].length != 0)
		{			
			for(j=0; j<aplist.length; j++)
			{
				document.getElementById("newap_"+i+"_"+j).value = aplist[j];
			}
			// hide the lastest <td class="value1">
			if(j%2)
			{
				document.getElementById("newap_td_"+i+"_"+j).style.visibility = "hidden";
				document.getElementById("newap_td_"+i+"_"+j).style.display = "none";
				j++;
			}
				
			// hide <tr> left
			for( var k=0; k< aplist.length; k+=2)
			{
			
				document.getElementById("id_"+i+"_"+k).style.visibility = "visible";
				document.getElementById("id_"+i+"_"+k).style.display = display_on_row();
			}
		}
		/*for(; j<ACCESSPOLICYLIST_MAX; j+=2)
		{
			document.getElementById("id_"+i+"_"+j).style.visibility = "hidden";
			document.getElementById("id_"+i+"_"+j).style.display = "none";
		}*/
	}
}

function selectMBSSIDChanged()
{
	// check if any security settings changed
	if(changed){
		ret = confirm("Are you sure to ignore changed?");
		if(!ret){
			document.security_form.ssidIndex.options.selectedIndex = old_MBSSID;
			return false;
		}
		else
			changed = 0;
	}
   var selected = document.security_form.ssidIndex.options.selectedIndex;
   // backup for user cancel action
	old_MBSSID = selected;
    MBSSIDChange(selected);
}

/*
 * When user select the different SSID, this function would be called.
 */ 
function MBSSIDChange(selected)
{
	// load wep/wpa/802.1x table for MBSSID[selected]
	LoadFields(selected);
	// update Access Policy for MBSSID[selected]
	ShowAP(selected);
	// radio button special case
	WPAAlgorithms = EncrypType[selected];
	IEEE8021XWEP = IEEE8021X[selected];
	PreAuthentication = PreAuth[selected];
	changeSecurityPolicyTableTitle(SSID[selected]);
	// clear all new access policy list field
	for(i=0; i<cur_ssids; i++)
		document.getElementById("newap_text_"+i).value = "";
	return true;
}

function changeSecurityPolicyTableTitle(t)
{
	var title = document.getElementById("sp_title");
	title.innerHTML = "\"" + t + "\"";
}

function delap(mbssid, num)
{
//makeRequest("/goform/APDeleteAccessPolicyList",mbssid+","+num,deleteAccessPolicyListHandler);
/*document.getElementById("rule_Del").action = "/goform/APDeleteAccessPolicyList?" +'mbssid='+mbssid+'&num='+num;*/

//debug( document.getElementById("rule_Del").action )
//document.getElementById("rule_Del").method="post";
document.getElementById("mbssid").value=mbssid;
document.getElementById("num").value=num;
document.getElementById("rule_Del").submit();
setTimeout("parent.menu.setUnderFirmwareUpload(0);top.view.location='../wireless/security.asp';",2000);
}

function initTranslation()
{
	var e = document.getElementById("secureSelectSSID");
	e.innerHTML = _("secure select ssid");
	e = document.getElementById("secureSSIDChoice");
	e.innerHTML = _("secure ssid choice");
	e = document.getElementById("securityTitle");
	e.innerHTML = _("secure ssid title");
	e = document.getElementById("securityIntroduction");
	e.innerHTML = _("secure ssid introduction");
	//e = document.getElementById("sp_title");
	//e.innerHTML = _("secure security policy");
	e = document.getElementById("secureSecureMode");
	e.innerHTML = _("secure security mode");
	
	e = document.getElementById("wep_mode_td");
	e.innerHTML = _("security options");
	e = document.getElementById("secureEncrypType");
	e.innerHTML = _("secure encryp type");
	e = document.getElementById("secureEncrypTypeNone");
	e.innerHTML = _("wireless none");

	e = document.getElementById("secureWEP");
	e.innerHTML = _("secure wep");
	e = document.getElementById("secureWEPDefaultKey");
	e.innerHTML = _("secure wep default key");
	e = document.getElementById("secureWEPDefaultKey1");
	e.innerHTML = _("secure wep default key1");
	e = document.getElementById("secureWEPDefaultKey2");
	e.innerHTML = _("secure wep default key2");
	e = document.getElementById("secureWEPDefaultKey3");
	e.innerHTML = _("secure wep default key3");
	e = document.getElementById("secureWEPDefaultKey4");
	e.innerHTML = _("secure wep default key4");
	e = document.getElementById("secureWEPKey");
	e.innerHTML = _("secure wep key");
	e = document.getElementById("secureWEPKey1");
	e.innerHTML = _("secure wep key1");
	e = document.getElementById("secureWEPKey2");
	e.innerHTML = _("secure wep key2");
	e = document.getElementById("secureWEPKey3");
	e.innerHTML = _("secure wep key3");
	e = document.getElementById("secureWEPKey4");
	e.innerHTML = _("secure wep key4");
	
	e = document.getElementById("secreWPA");
	e.innerHTML = _("secure wpa");
	e = document.getElementById("secureWPAAlgorithm");
	e.innerHTML = _("secure wpa algorithm");
	e = document.getElementById("secureWPAPassPhrase");
	e.innerHTML = _("secure wpa pass phrase");
	e = document.getElementById("secureWPAKeyRenewInterval");
	e.innerHTML = _("secure wpa key renew interval");
	e = document.getElementById("secureWPAPMKCachePeriod");
	e.innerHTML = _("secure wpa pmk cache period");
	e = document.getElementById("secureWPAPreAuth");
	e.innerHTML = _("secure wpa preauth");
	e = document.getElementById("secureWPAPreAuthDisable");
	e.innerHTML = _("wireless disable");
	e = document.getElementById("secureWPAPreAuthEnable");
	e.innerHTML = _("wireless enable");
	
	/*e = document.getElementById("secure8021XWEP");
	e.innerHTML = _("secure 8021x wep");
	e = document.getElementById("secure1XWEP");
	e.innerHTML = _("secure 1x wep");
	e = document.getElementById("secure1XWEPDisable");
	e.innerHTML = _("wireless disable");
	e = document.getElementById("secure1XWEPEnable");
	e.innerHTML = _("wireless enable");
	
	e = document.getElementById("secureRadius");
	e.innerHTML = _("secure radius");
	e = document.getElementById("secureRadiusIPAddr");
	e.innerHTML = _("secure radius ipaddr");
	e = document.getElementById("secureRadiusPort");
	e.innerHTML = _("secure radius port");
	e = document.getElementById("secureRadiusSharedSecret");
	e.innerHTML = _("secure radius shared secret");
	e = document.getElementById("secureRadiusSessionTimeout");
	e.innerHTML = _("secure radius session timeout");
	e = document.getElementById("secureRadiusIdleTimeout");
	e.innerHTML = _("secure radius idle timeout");*/

	/*
	e = document.getElementById("secureAccessPolicy");
	e.innerHTML = _("secure access policy");
	e = document.getElementById("secureAccessPolicyCapable");
	e.innerHTML = _("secure access policy capable");
	e = document.getElementById("secureAccessPolicyCapableDisable");
	e.innerHTML = _("wireless disable");
	e = document.getElementById("secureAccessPolicyCapableAllow");
	e.innerHTML = _("wireless allow");
	e = document.getElementById("secureAccessPolicyCapableReject");
	e.innerHTML = _("wireless reject ");
	e = document.getElementById("secureAccessPolicyNew");
	e.innerHTML = _("secure access policy new");
	*/
	
	e = document.getElementById("secureApply");
	e.value = _("wireless apply");
	e = document.getElementById("secureCancel");
	e.value = _("wireless cancel");
}

function initAll()
{
	 initTranslation();	
makeRequest("/goform/wirelessGetSecurity", "n/a", securityHandler);
   all_page_init();
}

function UpdateMBSSIDList()
{
	document.security_form.ssidIndex.length = 0;
	for(var i=0; i<SSID.length; i++){
		var j = document.security_form.ssidIndex.options.length;
		document.security_form.ssidIndex.options[j] = new Option(SSID[i], i, false, false);
	}
	document.security_form.ssidIndex.options.selectedIndex = defaultShownMBSSID;
	old_MBSSID = defaultShownMBSSID;
	changeSecurityPolicyTableTitle(SSID[defaultShownMBSSID]);
}

function setChange(c){
	changed = c;
}

var WPAAlgorithms;
function onWPAAlgorithmsClick(type)
{
	if(type == 0 && WPAAlgorithms == "TKIP") return;
	if(type == 1 && WPAAlgorithms == "AES") return;
	if(type == 2 && WPAAlgorithms == "TKIPAES") return;
	setChange(1);
}

var IEEE8021XWEP;
function onIEEE8021XWEPClick(type)
{
	if(type == 0 && IEEE8021XWEP == false) return;
	if(type == 1 && IEEE8021XWEP == true) return;
	setChange(1);
}

var PreAuthentication;
function onPreAuthenticationClick(type)
{
	if(type == 0 && PreAuthentication == false) return;
	if(type == 1 && PreAuthentication == true) return;
	setChange(1);
}

</script>
</head>
<body onLoad="initAll()">
<h2 id="securityTitle"  class="btnl">&nbsp;</h2>
<table  width="100%" class="tintro" >
  <tbody>
     <tr>
      <td class="intro" id="securityIntroduction" >&nbsp;</td>
      <td class ="image_col" align="right">
	     <script language="javascript" type="text/javascript" >
	     var lang =_("next");
	   if (lang == "Next") lang="en";
	  else lang="zh_cn";
	   var page_name = "wlansec"
	    help_display(page_name,lang);
	  </script>
	  </td>
    </tr>
  </tbody>
</table>
<hr>
<table width="571" class="body" bordercolor="#9BABBD">
  <tbody><tr><td width="563"><center>
  
<form method="post" name="security_form" action="/goform/APSecurity" target="hiddenframe0"  >
<!-- ---------------------  MBSSID Selection  --------------------- -->
 <table border="1" cellpadding="2" cellspacing="1" width="450" bordercolor="#9BABBD">
<tbody>

<tr>
  <td class="title" colspan="2" id="secureSelectSSID">&nbsp;</td>
</tr>
  <tr>
    <td class="head" id="secureSSIDChoice">&nbsp;</td>
    <td class="value1">
      <select name="ssidIndex" size="1" onChange="selectMBSSIDChanged()" id="ssidIndex">
			<!-- ....Javascript will update options.... -->
      </select> 
    </td>
  </tr>
</tbody></table>
<br>
<table border="1" bordercolor="#9babbd" cellpadding="3" cellspacing="1" hspace="2" vspace="2" width="450">
  <tbody>
  <tr>
    <td class="title" colspan="2"><span id="sp_title"></span></td>
  </tr>
  <tr id="div_security_infra_mode" name="div_security_infra_mode"> 
    <td class="head" id="secureSecureMode">&nbsp;</td>
    <td class="value1">
      <select name="security_mode" id="security_mode" size="1" style="width:110px" onChange="securityMode(1)">
			<!-- ....Javascript "LoadFields will" update options.... -->
      </select>

    </td>
  </tr>
  
  <tr id="div_wep_mode" name="div_wep_mode" style="visibility: hidden;"> 
    <td class="head" id="wep_mode_td">&nbsp;</td>
    <td class="value1">
      <select name="wep_mode" id="wep_mode" size="1" >
		<option value=OPEN >OPEN   </option>
		<option value=SHARED >SHARED  </option>
		<option value=WEPAUTO >AUTO   </option>
      </select>

    </td>
	
  <tr id="div_security_shared_mode" name="div_security_shared_mode" style="visibility: hidden;"> 
    <td class="head" id="secureEncrypType">&nbsp;</td>
    <td class="value1">
      <select name="security_shared_mode" id="security_shared_mode" size="1" onChange="securityMode(1)">
		<option value=WEP selected="selected">WEP</option>
		<option value=None id="secureEncrypTypeNone">None</option>
      </select>

    </td>
  </tr>

</tbody></table>
<br>

<!-- WEP -->
<table id="div_wep" name="div_wep" border="1" bordercolor="#9babbd" cellpadding="3" cellspacing="1" hspace="2" vspace="2" width="450" style="visibility: hidden;">
  <tbody><tr> 
    <td class="title" colspan="4" id="secureWEP">&nbsp;</td>
  </tr>

  <tr> 
    <td class="head" colspan="2" id="secureWEPDefaultKey">Default Key</td>
    <td colspan="2" class="value1">
      <select name="wep_default_key" id="wep_default_key" size="1" onChange="setChange(1)">
	<option value="1" id="secureWEPDefaultKey1">Key 1  </option>
	<option value="2" id="secureWEPDefaultKey2">Key 2</option>
	<option value="3" id="secureWEPDefaultKey3">Key 3</option>
	<option value="4" id="secureWEPDefaultKey4">Key 4</option>
      </select>
    </td>
  </tr>
  
  <tr> 
    <td width="9%" rowspan="4" class="head1" id="secureWEPKey">WEP Keys</td>
    <td width="29%" class="head2" id="secureWEPKey1">WEP Key 1 :</td>
    <td width="43%" class="value1"><input name="wep_key_1" id="WEP1" maxlength="26" value="" onKeyUp="setChange(1)"></td>
    <td width="19%" class="value1"><select id="WEP1Select" name="WEP1Select" onChange="setChange(1)"> 
		<option value="1">ASCII</option>
		<option value="0">Hex</option>
		</select></td>
  </tr>

  <tr> 
    <td class="head2" id="secureWEPKey2">WEP Key 2 : </td>
    <td class="value1"><input name="wep_key_2" id="WEP2" maxlength="26" value="" onKeyUp="setChange(1)"></td>
    <td class="value1"><select id="WEP2Select" name="WEP2Select" onChange="setChange(1)">
		<option value="1">ASCII</option>
		<option value="0">Hex</option>
		</select></td>
  </tr>
  <tr> 
    <td class="head2" id="secureWEPKey3">WEP Key 3 : </td>
    <td class="value1"><input name="wep_key_3" id="WEP3" maxlength="26" value="" onKeyUp="setChange(1)"></td>
    <td class="value1"><select id="WEP3Select" name="WEP3Select" onChange="setChange(1)">
		<option value="1">ASCII</option>
		<option value="0">Hex</option>
		</select></td>
  </tr>
  <tr> 
    <td class="head2" id="secureWEPKey4">WEP Key 4 : </td>
    <td class="value1"><input name="wep_key_4" id="WEP4" maxlength="26" value="" onKeyUp="setChange(1)"></td>
    <td class="value1"><select id="WEP4Select" name="WEP4Select" onChange="setChange(1)">
		<option value="1">ASCII</option>
		<option value="0">Hex</option>
		</select></td>
  </tr>

</tbody></table>
<!-- <br /> -->

<!-- WPA -->
<table id="div_wpa" name="div_wpa" border="1" bordercolor="#9babbd" cellpadding="3" cellspacing="1" hspace="2" vspace="2" width="450" style="visibility: hidden;">

  <tbody><tr>
    <td class="title" colspan="2" id="secreWPA">WPA</td>
  </tr>
  <tr id="div_wpa_algorithms" name="div_wpa_algorithms" style="visibility: hidden;"> 
    <td class="head" id="secureWPAAlgorithm">WPA Algorithms</td>
    <td class="value1">
      <input name="cipher" id="cipher" value="0" type="radio" onClick="onWPAAlgorithmsClick(0)">TKIP &nbsp;
      <input name="cipher" id="cipher" value="1" type="radio" onClick="onWPAAlgorithmsClick(1)">AES &nbsp;
      <input name="cipher" id="cipher" value="2" type="radio" onClick="onWPAAlgorithmsClick(2)">AUTO&nbsp;    
	  </td>
  </tr>
  <tr id="wpa_passphrase" name="wpa_passphrase" style="visibility: hidden;">
    <td class="head" id="secureWPAPassPhrase">Pass Phrase</td>
    <td class="value1">
      <input name="passphrase" id="passphrase" size="28" maxlength="64" value="" onKeyUp="setChange(1)">
    </td>
  </tr>

  <tr id="wpa_key_renewal_interval" name="wpa_key_renewal_interval" style="visibility: hidden;">
    <td class="head" id="secureWPAKeyRenewInterval">Key Renewal Interval</td>
    <td class="value1">
      <input name="keyRenewalInterval" id="keyRenewalInterval" size="4" maxlength="4" value="3600" onKeyUp="setChange(1)"> seconds
    </td>
  </tr>

  <tr id="wpa_PMK_Cache_Period" name="wpa_PMK_Cache_Period" style="visibility: hidden;">
    <td class="head" id="secureWPAPMKCachePeriod">PMK Cache Period</td>
    <td class="value1">
      <input name="PMKCachePeriod" id="PMKCachePeriod" size="4" maxlength="4" value="" onKeyUp="setChange(1)"> minute
    </td>
  </tr>

  <tr id="wpa_preAuthentication" name="wpa_preAuthentication" style="visibility: hidden;">
    <td class="head" id="secureWPAPreAuth">Pre-Authentication</td>
    <td class="value1">
      <input name="PreAuthentication" id="PreAuthentication" value="0" type="radio" onClick="onPreAuthenticationClick(0)"><font id="secureWPAPreAuthDisable">Disable &nbsp;</font>
      <input name="PreAuthentication" id="PreAuthentication" value="1" type="radio" onClick="onPreAuthenticationClick(1)"><font id="secureWPAPreAuthEnable">Enable &nbsp;</font>
    </td>
  </tr>
</tbody></table>


<!-- 802.1x -->
<!-- WEP  -->
<table id="div_8021x_wep" name="div_8021x_wep" border="1" bordercolor="#9babbd" cellpadding="3" cellspacing="1" hspace="2" vspace="2" width="450" style="visibility: hidden;">
  <tbody>
  <tr>
    <td class="title" colspan="2" id="secure8021XWEP">802.1x WEP</td>
  </tr>
  <tr>
		<td class="head" id="secure1XWEP"> WEP </td>
		<td class="value1">
	      <input name="ieee8021x_wep" id="ieee8021x_wep" value="0" type="radio" onClick="onIEEE8021XWEPClick(0)">
		  <font id="secure1XWEPDisable">Disable &nbsp;</font>
    	  <input name="ieee8021x_wep" id="ieee8021x_wep" value="1" type="radio" onClick="onIEEE8021XWEPClick(1)">
		  <font id="secure1XWEPEnable">Enable</font>
		</td>
  </tr>
</tbody></table>

<br>
<table id="div_radius_server" name="div_radius_server" border="1" bordercolor="#9babbd" cellpadding="3" cellspacing="1" hspace="2" vspace="2" width="450" style="visibility: hidden; display:none">
<tbody>
   <tr>
    <td class="title" colspan="2" id="secureRadius">Radius Server</td>
   </tr>
    <tr> 
		<td bgcolor="#E8F8FF"  class="head" id="secureRadiusIPAddr"> IP Address </td>
		<td class="value1"> <input name="RadiusServerIP" id="RadiusServerIP" size="16" maxlength="32" value="" onKeyUp="setChange(1)"> </td>
	</tr>
    <tr> 
		<td bgcolor="#E8F8FF"  class="head" id="secureRadiusPort"> Port </td>
		<td class="value1"> <input name="RadiusServerPort" id="RadiusServerPort" size="5" maxlength="5" value="" onKeyUp="setChange(1)"> </td>
	</tr>
    <tr> 
		<td bgcolor="#E8F8FF"  class="head" id="secureRadiusSharedSecret"> Shared Secret </td>
		<td class="value1"> <input name="RadiusServerSecret" id="RadiusServerSecret" size="16" maxlength="64" value="" onKeyUp="setChange(1)"> </td>
	</tr>
    <tr> 
		<td bgcolor="#E8F8FF"  class="head" id="secureRadiusSessionTimeout"> Session Timeout </td>
		<td class="value1"> <input name="RadiusServerSessionTimeout" id="RadiusServerSessionTimeout" size="3" maxlength="4" value="0" onKeyUp="setChange(1)"> </td>
	</tr>
    <tr> 
		<td bgcolor="#E8F8FF"  class="head" id="secureRadiusIdleTimeout"> Idle Timeout </td>
		<td class="value1"> <input name="RadiusServerIdleTimeout" id="RadiusServerIdleTimeout" size="3" maxlength="4" value="" onKeyUp="setChange(1)" readonly> </td>
	</tr>

</tbody></table>
                                        <!--	AccessPolicy for mbssid 		-->
<script language="JavaScript" type="text/javascript">
var aptable;//MBSSID_MAX

for(aptable = 0; aptable < cur_ssids; aptable++){
	document.write(" <table id=AccessPolicy_"+ aptable +" border=1 bordercolor=#9babbd cellpadding=3 cellspacing=1 hspace=2 vspace=2 width=450>");
	document.write(" <tbody> <tr> <td class=title colspan=2 >"+_("secure access policy")+"</td></tr>");
	document.write(" <tr> <td bgcolor=#E8F8FF class=head >"+_("secure access policy capable")+"</td>");
	document.write(" <td class=value1> <select name=apselect_"+ aptable + " id=apselect_"+aptable+" size=1 onchange=\"setChange(1)\">");
	document.write(" <option value=0 >"+_("wireless disable")+"</option> <option value=1 >"+_("wireless allow")+"</option><option value=2 >"+_("wireless reject")+"</option></select> </td></tr>");
	var this_ssid_macs = ACCESS_POLICY[aptable] + 1;
	for(var i = 0; i < parseInt(this_ssid_macs/2); i++){
	      debug("MAC numbers is "+this_ssid_macs);
		input_name = "newap_"+ aptable +"_" + (2*i);
		td_name = "newap_td_"+ aptable +"_" + (2*i);

		document.write("<tr id=id_"+aptable+"_");
		document.write(i*2);
		document.write(" style=\"display:none;visibility:hidden;\"> <td id=");
		document.write(td_name);
		document.write(" class=value1 width=225> <input style=\"width: 30px;\" value=Del onclick=\"delap("+aptable+",");
		document.write(2*i);
		document.write(")\" type=button > <input id=");
		document.write(input_name);
		document.write(" size=16 maxlength=20 readonly></td>");

		input_name = "newap_" + aptable + "_" + (2*i+1);
		td_name = "newap_td_" + aptable + "_" + (2*i+1);
		document.write("      <td id=");
		document.write(td_name);
		document.write(" class=value1 width=225><input style=\"width: 30px;\" value=Del onclick=\"delap("+aptable+", ");
		document.write(2*i+1);
		document.write(")\" type=button> <input id=");
		document.write(input_name);
		document.write(" size=16 maxlength=20 readonly></td> </tr>");
	}

	document.write("<tr><td bgcolor=#E8F8FF class=head11  >"+_("secure access policy new")+"</td>");
	document.write("<td class=value1 ><input name=newap_text_"+aptable+" id=newap_text_"+aptable+" size=18 maxlength=17 type=hidden>	");
	 document.write('<input type="text" size="2" maxlength="2" name=mac0'+aptable+'       id="dhcpStaticMac0" onKeyUp=check_value_long(2,this,"dhcpStaticMac1")>');
document.write(':<input type="text" size="2" maxlength="2" name=mac0'+aptable+'    id="dhcpStaticMac1" onKeyUp=check_value_long(2,this,"dhcpStaticMac2")>');
document.write(':<input type="text" size="2" maxlength="2" name=mac0'+aptable+'   id="dhcpStaticMac2" onKeyUp=check_value_long(2,this,"dhcpStaticMac3")>');
		  document.write(':<input type="text" size="2" maxlength="2" name=mac0'+aptable+' id="dhcpStaticMac3" onKeyUp=check_value_long(2,this,"dhcpStaticMac4")>');
			 document.write(':<input type="text" size="2" maxlength="2" name=mac0'+aptable+' id="dhcpStaticMac4" onKeyUp=check_value_long(2,this,"dhcpStaticMac5")>');
			 document.write(':<input type="text" size="2" maxlength="2" name=mac0'+aptable+' id="dhcpStaticMac5" onKeyUp=check_value_long(2,this,"dhcpStaticMac5")>');
			 document.write("</td> </tr> </tbody></table>")
}
</script>


<table border="0" cellpadding="2" cellspacing="1" width="450">
  <tbody><tr align="center">
 
    <td width="450"><input name="button" type="submit" class="button" id="secureApply"  
	onClick="return submit_apply()" value="Apply">      &nbsp; &nbsp;
      <input class="button" value="Cancel" id="secureCancel" type="reset" onClick="reset_clicked()" >    </td></tr>
</tbody></table>
<iframe name="hiddenframe0"  id="hiddenframe0" frameborder="0" border="0" style="display:none"></iframe>
</form>
 <form   action="/goform/APDeleteAccessPolicyList" name="rule_Del" id="rule_Del" style="visibility:hidden; display:none" target="hiddenframe1" method="post" >
 <input type="hidden" name="mbssid" id="mbssid" value="" />
<input type="hidden" name="num" id="num" value=""  />
 <iframe name="hiddenframe1"  id="hiddenframe1" frameborder="0" border="0" style="display:none"></iframe></form> 
</center>
</td></tr></tbody></table>
</body>

<script language="JavaScript" type="text/javascript">document.write('<div id="loading"  style="display:none;z-index = 9999"><br><br><br><div align="center">'+_("Submitting settings")+
'</div><br><br>&nbsp;&nbsp;' +_("Submitting be patient")+'</div>');
</script>
</html>
 
