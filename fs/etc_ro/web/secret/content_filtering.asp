
   <HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=gb2312">
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
<meta http-equiv="Pragma" content="no-cache">
<meta content="MSHTML 6.00.2800.1106" name="GENERATOR">
<title>Content Filter Settings</title>
<link href="../style/normal_ws.css" type="text/css" rel="stylesheet">
<link href="../style/style.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../lang/b28n.js"></script>
<script type="text/javascript" src="/js/global_js.js"></script>
<script type="text/javascript" >
parent.menu.setUnderFirmwareUpload(0);
var _singleton = 0;
   Butterlate.setTextDomain("firewall");
  var URLFilterNum = 0;
  var HostFilterNum = 0;

function deleteClick()
{
 if(_singleton==1) return false;
  _singleton=1
	//parent.menu.setUnderFirmwareUpload(1);
 	document.getElementById("loading").style.display="block";
setTimeout("top.view.location='../secret/content_filtering.asp';",1000);
    return true;
}

function formCheck()
{ 
  if(_singleton==1) return false;
   _singleton=1
	//parent.menu.setUnderFirmwareUpload(1);
	//document.getElementById("loading").style.display="block";
 setTimeout("top.view.location='../secret/content_filtering.asp';",1000);
   return true;
}

function initTranslation()
{
	var e = document.getElementById("ContentFilterTitle");
	e.innerHTML = _("content filter title");
	e = document.getElementById("ContentFilterIntrodution");
	e.innerHTML = _("content filter introduction");
	
       e = document.getElementById("WebsContentFilterFilter");
	e.innerHTML = _("content filter webs content filter filter");
	e = document.getElementById("WebsContentFilterApply");
	e.value = _("content filter webs content filter apply");	
	e = document.getElementById("WebsContentFilterReset");
	e.value = _("content filter webs content filter reset");	
	
	e = document.getElementById("WebURLFilterCurrent");
	e.innerHTML = _("content filter webs url filter current");	
	e = document.getElementById("WebURLFilterNo");
	e.innerHTML = _("content filter webs url fitler No");	
	e = document.getElementById("WebURLFilterURL");
	e.innerHTML = _("content filter webs url fitler url");	
	e = document.getElementById("WebURLFilterDel");
	e.value = _("content filter webs url fitler del");
	e = document.getElementById("WebURLFilterReset");
	e.value = _("content filter webs url fitler reset");
	e = document.getElementById("WebURLFilterAddTitle");
	e.innerHTML = _("content filter webs url fitler add title");	
	e = document.getElementById("WebURLFilterAdd");
	e.value = _("content filter webs url fitler add");		
	e = document.getElementById("WebURLFilterReset1");
	e.value = _("content filter webs url fitler reset");		
       	
	e = document.getElementById("WebsHostFilterCurrent");
	e.innerHTML = _("content filter webs host fitler current");	
	e = document.getElementById("WebsHostFilterNo");
	e.innerHTML = _("content filter webs host fitler no");	
	e = document.getElementById("WebsHostFilterHost");
	e.innerHTML = _("content filter webs host fitler host");	
	e = document.getElementById("WebsHostFilterDel");
	e.value = _("content filter webs host fitler del");	
	e = document.getElementById("WebsHostFilterReset");
	e.value = _("content filter webs host fitler reset");	
	e = document.getElementById("WebsHostFilterAddTitle");
	e.innerHTML = _("content filter webs host fitler add title");	
	e = document.getElementById("WebsHostFilterKeyword");
	e.innerHTML = _("content filter webs host fitler keyword");	
	e = document.getElementById("WebsHostFilterAdd");
	e.value = _("content filter webs host fitler add");	
	e = document.getElementById("WebsHostFilterReset1");
	e.value = _("content filter webs host fitler reset");	

    e = document.getElementById("WebsContentFilter");
      e.innerHTML = _("content filter webs content filter");
	e = document.getElementById("WebURLFilterTitle");
	e.innerHTML = _("content filter webs URL filter title");		
	e = document.getElementById("WebsHostFilterTitle");
       e.innerHTML = _("content filter webs host fitler title");	
}

function updateState()
{
	initTranslation();
	if (document.webContentFilter.websFilterProxy.value == "1")
		document.webContentFilter.websFilterProxy.checked = true;
	if (document.webContentFilter.websFilterJava.value == "1")
		document.webContentFilter.websFilterJava.checked = true;
	if (document.webContentFilter.websFilterActivex.value == "1")
		document.webContentFilter.websFilterActivex.checked = true;
/*
	if (document.webContentFilter.websFilterCookies.value == "1")
		document.webContentFilter.websFilterCookies.checked = true;
*/ 
   all_page_init();
}

function webContentFilterClick()
{
	document.webContentFilter.websFilterProxy.value = document.webContentFilter.websFilterProxy.checked ? "1": "0";
	document.webContentFilter.websFilterJava.value = document.webContentFilter.websFilterJava.checked ? "1": "0";
	document.webContentFilter.websFilterActivex.value = document.webContentFilter.websFilterActivex.checked ? "1": "0";
//	document.webContentFilter.websFilterCookies.value = //document.webContentFilter.websFilterCookies.checked ? "1": "0";
 if(_singleton==1) return false;
   _singleton=1
	//parent.menu.setUnderFirmwareUpload(1);
	//document.getElementById("loading").style.display="block";
	setTimeout("top.view.location='../secret/content_filtering.asp';",1000);
	return true;
}

function deleteWebsURLClick()
{
  if(_singleton==1) return false;
   
	for(i=0; i< URLFilterNum; i++){
		var tmp = eval("document.websURLFilterDelete.DR"+i);
		if(tmp.checked == true){
		_singleton=1
	 //parent.menu.setUnderFirmwareUpload(1);
	//document.getElementById("loading").style.display="block";
		setTimeout("top.view.location='../secret/content_filtering.asp';",1000);
			return true;
			}
	}
	alert(_("Please select the rule to be deleted."));
	return false;
}

function AddWebsURLFilterClick()
{
  if(_singleton==1) return false;
  
  var all_str = "<% getCfgGeneral(1, "websURLFilters"); %>";
   var new_URLFilter = document.websURLFilter.addURLFilter.value;
   new_URLFilter = new_URLFilter.Trim();
	if(new_URLFilter == ""){
		alert(_("Please enter a URL filter."));
		document.websURLFilter.addURLFilter.value="";
		document.websURLFilter.addURLFilter.focus();
		return false;
	}
	
	if(all_str.length)
	{  
	 var entries = new Array();
		entries = all_str.split(";");
		for(i=0; i<entries.length; i++)
		{
			if(new_URLFilter==entries[i]) 
			{
			 alert(_("This filter is exist in system,plese enter a new one."));
			// document.websURLFilter.addURLFilter.value="";
		     document.websURLFilter.addURLFilter.focus();
			 return false;
			 }
		}

	}
	debug(new_URLFilter)
 	_singleton=1
	//parent.menu.setUnderFirmwareUpload(1);
	//document.getElementById("loading").style.display="block";
	setTimeout("top.view.location='../secret/content_filtering.asp';",1000);
	document.websURLFilter.submit();
	return true;
}

function deleteWebsHostClick()
{ 
    if(_singleton==1) return false;
  
	for(i=0; i< HostFilterNum; i++){
		var tmp = eval("document.websHostFilterDelete.DR"+i);
		if(tmp.checked == true){
		 _singleton=1
	//parent.menu.setUnderFirmwareUpload(1);
	//document.getElementById("loading").style.display="block";
		setTimeout("top.view.location='../secret/content_filtering.asp';",1000);
			return true;
			}
	}
	alert(_("Please select the rule to be deleted."));
	return false;
}

function AddWebsHostFilterClick()
{ 
 if(_singleton==1) return false;
 var new_HostFilter = document.websHostFilter.addHostFilter.value;
 var all_str = "<% getCfgGeneral(1, "websHostFilters"); %>";
 new_HostFilter = new_HostFilter.Trim()
	if(new_HostFilter == ""){
		alert(_("Please enter a host filter."));
		document.websHostFilter.addHostFilter.value="";
		document.websHostFilter.addHostFilter.focus();
		return false;
	}
 	
	if(all_str.length)
	{  
	 var entries = new Array();
		entries = all_str.split(";");
		for(i=0; i<entries.length; i++)
		{
			if(new_HostFilter==entries[i]) 
			{
			 alert(_("This filter is exist in system,plese enter a new one."));
			// document.websHostFilter.addHostFilter.value="";
		     document.websHostFilter.addHostFilter.focus();
			 return false;
			 }
		}

	}
	
	
	 _singleton=1
	//parent.menu.setUnderFirmwareUpload(1);
	//document.getElementById("loading").style.display="block";
	setTimeout("top.view.location='../secret/content_filtering.asp';",1000);
	document.websHostFilter.submit()
	return true;
}

function addafilter_Click()
{
 var url_add_table = document.getElementById("url_add_table");
var rows_num = url_add_table.rows.length;
var delButtonN = rows_num;
var newTr = url_add_table.insertRow(rows_num);
 newTr.id = "row"+rows_num;
//添加两列 
var newTd0 = newTr.insertCell(-1); 
var newTd1 = newTr.insertCell(-1); 
newTd0.className = "head1";
newTd1.className = "value1";
newTd0.innerHTML = 'URL'+rows_num; 
newTd1.innerHTML='<input name="addURLFilter" size="26" maxlength="32" type="text" > <input type="button" value="Row Del" name="row_del"'+delButtonN+' id="row_del"'+delButtonN+' onClick="cur_row_del('+rows_num+')" />'; 
}

function cur_row_del(cur_row)
{  
  var url_add_table = document.getElementById("url_add_table");
    var i
    var tdname
    var tdln
    var line=cur_row;
	debug(line)
    if (line>0)
	 { 
		 for (i=2;i<url_add_table.rows.length;i++) 
		 {
			  tdname=url_add_table.rows[i].id;
			  tdln=tdname.substring(3,tdname.length);
			  debug(tdln)
			  if (tdln==line) 
			  {
			   debug("删除此行")
			   url_add_table.deleteRow(i);
			   break;
			  }
			  
		 }
    }
 
}
</script>
</head>
                         <!--     body      -->

<BODY  onLoad="updateState()">
<h2 class="btnl" id="ContentFilterTitle"><!--Content Filter  Settings --> </h2>
<!--% checkIfUnderBridgeModeASP(); %-->
<table  width="100%" class="tintro">
  <tbody>
    <tr>
      <td class="intro" id="ContentFilterIntrodution" >  <!--You can setup Content Filter to restrict the improper content access--></td>
      <td class ="image_col" align="right">
           <script language="javascript" type="text/javascript" >
	    var lang =_("next");
	   if (lang == "Next") lang="en";
	  else lang="zh_cn";
	   var page_name = "contentfilter"
	    help_display(page_name,lang);
	  </script>
      </td>
    </tr>
  </tbody>
</table>  

  <hr>
  
  <table width="571" class="body" bordercolor="#9BABBD">
  <tbody><tr><td width="563" valign="top"><CENTER>
  <blockquote><fieldset>

  <div align="left">
      <legend align="left"><span class="legendTite" id="WebsContentFilter">Webs Content Filter</span></legend>
    </div>
	  <form action=/goform/webContentFilter method=POST name="webContentFilter" target="hiddenframe0">
 <table border="1" cellpadding="2" cellspacing="1" width="450" bordercolor="#9BABBD">
<tbody>
    <tr>
        <td colspan=3 class=title align="left" id="WebsContentFilterFilter"> Filter: </td>
      </tr>
      <tr>
        
        <td class=head11 ><input type=checkbox name=websFilterProxy value="<% getCfgZero(1, "websFilterProxy"); %>" />
          Proxy
		  <td class=head11 >
          <input type=checkbox name=websFilterJava value="<% getCfgZero(1, "websFilterJava"); %>" />
          Java
	    <td class=head11 >
          <input type=checkbox name=websFilterActivex value="<% getCfgZero(1, "websFilterActivex"); %>" />
          ActiveX
          <!--	<input type=checkbox name=websFilterCookies value="<% getCfgZero(1, "websFilterCookies"); %>" > Cookies  -->
        </td>
      </tr>
    </tbody></table>
<table  cellpadding="2" cellspacing="1" width="450" >
<tbody>
<tr align="center">
  <td colspan="2">
  
	<input type="submit" style="width:110px;" value="Apply" id="WebsContentFilterApply" name="addFilterPort" onClick=" return webContentFilterClick() "> &nbsp;&nbsp;
	<input type="reset" style="width:110px;" value="Reset" id="WebsContentFilterReset" name="reset">
</td>
</tr>
</tbody></table>
<iframe name="hiddenframe0"  id="hiddenframe0" frameborder="0" border="0" style="display:none"></iframe>
</form>
</fieldset>
</blockquote>
  <blockquote><fieldset>

<div align="left">
      <legend align="left"><span class="legendTite" id="WebURLFilterTitle" >Webs URL Filter Settings</span></legend>
    </div>
	<form action=/goform/websURLFilterDelete method=POST name="websURLFilterDelete" target="hiddenframe1" >
<table border="1" cellpadding="2" cellspacing="1" width="450" bordercolor="#9BABBD">
  <tbody>
    <tr>
      <td class="title" colspan="5" align="left" id="WebURLFilterCurrent">Current Webs URL Filters: </td>
    </tr>
    <tr>
      <td width="35%" class=head1 id="WebURLFilterNo"> No.</td>
      <td id="WebURLFilterURL"class=head11> URL</td>
    </tr>
    <script language="JavaScript" type="text/javascript">
	var i;
	var entries = new Array();
	var all_str = "<% getCfgGeneral(1, "websURLFilters"); %>";

	if(all_str.length){
		entries = all_str.split(";");
		for(i=0; i<entries.length; i++){
			document.write("<tr><td class=value1 >");
			document.write(i+1);
			document.write("<input type=checkbox name=DR"+i+"></td>");
	
			document.write("<td class=value1 >"+ entries[i] +"</td>");
			document.write("</tr>\n");
		}

		URLFilterNum = entries.length;
	}
	</script>
  </tbody>
</table>
<table  cellpadding="2" cellspacing="1" width="450" >
<tbody>
<tr align="center">
  <td colspan="2">
<input type="submit" style="width:110px;" value="Delete" id="WebURLFilterDel" name="deleteSelPortForward" onClick="return deleteWebsURLClick()">&nbsp;&nbsp;
<input type="reset" style="width:110px;" value="Reset" id="WebURLFilterReset" name="reset"></td>
</tr>
</tbody></table>
<iframe name="hiddenframe1"  id="hiddenframe1" frameborder="0" border="0" style="display:none"></iframe>
</form>

<form action=/goform/websURLFilter method=POST name="websURLFilter" target="hiddenframe2" >
<table border="1" cellpadding="2" cellspacing="1" width="450" bordercolor="#9BABBD" id="url_add_table">
<tbody>
	<tr>
		<td align="left" colspan="2" class="title" id="WebURLFilterAddTitle">Add a URL Filter:
		 
		  </td>  
	</tr>
	<tr>
		<td class=head1  align="left">URL: </td>
		<td class=value1><input name="addURLFilter" size="26" maxlength="32" type="text"> 
		 </td>
	</tr>
</tbody></table>
<table  cellpadding="2" cellspacing="1" width="450" >
<tbody>
<tr align="center">
  <td colspan="2">
<input type="button" style="width:110px;" value="Add" id="WebURLFilterAdd" name="addwebsurlfilter" onClick="return AddWebsURLFilterClick()">&nbsp;&nbsp;
<input type="reset" style="width:110px;" value="Reset" id="WebURLFilterReset1" name="reset">
</td>
</tr>
</tbody></table>
<iframe name="hiddenframe2"  id="hiddenframe2" frameborder="0" border="0" style="display:none"></iframe>
</form></fieldset></blockquote>

  <blockquote><fieldset>
   <div align="left">
      <legend align="left"><span class="legendTite" id="WebsHostFilterTitle" >Webs Host Filter Settings</span></legend>
    </div>
    <form action=/goform/websHostFilterDelete method=POST name="websHostFilterDelete" target="hiddenframe3">
	    <table border="1" cellpadding="2" cellspacing="1" width="450" bordercolor="#9BABBD">
<tbody>
	<tr>
		<td colspan="5" align="left" class="title" id="WebsHostFilterCurrent">Current Website Host Filters: </td>
	</tr>

	<tr>
		<td class="head1" id="WebsHostFilterNo" > No.</td>
		<td class="head11" id="WebsHostFilterHost"> Host(Keyword)</td>
	</tr>

	<script language="JavaScript" type="text/javascript">
	var i;
	var entries = new Array();
	var all_str = "<% getCfgGeneral(1, "websHostFilters"); %>";

	if(all_str.length){
		entries = all_str.split(";");

		for(i=0; i<entries.length; i++){
			document.write("<tr><td class=value1 >");
			document.write(i+1);
			document.write("<input type=checkbox name=DR"+i+"></td>");

			document.write("<td class=value1 >"+ entries[i] +"</td>");
			document.write("</tr>\n");
		}

		HostFilterNum = entries.length;
	}
	</script></tbody></table>
	

<table  cellpadding="2" cellspacing="1" width="450" >
<tbody>
<tr align="center">
  <td colspan="2">

<input type="submit" style="width:110px;" value="Delete" id="WebsHostFilterDel" name="deleteSelPortForward" onClick="return deleteWebsHostClick()">&nbsp;&nbsp;
<input type="reset" style="width:110px;" value="Reset" id="WebsHostFilterReset" name="reset">
</td>
</tr>
</tbody></table>
<iframe name="hiddenframe3"  id="hiddenframe3" frameborder="0" border="0" style="display:none"></iframe>

</form>

<form action=/goform/websHostFilter method=POST name="websHostFilter" target="hiddenframe4">

<table border="1" cellpadding="2" cellspacing="1" width="450" bordercolor="#9BABBD">
<tbody>	
	<tr>
		<td colspan="5" align="left" class="title" id="WebsHostFilterAddTitle">Add a Host(keyword) Filter: </td>
	</tr>
	<tr>
		<td class=head1  align="left" id="WebsHostFilterKeyword">Keyword: </td>
		<td class=value1 > <input name="addHostFilter" size="16" maxlength="32" type="text" > </td>
	</tr>
</tbody></table>
<table  cellpadding="2" cellspacing="1" width="450" >
<tbody>
<tr align="center">
  <td colspan="2">
<input type="button" style="width:110px;" value="Add" id="WebsHostFilterAdd" name="addwebscontentfilter" onClick="return AddWebsHostFilterClick()">&nbsp;&nbsp;
<input type="reset" style="width:110px;" value="Reset" id="WebsHostFilterReset1" name="reset">
</td>
</tr>
</tbody></table>
<iframe name="hiddenframe4"  id="hiddenframe4" frameborder="0" border="0" style="display:none"></iframe>
</form></fieldset></blockquote>
</CENTER>
</td></tr></table>
</body>
<script language="JavaScript" type="text/javascript">document.write('<div id="loading"  style="display: none;z-index = 9999"><br><br><br><div align="center">'+_("Submitting settings")+
'</div><br><br>&nbsp;&nbsp;' +_("Submitting be patient")+'</div>');
</script>
</html>
