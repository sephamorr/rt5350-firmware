<html>
<head>
<title>IP/Port Filtering Settings</title>
<META http-equiv="Content-Type" content="text/html; charset=gb2312">
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
<meta http-equiv="Pragma" content="no-cache">
<meta content="MSHTML 6.00.2800.1106" name="GENERATOR">
<link href="../style/normal_ws.css" type="text/css" rel="stylesheet">
<link href="../style/style.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="/lang/b28n.js"></script>
<script type="text/javascript" src="/js/global_js.js"></script>
<script language="JavaScript" type="text/javascript">
Butterlate.setTextDomain("firewall");
parent.menu.setUnderFirmwareUpload(0);
var _singleton = 0;
var MAX_RULES = 32;
var secs
var timerID = null
var timerRunning = false
var timeout = 3
var delay = 1000
var rules_num = <% getIPPortRuleNumsASP(); %>;
var IPPortFilterEnable = "<% getCfgGeneral(1,"IPPortFilterEnable");%>";
var IPPortFilterRules = "<% getCfgGeneral(1,"IPPortFilterRules");%>";

function lastEnsure()
{  
    parent.menu.setUnderFirmwareUpload(1);
    submittingDiv(2,true,"loading","top.view.location='../secret/port_filtering.asp';",1000,0);    //弹出提交等待时的div框
	 
//	setTimeout("top.view.location='../secret/port_filtering.asp';",1000);
}

function basicformCheck()
{   
   if(_singleton==1) return false;
     
	lastEnsure();
	_singleton=1;
	return true;
}

function InitializeTimer(){
	// Set the length of the timer, in seconds
	secs = timeout
	StopTheClock()
	StartTheTimer()
}

function StopTheClock(){
	if(timerRunning)
    	clearTimeout(timerID)
	timerRunning = false
}

function StartTheTimer(){
	if (secs==0){
		StopTheClock()
		timerHandler();
	  secs = timeout
		StartTheTimer()
    }else{
		self.status = secs
		secs = secs - 1
		timerRunning = true
		timerID = self.setTimeout("StartTheTimer()", delay)
	}
}

var http_request = false;
function makeRequest(url, content) {
    http_request = creat_http_request();
    http_request.onreadystatechange = alertContents;
    http_request.open('POST', url, true);
    http_request.send(content);
}

function alertContents() {
	if (http_request.readyState == 4) {
		if (http_request.status == 200) {
			updatePacketCount( http_request.responseText);
		} else {
			//alert('There was a problem with the request.');
		}
	}
}

function updatePacketCount(str){
	var pc = new Array();
	pc = str.split(" ");
	for(i=0; i < pc.length; i++){
		e = document.getElementById("pktCnt" + i);
		e.innerHTML = pc[i];
	}
}

function timerHandler(){
	makeRequest("/goform/getRulesPacketCount", "n/a");
}

function deleteClick()
{  
 if(_singleton==1) return false;
     _singleton=1;
	lastEnsure()
    return true;
}

function checkIpAddr(field) //不能用global_js.js中的函数，alert不一样。
{
	if(field.value == "")
		return false;

	if (isAllNumAndSlash_(field.value) == 0){
		return false;
	}

	var ip_pair = new Array();
	ip_pair = field.value.split("/");

	if(ip_pair.length > 2){
		return false;
	}

	if(ip_pair.length == 2){
		// sub mask
		if(!ip_pair[1].length)
			return false;
		if(!isNumOnly(ip_pair[1])){
			return false;
		}
		tmp = parseInt(ip_pair[1], 10);
		if(tmp < 0 || tmp > 32){
			return false;
		}
	}

    if( (!checkRange_(ip_pair[0],1,0,255)) ||
		(!checkRange_(ip_pair[0],2,0,255)) ||
		(!checkRange_(ip_pair[0],3,0,255)) ||
		(!checkRange_(ip_pair[0],4,0,254)) ){
		return false;
    }
	return true;
}

 
 
function checkMac(str)   //不能用global_js.js中的函数，
{
	var len = str.length;
	if(len!=17)
		return false;

	for (var i=0; i<str.length; i++) 
	{
		if((i%3) == 2)
		{
			if(str.charAt(i) == ':')
				continue;
		}else
		{
			if (    (str.charAt(i) >= '0' && str.charAt(i) <= '9') ||
					(str.charAt(i) >= 'a' && str.charAt(i) <= 'f') ||
					(str.charAt(i) >= 'A' && str.charAt(i) <= 'F') )
			continue;
		 }
		return false;
	}
	return true;
}
String.prototype.emptyToZero = function(){
  if(this == "")
   return "0";
   else 
   return this;
  }
function ipportFormCheck()
{ 
 	if(_singleton==1) return false;
	if(rules_num >= (MAX_RULES-1) )
	{
		alert(_("The rule number is exceeded")+ MAX_RULES +".");
		return false;
	}
	var sip_address_ = document.ipportFilter.sip_address.value.Trim();
	
	var sFromPort_ = document.ipportFilter.sFromPort.value.Trim();
	var sToPort_ = document.ipportFilter.sToPort.value.Trim();
	var dip_address_ = document.ipportFilter.dip_address.value.Trim();
	var dFromPort_ = document.ipportFilter.dFromPort.value.Trim();
	var dToPort_ = document.ipportFilter.dToPort.value.Trim();
    var protocol_ = document.ipportFilter.protocol.value;
	var  protocol_NUM;
	
  switch(protocol_)
  {
   case "None":
   protocol_NUM=5;
   break;
   case "TCP":
   protocol_NUM=1;
   break;
   case "UDP":
   protocol_NUM=2;
   break;
   case "ICMP":
   protocol_NUM=4;
   break;
   }
// debug("当前协议号："+protocol_NUM)
   
	document.ipportFilter.sip_address.value = sip_address_;
	document.ipportFilter.dip_address.value = dip_address_;
	document.ipportFilter.sFromPort.value = sFromPort_;
	document.ipportFilter.sToPort.value = sToPort_;
	document.ipportFilter.dFromPort.value = dFromPort_;
	document.ipportFilter.dToPort.value = dToPort_;
  
 	if( sip_address_ == "" && dip_address_ == "" &&	sFromPort_ == "" &&	dFromPort_ == "") //正确的
	{
		alert(_("Please input any IP or/and port value."));
		return false;
	}
/*	if(document.getElementById("mac_address").value != "")
	 {
			if(!checkMac(document.getElementById("mac_address").value))
			{
				alert(_("The mac address is invalid."));
				return false;
			}
		
	} 
	*/
	if(document.ipportFilter.sFromPort.value != "")
	{
		d1 = atoi(document.ipportFilter.sFromPort.value, 1);
		if(isNumOnly( document.ipportFilter.sFromPort.value ) == 0)
		{
			alert(_("Invalid port number: source port."));
			document.ipportFilter.sFromPort.focus();
			return false;
		}
		if(d1 > 65535 || d1 < 1)
		{
			alert(_("Invalid port number: source port."));
			document.ipportFilter.sFromPort.focus();
			return false;
		}
		
		if(document.ipportFilter.sToPort.value=="")  document.ipportFilter.sToPort.value = d1;  //结束端口为空时，将以开始端口赋值

 		if(document.ipportFilter.sToPort.value != "")
		{
			if(isNumOnly( document.ipportFilter.sToPort.value ) == 0)
			{
				alert(_("Invalid port number: source port."));
				return false;
			}		
			d2 = atoi(document.ipportFilter.sToPort.value, 1);
			if(d2 > 65535 || d2 < 1){
				alert(_("Invalid port number: source port."));
				return false;
			}
			if(d1 > d2){
			alert(_("Invalid source port range setting."));
			return false;
			}
		}
	}

	if(document.ipportFilter.dFromPort.value != "")
	{
		d1 = atoi(document.ipportFilter.dFromPort.value, 1);
		if(isNumOnly( document.ipportFilter.dFromPort.value ) == 0)
		{
			alert(_("Invalid port number: dest port."));
			return false;
		}
		if(d1 > 65535 || d1 < 1)
		{
			alert(_("Invalid port number: dest port."));
			return false;
		}
		
		if(document.ipportFilter.dToPort.value =="")  document.ipportFilter.dToPort.value = d1;  //结束端口为空时，将以开始端口赋值
		
		if(document.ipportFilter.dToPort.value != "")
		{
			if(isNumOnly( document.ipportFilter.dToPort.value ) == 0)
			{
				alert(_("Invalid port number: dest port."));
				return false;
			}		
			d2 = atoi(document.ipportFilter.dToPort.value, 1);
			if(d2 > 65535 || d2 < 1)
			{
				alert(_("Invalid port number: dest port."));
				return false;
			}
			if(d1 > d2)
			{
				alert(_("Invalid dest port range setting."));
				return false;
			}
		}
	}
	// check ip address format
	if(document.ipportFilter.sip_address.value != "")
	{
		if(!checkIpAddr(document.ipportFilter.sip_address) )
		{
			alert(_("Source IP address format error."));
			return false;
		}
    }
	
	if(document.ipportFilter.dip_address.value != "")
	{
		if(!checkIpAddr(document.ipportFilter.dip_address) )
		{
			alert(_("Dest IP address format error."));
			return false;
		}
    }
	
	var filter_rules_array = new Array();
	if(IPPortFilterRules.length != 0)
	{  
		sFromPort_ = sFromPort_.emptyToZero();
		sToPort_ = sToPort_.emptyToZero();
		dFromPort_ = dFromPort_.emptyToZero();
		dToPort_ = dToPort_.emptyToZero() ;
		filter_rules_array=IPPortFilterRules.split(";");
		debug("系统规则数："+filter_rules_array.length);
		for(var i = 0 ; i < filter_rules_array.length ; i++)
		{ 
			var cur_rule = new Array();
			var cur_rule = filter_rules_array[i].split(",");
		//	debug( filter_rules_array[i] );
		//	debug("规则中数据个数："+ cur_rule.length);
			//if(cur_rule.length==12)
			if(cur_rule[0]=="any/0" ) cur_rule[0] = "";
			if(cur_rule[4]=="any/0" ) cur_rule[4] = "";
		
			/*if( cur_rule[0] == sip_address_ )  debug(cur_rule[0] +"=="+ sip_address_)
			else debug(cur_rule[0] +"!="+ sip_address_)
			if( cur_rule[2] == sFromPort_  )   debug( cur_rule[2] +"==" +sFromPort_)
			else debug( cur_rule[2] +"!=" +sFromPort_)
			if( cur_rule[3] == sToPort_ )    debug(cur_rule[3]+"=="+sToPort_) 
			else  debug(cur_rule[3]+"!="+sToPort_) 
			if( cur_rule[4] == dip_address_ )	debug( cur_rule[4] +"=="+ dip_address_ )
			else debug( cur_rule[4] +"!="+ dip_address_ )
			if( cur_rule[6] == dFromPort_  )	 debug(cur_rule[6] +"==" +dFromPort_)
			else  debug(cur_rule[6] +"!=" +dFromPort_)
			if(  cur_rule[7] == dToPort_)	  debug(cur_rule[7]+"==" +dToPort_)
			else debug(cur_rule[7]+"!=" +dToPort_)
			if( cur_rule[8] == protocol_NUM )	   debug(cur_rule[8] +"=="+ protocol_NUM)
			else  debug(cur_rule[8] +"!="+ protocol_NUM)*/
			  
			if( cur_rule[0] == sip_address_ && cur_rule[2] == sFromPort_ && cur_rule[3] == sToPort_ && cur_rule[4] == dip_address_&& cur_rule[6] == dFromPort_ && cur_rule[7] == dToPort_ && cur_rule[8] == protocol_NUM )
			{
				alert(_("The same ip/port filter in system ,please enter a new one."));
				return false;
			}
		}
	}
	_singleton=1;
	lastEnsure()
 	return true;
}
function MACFormCheck()
{ 
if(_singleton==1) return false;
	if(rules_num >= (MAX_RULES-1) ){
		alert(_("The rule number is exceeded")+ MAX_RULES +".");
		return false;
	}
 
	//modyfy at 2009 9.21 by cairong
    	/* 
	if( document.MACFilter.mac_address.value == ""){
		alert(_("Please input a MAC address."));
		return false;
	   }

  if(document.MACFilter.mac_address.value != "")
	 {
		if(!checkMac(document.MACFilter.mac_address.value))
		{
		 alert(_("The mac address is invalid."));
			 return false;
		} 

	 	if( IPPortFilterRules.indexOf(document.MACFilter.mac_address.value)!=-1)
		{ 
		 alert(_("There is a same mac address in system ,please enter a new one."));
				return false;
		}
	}*/
	  if(combinMAC("mac1","mac_address") == ""){
		alert(_("Please input a MAC address."));
		return false;
	   }
	   
	 if(!checkMac_("mac_address",false))
	   return false;	
	   
     if( !sameRuleCheck(IPPortFilterRules,document.MACFilter.mac_address.value) )       return false;	
	
    _singleton=1;
	lastEnsure()
	return true;
}


function display_on()
{
  if(window.XMLHttpRequest){ // Mozilla, Firefox, Safari,...
    return "table-row";
  } else if(window.ActiveXObject){ // IE
    return "block";
  }
}

function initTranslation()
{
	var e = document.getElementById("portTitle");
	e.innerHTML = _("port title");
	e = document.getElementById("portIntroduction");
	e.innerHTML = _("port introduction");

    e = document.getElementById("MacFilterSet");
	e.innerHTML = _("port MacFilterSet");
	e = document.getElementById("portBasicSet");
	e.innerHTML = _("port basic setting");
	e = document.getElementById("portBasicFilter");
	e.innerHTML = _("port basic filter");
	e = document.getElementById("portBasicDisable");
	e.innerHTML = _("firewall disable");
	e = document.getElementById("portBasicEnable");
	e.innerHTML = _("firewall enable");
	e = document.getElementById("portBasicDefaultPolicy");
	e.innerHTML = _("port basic default policy");
	e = document.getElementById("portBasicDefaultPolicyAccept");
	e.innerHTML = _("port basic default policy accepted");
	e = document.getElementById("portBasicDefaultPolicyDrop");
	e.innerHTML = _("port basic default policy dropped");
	e = document.getElementById("portBasicApply");
	e.value = _("firewall apply");
	e = document.getElementById("portBasicReset");
	e.value = _("firewall reset");

	e = document.getElementById("portFilterSet");
	e.innerHTML = _("port filter setting");
	e = document.getElementById("portFilterMac");
	e.innerHTML = _("port filter macaddr");
	e = document.getElementById("portFilterSIPAddr");
	e.innerHTML = _("port filter source ipaddr");
	e = document.getElementById("portFilterSPortRange");
	e.innerHTML = _("port filter source port range");
	e = document.getElementById("portFilterDIPAddr");
	e.innerHTML = _("port filter dest ipaddr");
	e = document.getElementById("portFilterDPortRange");
	e.innerHTML = _("port filter dest port range");
	e = document.getElementById("portFilterProtocol");
	e.innerHTML = _("firewall protocol");
	e = document.getElementById("portFilterAction");
	e.innerHTML = _("port filter action");
	e = document.getElementById("MACFilterAction");
	e.innerHTML = _("port filter action");
	e = document.getElementById("portFilterActionDrop");
	e.innerHTML = _("port filter action drop");
	e = document.getElementById("portFilterActionAccept");
	e.innerHTML = _("port filter action accept");
	e = document.getElementById("MACFilterActionDrop");
	e.innerHTML = _("port filter action drop");
	e = document.getElementById("MACFilterActionAccept");
	e.innerHTML = _("port filter action accept");
	e = document.getElementById("portFilterComment");
	e.innerHTML = _("firewall comment");
	e = document.getElementById("MACFilterComment");
	e.innerHTML = _("firewall comment");
	e = document.getElementById("portFilterApply");
	e.value = _("firewall add");
	e = document.getElementById("portFilterReset");
	e.value = _("firewall reset");
e = document.getElementById("MACFilterApply");
	e.value = _("firewall add");
	e = document.getElementById("MACFilterReset");
	e.value = _("firewall reset");
	e = document.getElementById("portCurrentFilter");
	e.innerHTML = _("port current filter");
	e = document.getElementById("portCurrentFilterNo");
	e.innerHTML = _("firewall no");
	e = document.getElementById("portCurrentFilterSIP");
	e.innerHTML = _("port filter source ipaddr");
	e = document.getElementById("portCurrentFilterSPort");
	e.innerHTML = _("port filter source port range");
	e = document.getElementById("portCurrentFilterDIP");
	e.innerHTML = _("port filter dest ipaddr");
	e = document.getElementById("portCurrentFilterMac");
	e.innerHTML = _("port filter macaddr");
	e = document.getElementById("portCurrentFilterDPort");
	e.innerHTML = _("port filter dest port range");
	e = document.getElementById("portCurrentFilterProtocol");
	e.innerHTML = _("firewall protocol");
	e = document.getElementById("portCurrentFilterAction");
	e.innerHTML = _("port filter action");
	e = document.getElementById("portCurrentFilterPacketCount");
	e.innerHTML = _("port filter packetcount");
	e = document.getElementById("portCurrentFilterComment");
	e.innerHTML = _("firewall comment");
	e = document.getElementById("portCurrentFilterDel");
	e.value = _("firewall del select");
	e = document.getElementById("portCurrentFilterReset");
	e.value = _("firewall reset");

	if(document.getElementById("portCurrentFilterDefaultDrop")){
		e = document.getElementById("portCurrentFilterDefaultDrop");
		e.innerHTML = _("firewall default drop");
	}
	if(document.getElementById("portCurrentFilterDefaultAccept")){
		e = document.getElementById("portCurrentFilterDefaultAccept");
		e.innerHTML = _("firewall default accept");
	}

	var i=0;
	while( document.getElementById("portFilterActionDrop"+i) ||
			document.getElementById("portFilterActionAccept"+i) ){
		if(document.getElementById("portFilterActionDrop"+i)){
			e = document.getElementById("portFilterActionDrop"+i);
			e.innerHTML = _("port filter action drop");
		}

		if(document.getElementById("portFilterActionAccept"+i)){
			e = document.getElementById("portFilterActionAccept"+i);
			e.innerHTML = _("port filter action accept");
		}

		i++;
	}
}


function defaultPolicyChanged()
{
	if( document.BasicSettings.defaultFirewallPolicy.options.selectedIndex == 0){
		document.ipportFilter.action.options.selectedIndex = 0;
		document.MACFilter.action.options.selectedIndex = 0;
	 }else
	 {
	    document.ipportFilter.action.options.selectedIndex = 1;
	    document.MACFilter.action.options.selectedIndex = 1;
     }
}
	
function init()
{    
	 initTranslation();
	 all_page_init();
	document.BasicSettings.portFilterEnabled.options.selectedIndex = IPPortFilterEnable*1;
     
	  
    if(! rules_num ){
 		disableTextField(document.ipportFilterDelete.deleteSelFilterPort);
 		disableTextField(document.ipportFilterDelete.reset);
	}else{
        enableTextField(document.ipportFilterDelete.deleteSelFilterPort);
        enableTextField(document.ipportFilterDelete.reset);
	}

	if( document.BasicSettings.defaultFirewallPolicy.options.selectedIndex == 0){
		document.ipportFilter.action.options.selectedIndex = 0;
		document.MACFilter.action.options.selectedIndex = 0;
	}else
		{document.ipportFilter.action.options.selectedIndex = 1;
		document.MACFilter.action.options.selectedIndex = 1;
     }
 updateState();
 protocolChange();
}

function updateState()
{ 
      document.getElementById("set_table").style.visibility= "hidden";
	  document.getElementById("set_table").style.display= "none"; 
	  document.getElementById("set_table_MAC").style.visibility= "hidden";
	  document.getElementById("set_table_MAC").style.display= "none"; 
	  document.getElementById("Default_Policy").style.visibility= "hidden";
	  document.getElementById("Default_Policy").style.display="none";   
	  document.getElementById("rules").style.visibility= "hidden";
	  document.getElementById("rules").style.display= "none"; 
	  document.getElementById("delete_rule_button").style.visibility= "hidden";
	  document.getElementById("delete_rule_button").style.display= "none";  
	  document.getElementById("delete_rule_button1").style.visibility= "hidden";
	  document.getElementById("delete_rule_button1").style.display= "none";  
	if( document.BasicSettings.portFilterEnabled.options.selectedIndex == 1)
		{ // update packet count
		  InitializeTimer();
		  document.getElementById("set_table").style.visibility= "visible";
		  document.getElementById("set_table").style.display= style_display_on(); 
		  document.getElementById("set_table_MAC").style.visibility= "visible";
		  document.getElementById("set_table_MAC").style.display= style_display_on(); 
		  
		//  document.getElementById("emp_row").style.visibility= "visible";
	    //   document.getElementById("emp_row").style.display= display_on();  
		  document.getElementById("Default_Policy").style.visibility= "visible";
		  document.getElementById("Default_Policy").style.display=style_display_on();  
		  document.getElementById("rules").style.visibility= "visible";
		  document.getElementById("rules").style.display= style_display_on();  
		  document.getElementById("delete_rule_button").style.visibility= "visible";
		  document.getElementById("delete_rule_button").style.display= style_display_on();
		  document.getElementById("delete_rule_button1").style.visibility= "visible";
		  document.getElementById("delete_rule_button1").style.display= style_display_on();    
		  
	}
}

function style_display_on()
{
	if(window.ActiveXObject) 
	{ // IE
		return "block";
	}
	else if(window.XMLHttpRequest) 
	{ // Mozilla, Firefox, Safari,...
		return "table";
	}
}

function display_on()
{
	if(window.ActiveXObject) 
	{ // IE
		return "block";
	}
	else if(window.XMLHttpRequest) 
	{ // Mozilla, Firefox, Safari,...
		return "table-row";
	}
}

function actionChanged()
{
	if( document.BasicSettings.defaultFirewallPolicy.options.selectedIndex != 
		document.ipportFilter.action.options.selectedIndex || document.BasicSettings.defaultFirewallPolicy.options.selectedIndex != 
		document.MACFilter.action.options.selectedIndex)
		alert(_("The action of this rule would be the same with default policy."));
}

function protocolChange()
{
	if( document.ipportFilter.protocol.options.selectedIndex == 1 ||
		document.ipportFilter.protocol.options.selectedIndex == 2){
		document.ipportFilter.dFromPort.disabled = false;
		document.ipportFilter.dToPort.disabled = false;
		document.ipportFilter.sFromPort.disabled = false;
		document.ipportFilter.sToPort.disabled = false;
	}else{
		document.ipportFilter.dFromPort.disabled = true;
		document.ipportFilter.dToPort.disabled = true;
		document.ipportFilter.sFromPort.disabled = true;
		document.ipportFilter.sToPort.disabled = true;

		    document.ipportFilter.dFromPort.value ="" ;
			document.ipportFilter.dToPort.value ="" ;
			document.ipportFilter.sFromPort.value = "";
			document.ipportFilter.sToPort.value = "";
	}
}

</script>
</head>
<!--     body      -->
<body onLoad="init()">
<h2 id="portTitle" class="btnl">
  <!--MAC/IP/Port Filtering Settings-->
</h2>
<% checkIfUnderBridgeModeASP(); %>
<table  width="100%" class="tintro" >
  <tbody>
    <tr>
      <td class="intro" id="portIntroduction" ><!--You may setup firewall rules to protect your network from virus,worm and malicious activity on the Internet-->
      </td>
      <td class ="image_col" align="right"><script language="javascript" type="text/javascript" >
	   var lang =_("next");
	   if (lang == "Next") lang="en";
	  else lang="zh_cn";
	   var page_name = "basicfiter"
	    help_display(page_name,lang);
	  </script>
      </td>
    </tr>
  </tbody>
</table>
<hr>
<table width="571" class="body" bordercolor="#9BABBD">
  <tbody>
    <tr>
      <td width="563"><center>
          <!-- ====================   BASIC  form  ==================== -->
          <form method=post name="BasicSettings" action=/goform/BasicSettings target="hiddenframe" style="margin:0">
            <table width="450"  cellpadding="2" cellspacing="1"  border="1" bordercolor="#9BABBD">
              <tbody>
                <tr>
                  <td class="title" colspan="2" id="portBasicSet"></td>
                </tr>
                <tr style="border:1">
                  <td  class="head" id="portBasicFilter"></td>
                  <td class=value1 ><select  name="portFilterEnabled" size="1" id="portFilterEnabled" onChange="updateState()">
                      <option value=0  id="portBasicDisable">Disable&nbsp;</option>
                      <option value=1  id="portBasicEnable">Enable</option>
                    </select>
                  </td>
                </tr>
              </tbody>
            </table>
            <table style="display:none;visibility:hidden; margin-top:20px"  id="Default_Policy" width="450"  cellpadding="2" cellspacing="1" border="1" bordercolor="#9BABBD">
              <tbody>
                <tr>
                  <td class="head" id="portBasicDefaultPolicy"> Default Policy
                    <!--The packet that don't match with any rules would be:-->
                  </td>
                  <td class=value1 ><select onChange="defaultPolicyChanged()" name="defaultFirewallPolicy">
                      <option value=0 <% getDefaultFirewallPolicyASP(0); %> id="portBasicDefaultPolicyAccept">Accepted</option>
                      <option value=1 <% getDefaultFirewallPolicyASP(1); %> id="portBasicDefaultPolicyDrop">Dropped</option>
                    </select>
                  </td>
                </tr>
              </tbody>
            </table>
            <table  cellpadding="2" cellspacing="1" width="397" >
              <tr align="center">
                <td><input type="submit" style="width:110px;" value="Apply" id="portBasicApply" name="addDMZ" onClick="return basicformCheck()">
                  &nbsp;&nbsp;
                  <input type="button" style="width:110px;" value="Reset" id="portBasicReset" name="reset" onClick="reset_clicked()">
                </td>
              </tr>
            </table>
            <iframe name="hiddenframe"  id="hiddenframe" frameborder="0" border="0" style="display:none; visibility:hidden; margin:0"></iframe>
          </form>
          <!-- ====================   IP/Port form   ==================== -->
          <form method=post name="ipportFilter" action=/goform/ipportFilter target="hiddenframe0">
            <table width="450" border="1" cellpadding="2" cellspacing="1" bordercolor="#9BABBD" id="set_table"  style="display:none; visibility:hidden">
              <tr>
                <td class="title" colspan="2" id="portFilterSet">IP/Port Filter Settings </td>
              </tr>
              <tr style="display:none; visibility:hidden">
                <td class="head"  id="portFilterMac0"> Mac address </td>
                <td  class="value1" ><!--<input type="text" size="18" maxlength="17" name="mac_address"  id="mac_address0">
-->
                </td>
              </tr>
              <tr>
                <td class="head"  id="portFilterDIPAddr"> Dest IP Address </td>
                <td  class="value1" ><input type="text" size="18" maxlength="15" name="dip_address">
                  <!-- we dont support ip range in kernel 2.4.30 
		-<input type="text" size="16" name="dip_address2">
		-->
                </td>
              </tr>
              <tr>
                <td class="head" id="portFilterSIPAddr"> Source IP Address </td>
                <td  class="value1" ><input type="text" size="18" maxlength="15" name="sip_address">
                  <!-- we dont support ip range in kernel 2.4.30 
		-<input type="text" size="16" name="sip_address2">
		-->
                </td>
              </tr>
              <tr>
                <td class="head" id="portFilterProtocol"> Protocol </td>
                <td  class="value1" ><select onChange="protocolChange()" name="protocol" id="protocol_ip_port">
                    <option value="None">ALL</option>
                    <option value="TCP">TCP</option>
                    <option value="UDP">UDP</option>
                    <option value="ICMP">ICMP</option>
                  </select>
                  &nbsp;&nbsp; </td>
              </tr>
              <tr>
                <td class="head"  id="portFilterDPortRange"> Dest. Port Range </td>
                <td  class="value1" ><input type="text" size="5" maxlength="5" name="dFromPort" id="dFromPort">
                  ---
                  <input type="text" size="5" maxlength="5" name="dToPort" id="dToPort">
                </td>
              </tr>
              <tr>
                <td class="head"  id="portFilterSPortRange"> Src Port Range </td>
                <td  class="value1" ><input type="text" size="5" maxlength="5" name="sFromPort" id="sFromPort">
                  ---
                  <input type="text" size="5"  maxlength="5"name="sToPort" id="sToPort">
                </td>
              </tr>
              <tr>
                <td class="head"  id="portFilterAction"> Action </td>
                <td  class="value1" ><select onChange="actionChanged()" name="action">
                    <option value="Drop" id="portFilterActionDrop">Drop</option>
                    <option value="Accept" id="portFilterActionAccept">Accept</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td class="head"  id="portFilterComment"> Comment </td>
                <td  class="value1" ><input type="text" name="comment" size="18" maxlength="32" >
                </td>
              </tr>
            </table>
            <table  cellpadding="2" cellspacing="1" width="450"  id="delete_rule_button"  style="display:none; visibility:hidden">
              <tr align="left" id="maximum">
                <td id="maximum_rule"><script>
	document.write("("+_("The maximum rule count is")+ MAX_RULES +".)");
</script>
                </td>
              </tr>
              <tr align="center">
                <td ><input type="submit" style="width:110px;" value="Apply" id="portFilterApply" name="addFilterPort0" onClick="return ipportFormCheck()">
                  &nbsp;&nbsp;
                  <input type="reset" style="width:110px;" value="Reset" id="portFilterReset" name="reset0">
                </td>
              </tr>
            </table>
            <iframe name="hiddenframe0"  id="hiddenframe0" frameborder="0" border="0" style="display:none"></iframe>
          </form>
          <!-- ====================   MAC  ==================== -->
          <form method=post name="MACFilter" action=/goform/ipportFilter target="hiddenframe1" >
            <table width="450" border="1" cellpadding="2" cellspacing="1" bordercolor="#9BABBD" id="set_table_MAC" style="display:none; visibility:hidden">
              <tr>
                <td class="title" colspan="2" id="MacFilterSet">Mac Filter Settings </td>
              </tr>
              <tr>
                <td class="head"  id="portFilterMac"> Mac address </td>
                <td  class="value1" ><input type="hidden" size="18" maxlength="17" name="mac_address"  id="mac_address" />
                  <input type="text" size="2" maxlength="2" name="mac1"  id="mac1_0" onKeyUp="check_value_long(2,this,'mac1_1')" />
                  :
                  <input type="text" size="2" maxlength="2" name="mac1"  id="mac1_1" onKeyUp="check_value_long(2,this,'mac1_2')" />
                  :
                  <input type="text" size="2" maxlength="2" name="mac1"  id="mac1_2"onKeyUp="check_value_long(2,this,'mac1_3')">
                  :
                  <input type="text" size="2" maxlength="2" name="mac1"  id="mac1_3"onKeyUp="check_value_long(2,this,'mac1_4')">
                  :
                  <input type="text" size="2" maxlength="2" name="mac1"  id="mac1_4"onKeyUp="check_value_long(2,this,'mac1_5')">
                  :
                  <input type="text" size="2" maxlength="2" name="mac1"  id="mac1_5" onKeyUp="check_value_long(2,this,'mac1_5')"/>
                </td>
              </tr>
              <tr style="display:none; visibility:hidden">
                <td class="head"  id="portFilterDIPAddr"> Dest IP Address </td>
                <td  class="value1" ><input type="text" size="18" maxlength="15" name="dip_address">
                  <!-- we dont support ip range in kernel 2.4.30 
		-<input type="text" size="16" name="dip_address2">
		-->
                </td>
              </tr>
              <tr style="display:none; visibility:hidden">
                <td class="head" id="portFilterSIPAddr"> Source IP Address </td>
                <td  class="value1" ><input type="text" size="18" maxlength="15" name="sip_address">
                </td>
              </tr>
              <tr style="display:none; visibility:hidden">
                <td class="head" id="portFilterProtocol"> Protocol </td>
                <td  class="value1" ><select onChange="protocolChange()" name="protocol" id="protocol_mac">
                    <option value="None">ALL</option>
                    <option value="TCP">TCP</option>
                    <option value="UDP">UDP</option>
                    <option value="ICMP">ICMP</option>
                  </select>
                  &nbsp;&nbsp; </td>
              </tr>
              <tr style="display:none; visibility:hidden">
                <td class="head"  id="portFilterDPortRange"> Dest. Port Range </td>
                <td  class="value1"><input type="text" size="5" maxlength="5" name="dFromPort" id="dFromPort">
                  ---
                  <input type="text" size="5" maxlength="5" name="dToPort" id="dToPort">
                </td>
              </tr>
              <tr style="display:none;visibility:hidden">
                <td class="head"  id="portFilterSPortRange"> Src Port Range </td>
                <td  class="value1" ><input type="text" size="5" maxlength="5" name="sFromPort" id="sFromPort">
                  ---
                  <input type="text" size="5"  maxlength="5"name="sToPort" id="sToPort">
                </td>
              </tr>
              <tr>
                <td class="head"  id="MACFilterAction"> Action </td>
                <td  class="value1" ><select onChange="actionChanged()" name="action">
                    <option value="Drop" id="MACFilterActionDrop">Drop</option>
                    <option value="Accept" id="MACFilterActionAccept">Accept</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td class="head"  id="MACFilterComment"> Comment </td>
                <td  class="value1" ><input type="text" name="comment" size="18" maxlength="32" >
                </td>
              </tr>
            </table>
            <table  cellpadding="2" cellspacing="1" width="450"  id="delete_rule_button1" style="display:none; visibility:hidden">
              <tr align="left" id="maximum">
                <td id="maximum_rule"><script>
	 	document.write("("+_("The maximum rule count is")+ MAX_RULES +".)");
</script>
                </td>
              </tr>
              <tr align="center">
                <td ><input type="submit" style="width:110px;" value="Apply" id="MACFilterApply" name="addFilterPort" onClick="return MACFormCheck()">
                  &nbsp;&nbsp;
                  <input type="reset" style="width:110px;" value="Reset" id="MACFilterReset" name="reset">
                </td>
              </tr>
            </table>
            <iframe name="hiddenframe1"  id="hiddenframe1" frameborder="0" border="0" style="display:none"></iframe>
          </form>
        </center></td>
    </tr>
  </tbody>
</table>
<!-- =========================  delete rules  ========================= -->
<table width="650" class="body" bordercolor="#9BABBD" id="rules" style="display:none;visibility:hidden;margin-left:10">
  <tbody >
    <tr>
      <td width="650"><center>
          <form action=/goform/ipportFilterDelete method=POST name="ipportFilterDelete" target="hiddenframe1">
            <table width="650" border="1" cellpadding="2" cellspacing="1" bordercolor="#9BABBD" align="center">
              <tbody >
                <tr>
                  <td class="title" colspan="10" id="portCurrentFilter">Current IP/Port filtering rules in system </td>
                </tr>
                <tr>
                  <td width="6%" class="head11" id="portCurrentFilterNo"> No.</td>
                  <td width="13%" align=center class="head11" id="portCurrentFilterMac"> Mac Address </td>
                  <td width="13%" align=center class="head11" id="portCurrentFilterDIP"> Dest IP Address</td>
                  <td width="13%" align=center class="head11" id="portCurrentFilterSIP"> Source IP Address</td>
                  <td width="9%" align=center class="head11" id="portCurrentFilterProtocol"> Protocol</td>
                  <td width="10%" align=center class="head11" id="portCurrentFilterDPort">Dest Port Range</td>
                  <td width="11%" align=center class="head11" id="portCurrentFilterSPort">Source Port Range</td>
                  <td width="7%" align=center class="head11" id="portCurrentFilterAction"> Action</td>
                  <td width="10%" align=center class="head11" id="portCurrentFilterComment"> Comment</td>
                  <td width="8%" align=center class="head11" id="portCurrentFilterPacketCount"> PktCnt</td>
                </tr>
                <% showIPPortFilterRulesASP(); %>
            </table>
            <table  cellpadding="2" cellspacing="1" width="490" >
              <tbody>
                <tr align="center">
                  <td ><input type="submit" style="width:110px;" value="Delete Selected" id="portCurrentFilterDel" name="deleteSelFilterPort" onClick="return deleteClick()">
                    &nbsp;&nbsp;
                    <input type="reset" style="width:110px;" value="Reset" id="portCurrentFilterReset" name="reset"></td>
                </tr>
              </tbody>
            </table>
            <iframe name="hiddenframe1"  id="hiddenframe1" frameborder="0" border="0" style="display:none"></iframe>
          </form>
        </center></td>
    </tr>
  </tbody>
</table>
</BODY>
<script language="JavaScript" type="text/javascript">document.write('<div id="loading"  style="display:none;z-index:9999"><br><br><br><div align="center">'+_("Submitting settings")+
'</div><br><br>&nbsp;&nbsp;' +_("Submitting be patient")+'</div>');
</script>
</HTML>
