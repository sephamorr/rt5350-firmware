<html>
<head>
<title>System Settings</title>
<META http-equiv="Content-Type" content="text/html; charset=gb2312">
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
<meta http-equiv="Pragma" content="no-cache">
<meta content="MSHTML 6.00.2800.1106" name="GENERATOR">
<link href="../style/normal_ws.css" type="text/css" rel="stylesheet">
<link href="../style/style.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="/lang/b28n.js"></script>
<script type="text/javascript" src="/js/global_js.js"></script>
<script language="JavaScript" type="text/javascript">
var _singleton = 0;
Butterlate.setTextDomain("firewall");
 
function lastEnsure()
{  
   //parent.menu.setUnderFirmwareUpload(1);
	 submittingDiv(1,1,"","top.view.location='../secret/filterSwitch.asp';",500,0);    //弹出提交等待时的div框
	//setTimeout("top.view.location='../secret/filterSwitch.asp';",500);
}

 function submitClick()
{   
  if(_singleton==1) return false;
    _singleton=1
	lastEnsure();
     return true;
}
function initTranslation()
{
	var e = document.getElementById("sysfwTitle");
	e.innerHTML = _("filter function title");
	e = document.getElementById("filterSwitchIntroduction");
	e.innerHTML = _("filter function introduction");
	e = document.getElementById("filterEnableTit");
	e.innerHTML =_("filter Enable Title");
 	e = document.getElementById("filterEnableTd");
	e.innerHTML = _("filter Enable");
 	e = document.getElementById("filterD");
 	e.innerHTML = _("firewall disable");
	e = document.getElementById("filterE");
 	e.innerHTML = _("firewall enable");
 	e = document.getElementById("sysfwApply");
	e.value = _("sysfw apply");
	e = document.getElementById("sysfwReset");
	e.value = _("inet cancel");
}


function updateState()
{
	 initTranslation();
     all_page_init();
	var filterEnable = '<% getCfgGeneral(1,"filterEnable"); %>' ;
 	if(filterEnable == "0")
		document.filterFrom.filterEnable.options.selectedIndex = 0;
	else
		document.filterFrom.filterEnable.options.selectedIndex = 1;
   }  
   </script>
</head>


<!--     body      -->
<body onLoad="updateState()">

<h2 id="sysfwTitle" class="btnl"> Firewall Settings </h2>

<table  width="100%" class="tintro" >
  <tbody>
     <tr>
      <td class="intro" id="filterSwitchIntroduction" > You may configure the system firewall to protect itself from attacking.
         </td>
      <td class ="image_col" align="right">
	   <script language="javascript" type="text/javascript" >
	     var lang =_("next");
	   if (lang == "Next") lang="en";
	  else lang="zh_cn";
	   var page_name = "filterSwitch"
	    help_display(page_name,lang);
	  </script>
	  </td>
    </tr>
  </tbody>
</table>

<hr>
<table width="571" class="body" >
  <tbody><tr><td width="563">
<center>
<form method=post name="filterFrom" action=/goform/filterEnable target="hiddenframe">
 
  <table width="450" cellpadding="2" cellspacing="1" border="1" bordercolor="#9BABBD">
<tr>
	<td class="title" colspan="2" id="filterEnableTit"></td>
</tr>
<tr>
	<td class="head" id="filterEnableTd">Filter Enable	 </td>
	<td  class="value1" > 
    <select name="filterEnable" size="1" id="filterEnable" >
    <option value=0 id="filterD">Disable</option>
    <option value=1 id="filterE">Enable</option>
  </select> 
   </td>
</tr>
</table>
	<input type="submit" style="width:110px;" value="Apply" id="sysfwApply" name="sysfwApply"  onClick="submitClick()" > &nbsp;&nbsp;
	<input type="button" style="width:110px;" value="Reset" id="sysfwReset" name="sysfwReset" onClick="reset_clicked()">
	<iframe name="hiddenframe"  id="hiddenframe" frameborder="0" border="0" style="display:none"></iframe>
</form>

<br>
</center></td></tr></tbody></table>
 
</body>
<script language="JavaScript" type="text/javascript">document.write('<div id="loading"  style="display: none;z-index:9999"><br><br><br><div align="center">'+_("Submitting settings")+
'</div><br><br>&nbsp;&nbsp;' +_("Submitting be patient")+'</div>');
</script>
</html>
