#!/bin/sh

# udhcpc script edited by Tim Riker <Tim@Rikers.org>

[ -z "$1" ] && echo "Error: should be called from udhcpc" && exit 1

RESOLV_CONF="/etc/resolv.conf"
[ -n "$broadcast" ] && BROADCAST="broadcast $broadcast"
[ -n "$subnet" ] && NETMASK="netmask $subnet"

case "$1" in
    deconfig)
        /sbin/ifconfig $interface 0.0.0.0
        # add by Lamon 
        gpio l 20 1 2 4000 0 4000
        ;;
    renew|bound)
        /sbin/ifconfig $interface $ip $BROADCAST $NETMASK

        if [ -n "$router" ] ; then
            echo "deleting routers"
            while route del default gw 0.0.0.0 dev $interface ; do
                :
            done

            metric=0
            for i in $router ; do
                metric=`expr $metric + 1`
                route add default gw $i dev $interface metric $metric
            done
        fi

        echo -n > $RESOLV_CONF
        [ -n "$domain" ] && echo search $domain >> $RESOLV_CONF
        for i in $dns ; do
            echo adding dns $i
            echo nameserver $i >> $RESOLV_CONF
        done
		# notify goahead when the WAN IP has been acquired. --yy
		killall -SIGUSR2 goahead

        # add by ZLMnet
        #killall dhcpd-reset.sh
        #/sbin/dhcpd-reset.sh
        goahead wan-up
#        /sbin/ntp.sh
#        /sbin/ddns.sh
        /sbin/igmpproxy-reset.sh
#        /sbin/upnp-reset.sh
        # dns proxy
#        dnsp=`nvram_get 2860 dnsPEnabled`
#        if [ "$dnsp" = "1" ]; then
#            killall -9 dnsmasq
#            dnsmasq -r /etc/resolv.conf &
#        fi  
                
        gpio l 20 4000 0 4000 0 4000
        ;;
esac

exit 0

