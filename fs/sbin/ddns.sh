#!/bin/sh
#
# $Id: ddns.sh,v 1.1 2007-09-24 09:34:52 winfred Exp $
#
# usage: ddns.sh
#

. /sbin/global.sh

sta=`nvram_get 2860 ddnsEnable`
srv=`nvram_get 2860 DDNSProvider`
ddns=`nvram_get 2860 DDNS`
u=`nvram_get 2860 DDNSAccount`
pw=`nvram_get 2860 DDNSPassword`


killall -q ez-ipupdate

if [ "$sta" = "" -o "$sta" = "0" ]; then
	exit 0
fi
if [ "$srv" = "" -o "$srv" = "none" ]; then
	exit 0
fi
if [ "$ddns" = "" -o "$u" = "" -o "$pw" = "" ]; then
	exit 0
fi

# debug
echo "sta=$sta"
echo "srv=$srv"
echo "ddns=$ddns"
echo "u=$u"
echo "pw=$pw"

ez-ipupdate -S $srv -i $wan_ppp_if -u "$u:$pw" -h $ddns -w "/tmp/ddns.cache"&

