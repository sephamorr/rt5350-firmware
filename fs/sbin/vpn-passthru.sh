#!/bin/sh

. /sbin/config.sh
. /sbin/global.sh

l2tp_pt=`nvram_get 2860 l2tpPassThru`
ipsec_pt=`nvram_get 2860 ipsecPassThru`
pptp_pt=`nvram_get 2860 pptpPassThru`
sip_pt=`nvram_get 2860 sipPassThru`
h323_pt=`nvram_get 2860 h323PassThru`
rtsp_pt=`nvram_get 2860 rtspPassThru`


# note: they must be removed in order
if [ "$CONFIG_NF_CONNTRACK_SUPPORT" = "y" ]; then
	rmmod nf_nat_pptp
	rmmod nf_conntrack_pptp
	rmmod nf_nat_proto_gre
	rmmod nf_conntrack_proto_gre
    rmmod nf_conntrack_sip
    rmmod nf_conntrack_sip
    rmmod nf_nat_sip
    rmmod nf_conntrack_h323
    rmmod nf_conntrack_h323
    rmmod nf_nat_h323
else
	rmmod ip_nat_pptp
	rmmod ip_conntrack_pptp
	rmmod ip_nat_proto_gre
	rmmod ip_conntrack_proto_gre
    rmmod ip_conntrack_sip
    rmmod ip_conntrack_sip
    rmmod ip_nat_sip
    rmmod ip_conntrack_h323
    rmmod ip_conntrack_h323
    rmmod ip_nat_h323
fi

if [ "$pptp_pt" = "1" -o "$l2tp_pt" = "1" -o "$ipsec_pt" = "1" ]; then
if [ "$CONFIG_NF_CONNTRACK_SUPPORT" = "y" ]; then
	insmod -q nf_conntrack_proto_gre
	insmod -q nf_nat_proto_gre

	if [ "$pptp_pt" = "1" ]; then
		insmod -q nf_conntrack_pptp
		insmod -q nf_nat_pptp
	fi
else
	insmod -q ip_conntrack_proto_gre
	insmod -q ip_nat_proto_gre

	if [ "$pptp_pt" = "1" ]; then
		insmod -q ip_conntrack_pptp
		insmod -q ip_nat_pptp
	fi
fi 
fi 


# add by ZLMnet
if [ "$rtsp_pt" = "1" -o "$h323_pt" = "1" -o "$sip_pt" = "1" ]; then
if [ "$CONFIG_NF_CONNTRACK_SUPPORT" = "y" ]; then
	if [ "$h323_pt" = "1" ]; then
		insmod -q nf_conntrack_h323
		insmod -q nf_nat_h323
	fi
	if [ "$sip_pt" = "1" ]; then
		insmod -q nf_conntrack_sip
		insmod -q nf_nat_sip
	fi
else
	if [ "$h323_pt" = "1" ]; then
		insmod -q ip_conntrack_h323
		insmod -q ip_nat_h323
	fi
	if [ "$sip_pt" = "1" ]; then
		insmod -q ip_conntrack_sip
		insmod -q ip_nat_sip
	fi
fi 
fi 

