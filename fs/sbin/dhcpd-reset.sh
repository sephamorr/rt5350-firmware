#!/bin/sh

. /sbin/config.sh
. /sbin/global.sh

dnsp=`nvram_get 2860 dnsPEnabled`
ip=`nvram_get 2860 lan_ipaddr`
killall -9 udhcpd
rm /etc/udhcpd.conf
# dhcp server
dhcp=`nvram_get 2860 dhcpEnabled`
if [ "$dhcp" = "1" ]; then
	start=`nvram_get 2860 dhcpStart`
	end=`nvram_get 2860 dhcpEnd`
	mask=`nvram_get 2860 dhcpMask`
	pd=`nvram_get 2860 dhcpPriDns`
	sd=`nvram_get 2860 dhcpSecDns`
	gw=`nvram_get 2860 dhcpGateway`
	lease=`nvram_get 2860 dhcpLease`
	static1=`nvram_get 2860 dhcpStatic1 | sed -e 's/;/ /'`
	static2=`nvram_get 2860 dhcpStatic2 | sed -e 's/;/ /'`
	static3=`nvram_get 2860 dhcpStatic3 | sed -e 's/;/ /'`
	config-udhcpd.sh -s $start
	config-udhcpd.sh -e $end
	config-udhcpd.sh -i $lan_if
	config-udhcpd.sh -m $mask
	if [ "$pd" != "" -o "$sd" != "" ]; then
		config-udhcpd.sh -d $pd $sd
	else
        if [ "$dnsp" = "1" ]; then
            config-udhcpd.sh -d $ip
        else
            #pd=`cat /etc/resolv.conf | awk '/nameserver/ { print $2 }'|sed -n 1p`
            #sd=`cat /etc/resolv.conf | awk '/nameserver/ { print $2 }'|sed -n 2p`
            #if [ "$pd" != "" -o "$sd" != "" ]; then
            #    config-udhcpd.sh -d $pd $sd
            #fi
            echo "ok"
        fi
	fi
	if [ "$gw" != "" ]; then
		config-udhcpd.sh -g $gw
	fi
	if [ "$lease" != "" ]; then
		config-udhcpd.sh -t $lease
	fi
	if [ "$static1" != "" ]; then
		config-udhcpd.sh -S $static1
	fi
	if [ "$static2" != "" ]; then
		config-udhcpd.sh -S $static2
	fi
	if [ "$static3" != "" ]; then
		config-udhcpd.sh -S $static3
	fi
	config-udhcpd.sh -r
fi


