#!/bin/sh

. /sbin/config.sh
. /sbin/global.sh

killall igmpproxy
igmp=`nvram_get 2860 igmpEnabled`
if [ "$igmp" = "1" ]; then
    killall igmpproxy
    if [ "$internet_mode" = "3G" ]; then
        igmpproxy.sh $wan_ppp_if $lan_if ppp0
    else
        igmpproxy.sh $wan_if $lan_if ppp0
    fi
    #config-igmpproxy.sh $wan_if $lan_if
    igmpproxy
fi

