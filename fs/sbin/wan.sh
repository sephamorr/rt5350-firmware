#!/bin/sh
#
# $Id: wan.sh,v 1.11 2008-09-10 06:39:13 steven Exp $
#
# usage: wan.sh
#

. /sbin/global.sh

# stop all

# comment by ZLMnet
#killall -q syslogd

# add by ZLMnet
MANUAL_EXEC="$1"

killall -q udhcpc
killall -q l2tpd
killall -q pppd


clone_en=`nvram_get 2860 macCloneEnabled`
clone_mac=`nvram_get 2860 macCloneMac`
#MAC Clone: bridge mode doesn't support MAC Clone
if [ "$opmode" != "0" -a "$clone_en" = "1" ]; then
	ifconfig $wan_if down
	ifconfig $wan_if hw ether $clone_mac
	ifconfig $wan_if up
fi


if [ "$internetmode" = "3G" ]; then
# for huawei
    

    # take on the modem power
    #gpio l 12 0 4000 0 1 4000
	#sleep 3
	umodem&

	#modprobe usbserial vendor=0x12d1 product=0x1001
	#pppd call e1260_script &
#for sim
#	modprobe usbserial vendor=0x1e0e product=0xce16
#	eject
#	sleep 3
#	pppd call sim_script &	
#for wireless 
#	pppd call gprs_script &
#for zte 
#	modprobe usbserial vendor=0x19d2 product=0x2003
#	eject
#	sleep 3
#	pppd call zte_script &
elif [ "$internetmode" = "WIFI" ]; then
    wifi_client&

    # take off the modem power
    #gpio l 12 4000 0 1 0 4000

    gpio l 20 10 10 4000 0 4000
	udhcpc -i $wan_if -s /sbin/udhcpc.sh -p /var/run/udhcpc.pid &
elif [ "$internetmode" = "WAN" ]; then

    # add by Lamon 
    # take off the modem power
    #gpio l 12 4000 0 1 0 4000

if [ "$wanmode" = "STATIC" -o "$opmode" = "0" ]; then
	#always treat bridge mode having static wan connection
	ip=`nvram_get 2860 wan_ipaddr`
	nm=`nvram_get 2860 wan_netmask`
	gw=`nvram_get 2860 wan_gateway`
	pd=`nvram_get 2860 wan_primary_dns`
	sd=`nvram_get 2860 wan_secondary_dns`

	#lan and wan ip should not be the same except in bridge mode
	if [ "$opmode" != "0" ]; then
		lan_ip=`nvram_get 2860 lan_ipaddr`
		if [ "$ip" = "$lan_ip" ]; then
			echo "wan.sh: warning: WAN's IP address is set identical to LAN"
			exit 0
		fi
	else
		#use lan's ip address instead
		ip=`nvram_get 2860 lan_ipaddr`
		nm=`nvram_get 2860 lan_netmask`
	fi
	ifconfig $wan_if $ip netmask $nm
	route del default
	if [ "$gw" != "" ]; then
	route add default gw $gw
	fi

    # add by Lamon
    #/sbin/ddns.sh
    #/sbin/ntp.sh
    gpio l 20 4000 0 4000 0 4000

	config-dns.sh $pd $sd
elif [ "$wanmode" = "DHCP" ]; then

    # add by Lamon 
    gpio l 20 10 10 4000 0 4000

	udhcpc -i $wan_if -s /sbin/udhcpc.sh -p /var/run/udhcpc.pid &
elif [ "$wanmode" = "PPPOE" ]; then

    # add by Lamon 
    gpio l 20 10 10 4000 0 4000
            
	u=`nvram_get 2860 wan_pppoe_user`
	pw=`nvram_get 2860 wan_pppoe_pass`
	pppoe_opmode=`nvram_get 2860 wan_pppoe_opmode`
    
    # modify by Lamon
	if [ "$pppoe_opmode" = "manual" ]; then
        if [ "$MANUAL_EXEC" != "1" ]; then
            return
        fi
    fi
	#if [ "$pppoe_opmode" = "" ]; then
	#	echo "pppoecd $wan_if -u $u -p $pw"
	#	pppoecd $wan_if -u "$u" -p "$pw"
	#else

		pppoe_optime=`nvram_get 2860 wan_pppoe_optime`
		config-pppoe.sh $u $pw $wan_if $pppoe_opmode $pppoe_optime

    # comment by Lamon
	#fi

elif [ "$wanmode" = "L2TP" ]; then

    # add by Lamon 
    gpio l 20 10 10 4000 0 4000

	srv=`nvram_get 2860 wan_l2tp_server`
	u=`nvram_get 2860 wan_l2tp_user`
	pw=`nvram_get 2860 wan_l2tp_pass`
	mode=`nvram_get 2860 wan_l2tp_mode`
	l2tp_opmode=`nvram_get 2860 wan_l2tp_opmode`

    # modify by Lamon
	if [ "$l2tp_opmode" = "manual" ]; then
        if [ "$MANUAL_EXEC" != "1" ]; then
            return
        fi
    fi

	l2tp_optime=`nvram_get 2860 wan_l2tp_optime`
	if [ "$mode" = "0" ]; then
		ip=`nvram_get 2860 wan_l2tp_ip`
		nm=`nvram_get 2860 wan_l2tp_netmask`
		gw=`nvram_get 2860 wan_l2tp_gateway`
		if [ "$gw" = "" ]; then
			gw="0.0.0.0"
		fi
		config-l2tp.sh static $wan_if $ip $nm $gw $srv $u $pw $l2tp_opmode $l2tp_optime
	else
		config-l2tp.sh dhcp $wan_if $srv $u $pw $l2tp_opmode $l2tp_optime
	fi
elif [ "$wanmode" = "PPTP" ]; then

    # add by Lamon 
    gpio l 20 10 10 4000 0 4000


	srv=`nvram_get 2860 wan_pptp_server`
	u=`nvram_get 2860 wan_pptp_user`
	pw=`nvram_get 2860 wan_pptp_pass`
	mode=`nvram_get 2860 wan_pptp_mode`
	pptp_opmode=`nvram_get 2860 wan_pptp_opmode`
	pptp_optime=`nvram_get 2860 wan_pptp_optime`

    # modify by ZLMnet
	if [ "$pptp_opmode" = "manual" ]; then
        if [ "$MANUAL_EXEC" != "1" ]; then
            return
        fi
    fi



	if [ "$mode" = "0" ]; then
		ip=`nvram_get 2860 wan_pptp_ip`
		nm=`nvram_get 2860 wan_pptp_netmask`
		gw=`nvram_get 2860 wan_pptp_gateway`
		if [ "$gw" = "" ]; then
			gw="0.0.0.0"
		fi
		config-pptp.sh static $wan_if $ip $nm $gw $srv $u $pw $pptp_opmode $pptp_optime
	else
		config-pptp.sh dhcp $wan_if $srv $u $pw $pptp_opmode $pptp_optime
	fi
else

    # take off the modem power
    #gpio l 12 4000 0 1 0 4000

	echo "wan.sh: unknown wan connection type: $wanmode"
	exit 1
fi

fi
