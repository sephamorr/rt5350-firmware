#!/bin/sh

. /sbin/config.sh
. /sbin/global.sh

killall upnpd
upnp=`nvram_get 2860 upnpEnabled`
ip=`nvram_get 2860 lan_ipaddr`
if [ "$upnp" = "1" ]; then
	route add -net 239.0.0.0 netmask 255.0.0.0 dev $lan_if
	upnp_xml.sh $ip
	upnpd -f $wan_ppp_if $lan_if &
fi


