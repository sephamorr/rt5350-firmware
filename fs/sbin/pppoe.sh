#!/bin/sh

PPPOE_FILE=/etc/options.pppoe

if [ ! -n "$3" ]; then
  echo "insufficient arguments!"
  echo "Usage: $0 <user> <password> <eth_name>"
  exit 0
fi

PPPOE_USER_NAME="$1"
PPPOE_PASSWORD="$2"
PPPOE_IF="$3"
PPPOE_OPMODE="$4"
PPPOE_OPTIME="$5"

echo "noauth" > $PPPOE_FILE
echo "user '$PPPOE_USER_NAME'" >> $PPPOE_FILE
echo "password '$PPPOE_PASSWORD'" >> $PPPOE_FILE
echo "nomppe" >> $PPPOE_FILE
echo "hide-password" >> $PPPOE_FILE
echo "noipdefault" >> $PPPOE_FILE
echo "defaultroute" >> $PPPOE_FILE
echo "nodetach" >> $PPPOE_FILE
echo "usepeerdns" >> $PPPOE_FILE

# modify by dimmalex
#if [ $PPPOE_OPMODE == "KeepAlive" ]; then
echo "lock" >> $PPPOE_FILE
if [ $PPPOE_OPMODE == "online" ]; then

	echo "persist" >> $PPPOE_FILE
	echo "holdoff $PPPOE_OPTIME" >> $PPPOE_FILE

# modify by dimmalex
#elif [ $PPPOE_OPMODE == "OnDemand" ]; then
elif [ $PPPOE_OPMODE == "trigger" ]; then
    
	#PPPOE_OPTIME=`expr $PPPOE_OPTIME \* 60`
	echo "demand" >> $PPPOE_FILE
	echo "idle $PPPOE_OPTIME" >> $PPPOE_FILE
    echo "nameserver 1.1.1.1" > /etc/resolv.conf
fi
echo "ipcp-accept-remote" >> $PPPOE_FILE 
echo "ipcp-accept-local" >> $PPPOE_FILE 

# change by dimmalex
#echo "lcp-echo-failure 3" >> $PPPOE_FILE
#echo "lcp-echo-interval 20" >> $PPPOE_FILE
echo "lcp-echo-failure 40" >> $PPPOE_FILE
echo "lcp-echo-interval 30" >> $PPPOE_FILE

echo "ktune" >> $PPPOE_FILE

# modify by dimmalex
#echo "default-asyncmap nopcomp noaccomp" >> $PPPOE_FILE
echo "nopcomp noaccomp" >> $PPPOE_FILE

echo "novj nobsdcomp nodeflate" >> $PPPOE_FILE
echo "plugin /etc_ro/ppp/plugins/rp-pppoe.so $PPPOE_IF" >> $PPPOE_FILE

